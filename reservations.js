var min_search_length = 3;
var person_id = 0;
var complain_none_designated = true;
var opened_person_id = 0;
var live_search = true;
var batch_delete = Array();

function hold_reservation(reservation_id) {
	$.get("/reservations/hold",
			{ rid: reservation_id, hold: 1 },
		function(resp) {
			if (resp.status) refresh_chart();
			},
		"json");
	}
function activate_reservation(reservation_id) {
	$.get("/reservations/activate",
			{ rid: reservation_id, hold: 0 },
		function(resp) {
			if (resp.status) refresh_chart();
			},
		"json");
	}
function select_skydive(reservation_id) {
	var menu = document.getElementById("res-jump" + reservation_id);
	$.get("/reservations/chjump",
			{ rid: reservation_id, jump: menu.value },
		function(resp) {
			var id = resp.id;
			if (id == 0) id = "";
			var menu = document.getElementById("res-media" + id);
			menu.options[0].text = resp.label;
			if (resp.id != 0) update_res(resp.id, person_id);
			}, "json");
	}
function select_media(reservation_id) {
	update_res(reservation_id, person_id);
	}
function stop_prop(event) {
	if (event.stopPropagation) event.stopPropagation();
	else if (window.event) window.event.cancelBubble = true;
	}
function on_hold_showing() {
	return (document.getElementById("hold-chart").style.display == "block");
	}
function refresh_on_hold(all) {
	$("#spinner").show();
	$.get("/reservations/onhold",
			{
				from: (all ? "" : yymmdd("from-date")),
				to: (all ? "" : yymmdd("to-date"))
				},
			function(resp) {
				var box = document.getElementById("hold-chart");
				box.style.display = "block";
				box.innerHTML = resp.html;
				$("#spinner").hide();
				}, "json");
	}
function toggle_on_hold() {
	var box = document.getElementById("hold-chart");
	if (box.style.display == "block") box.style.display = "none";
	else refresh_on_hold(true);
	}
function toggle_new_res() {
	var box = document.getElementById("reservations-form");
	var display = box.style.display;
	if ((display == "none") || (display == "")) display = "block";
	else display = "none";
	box.style.display = display;
	}
function new_res(group_id) {
	live_search = true;
	$.get("/reservations/newres",
			{ rid: group_id }, detail_handler, "json");
	}
function peek_a_boo(id, hide) {
	document.getElementById(id).style.display = (hide ? "none" : "block");
	}
function show_handler(resp) {
	var menu = document.getElementById("date-presets");
	var prev_select = menu.value;
	var value;
	peek_a_boo("on-hold-toggler", resp.read_only);
	peek_a_boo("form-toggler", resp.read_only);
	peek_a_boo("search-form", resp.read_only);
	$("#spinner").hide();
	menu.options.length = 0;
	var date_menu = resp.date_menu;
	if (date_menu == null) date_menu = new Array();
	menu.add(new Option("Write-In", "manual"));
	menu.add(new Option("All", "all"));
	menu.add(new Option("This Month", "month"));
	menu.add(new Option("Next 4 Dates", "4day"));
	menu.add(new Option("From Today", "today"));
	for (var i = 0; i < date_menu.length; i++) {
		menu.add(new Option(date_menu[i][1], date_menu[i][0]));
		}
	for (var i = 0; i < menu.length; i++) {
		value = menu.options[i].value;
		if (value == prev_select) {
			menu.selectedIndex = i
			break;
			}
		}
	document.getElementById("reservations-chart").innerHTML = resp.html;
	document.getElementById("hold-count").innerHTML = resp.hold_count;
	if (on_hold_showing()) refresh_on_hold(false)
	}
function refresh_chart() {
	var on_hold = document.getElementById("show-on-hold").checked;
	var active = document.getElementById("show-active").checked;
	$.get("/reservations/showres",
		{
			from: yymmdd("from-date"),
			to: yymmdd("to-date"),
			active: active ? 1 : 0,
			on_hold: on_hold ? 1 : 0,
			student: document.getElementById("view-student").value.trim()
			},
		show_handler, "json");
	$("#spinner").show();
	}
function confirm_cancel(reservation_id, is_group, confirm_callback) {
	$.get("/reservations/confcan",
			{ rid: reservation_id, is_group: (is_group ? 1 : 0) },
			function(resp) { confirm_callback(resp); }, "json");
	}
function do_delres(reservation_id) {
	$.get("/reservations/delres", { rid: reservation_id },
		function(resp) {
			if (resp.refresh) refresh_chart();
			else {
				detail_handler(resp);
				update_handler(resp);
				}
			},
		"json");
	}
function delete_res(reservation_id) {
	live_search = true;
	var name = document.getElementById("res-name" + reservation_id).value;
	if (name == "") do_delres(reservation_id);
	else {
		confirm_cancel(reservation_id, false,
			function(resp) {
				local_confirm("Reservations", "Yes", "No",
					function() { do_delres(resp.rid); }, null,
					"Remove " + resp.name + " from this reservation?");
				});
		}
	}
function add_note(reservation_id) {
	$.get("/reservations/addnote",
			{ rid: reservation_id },
			function(resp) {
				refresh_log(resp.id, resp.log);
				refresh_chart_cell("contact", resp.gid, resp.contact);
				},
			"json");
	}
function edit_log(log_id, remark) {
	$.get("/reservations/edlog",
			{ lid: log_id, remark: remark },
			function(resp) {
				refresh_log(resp.id, resp.log);
				},
			"json");
	}
function liquify_note(cell, log_id) {
	if ((cell.block != null) && cell.block) return;
	cell.block = true;
	var remark = cell.innerHTML;
	if (remark.match(/^<span /)) {
		// strip span wrapper
		remark = remark.replace(/^<span [^>]+>/, "");
		remark = remark.replace("</span>", "");
		}
	var box_id = "edbox" + log_id;
	cell.innerHTML = "<input id=\"" + box_id + "\" type=\"text\" onchange=\"edit_log('" + log_id + "',this.value)\" style=\"width: 95%\"/>";
	var box = document.getElementById(box_id);
	box.focus();
	box.value = remark;
	}
function update_prepay_total(reservation_id, total) {
	var span = document.getElementById("res-paid" + reservation_id);
	if (span != null) span.innerHTML = total;
	}
function edit_pay(log_id, amount) {
	var pennies = cents(amount);
	if (pennies < 0) pennies = 0;
	$.get("/reservations/edpay",
			{ lid: log_id, amount: pennies },
			function(resp) {
				refresh_log(resp.id, resp.log);
				update_prepay_total(resp.id, resp.total);
				refresh_chart_cell("due", resp.gid, resp.due);
				},
			"json");
	}
function liquify_pay(cell, log_id) {
	if ((cell.block != null) && cell.block) return;
	cell.block = true;
	var payment = cell.innerHTML;
	var box_id = "edpay" + log_id;
	cell.innerHTML = "<input id=\"" + box_id + "\" type=\"text\" onchange=\"edit_pay('" + log_id + "',this.value)\" style=\"width: 95%; text-align: right\"/>";
	var box = document.getElementById(box_id);
	box.focus();
	box.value = payment;
	}
function delete_log(reservation_id, log_id) {
	local_confirm("Reservations", "Yes", "No",
		function() {
			$.get("/reservations/dellog",
					{ rid: reservation_id, lid: log_id },
					function(resp) {
						refresh_log(resp.id, resp.log);
						update_prepay_total(resp.id, resp.total);
						refresh_chart_cell("contact",
							resp.gid, resp.contact);
						refresh_chart_cell("due", resp.gid, resp.due);
						},
					"json");
			}, null,
		"Drop this reservation note?<br/>(May affect prepayment total!)");
	}
function refresh_log(reservation_id, log) {
	var obj = document.getElementById("logrow" + reservation_id);
	if (log) {
		obj.style.display = "block";
		obj.innerHTML = log;
		}
	else obj.style.display = "none";
	}
function really_send_confirm(reservation_id, group_confirm) {
	$.get("/reservations/sendconfirm",
			{ rid: reservation_id,
				group: (group_confirm ? "1" : "0")
				},
			function(resp) {
				if (resp.status) {
					if (resp.log) refresh_log(resp.id, resp.log);
					}
				else local_alert("Confirmation Failed", resp.msg);
				},
			"json");
	}
function send_confirm(reservation_id, prompt_for_group) {
	if (prompt_for_group) {
		$("#group-confirm-dialog").dialog({
			resizable: false,
			buttons: {
				"Yes": function() {
							really_send_confirm(reservation_id, true);
							$(this).dialog("close");
							},
				"No": function() {
							really_send_confirm(reservation_id, false);
							$(this).dialog("close");
							},
				"Cancel": function() {
							$(this).dialog("close");
							}
				},
			modal: true
			});
		}
	else really_send_confirm(reservation_id, false);
	}
function arm_update(box) {
	box.style.border = "2px solid green";
	}
function detail_handler(resp) {
	var obj = document.getElementById("dcell" + resp.gid);
	obj.innerHTML = resp.html;
	if (resp.log) refresh_log(resp.gid, resp.log);
	}
function popcal(box, format) {
	$("#" + box.id).datepicker({
		dateFormat: format,
		altField: "#res-date-fmt",
		altFormat: "yy-mm-dd",
		firstDay: 1,
		changeYear: true
		});
	$("#" + box.id).datepicker("show");
	}
function do_redate(reservation_id, date) {
	$.get("/reservations/redate",
			{
				rid: reservation_id,
				date: $.datepicker.formatDate("yy-mm-dd", date)
				},
			function(resp) { refresh_chart(); }, "json");
	}
function date_change(box, reservation_id) {
	var date = $("#" + box.id).datepicker("getDate");
	var today = new Date();
	today.setHours(0);
	today.setMinutes(0);
	today.setSeconds(0);
	today.setMilliseconds(0);
	if (date < today) {
		local_confirm("Reservations", "Yes", "No",
				function() { do_redate(reservation_id, date); },
				function() { refresh_chart() },
				"Move this reservation to a date in the past?"
				);
		}
	else do_redate(reservation_id, date);
	}
function slot_change(menu, reservation_id) {
	$.get("/reservations/reslot",
			{
				rid: reservation_id,
				slot: menu.value
				},
		function(resp) {
			if (resp.status) refresh_chart();
			else local_alert("Data Errors", resp.msg);
			},
		"json");
	}
function group_detail(group_id) {
	var row = document.getElementById("drow" + group_id);
	if (row.style.display == "none") {
		row.style.display = "table-row";
		$.get("/reservations/gdetail", { rid: group_id },
				detail_handler, "json");
		}
	else row.style.display = "none";
	}
function first_focus(box) {
	if ((box.style.color == "") || (box.style.color == "gray")) {
		box.value = "";
		box.style.color = "black";
		}
	}
function activate_returning(activate, reserve_id) {
	document.getElementById("is-returning" + reserve_id).checked = activate;
	document.getElementById("is-returning-slot" +
			reserve_id).style.display = (activate ? "inline" : "none");
	document.getElementById("res-name" + reserve_id).disabled = activate;
	document.getElementById("res-email" + reserve_id).disabled = activate;
	document.getElementById("res-phone" + reserve_id).disabled = activate;
	if (!activate) person_id = 0;
	}
function set_name_phone_email(reserve_id, name, phone, email) {
	document.getElementById("res-name" + reserve_id).value = name;
	document.getElementById("res-phone" + reserve_id).value = phone;
	document.getElementById("res-email" + reserve_id).value = email;
	}
function select_person(event, ui, reserve_id) {
	if (ui.item.new) {
		live_search = false;
		return;
		}
	var name = ui.item.name;
	if (name == null) name = ui.item.label;
	set_name_phone_email(reserve_id, name, ui.item.phone, ui.item.email);
	person_id = ui.item.pid;
	activate_returning(person_id != 0, reserve_id);
	if (reserve_id != "") update_res(reserve_id, person_id);
	return true;
	}
function extract_reserve_id(box) {
	var m = box.id.match(/(\d+)$/);
	if (m == null) return "";
	return m[1];
	}
function live_name_search(box) {
	if (!live_search) return;
	var res_id = extract_reserve_id(box);
	$("#" + box.id).autocomplete({
		source: "/accounting/search?allow_new=1",
		minLength: min_search_length,
		select: function(event, ui) {
					return select_person(event, ui, res_id);
					}
		});
	}
function live_email_search(box) {
	if (!live_search) return;
	var res_id = extract_reserve_id(box);
	$("#" + box.id).autocomplete({
		source: "/reservations/srchemail",
		minLength: min_search_length,
		select: function(event, ui) {
					return select_person(event, ui, res_id);
					}
		});
	}
function live_phone_search(box) {
	if (!live_search) return;
	var res_id = extract_reserve_id(box);
	$("#" + box.id).autocomplete({
		source: "/reservations/srchphone",
		minLength: min_search_length,
		select: function(event, ui) {
					return select_person(event, ui, res_id);
					}
		});
	}
function load_menu(menu_id, items) {
	if (items == null) {
		if (complain_none_designated)
			local_alert("Warning", "no reservation offerings designated")
		complain_none_designated = false;
		return;
		}
	var menu = document.getElementById(menu_id);
	var option;
	menu.options.length = 0;
	for (var i = 0; i < items.length; i++) {
		option = new Option(items[i].label, items[i].value);
		menu.add(option);
		}
	return menu;
	}
function preset_dates() {
	var menu = document.getElementById("date-presets");
	var refresh = true;
	if (menu.value == "manual") {
		document.getElementById("from-date").value = "";
		document.getElementById("to-date").value = "";
		refresh = false;
		}
	else if (menu.value == "month") {
		var today = new Date();
		$("#from-date").datepicker("setDate",
			new Date(today.getFullYear(), today.getMonth(), 1));
		$("#to-date").datepicker("setDate",
			new Date(today.getFullYear(), today.getMonth() + 1, 0));
		}
	else if (/^\d\d/.test(menu.value)) {
		document.getElementById("from-date").value = menu.value;
		document.getElementById("to-date").value = menu.value;
		}
	else {
		var on_hold = document.getElementById("show-on-hold").checked;
		var active = document.getElementById("show-active").checked;
		$.get("/reservations/dpreset",
				{
					key: menu.value,
					active: active ? 1 : 0,
					on_hold: on_hold ? 1 : 0
					},
				function(resp) {
					document.getElementById("from-date").value = resp.from;
					document.getElementById("to-date").value = resp.to;
					refresh_chart();
					}, "json");
		refresh = false;
		}
	if (refresh) refresh_chart();
	}
function dress_date(id, format) {
	$("#" + id).datepicker({
		dateFormat: format,
		altField: "#" + id + "-fmt",
		altFormat: "yy-mm-dd",
		firstDay: 1,
		changeYear: true
		});
	}
function dress(resp) {
	dress_date("res-date", resp.date_format);
	dress_date("from-date", resp.date_format);
	dress_date("to-date", resp.date_format);
	min_search_length = resp.search_length;
	load_menu("res-jump", resp.skydive_menu);
	load_menu("res-media", resp.media_menu);
	load_menu("res-slot", resp.slot_menu);
	document.getElementById("hold-count").innerHTML = resp.on_hold;
	document.getElementById("res-name").focus();
	select_skydive("");
	}
function cell_value(field, reservation_id) {
	var id = field;
	if (reservation_id != "0") id += reservation_id;
	return document.getElementById(id).value.trim();
	}
function refresh_chart_cell(name, res_id, value) {
	var cell = document.getElementById("ch-" + name + res_id);
	cell.innerHTML = value;
	}
function money(box_id, reservation_id, neg_ok) {
	var value = cell_value(box_id, reservation_id);
	var pat = (neg_ok ? /[^\-\d\.]/g : /[^\d\.]/g);
	return value.replace(pat, "");
	}
function cents(dollars) {
	var fdollars = parseFloat(dollars.trim().replace("$", ""));
	if (isNaN(fdollars)) fdollars = 0.0;
	return Math.round(fdollars * 100);
	}
function yymmdd(picker_id) {
	var date = $("#" + picker_id).datepicker("getDate");
	if (date == null) return "";
	return $.datepicker.formatDate("yy-mm-dd", date);
	}
function check_value(id) {
	var box = document.getElementById(id);
	if (box == null) return 0;
	return (box.checked ? 1 : 0);
	}
function save_res() {
	var reservation_id = '0';
	live_search = true;
	var data = {
		pid: person_id,
		name: cell_value("res-name", reservation_id),
		email: cell_value("res-email", reservation_id),
		phone: cell_value("res-phone", reservation_id),
		jump: cell_value("res-jump", reservation_id),
		media: cell_value("res-media", reservation_id),
		paid: cents(money("res-paid", reservation_id, false)),
		discount: cents(money("res-discount", reservation_id, true)),
		date: yymmdd("res-date"),
		slot: cell_value("res-slot", reservation_id),
		chk_age: check_value("chk-age"),
		chk_weight: check_value("chk-weight"),
		chk_deposit: check_value("chk-deposit"),
		chk_waiver_fee: check_value("chk-waiver-fee"),
		chk_audio_consent: check_value("chk-audio-consent")
		}
	$.post("/reservations/savres", data,
			function(resp) {
				if (resp.status) {
					document.getElementById("res-name").focus();
					person_id = 0;
					refresh_chart();
					}
				else local_alert("Data Errors", resp.msg);
				},
			"json");
	}
function restore_box_border(base, reservation_id) {
	var box = document.getElementById(base + reservation_id);
	if (box != null) {
		box.style.border = "";
		box.blur();
		}
	}
function update_handler(resp) {
	if (resp.status) {
		refresh_chart_cell("count", resp.gid, resp.booked);
		refresh_chart_cell("media", resp.gid, resp.media);
		refresh_chart_cell("contact", resp.gid, resp.contact);
		refresh_chart_cell("due", resp.gid, resp.due);
		var total = document.getElementById("res-total" + resp.id);
		if (total != null) total.innerHTML = resp.total;
		}
	else local_alert("Data Errors", resp.msg);
	}
function make_contact(reservation_id) {
	$.get("/reservations/contact",
			{ rid: reservation_id }, update_handler, "json");
	}
function update_res(reservation_id, person_id) {
	var data = {
		rid: reservation_id,
		pid: person_id,
		name: cell_value("res-name", reservation_id),
		email: cell_value("res-email", reservation_id),
		phone: cell_value("res-phone", reservation_id),
		jump: cell_value("res-jump", reservation_id),
		media: cell_value("res-media", reservation_id),
		discount: cents(money("res-discount", reservation_id, true))
		}
	restore_box_border("res-name", reservation_id);
	restore_box_border("res-email", reservation_id);
	restore_box_border("res-phone", reservation_id);
	restore_box_border("res-discount", reservation_id);
	person_id = 0;
	$.post("/reservations/update", data,
			function(resp) {
				var btn = document.getElementById("confirm" + resp.id);
				btn.disabled = !resp.has_email;
				update_handler(resp);
				},
			"json");
	}
function set_cell(name, reservation_id, value, gray) {
	var id = name;
	if (reservation_id != "0") id += reservation_id;
	var box = document.getElementById(id);
	if (gray) box.style.color = "gray";
	box.value = value;
	}
function set_checkbox(name, state) {
	document.getElementById(name).checked = state;
	}
function reset_form(reservation_id) {
	var box;
	activate_returning(false, "");
	live_search = true;
	set_cell("res-name", reservation_id, "", false);
	set_cell("res-email", reservation_id, "", false);
	set_cell("res-phone", reservation_id, "", false);
	set_cell("res-paid", reservation_id, "$0", true);
	set_cell("res-date", reservation_id, "date", true);
	set_cell("res-slot", reservation_id, "hh:mm", true);
	set_checkbox("chk-age", false);
	set_checkbox("chk-weight", false);
	set_checkbox("chk-deposit", false);
	set_checkbox("chk-waiver-fee", false);
	set_checkbox("chk-audio-consent", false);
	document.getElementById("checklist").style.display = "none";
	$.get("/reservations/dress", {}, dress, "json");
	}
function can_res(res_id) {
	confirm_cancel(res_id, true,
		function(resp) {
			if (!resp.detail) {
				local_alert("Reservations", "Can't cancel, admin-only.");
				return;
				}
			local_confirm("Reservations", "Yes", "No",
				function() {
					$.get("/reservations/cangrp",
						{ rid: resp.rid },
						function(resp) { refresh_chart(); },
						"json");
					}, null,
				"Cancel " + resp.name +
					"'s reservation for " + resp.date + "?"
				);
			});
	}
function set_menu(mname, value) {
	var menu = document.getElementById(mname);
	var option;
	for (var i = 0; i < menu.length; i++) {
		option = menu.options[i];
		option.selected = (option.value == value);
		}
	}
function hilite(trow) {
	trow.style.backgroundColor = "#ffcc99";
	}
function lolite(trow, color) {
	trow.style.backgroundColor = color;
	}
function flick_returning() {
	var cb = document.getElementById("is-returning");
	if (!cb.checked) activate_returning(false, "");
	}
function verify_dereturn(res_id, chkbox) {
	local_confirm("Reservations", "Yes", "No",
		function() {
			$.get("/reservations/dereturn",
					{ rid: res_id },
					function(resp) {
						document.getElementById("res-name"
								+ resp.rid).disabled = false;
						document.getElementById("res-email"
								+ resp.rid).disabled = false;
						document.getElementById("res-phone"
								+ resp.rid).disabled = false;
						document.getElementById("is-returning-slot"
								+ resp.rid).style.display = "none";
						},
					"json");
			},
		function() { chkbox.checked = true; },
		"Are you sure?<p>You're about to disassociate this reservation from a returning student ID. There's no undo.</p>"
		);
	}
function people_note_add(pid) {
	$.get("/people/addnote", { pid: pid },
			function(resp) {
				var div = document.getElementById("people-notes" + pid);
				div.innerHTML = resp.html;
				}, "json"
			);
	}
function people_note_edit(box, note_id) {
	$.post("/people/ednote",
			{ nid: note_id, note: box.value },
			function(resp) {
				}, "json"
			);
	}
function people_note_del(note_id, person_id) {
	$.get("/people/delnote",
			{ nid: note_id, pid: person_id },
			function(resp) {
				var div = document.getElementById("people-notes" + person_id);
				div.innerHTML = resp.html;
				}, "json"
			);
	}
function close_detail(div) {
	div.innerHTML = "";
	if (opened_person_id != 0) {
		$.get("/people/close_det", { pid: opened_person_id },
			function(resp) {}, "json");
		opened_person_id = 0;
		}
	}
function show_detail(resp, res_id) {
	var div = document.getElementById("pplrow" + res_id);
	div.innerHTML = resp.html;
	People.panel_config(resp.pid,
		resp.date_format, resp.first_last, false,
		function() { // save function
			return People.update_detail(resp.pid,
				function(resp) {
					$.get("/reservations/ident",
							{ pid: resp.pid, rid: res_id },
							function(resp) {
								set_name_phone_email(resp.rid,
									resp.name, resp.phone, resp.email);
								update_res(resp.rid, resp.pid);
								}, "json");
					});
			},
		function() { close_detail(div); }, // cancel
		false // delete
		)
	}
function detail_panel(resp) {
	opened_person_id = resp.pid;
	if (resp.status) {
		var whiteboard_btn = document.getElementById("wbd" + resp.rid);
		if (whiteboard_btn != null) whiteboard_btn.disabled = true;
		}
	$.get("/reservations/pplpanel", { pid: resp.pid, rid: resp.rid },
			function(resp) {
				$.get("/people/detail", { pid: resp.pid },
						function(resp2) { show_detail(resp2, resp.rid); },
						"json");
				}, "json");
	}
function open_detail(resp, check_in) {
	if (check_in) {
		$.get("/reservations/checkin",
				{ rid: resp.rid, pid: resp.pid },
				detail_panel, "json");
		}
	else detail_panel(resp);
	}
function people_detail(res_id, check_in) {
	var div = document.getElementById("pplrow" + res_id);
	if (div.innerHTML != "") {
		close_detail(div);
		return;
		}
	$.get("/reservations/getpid", { rid: res_id },
			function(resp) {
				if (resp.pid == 0) {
					$.post("/people/new", {
						last_name: resp.last,
						first_name: resp.other,
						role: resp.role },
						function(resp2) {
							resp2.rid = resp.rid;
							open_detail(resp2, check_in);
							}, "json");
					}
				else open_detail(resp, check_in);
				}, "json");
	}
function init() {
	mark_page("Reservations");
	$.get("/reservations/dress", {},
			function(resp) {
				dress(resp);
				preset_dates();
				}, "json");
	}
function logout() {
	$.get("/login/logout", {},
		function(resp) { window.location = "/manifest"; },
		"json");
	}
function close_match_dialog() {
	$("#db-matches").dialog("close");
	return false;
	}
function show_matches(search_term, reservation_id, resp) {
	var pids = [];
	for (var i = 0; i < resp.length; i++) {
		if (resp[i].pid == 0) continue;
		pids.push(resp[i].pid);
		}
	$.get("/reservations/matches",
			{ matches: JSON.stringify(pids),
				rid: reservation_id,
				term: search_term },
			function(resp) {
				var div = document.getElementById("db-matches");
				div.innerHTML = resp.html;
				$("#db-matches").dialog({
					resizable: true,
					width: 500,
					modal: true
					});
				if (resp.close)
					window.setTimeout(close_match_dialog, 2000);
				}, "json");
	}
function find_by(box_id, reservation_id, service) {
	var box = document.getElementById(box_id + reservation_id);
	var term = box.value.trim();
	$.get(service, { term: term },
			function(resp) {
				show_matches(term, reservation_id, resp)
			}, "json");
	return false;
	}
function find_by_name(reservation_id) {
	return find_by("res-name", reservation_id, "/accounting/search");
	}
function find_by_email(reservation_id) {
	return find_by("res-email", reservation_id, "/reservations/srchemail");
	}
function find_by_phone(reservation_id) {
	return find_by("res-phone", reservation_id, "/reservations/srchphone");
	}
function prefill_update(person_id, reservation_id) {
	$.get("/reservations/ident",
			{ pid: person_id, rid: reservation_id },
			function(resp) {
				set_name_phone_email(resp.rid,
					resp.name, resp.phone, resp.email)
				update_res(resp.rid, resp.pid);
				activate_returning(true, resp.rid);
				}, "json");
	}
function bind_to_record(person_id, reservation_id) {
	prefill_update(person_id, reservation_id);
	close_match_dialog();
	return false;
	}
function split_res(reservation_id) {
	$.get("/reservations/splitres", { rid: reservation_id },
		function(resp) {
			refresh_chart();
			}, "json");
	}
function toggle_checklist(rid) {
	var div = document.getElementById("checklist" + rid);
	div.style.display = (div.style.display == "block" ? "none" : "block");
	}
function update_checklist(cbox, reservation_id, column) {
	$.post("/reservations/upd_checklist",
			{
				rid: reservation_id,
				col: column,
				state: (cbox.checked ? "1" : "0")
				},
			function(resp) {
				var btn = document.getElementById(resp.btn_id);
				if (btn.style.color != resp.color)
					refresh_chart_cell("contact", resp.gid, resp.contact);
				btn.style.color = resp.color;
				btn.title = resp.title;
				}, "json");
	}
function update_owl(cbox, reservation_id) {
	$.post("/reservations/upd_owl",
			{
				rid: reservation_id,
				state: (cbox.checked ? "1" : "0")
				},
			function(resp) {
				refresh_chart_cell("contact", resp.gid, resp.contact);
				}, "json");
	}
function mark_for_delete(btn, res_id) {
	if (btn.checked)
		batch_delete.push(res_id);
	else {
		var index = batch_delete.indexOf(res_id);
		if (index > -1) batch_delete.splice(index, 1);
		}
	document.getElementById("batch-delete").disabled =
		(batch_delete.length < 1);
	}
function uncheck_batch(ids) {
	var b;
	for (var i = 0; i < ids.length; i++) {
		b = document.getElementById("batchdel" + ids[i]);
		if (b != null) b.checked = false;
		}
	batch_delete = Array();
	document.getElementById("batch-delete").disabled = true;
	}
function confirm_batch_delete() {
	if (batch_delete.length < 1) return;
	$.post("/reservations/conf_batch_delete",
			{ ids: JSON.stringify(batch_delete) },
			function(resp) {
				var body = "Delete all?<p><ul>"
				for (var i = 0; i < resp.names.length; i++) {
					body += "<li>" + resp.names[i] + "</li>\n";
					}
				body += "</ul></p>";
				local_confirm("On-hold Reservations", "Yes", "No",
					function() {
						$.post("/reservations/do_batch_delete",
								{ ids: JSON.stringify(resp.ids) },
								function(resp2) {
									uncheck_batch(resp.ids);
									if (on_hold_showing())
										refresh_on_hold(true);
									refresh_chart();
									}, "json");
						},
					function() {
						uncheck_batch(resp.ids);
						},
					body);
				}, "json");
	}
