
(define default-person-weight #f)
(define (sum-weights pairs)
	; (car pair) indicates whether weight/moment object is present
	; (cdr pair) is the value of the weight/moment, or some default
	(define (crunch item prev)
		(cons (and (car item) (car prev))
			(+ (cdr item) (cdr prev))))
	(fold crunch (cons #t 0) pairs))
(define (weight-pair weight missing-ok calc)
	; missing weights are so noted and count as zero
	(let ([value (if (null? weight) '() (calc weight))])
		(cons
			(or (not (null? value)) missing-ok)
			(if (null? value) 0 value))))
(define (calc-moment weight-pair arm)
	; missing weights and/or arms are so noted and count as zero
	(cons
		(and (car weight-pair) (not (null? arm)))
		(if (null? arm) 0 (* (cdr weight-pair) arm))))
(define pilot-weight-balance
	(let ([query
				"select pilot_arm, pilot, copilot
					from load_sheets, aircraft
					where (load_sheets.id=~a)
					and (load_sheets.aircraft_id=aircraft.id)"]
			[weight-query
				"select exit_weight from people where (id=~a)"])
		(define (person-weight person-id dbh)
			(if (null? person-id) '()
				(let* ([row (pg-one-row dbh weight-query person-id)]
						[weight (pg-cell row 'exit_weight)])
					(if (and (null? weight) default-person-weight)
						default-person-weight weight))))
		(lambda (load-id dbh)
			(let* ([row (pg-one-row dbh query load-id)]
					[weight (sum-weights
						(list
							(weight-pair
								(person-weight (pg-cell row 'pilot) dbh)
								#f identity)
							(weight-pair
								(person-weight (pg-cell row 'copilot) dbh)
								#t identity)))])
				(cons
					weight
					(calc-moment weight
						(pg-cell row 'pilot_arm)))))))
(define fuel-weight-balance
	(let ([query
				"select fuel_arm, density,
						fuel_at_takeoff as fuel_gals
					from load_sheets, aircraft, fuel_type
					where (load_sheets.id=~a)
					and (aircraft.id=load_sheets.aircraft_id)
					and (aircraft.fuel_type=fuel_type.symbol)"])
		(lambda (load-id dbh)
			(let* ([row (pg-one-row dbh query load-id)]
					[weight
						(and row
							(weight-pair (pg-cell row 'fuel_gals) #f
								(lambda (fuel)
									(* fuel (pg-cell row 'density)))))])
				(cons
					(if weight weight (cons #f 0))
					(if weight
						(calc-moment weight (pg-cell row 'fuel_arm))
						(cons #f 0)))))))
(define empty-weight-balance
	(let ([query
				"select aircraft.empty_weight as empty_weight,
						basic_arm
					from load_sheets, aircraft
					where (load_sheets.id=~a)
					and (aircraft.id=load_sheets.aircraft_id)"])
		(lambda (load-id dbh)
			(let* ([row (pg-one-row dbh query load-id)]
					[weight
						(weight-pair (pg-cell row 'empty_weight) #f
							identity)])
				(cons
					weight
					(calc-moment weight (pg-cell row 'basic_arm)))))))
(define aircraft-weight-balance
	; compute aircraft weight and moment, sum:
	; - empty weight, basic arm
	; - fuel weight, fuel arm
	; - pilots weight(s), pilot_arm
	(lambda (load-id dbh)
		(let* ([pilot-w-b (pilot-weight-balance load-id dbh)]
				[fuel-w-b (fuel-weight-balance load-id dbh)]
				[empty-w-b (empty-weight-balance load-id dbh)])
			(cons
				(sum-weights
					(list
						(car empty-w-b)
						(car fuel-w-b)
						(car pilot-w-b)))
				(sum-weights
					(list
						(cdr empty-w-b)
						(cdr pilot-w-b)
						(cdr fuel-w-b)))))))
(define payload-weight-balance
	; compute payload (jumper) weight and moment
	; don't include passenger seated at copilot here
	(let ([query
				"select exit_weight as weight
					from load_slots, people
					where (load_slots.load_id=~a)
					and (passenger is not null)
					and (passenger != ~a)
					and (passenger=people.id)"]
			[copilot-query
				"select copilot
					from load_sheets
					where (id=~a)"]
			[arm-query
				"select avg_payload_arm as arm
					from aircraft, load_sheets
					where (load_sheets.id=~a)
					and (load_sheets.aircraft_id=aircraft.id)"])
		(define (pair row)
			(let ([weight (pg-cell row 'weight)])
				(cons (not (null? weight)) (to-i weight))))
		(define (copilot load-id dbh)
			(let* ([row (pg-one-row dbh copilot-query load-id)]
					[copilot (pg-cell row 'copilot)])
				(if (null? copilot) 0 copilot)))
		(define (payload-arm load-id dbh)
			(pg-cell (pg-one-row dbh arm-query load-id dbh) 'arm))
		(define (person-weight row)
			(let ([weight (pg-cell row 'weight)])
				(if (and (null? weight) default-person-weight)
					(cons #t default-person-weight)
					(weight-pair weight #f identity))))
		(lambda (load-id dbh)
			(let* ([weights
						(pg-map-rows
							(pg-exec dbh query
								load-id
								(copilot load-id dbh))
							person-weight)]
					[sum (sum-weights weights)])
				(cons
					sum
					(calc-moment sum (payload-arm load-id dbh)))))))
(define (total-weight-balance load-id dbh)
	; (cons total-weight total-moment)
	(let ([aircraft (aircraft-weight-balance load-id dbh)]
			[payload (payload-weight-balance load-id dbh)])
		(cons
			(sum-weights (list (car aircraft) (car payload)))
			(sum-weights (list (cdr aircraft) (cdr payload))))))
