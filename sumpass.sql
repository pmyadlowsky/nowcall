-- convert bare ascii passwords to sha256 sums
-- as root user: create extension pgcrypto;
update people set
	login_password=(select right(cast(digest(login_password,'sha256') as varchar), 64))
	where (login_password is not null)
	and (char_length(login_password) != 64);
