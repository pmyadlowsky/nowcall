
(use-modules (ice-9 regex))

(define smtp-config-file "/vol/profile/smtp.conf")
(define env-dz-copy-to-addrs (setting-cache 'dz-copy-to-addrs))
(define send-email
	(let ([config-file (make-doc 'file smtp-config-file)])
		(lambda (from recipients msg)
			(let ([config-doc (fetch-doc config-file)])
				(and
					config-doc
					(let ([config (json-decode config-doc)])
						(smtp-send
							(assq-ref config 'url)
							from
							recipients
							(assq-ref config 'username)
							(assq-ref config 'password) msg)))))))
(define bcc-list
	(let ([pat (make-regexp "[^, ]+")])
		(lambda ()
			(let ([bcc (or (env-dz-copy-to-addrs) "")])
				(if (string=? bcc "") '()
					(cons bcc
						(map match:substring (list-matches pat bcc))))))))

