var touched = null;
var day_cache = Array();
var day_cache_clear = true;
function set_cell_title(cell, title) {
	cell.html("<div class=\"day-title\">" + title + "</div>");
	}
function table_header(table, headers) {
	var row = document.createElement("tr");
	var cell;
	for (var i = 0; i < headers.length; i++) {
		cell = document.createElement("th");
		cell.innerHTML = headers[i];
		row.appendChild(cell);
		}
	table.appendChild(row);
	}
function table_row(table, data) {
	var row = document.createElement("tr");
	var cell;
	cell = document.createElement("td");
	cell.className = "work-cell-rj";
	cell.innerHTML = data.slot;
	row.appendChild(cell);
	cell = document.createElement("td");
	cell.className = "work-cell";
	cell.innerHTML = data.skydive;
	row.appendChild(cell);
	cell = document.createElement("td");
	cell.className = "work-cell-rj";
	cell.innerHTML = data.instructors + "";
	row.appendChild(cell);
	cell = document.createElement("td");
	cell.className = "work-cell";
	cell.innerHTML = data.media;
	row.appendChild(cell);
	table.appendChild(row)
	}
function work_dialog(resp) {
	$("#work-dialog").dialog({
		draggable: true,
		show: true,
		hide: true,
		title: "Work Slots: " + resp.date,
		closeOnEscape: true,
		autoResize: true,
		minWidth: 380,
		modal: true
		});
	$("#work-dialog").dialog("open");
	var div = document.getElementById("work-dialog");
	while (div.lastChild) div.removeChild(div.lastChild);
	$("work-dialog").empty();
	var table = document.createElement("table");
	table_header(table, ["Slot", "Skydive", "Instrs", "Media"]);
	for (var i = 0; i < resp.slots.length; i++) {
		table_row(table, resp.slots[i]);
		}
	div.appendChild(table);
	}
function instr_dialog(resp, date, day_cell) {
	$("#instr-dialog").dialog({
		draggable: true,
		show: true,
		hide: true,
		title: "Staffing " + resp.tdate,
		closeOnEscape: true,
		autoResize: true,
		modal: true
		});
	$("#instr-dialog").keypress(
		function(evnt) {
			if (evnt.keyCode == 13) {
				evnt.preventDefault();
				var box = document.getElementById("day-title-" + date);
				if (box != null) day_title(box, date, day_cell);
				$("#instr-dialog").dialog("close"); }});
	var box = document.getElementById("instr-dialog");
	box.innerHTML = resp.html;
	$("#instr-dialog").dialog("open");
	select_role(date);
	}
function entry_dialog(html) {
	$("#entry-dialog").dialog({
		draggable: true,
		show: true,
		hide: true,
		title: "Edit Entry",
		closeOnEscape: true,
		autoResize: true,
		modal: true
		});
	var box = document.getElementById("entry-dialog");
	box.innerHTML = html;
	$("#entry-dialog").dialog("open");
	}
function cancel_entry(entry_id, checkbox) {
	$.get("/schedule/cancel",
			{
				id: entry_id,
				state: (checkbox.checked ? "1" : "0")
				},
			function(resp) {
				if (resp.status)
					$("#calendar").fullCalendar("refetchEvents");
				}, "json");
	}
function admin_cancel_entry(entry_id, checkbox) {
	$.get("/schedule/admcancel",
			{
				id: entry_id,
				state: (checkbox.checked ? "1" : "0")
				},
			function(resp) {
				if (resp.status)
					$("#calendar").fullCalendar("refetchEvents");
				}, "json");
	}
function set_maybe(entry_id, checkbox) {
	$.get("/schedule/maybe",
			{
				id: entry_id,
				state: (checkbox.checked ? "1" : "0")
				},
			function(resp) {
				if (resp.status)
					$("#calendar").fullCalendar("refetchEvents");
				}, "json");
	}
function staff(date, person_id) {
	var menu = document.getElementById("staff-role");
	$.get("/schedule/staff",
			{ pid: person_id, date: date, role: menu.value },
			function(resp) {
				if (resp.status)
					$("#calendar").fullCalendar("refetchEvents");
				}, "json");
	}
function delete_entry(entry_id, event) {
	event.preventDefault();
	event.stopPropagation();
	$.get("/schedule/rem", { id: entry_id },
			function(resp) {
				if (resp.status)
					$("#calendar").fullCalendar("removeEvents", resp.id);
				}, "json");
	return false;
	}
function entry_button(entry) {
	var html = '<div class="fc-content" title="' +
					entry.role + '"><div style="float: left">' +
			'<span class="fc-title">' +
			entry.title + '</span></div>';
		if (entry.can_delete) {
			html += '<div style="float: right"><img onclick="delete_entry('
				+ entry.id + ',event)" src="/img/delete-8.png"/></div>'
			}
		html += '</div>';
	return html;
	}
function select_role(date) {
	var menu = document.getElementById("staff-role");
	$.get("/schedule/smenu", { role: menu.value, date: date },
			function(resp) {
				if (resp.status)
					document.getElementById("staff-list").innerHTML =
						resp.html;
				}, "json");
	}
function check_stamp() {
	$.get("/schedule/check", {},
		function(resp) {
			if ((touched != null) && (resp.stamp > touched)) {
				$("#calendar").fullCalendar("refetchEvents");
				}
			touched = resp.stamp;
		//	setTimeout(check_stamp, 10000);
			}, "json");
	}
function set_day_header(cell, title) {
	var html = "";
	if (title != null) {
		html += '<div style="float: left">' + title + '</div>';
		}
	set_cell_title(cell, html);
	}
function process_events(resp, callback) {
	var cell;
	callback(resp.events);
	if (resp.titles != null) {
		for (var i = 0; i < resp.titles.length; i++) {
			cell = day_cache[resp.titles[i][0]];
			if (cell == null) continue;
			set_day_header(cell, resp.titles[i][1]);
			}
		}
	}
function pull_events(start, end, timezone, callback) {
	$.get("/schedule/slots", 
			{ start: start.format(),
				end: end.format()
				},
			function(resp) {
				process_events(resp, callback);
				day_cache_clear = true;
				}, "json");
	}
function init() {
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,basicWeek,basicDay'
			},
		navLinks: true, // can click day/week names to navigate views
		dayRender: function(date, cell) {
			if (day_cache_clear) {
console.log("CLEAR CACHE");
				day_cache_clear = false;
				day_cache = Array();
				}
			cell.css("background-color", "white");
			key = date.format();
			day_cache[key] = cell;
			},
		eventRender: function(event, element, view) {
			key = event.start.format();
			element.html(entry_button(event));
			},
		dayClick: function(date, event, view) {
			var fdate = date.format("YYYY-MM-DD");
			var day_cell = this;
			$.get("/schedule/imenu",
					{
						date: fdate,
						tdate: date.format("MMM D, YYYY")
						},
					function(resp) {
						instr_dialog(resp, fdate, day_cell);
						},
					"json");
			},
		eventClick: function(event, js_event, view) {
			if (event.type == "person") {
				$.get("/schedule/edit", { id: event.id },
						function(resp) {
							if (resp.html) entry_dialog(resp.html);
							else local_alert("Staff Schedule",
									"Entry seems to be missing.");
							}, "json");
				return;
				}
			if (event.type == "meta") {
				$.get("/schedule/workslots",
						{ start: event.start.format() },
						function(resp) {
							work_dialog(resp);
							}, "json");
				}
			},
		eventOrder: "seq",
		eventTextColor: "#000000",
		firstDay: 1,
		events: pull_events,
		editable: true
		});
	check_stamp();
	mark_page("Staff Schedule");
	}
function logout() {
	$.get("/login/logout", {},
			function(resp) { window.location = "/manifest"; },
			"json");
	}
function day_title(box, date, day_cell) {
	var title = box.value.trim();
	$.get("/schedule/stitle",
		{
			date: date,
			title: title
			},
		function(resp) {
			if (resp.status) set_cell_title(day_cell, title);
			}, "json");
	}
