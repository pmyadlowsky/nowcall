(define longpoll-responder
	; Produce a function that accepts a payload function and
	; a polling tag and either returns a newsworthy event or
	; signals that nothing happened during the polling interval.
	(let ()
		(define (wrap-payload payload)
			(and payload
				(list
					(cons 'timeout #f)
					(cons 'time_ref (time-epoch (time-now)))
					(cons 'payload payload))))
		(define (nothing-happened mutex tag)
			(when mutex (unlock-mutex mutex))
			(list
				(cons 'timeout #t)
				(cons 'payload
					(list (cons 'tag tag)))))
		(define (something-happened mutex payload tag)
			(unlock-mutex mutex)
			(or
				(wrap-payload payload)
				(nothing-happened #f tag)))
		(define (await-event payload-thunk tag mutex
					condvar long-poll-interval)
			(lock-mutex mutex)
			(if (wait-condition-variable condvar
					mutex (+ (current-time) long-poll-interval))
				(something-happened mutex (payload-thunk) tag)
				(nothing-happened mutex tag)))
		(lambda (condvar long-poll-interval)
			(let ([mutex (make-mutex)])
				(lambda (payload-thunk tag)
					(or
						(wrap-payload (payload-thunk))
						(await-event payload-thunk tag mutex
							condvar long-poll-interval)))))))
