drop table reg_tickets;
create table reg_tickets (
	key varchar,
	email varchar,
	person_id integer,
	stamp timestamp default current_timestamp,
	foreign key (person_id) references people(id)
	);
