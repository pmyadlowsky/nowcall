/*
** Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
**
** This program is free software ; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation ; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY ; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program ; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var days_mon =
	["Monday", "Tuesday", "Wednesday",
	"Thursday", "Friday", "Saturday", "Sunday"];
var days_sun =
	["Sunday", "Monday", "Tuesday", "Wednesday",
	"Thursday", "Friday", "Saturday"];
var flash_threshold = 300; // seconds to start flashing load calls
var unauthorized_tmp = "<p style=\"font-weight: bold; color: COLOR\">\
Sorry, you're not authorized for this action.\
<br/>An administrator should be able to help you.\
</p>";
var big_screen = true;
var weekdays;
var menu_preset = "";
var track_tag = 1;
var dates_with_loads = new Array();
var manifast_need_init = true;
var now_dragging = ""
var post_scratch = false;
var drag_from_load = 0;
var showing_calls = new Array();
var selected_aircraft = null; // panel
var selected_aircraft_id = "";
var drag_mode = "";
var loaded_tabs = new Array();
var group = new Array();
var occupied_slots = new Array();
var scroll_target = null;
var vp_top = 0.0;
var vp_bottom = 0.0;
var scroll_step = 0;
var scroll_incr = 80;
var scroll_count = 0;
var today = (new Date()).getDate();
var current_people_id = -1;
var current_client = 0;
var date_format = "";
var min_search_length = 3;
var focus_on_role = "";
var exit_weight = "";
var javascript_catalog = "";
var search_buf = "";
var row_markers = Array();
var scroll_cursor = Array();
var empty_loads = Array();
var slots_focused = 0;
var document_hidden = false;
var bink_count = 1;
var binking = false;
var current_icon = "";
var row_width = 0;

function unauthorized(color) {
	return unauthorized_tmp.replace("COLOR", color);
	}
function echo_multi(base, key, action) {
	// echo DOM action to multi-aircraft display, if present
	var obj = document.getElementById(base + key);
	if (obj != null) action(obj);
	obj = document.getElementById(base + "-all-" + key);
	if (obj != null) action(obj);
	}
function slot_selected(event, ui) {
	var id = ui.selecting.id;
	var slot;
	slot = document.getElementById(id.replace("row", "name"));
	if (slot == null)
		slot = document.getElementById(id.replace("row", "name-all-"));
	slot.style.backgroundColor = "#cedbe4";
	if (group.indexOf(id) < 0) group.push(id);
	}
function slot_unselected(event, ui) {
	var id = ui.unselecting.id;
	var slot = document.getElementById(id.replace("row", "name"));
	slot.style.backgroundColor = "white";
	group = group.filter(
		function(el, n, arr) {
			return (el != id);
			});
	}
function select_start(event, ui) {
	}
function ignore(resp) {
	}
function sheet_date(tab_id, day_offset) {
	var date_id = "date" + tab_id;
	var date = document.getElementById(date_id).value;
	date = $.datepicker.parseDate(date_format, date);
	if (day_offset != 0) {
		date = new Date(date.getTime() +
						(day_offset * 24 + 3) * 3600 * 1000);
		}
	return $.datepicker.formatDate("yy-mm-dd", date);
	}
function clear_group() {
	$(".ui-selected").removeClass("ui-selected");
	group = new Array();
	}
function no_select(event, ui) {
	clear_group();
	}
function select_stop(event, ui) {
	if (group.length < 1) return;
	var m;
	var bname;
	var slot;
	var unocc = new Array();
	occupied_slots = new Array();
	m = group[0].match(/^row(\d+)_(\d+)/)
	var aircraft = m[1];
	var load = m[2];
	// separate occupied and unoccupied load slots
	for (var i = 0; i < group.length; i++) {
		m = group[i].match(/^row(\d+_\d+_)(\d+)/);
		slot = parseInt(m[2], 10);
		bname = "bname" + aircraft + "_" + load + "_" + slot;
		if (document.getElementById(bname) == null)
			occupied_slots.push(slot);
		else unocc.push(slot);
		}
console.log("occ:   " + occupied_slots.join(","));
console.log("unocc: " + unocc.join(","));
	// if there are any unoccupied, clear all selections,
	// process reservations and exit
	if (unocc.length > 0) {
		clear_group();
		occupied_slots = new Array();
		$.post("/manifest/reserve", {
				aircraft: aircraft,
				date: sheet_date(aircraft, 0),
				load: load,
				slots: unocc.join(",")
				},
			function(resp) {
				var pair;
				var slot;
				var n = resp.slots.length;
				for (var i = 0; i < n; i++) {
					pair = resp.slots[i];
					slot = document.getElementById(pair[0]);
					slot.innerHTML = pair[1];
					}
				},
			"json");
		return;
		}
	}
function start_scrolling(incr) {
	if (scroll_step != 0) return;
	scroll_count = 0;
	scroll_step = incr;
	scroll_sheets();
	}
function stop_scrolling() {
	scroll_step = 0;
	}
function scroll_sheets() {
	if (scroll_step == 0) return;
	scroll_count += 1;
	if (scroll_count == 10) { // accelerate over time
		scroll_step *= 2;
		scroll_count = 0;
		}
	scroll_target.scrollTop += scroll_step;
	if ((scroll_step < 0) && (scroll_target.scrollTop <= 0)) {
		stop_scrolling();
		}
	else if ((scroll_step > 0) &&
				(scroll_target.scrollTop >= scroll_target.scrollTopMax)) {
		stop_scrolling();
		}
	else setTimeout(scroll_sheets, 100);
	}
function drag_watch(event, ui) {
	if (ui.offset.top < vp_top) {
		start_scrolling(-scroll_incr);
		}
	else if (ui.offset.top > vp_bottom) {
		start_scrolling(scroll_incr);
		}
	else {
		stop_scrolling();
		}
	}
function drag_start(ev, ui) {
	now_dragging = ev.target.id;
	now_dragging.match(/^fname\d+_(\d+)_\d+_\d+/);
	scroll_target = document.getElementById("sheets" + RegExp.$1);
	var rect = scroll_target.getBoundingClientRect();
	vp_top = rect.top;
	vp_bottom = rect.bottom;
	drag_mode = (ev.ctrlKey ? "copy" : "move");
	}
function get_pids(slots, src_aircraft, dst_aircraft, src_load, dst_load) {
	$.post("/manifest/pids",
		{
			slots: slots.join(","),
			src_aircraft: src_aircraft,
			dst_aircraft: dst_aircraft,
			src_load: src_load,
			dst_load: dst_load,
			date: sheet_date(src_aircraft, 0)
			},
		function(resp) {
			var pids = new Array();
			var roles = new Array();
			var extras = new Array();
			for (var i = 0; i < resp.pids.length; i++) {
				pids.push(resp.pids[i][0]);
				roles.push(resp.pids[i][1]);
				extras.push(resp.pids[i][2]);
				}
			board_persons(pids, roles, extras,
							resp.dst_aircraft, resp.src_aircraft,
							resp.dst_load, 0, 0);
			},
		"json");
	}
function reseq(aircraft_id, load, src_slot, dst_slot) {
	$.post("/manifest/reseq",
		{
			aircraft: aircraft_id,
			load: load,
			date: sheet_date(aircraft_id, 0),
			src_slot: src_slot,
			dst_slot: dst_slot,
			tag: track_tag
			},
		function(resp) {
			if (!resp.status) return;
			track_tag += 1;
			if (resp.cmd == "patch") update_manifest(resp, false);
			},
		"json");
	}
function drag_drop(ev, ui) {
	now_dragging.match(/^fname(\d+)_(\d+)_(\d+)_(\d+)/);
	var src_aircraft = RegExp.$2;
	var n;
	drag_from_load = parseInt(RegExp.$3, 10);
	var src_slot = parseInt(RegExp.$4, 10);
	if ((n = occupied_slots.indexOf(src_slot)) < 0) {
		occupied_slots.push(src_slot);
		}
console.log("drop on " + ev.target.id);
	ev.target.id.match(/^slot(\d+)_(\d+)_(\d+)/)
	var dst_aircraft = RegExp.$1;
	var target_load = parseInt(RegExp.$2, 10);
	var dst_slot = parseInt(RegExp.$3, 10);
	if ((src_aircraft == dst_aircraft) &&
			(drag_from_load == target_load)) {
		clear_group();
		occupied_slots = new Array();
		reseq(src_aircraft, drag_from_load, src_slot, dst_slot);
		return;
		}
	post_scratch = (drag_mode == "move");
console.log("drag occ :" + occupied_slots.join(",") + ":");
	get_pids(occupied_slots, src_aircraft, dst_aircraft,
				drag_from_load, target_load);
	}
function drag_to_tab(event, ui) {
	event.target.id.match(/^atab(\d+)/);
	var aircraft_index = parseInt(RegExp.$1, 10);
	$("#tabs").tabs("option", "active", aircraft_index);
	}
function set_drag_select(admin) {
	$(".selectable").selectable({
		tolerance: "touch",
		selecting: slot_selected,
		unselecting: slot_unselected,
		stop: (admin ? select_stop : no_select),
		start: select_start,
		filter: ".row_select"
		});
	$(".dragger").draggable({
		appendTo: "body",
		drag: drag_watch,
		cursor: "move",
		start: drag_start,
		scroll: false,
		scrollSensitivity: 5,
		scrollSpeed: 60,
		helper: "clone",
		revert: false,
		opacity: 0.8
		});
	$(".catcher").droppable({
		accept: ".dragger",
		drop: drag_drop
		});
	$(".tab-catcher").droppable({
		accept: ".dragger",
		over: drag_to_tab
		});
	}
function panel_id(panel) {
	// extract aircraft ID from "tabs-xxxx"
	var pid = panel.attr("id");
	if (pid == "tabs-all") return "all";
	var m = pid.match(/([0-9]+)/);
	return m[1];
	}
function track_date_change() {
	var now = new Date();
	if (now.getDate() == today) return;
	var yyyymmdd = now.getFullYear() + "-";
	var month = now.getMonth() + 1;
	if (month < 10) yyyymmdd += "0";
	yyyymmdd += month + "-";
	var day = now.getDate();
	if (day < 10) yyyymmdd += "0";
	yyyymmdd += day;
	window.location = url_with_date(yyyymmdd);
	}
function add_row(aircraft_id, cells, multi_aircraft) {
	var table_base = "sheets";
	if (multi_aircraft) table_base += "-all-";
	var table = document.getElementById(table_base + aircraft_id);
	//table.style.overflow = "auto";
	var cell, row, rows, cols;
	if (multi_aircraft) {
		rows = cells.length;
		cols = 1;
		}
	else {
		rows = 1;
		cols = cells.length
		}
	var cell_index = 0;
	row_width = 0;
	for (var j = 0; j < rows; j++) {
		var cell_group = new Array();
		if (!(aircraft_id in row_markers))
			row_markers[aircraft_id] = Array();
		row_markers[aircraft_id].push(cell_group);
		for (var i = 0; i < cols; i++) {
			cell_group.push(cells[cell_index].marker);
			cell = document.createElement("div");
			if (!multi_aircraft) {
				if (big_screen) {
					// let load sheets flow, otherwise single column
					cell.style.display = "inline-block";
					cell.style.verticalAlign = "top";
					}
				cell.style.marginRight = "4px";
				}
			cell.style.marginBottom = "4px";
			cell.innerHTML = cells[cell_index].html;
			table.appendChild(cell);
			row_width += cell.clientWidth;
			cell_index += 1;
			}
		}
	}
function scroll_to_load(load_id, align_top) {
	var sheet = document.getElementById(load_id);
	if (sheet != null) {
		sheet.scrollIntoView(align_top);
		scroll_cursor[selected_aircraft_id] = load_id;
		}
	}
function scroll_one_step(step) {
	var markers = row_markers[selected_aircraft_id];
	if (!(selected_aircraft_id in scroll_cursor)) {
		scroll_to_load(markers[0][0], true);
		}
	var n = -1;
	var truffle = scroll_cursor[selected_aircraft_id];
	for (var i = 0; i < markers.length; i++) {
		if (markers[i].indexOf(truffle) >= 0) {
			n = i + step;
			break;
			}
		}
	if ((n >= 0) && (n < markers.length)) {
		scroll_to_load(markers[n][0], true);
		}
	}
function scroll_to_recent_load(force) {
	$.post("/manifest/autoscroll", {
				force: (force ? 1 : 0),
				aircraft: selected_aircraft_id,
				date: sheet_date(selected_aircraft_id, 0)
				},
			function(resp) {
				scroll_to_load(resp.recent_load, false);
				},
			"json");
	}
function change_icon(src) {
	if (src == current_icon) return;
	current_icon = src;
	var link = document.createElement('link');
	var old_link = document.getElementById("favicon");
	link.id = "favicon";
	link.rel = "icon";
	bink_count += 1;
	link.href = src + "?v=" + bink_count;
	if (old_link != null) document.head.removeChild(old_link);
	document.head.appendChild(link);
	}
function bink_icon() {
	if (document_hidden) {
		if (binking) return;
		binking = true;
		change_icon("/img/favicon-dot2.png");
		setTimeout(
			function() {
				if (document_hidden) {
					change_icon("/img/favicon-dot.png");
					}
				else {
					change_icon("/favicon.ico");
					}
				binking = false;
				},
			250);
		}
	else {
		change_icon("/favicon.ico");
		binking = false;
		}
	}
function sheetrow_handler(resp) {
	var multi = ((resp.rowgroups.length > 1) || resp.multi);
	for (var j = 0; j < resp.rowgroups.length; j++) {
		var group = resp.rowgroups[j];
		var aircraft = group.aircraft;
		add_row(aircraft, group.cells, multi);
		for (var i = 0; i < group.cells.length; i++) {
			if (group.cells[i].empty) empty_loads.push(group.cells[i].id);
			else empty_loads = Array();
			}
		scroll_to_load(group.recent_load, true);
		if (group.next == 0) { // done rendering load sheets
			var table_base = "sheets";
			if (multi) table_base += "-all-";
			var table = document.getElementById(table_base + aircraft);
			var div = document.createElement("div");
			div.style.clear = "both";
			table.appendChild(div);
			backup_timeout = group.backup_timeout;
			loaded_tabs[multi ? "all" : aircraft] = "x";
			set_drag_select(group.admin);
			set_slot_context_menus();
			LongPoll.poll();
			$.get("/manifest/occdates", {},
					function(resp) {
						if (resp.dates == null)
							dates_with_loads = new Array();
						else dates_with_loads = resp.dates;
						},
					"json");
			}
		else { // get the next row of load sheets
			$.post("/manifest/sheetrow", {
					tab: aircraft,
					multi: (multi ? "1" : "0"),
					active: $("#tabs").tabs("option", "active"),
					date: sheet_date(aircraft, 0),
					start_load: group.next,
					sheets: group.sheets
					}, sheetrow_handler, "json");
			}
		}
	}
function show_search(aircraft_id) {
console.log("SHOW SEARCH " + aircraft_id);
	if (aircraft_id == "all") return;
	var span = document.getElementById("search-track" + aircraft_id);
	if (span.style.display == "none") span.style.display = "inline";
	}
function load_sheets(tab_id, force) {
	if (kiosk_mode) show_search(tab_id);
	if (force) {
		window.location.reload(true);
		return;
		}
	if (tab_id in loaded_tabs) {
		scroll_to_recent_load(true);
		return;
		}
	row_markers[tab_id] = Array();
	empty_loads = Array();
	$.post("/manifest/sheetrow", {
			tab: tab_id,
			multi: (tab_id == "all" ? "1" : "0"),
			active: $("#tabs").tabs("option", "active"),
			date: sheet_date(tab_id, 0),
			start_load: 0,
			sheets: 0
			}, sheetrow_handler, "json");
	}
function check_has_loads(date) {
	var fdate = $.datepicker.formatDate("yymmdd", date);
	if (typeof(dates_with_loads[fdate]) == "undefined")
		return [true, "", ""];
	else return [true, "date-occupied", "has loads"];
	}
function open_account(ev, ui) {
	var m = ui.id.match(/^fname(\d+)_\d+_\d+_\d+/);
	if (m == null) return;
	current_client = m[1];
	account_panel(current_client);
	}
function open_profile(ev, ui) {
	var passenger, aircraft, load_num, slot_num;
	var m = ui.id.match(/^fname(\d+)_(\d+)_(\d+)_(\d+)/);
	if (m != null) {
		passenger = m[1];
		aircraft = m[2];
		load_num = m[3];
		slot_num = m[4];
		}
	else {
		m = ui.id.match(/^bname(\d+)_(\d+)_(\d+)/);
		if (m == null) return;
		passenger = -1;
		aircraft = m[1];
		load_num = m[2];
		slot_num = m[3];
		}
	people_panel(passenger, aircraft, load_num, slot_num, "", "", false);
	}
function create_menu_responder(extra_id, jumper, load_date, slot_id) {
	return function(ev, ui) {
				$.get("/manifest/chgextra", {
							jumper_id: jumper,
							item_id: extra_id,
							date: load_date,
							slot: slot_id
							},
						function(resp) { },
						"json")
				}
	}
function slot_context_handler(resp) {
	if (!resp.status) return;
	for (var i = 0; i < resp.menu.length; i++) {
		var menu = new Object();
		menu["Profile"] = open_profile;
		menu["Account"] = open_account;
		if (resp.menu[i].extras) {
			for (var j = 0; j < resp.menu[i].extras.length; j++) {
				var extra = resp.menu[i].extras[j];
				menu[extra.name] =
					create_menu_responder(
							extra.id,
							resp.menu[i].jumper,
							resp.menu[i].date,
							resp.menu[i].slot_id
							);
				}
			}
		$("#" + resp.menu[i].slot_id).contextmenu(menu, "right", 1000);
		}
	}
function set_slot_context_menus() {
	var m, role, aircraft;
	var els = document.getElementsByClassName("filled-slot-ctx");
	var roles = new Array();
	var jumpers = new Array();
	var slot_ids = new Array();
	var dates = new Array();
	for (var i = 0; i < els.length; i++) {
		m = els[i].id.match(/^fname(\d+)_(\d+)(.+)$/);
		if (m == null) continue;
		jumpers.push(m[1]);
		aircraft = m[2];
		role = document.getElementById("role" + aircraft + m[3]);
		if (role != null) roles.push(role.value);
		role = document.getElementById("role-all-" + aircraft + m[3]);
		if (role != null) roles.push(role.value);
		dates.push(sheet_date(aircraft, 0));
		slot_ids.push(els[i].id);
		}
	if (roles.length > 0) {
		$.post("/manifest/ctxmenu", {
					roles: JSON.stringify(roles),
					jumpers: JSON.stringify(jumpers),
					slot_ids: JSON.stringify(slot_ids),
					dates: JSON.stringify(dates)
					},
				slot_context_handler, "json");
		}
	$(".blank-slot-ctx").contextmenu({
		"New Profile": open_profile
		}, 'right', 1000);
	}
function blinkers() {
	$(".blinker").each(
		function() {
			$(this).css('visibility',
				$(this).css('visibility') == "hidden" ? "" : "hidden");
			}
		)
	}
function time_ref() {
	var msec = (new Date().getTime()) % 1000;
	setTimeout(time_ref, 1000 - msec);
	refresh_countdowns();
	}
function resize_sheets() {
	$('#sheets' + selected_aircraft_id).css(
		'height', (window.innerHeight - 250) + "px"
		);
	// why 250? I don't know, just seems to work.
	$('#sheets' + selected_aircraft_id).css(
		'width', (window.innerWidth - 50) + "px"
		);
	}
function set_dz_address() {
	$.get("/manifest/setip", {},
		function(resp) {
			if (resp.status)
				local_alert("Self-Manifest Check", resp.msg);
			}, "json");
	}
function stretch_loadsheets(sheets_div) {
	// Make loadsheet viewport fill height of window down to
	// repo link.
	var sheetbox = sheets_div.getBoundingClientRect();
	var ref = document.getElementById("repo-link").getBoundingClientRect();
	var height = ref.top - sheetbox.top - 30;
	sheets_div.style.height = sheets_div.style.maxHeight = height + "px";
	}
function init_stage2(resp) {
	date_format = resp.format;
	min_search_length = resp.search_length;
	setInterval("track_date_change()", 60000);
	time_ref();
	$("#tabs").tabs({
			show: false,
			hide: false,
			active: resp.aircraft_index,
			activate: function(ev, ui) {
						selected_aircraft = ui.newPanel;
						selected_aircraft_id = panel_id(ui.newPanel);
						load_sheets(selected_aircraft_id, false);
						document.getElementById("sheets" +
							selected_aircraft_id).focus();
						},
			create: function(ev, ui) {
						var selector = panel_id(ui.panel);
						if (selected_aircraft_id == "") { 
							selected_aircraft_id = selector;
							}
						load_sheets(selected_aircraft_id, false);
						var sheets = document.getElementById("sheets" +
							selected_aircraft_id);
						sheets.focus();
						stretch_loadsheets(sheets);
						}
		});
	resize_sheets();
	$("input.dp").datepicker({
		beforeShowDay: check_has_loads,
		dateFormat: date_format,
		altFormat: "yy-mm-dd",
		setDate: new Date(Date.now),
		firstDay: 1,
		changeYear: true
		});
	set_slot_context_menus();
	setInterval(blinkers, 500);
	$.get("/manifest/checkadmin", {},
			function(resp) {
				set_drag_select(resp.mani_admin);
				}, "json");
	document.getElementById("squawky-bird").style.visibility = "visible";
	}
function init() {
	var test_date = new Date(Date.parse("2014-11-08"));
	if (test_date.getDay() == 5) weekdays = days_mon;
	else weekdays = days_sun;
	big_screen = (!/Mobi/.test(navigator.userAgent));
	if (!big_screen) {
		var header = document.getElementById("header");
		if (header != null) header.style.display = "none";
		}
	LongPoll.set_poll_url("/manifest/track");
	LongPoll.set_payload_handler(process_payload);
	LongPoll.set_fallback_timeout(30);
	$.get("/manifest/datefmt", {}, init_stage2, "json");
	mark_page("Manifest");
	detect_viz_api();
	}
function manifast_up() {
	if (manifast_need_init) return false;
	return $("#manifast-dialog").dialog("isOpen");
	}
function refresh_manifast(aircraft_id, load_num) {
	if (!manifast_up()) return;
	$.get("/manifest/manifast", {
				load: load_num,
				date: sheet_date(aircraft_id, 0),
				aircraft: aircraft_id
				 },
			function(resp) {
				var box = document.getElementById("manifast-dialog");
				box.innerHTML = resp.manifast;
				},
			"json");
	}
function open_manifast(aircraft_id, load_num) {
	if (!manifast_need_init && $("#manifast-dialog").dialog("isOpen")) {
		//$("#manifast-dialog").dialog("close");
		return;
		}
	var div = document.getElementById("load" + aircraft_id +
				"_" + load_num);
	var my, at;
	if (div.offsetLeft < 10) {
		my = "left top";
		at = "right top";
		}
	else {
		my = "right top";
		at = "left top";
		}
	$("#manifast-dialog").dialog({
		draggable: true,
		show: true,
		hide: true,
		closeOnEscape: true,
		height: "auto",
		width: "auto",
		autoResize: true,
		position: { my: my, at: at, of: div },
		modal: false
		});
	manifast_need_init = false;
	$("#manifast-dialog").dialog("option", "title",
			"ManiFast: LOAD " + load_num);
	$("#manifast-dialog").dialog("open");
	refresh_manifast(aircraft_id, load_num);
	}
function url_with_date(date) {
	var sdate = date.replace(/\-/g, "");
	var m = window.location.pathname.match(/\d\d\d\d\d\d\d\d/)
	if (m == null) return window.location.pathname + "/" + sdate;
	else return window.location.pathname.replace(m[0], sdate);
	}
function date_change(aircraft_id, day_offset) {
	window.location = url_with_date(sheet_date(aircraft_id, day_offset));
	}
function roll_role(menu) {
	var m = menu.id.match(/^role[^0-9]*([0-9]+)_([0-9]+)_([0-9]+)/);
	var aircraft_id = m[1];
	var load_num = parseInt(m[2]);
	var slot_num = parseInt(m[3]);
	var role = menu.value;
	$.post("/manifest/role",
			{
				aircraft: aircraft_id,
				load: load_num,
				slot: slot_num,
				date: sheet_date(aircraft_id, 0),
				role: role
				},
			function(resp) {
				if (!resp.scratch) return;
				scratch(resp.aircraft, resp.load, [resp.slot]);
				}, "json")
	}
function set_exit_weight(person_id) {
	$.get("/manifest/setxitwt",
		{ pid: person_id, weight: exit_weight },
		function (resp) {}, "json");
	exit_weight = "";
	}
function select_person(event, ui) {
	if (ui.item.pid == 0) {
		// no match
		var m;
		var last_name = "";
		var first_name = "";
		var name = ui.item.value.trim();
		m = name.match(/^([^ ,]+)[ ]*,[ ]*([^ ]+)/);
		if (m != null) {
			last_name = m[1];
			first_name = m[2]
			}
		else {
			m = name.match(/^([^ ]+)[ ]+([^ ]+)/);
			if (m != null) {
				first_name = m[1];
				last_name = m[2];
				}
			}
		people_panel(-1, ui.item.aircraft, ui.item.load, ui.item.slot,
					last_name, first_name, false);
		}
	else {
		if (exit_weight != "") set_exit_weight(ui.item.pid);
		board_persons([ui.item.pid],[''], [false],
			ui.item.aircraft, ui.item.aircraft,
			ui.item.load, ui.item.slot, 0);
		}
	return false;
	}
var cr_handler = null;
function cr_shortcut(ui, aircraft, load, slot, box_id) {
	// set up to process carriage return
	if (ui.content[0].pid == 0) {
		cr_handler = function() {
			var data = new Object();
			data.item = new Object();
			data.item.pid = 0;
			data.item.value = ui.content[0].value;
			data.item.aircraft = aircraft;
			data.item.load = load;
			data.item.slot = slot;
			select_person(null, data);
			}
		}
	else  {
		cr_handler = function() {
			var data = new Object();
			data.item = new Object();
			data.item.pid = ui.content[0].pid;
			data.item.aircraft = aircraft;
			data.item.load = load;
			data.item.slot = slot;
			select_person(null, data);
			$("#" + box_id).autocomplete("close");
			}
		}
	}
function set_keypad(checkbox) {
	var cb = document.getElementById("keypad");
	$.get("/manifest/keypad",
		{ state: (cb.checked ? "1" : "0") },
		function(resp) {},
		"json");
	}
function collect_exit_weight(text_box) {
	// Any number following "#" in search term
	// is considered an exit weight value, and is
	// used to set jumper's exit weight when selected
	// from search menu.
	var m = text_box.value.match(/#([0-9]+)/);
	if (m != null) exit_weight = m[1];
	}
function slot_unfocused() {
	slots_focused -= 1;
	if (slots_focused < 0) slots_focused = 0;
	}
function live_search(box, aircraft_id, load, slot, evnt) {
	slots_focused += 1;
	if ((box.id == "") || (box.id == null)) return true;
	var selector = "#" + box.id;
	var keypad = document.getElementById("keypad").checked;
	box.value = "";
	exit_weight = "";
	if (slot >= 0)
		focus_on_role = "role" + aircraft_id + "_" + load + "_" + slot;
	if (keypad) {
		box.onfocus = null;
		box.onkeypress = null;
		$(selector).keyboard({
			layout: "custom",
			customLayout: {
				"default":
					[
						"1 2 3",
						"4 5 6",
						"7 8 9",
						"0 {bksp} {sp:0.1}",
						"{cancel}"
						]
				}
			}).autocomplete({
				source: "/manifest/search?allow_new=0",
				minLength: min_search_length,
				select: function(ev, ui) {
							ui.item.aircraft = aircraft_id;
							ui.item.load = load;
							ui.item.slot = slot;
							$(selector).data("keyboard").close(false);
							select_person(ev, ui);
							},
				response: function(ev, ui) {
							collect_exit_weight(box);
							cr_shortcut(ui, aircraft_id,
									load, slot, box.id);
							}
			}).addAutocomplete();
			$(selector).data("keyboard").reveal();
		}
	else {
		$("#" + box.id).autocomplete({
			source: "/manifest/search?allow_new=0",
			minLength: min_search_length,
			select: function(ev, ui) {
						ui.item.aircraft = aircraft_id;
						ui.item.load = load;
						ui.item.slot = slot;
						select_person(ev, ui);
						},
			response: function(ev, ui) {
						collect_exit_weight(box);
						cr_shortcut(ui, aircraft_id, load, slot, box.id);
						}
			});
		}
	return true;
	}
function role_menu(key, roles, finish) {
	echo_multi("role", key,
		function(menu) {
			var option;
			menu.options.length = 0;
			if (roles == null) {
				menu.add(new Option("-", "-"));
				}
			else {
				var preset = roles[0];
				var selected = 0;
				for (var i = 1; i < roles.length; i++) {
					option = new Option(roles[i], roles[i]);
					menu.add(option);
					if (roles[i] == preset) selected = i - 1;
					}
				menu.selectedIndex = selected;
				}
			if (finish != null) finish(menu);
			});
	}
function refresh_copilot_menu(aircraft_id, load_num) {
	return;
/*
	var detail = document.getElementById("call" +
			aircraft_id + "_" + load_num);
	if (detail == null) return;
	if (detail.style.display == "none") return;
	$.get("/manifest/copilots",
			{
				aircraft: aircraft_id,
				load: load_num,
				date: sheet_date(aircraft_id, 0)
				},
			function(resp) {
				var div = document.getElementById("copilot" +
						resp.aircraft + "_" + resp.load);
				div.innerHTML = resp.html;
				}, "json");
*/
	}
function reduce_empties(load_id) {
	var i;
	var keepers = Array();
	for (i = 0; i < empty_loads.length; i++) {
		if (empty_loads[i] == load_id) continue;
		keepers.push(empty_loads[i]);
		}
	empty_loads = keepers;
	}
function fill_slots(resp) {
	clear_group();
	var name;
	if (!resp.status) {
		name = document.getElementById("name" + resp.slot_key);
		if (name != null) {
			name.innerHTML = resp.form;
			name.childNodes[0].focus();
			}
		occupied_slots = new Array();
		post_scratch = false;
		local_alert("Boarding Errors", resp.msg);
		return;
		}
	var key, menu, alt_key;
	for (var i = 0; i < resp.slot_keys.length; i++) {
		key = resp.slot_keys[i];
		echo_multi("name", key,
			function(obj){ obj.innerHTML = resp.forms[i] }
			);
		role_menu(key, resp.roles[i],
			function(menu){ menu.disabled = false });
		}
	reduce_empties(resp.load_id);
	set_drag_select(resp.admin);
	set_slot_context_menus();
	if (manifast_up()) open_manifast(resp.dst_aircraft, resp.load_num);
	if (post_scratch) {
		post_scratch = false;
console.log("occupied :" + occupied_slots.join(",") + ":");
		scratch(resp.src_aircraft + "", drag_from_load, occupied_slots);
		}
	occupied_slots = new Array();
	refresh_copilot_menu(resp.dst_aircraft, resp.load_num);
	update_load_weight(resp.dst_aircraft, resp.load_num);
	}
function board_persons(ids, roles, extras, dst_aircraft, src_aircraft,
						load, slot, speed_dial) {
	$("#search_menu").hide();
	$.post("/manifest/board",
		{
			ids: ids.join(","),
			roles: roles.join(","),
			extras: JSON.stringify(extras),
			dst_aircraft: dst_aircraft,
			src_aircraft: src_aircraft,
			load: load,
			slot: slot,
			date: sheet_date(src_aircraft, 0),
			sd: speed_dial }, fill_slots, "json");
	}
function scratch(aircraft_id, load, slots) {
	$.post("/manifest/scratch",
		{
			date: sheet_date(aircraft_id, 0),
			load: load,
			aircraft: aircraft_id,
			slots: slots.join(",")
			},
		function(resp) {
			if (!resp.status) return;
			var key;
			for (var i = 0; i < resp.slots.length; i++) {
				key = resp.aircraft_id + "_" + resp.load +
					"_" + resp.slots[i];
				echo_multi("name", key,
					function(slot){
						slot.innerHTML = resp.forms[i];
						slot.style.backgroundColor = "white";
						slot.focus();
						});
				role_menu(key, null,
					function(menu){ menu.disabled = true; });
				}
			if (resp.emptied != "")
				empty_loads.push(resp.emptied);
			refresh_manifast(resp.aircraft_id, resp.load);
			refresh_copilot_menu(resp.aircraft_id, resp.load);
			update_load_weight(resp.aircraft_id, resp.load);
			},
		"json");
	occupied_slots = new Array();
	}
function mmss(sec) {
	if (sec < 1) return "NOW!";
	var mins = Math.floor(sec / 60);
	var secs = sec % 60;
	var out = "";
	if (mins < 10) out += "0";
	out += mins;
	out += ":";
	if (secs < 10) out += "0";
	out += secs;
	return out
	}
function hide_countdown(aircraft) {
	var field = document.getElementById("loadcall" + aircraft);
	if (field != null) field.innerHTML = "";
	field = document.getElementById("loadcall-1-" + aircraft);
	if (field != null) field.innerHTML = "";
	field = document.getElementById("tabcount" + aircraft);
	if (field != null) field.innerHTML = "";
	showing_calls[aircraft] = {
		active: false,
		load: 0,
		remain: 0
		};
	}
function countdown_html(remain, load_num, clock) {
	var html = ""
	if (remain <= flash_threshold) html += "<div class=\"blinker\">";
	html += "LOAD " + load_num + " - " + clock;
	if (remain <= flash_threshold) html += "</div>";
	return html;
	}
function show_countdown(aircraft, load_num, remain) {
	var clock = mmss(remain);
	var div = document.getElementById("loadcall" + aircraft);
	if (div != null)
		div.innerHTML = countdown_html(remain, load_num, clock);
	div = document.getElementById("loadcall-1-" + aircraft);
	if (div != null)
		div.innerHTML = countdown_html(remain, load_num, clock);
	var tab = document.getElementById("tabcount" + aircraft);
	tab.innerHTML = "L" + load_num + "-" + clock;
	}
function paint_load_calls(calls) {
	// update load call countdowns
	var n = (calls == null ? 0 : calls.length);
	for (var i = 0; i < n; i++) {
		tuple = calls[i];
		if (tuple.drop) hide_countdown(tuple.aircraft);
		else {
			showing_calls[tuple.aircraft] = {
				active: true,
				load: tuple.load,
				remain: tuple.remain
				};
			}
		}
	}
function refresh_countdowns() {
	var aircraft;
	for (aircraft in showing_calls) {
		if (!showing_calls[aircraft].active) continue;
		show_countdown(aircraft,
						showing_calls[aircraft].load,
						showing_calls[aircraft].remain);
		showing_calls[aircraft].remain -= 1;
		}
	}
function update_manifest(resp, retrack) {
	var n;
	var div;
	var tuple;
	if (resp.load_calls != null) paint_load_calls(resp.load_calls);
	else {
		var aircraft;
		for (aircraft in showing_calls) {
			if (showing_calls[aircraft].active) hide_countdown(aircraft);
			}
		}
	if (!resp.refresh || (resp.patches == null)) {
		//if (retrack) LongPoll.poll();
		return;
		}
	n = resp.patches.length;
	var date;
	var patch;
	var key;
	var manifast;
	var closed;
	var loads_touched = {};
	for (var i = 0; i < n; i++) {
		patch = resp.patches[i];
		key = patch.aircraft_id + "_" + patch.load;
		date = sheet_date(patch.aircraft_id, 0);
		if (date != patch.date) continue;
		if (!loads_touched.hasOwnProperty(key)) {
			loads_touched[key] = true;
			color_sheet(patch.aircraft_id, patch.load,
				patch.color, patch.status);
			manifast = document.getElementById("manifast_" + key);
			if (manifast != null) {
				if (resp.admin || !patch.closed)
					manifast.style.display = "block";
				else manifast.style.display = "none";
				}
			update_load_weight(patch.aircraft_id, patch.load);
			}
		closed = (patch.closed && !resp.admin);
		key = key + "_" + patch.slot;
		echo_multi("name", key,
			function(form){ form.innerHTML = patch.name_form; });
		echo_multi("scratch", key,
			function(form) {
				form.disabled = closed || (patch.locked && !resp.admin);
				});
		role_menu(key, patch.roles,
			function(menu) {
				menu.disabled = (closed || (patch.roles == null));
				});
		}
	if (focus_on_role != "") {
		var role = document.getElementById(focus_on_role);
		if (role != null) role.focus();
		}
	set_drag_select(resp.admin);
	set_slot_context_menus();
	}
function process_payload(payload) {
	if (payload.cmd == "patch") {
		update_manifest(payload, true);
		scroll_to_recent_load(false);
		var patch, slot;
		for (var i = 0; i < payload.patches.length; i++) {
			patch = payload.patches[i];
			slot = document.getElementById("tabload" + patch.aircraft_id);
			if (slot == null) continue;
			if (patch.recent_load > 0)
				slot.innerHTML = "[" + patch.recent_load + "]";
			else slot.innerHTML = "";
			}
		bink_icon();
		}
	else if (payload.cmd == "calls-only") {
		if (payload.load_calls != null)
			paint_load_calls(payload.load_calls);
		}
	else if (payload.cmd == "update-qualifiers") {
		var n = payload.qualifiers.length;
		var qualifier, div;
		for (var i = 0; i < n; i++) {
			qualifier = payload.qualifiers[i];
			div = document.getElementById("qualifier" + qualifier.id);
			div.innerHTML = qualifier.button;
			}
		}
	else if (payload.cmd == "repaint") {
		window.location.reload(true);
		}
	}
function title_detail_button(title, aircraft, load, suffix) {
	var btn = document.getElementById("call-btn" + suffix +
						aircraft + "_" + load);
	btn.value = title;
	}
function toggle_call(aircraft_id, load_num, suffix) {
	var div = document.getElementById("call" + suffix +
				aircraft_id + "_" + load_num);
	if (div.style.display == "block") {
		title_detail_button("Detail", aircraft_id, load_num, suffix);
		div.style.display = "none";
		return;
		}
	$.get("/manifest/call",
		{
			aircraft: aircraft_id,
			date: sheet_date(aircraft_id, 0),
			suffix: suffix,
			load: load_num},
		function(resp) {
			var div = document.getElementById("call" + suffix +
							resp.aircraft + "_" + resp.load);
			title_detail_button("Dismiss", resp.aircraft,
							resp.load, suffix);
			div.innerHTML = resp.html;
			div.style.display = "block";
			}, "json");
	}
function qualify(aircraft_id, load_num, qualifier) {
	$.get("/manifest/qualload",
			{
				aircraft: aircraft_id,
				date: sheet_date(aircraft_id, 0),
				qualifier: qualifier,
				load: load_num},
				function(resp) {
					var key = resp.aircraft + "_" + resp.load;
					var target = document.getElementById("fuelob" + key);
					if (resp.fuel == "restore") {
						var id = "fobret" + key;
						target.value = document.getElementById(id).value;
						}
					else target.value = resp.fuel;
					set_load_fuel(target.value, resp.aircraft, resp.load);
					},
			"json");
	}
function set_call(aircraft_id, load_num, suffix) {
	var div = document.getElementById("call" + suffix +
				aircraft_id + "_" + load_num);
	div.style.display = "none";
	var call_time = document.getElementById("calltime" + suffix +
				aircraft_id + "_" + load_num).value;
	title_detail_button("Detail", aircraft_id, load_num, suffix);
	$.get("/manifest/setcall",
		{
			aircraft: aircraft_id,
			date: sheet_date(aircraft_id, 0),
			load: load_num,
			time: call_time },
		function(resp) {
			paint_load_calls(resp.load_calls);
			},
		"json");
	}
function abort_call(aircraft_id, load_num, suffix) {
	var div = document.getElementById("call" + suffix +
				aircraft_id + "_" + load_num);
	div.style.display = "none";
	title_detail_button("Detail", aircraft_id, load_num, suffix);
	$.get("/manifest/abtcall",
		{
			aircraft: aircraft_id,
			date: sheet_date(aircraft_id, 0),
			load: load_num },
			function(resp) {
				hide_countdown(resp.aircraft);
				},
			"json");
	}
function speedman(pid, load_num, aircraft_id) {
	board_persons([pid], [""], [false],
			aircraft_id, aircraft_id, load_num, 0, 1);
	}
function color_sheet(aircraft_id, load, color, sheet_status) {
	var key = aircraft_id + "_" + load;
	var sheet = document.getElementById("load" + key);
	if (sheet == null) return;
	sheet.style.border = "6px solid " + color;
	var toprow = document.getElementById("toprow" + key);
	toprow.style.backgroundColor = color;
	var btn = document.getElementById("access" + key);
	if (btn != null) btn.value = sheet_status;
	}
function access_change(resp) {
	if (!resp.status) return;
	color_sheet(resp.aircraft_id, resp.load, resp.color, resp.access);
	}
function toggle_access(aircraft_id, load_num) {
	var btn = document.getElementById("access" +
				aircraft_id + "_" + load_num);
	var access = btn.value;
	$.get("/manifest/access", {
			date: sheet_date(aircraft_id, 0),
			aircraft: aircraft_id,
			load: load_num,
			access: access
			}, access_change, "json");
	}
function show_stats(resp) {
	var div = document.getElementById(resp.id);
	div.innerHTML = resp.html;
	div.style.display = "block";
	}
function toggle_stats(id) {
	var div = document.getElementById(id);
	var date = document.getElementById(id.replace("stats", "date"));
	if (div.style.display == "none") {
		$.get("/manifest/stats",
			{ id: id, date: date.value }, show_stats, "json");
		}
	else div.style.display = "none"; 
	}
function sheets_added(resp) {
	add_row(resp.aircraft, resp.cells, false);
	var button = document.getElementById("addrow" + resp.aircraft);
	var n = resp.cells.length;
	button.title = "+ " + n + " load sheets = " + (resp.end_load + n);
	}
function add_sheets(aircraft_id, date) {
	LongPoll.skip_one();
	$.get("/manifest/addsheets",
			{ aircraft: aircraft_id, date: date}, sheets_added, "json");
	}
function set_first_load(aircraft_id, first_load) {
	$.get("/manifest/setfirst", {
			aircraft: aircraft_id,
			date: sheet_date(aircraft_id, 0),
			load: first_load
			},
		function(resp) { load_sheets(resp.aircraft, true); },
		"json");
	}
function logout() {
	$.get("/login/logout", {},
		function(resp) { window.location = "/manifest"; },
		"json");
	}
function close_trap(ev, ui) {
	if (current_people_id < 0) return true;
	var pid = current_people_id;
	current_people_id = -1;
	close_detail(pid);
	return false;
	}
function people_panel(pid, aircraft, load_num, slot_num,
						last_name, first_name, board_on_close) {
	if (pid < 0) {
		$.get("/people/new", {
				last_name: last_name,
				first_name: first_name,
				role: "TAN"
				},
			function(resp) {
				people_panel(resp.pid, aircraft,
					load_num, slot_num, "", "", true);
				},
			"json");
		return;
		}
	current_people_id = pid;
	$("#people-dialog").dialog({
		beforeClose: close_trap,
		draggable: true,
		show: true,
		hide: true,
		closeOnEscape: true,
		height: "auto",
		width: 1200,
		autoResize: false,
		resizable: true,
		position: { my: "center", at: "center",
						of: selected_aircraft },
		modal: false
		});
	$("#people-dialog").dialog("option", "title", "People");
	$("#people-dialog").dialog("open");
	$.get("/people/detail", { pid: pid },
			function(resp) {
				resp.board = board_on_close;
				resp.aircraft = aircraft;
				resp.load_num = load_num;
				resp.slot_num = slot_num;
				load_person(resp);
				},
			"json");
	}
function render_accounts(resp) {
	$.get("/accounting/myaccount", { pid: resp.owner },
				load_account, "json");
	}
function get_ledger_limit() {
	return 25;
	}
function enable_acct_tracking(state) {};
function select_xfer(event, ui) {
	document.getElementById("acct-xfer-id").value = ui.item.acct_id;
	}
function live_xfer_search(box) {
	box.value = "";
	$("#" + box.id).autocomplete({
		source: "/accounting/search",
		minLength: min_search_length,
		select: select_xfer
		});
	}
function load_javascript(url, signal, callback) {
	var key = "[" + url + "]";
	if (javascript_catalog.indexOf(key) >= 0) {
		callback();
		return;
		}
	var js = document.createElement("script");
	js.setAttribute("type", "text/javascript");
	js.setAttribute("src", url);
	javascript_catalog += key;
	document.getElementsByTagName("head")[0].appendChild(js);
	var wait = setInterval(
			function() {
				if (eval("typeof " + signal) == "function") {
					clearInterval(wait);
					callback();
					}
				}, 50);
	return;
	}
	
function load_account(resp) {
	var box = document.getElementById("account-dialog");
	if (!resp.status) {
		box.innerHTML = unauthorized("black");
		return;
		}
	Acct.set_current_account(resp.acct);
	Acct.set_manifest_context(true);
	box.innerHTML = resp.html;
	Acct.configure_product_item_menus();
	$("input.dp").datepicker({
		dateFormat: date_format,
		altFormat: "yy-mm-dd",
		setDate: new Date(Date.now),
		firstDay: 1,
		changeYear: true
		});
	var chart_wrap = document.getElementById("chart-wrap");
	chart_wrap.scrollTop = chart_wrap.scrollHeight;
	}
function account_panel(pid) {
	$("#account-dialog").dialog({
		draggable: true,
		show: true,
		hide: true,
		closeOnEscape: true,
		height: "auto",
		width: "auto",
		autoResize: false,
		resizable: true,
		position: { my: "center", at: "center",
						of: selected_aircraft },
		modal: false
		});
	$("#account-dialog").dialog("option", "title", "Account");
	$("#account-dialog").dialog("open");
	$("input.dp").datepicker({
		dateFormat: date_format,
		altFormat: "yy-mm-dd",
		setDate: new Date(Date.now),
		firstDay: 1,
		changeYear: true
		});
	$.get("/accounting/myaccount", { pid: pid }, load_account, "json");
	}
function delete_person(pid) {
	$.get("/people/del", { pid: pid },
		function() { close_detail(current_people_id) },
		"json");
	}
function detail_closed(resp) {
	$("#people-dialog").dialog("close");
	if (resp.delete)
		$.get("/people/del", { pid: resp.pid },
			function() {}, "json");
	}
function close_detail(pid) {
	current_people_id = -1;
	$.get("/people/close_det", { pid: pid }, detail_closed, "json");
	}
function person_detail_updated(resp) {
	if (!resp.status) {
		local_alert("Update Failed", resp.msg);
		return;
		}
	close_detail(resp.pid);
	if (resp.board) {
		board_persons([resp.pid], [""], [false],
					resp.aircraft,
					resp.aircraft,
					resp.load_num,
					resp.slot_num, 0);
		}
	else {
		// force refresh on affected slot to reflect any
		// changes to name and/or roles
		var id = "role" + resp.aircraft + "_" +
				resp.load_num  + "_" +
				resp.slot_num;
		roll_role(document.getElementById(id));
		}
	}
function cell_value(id, column) {
	return document.getElementById("people-" + column + id).value;
	}
function checkbox_value(id, column) {
	if (document.getElementById("people-" + column + id).checked)
		return 1;
	return 0;
	}
function load_person(resp) {
	var box = document.getElementById("people-dialog");
	if (!resp.status) {
		box.innerHTML = unauthorized("white");
		return;
		}
	if (resp.busy) {
		local_alert("Record Busy", "This record is held by " + resp.holder
				+ ".<br/>Access is read-only till released.");
		}
	box.innerHTML = resp.html;
	var button = document.getElementById("people-del-button" + resp.pid);
	People.panel_config(resp.pid, date_format, resp.first_last, false,
		!resp.busy && //save
			function() {
				return People.update_detail(resp.pid,
					function(resp2) {
						resp2.aircraft = resp.aircraft;
						resp2.load_num = resp.load_num;
						resp2.slot_num = resp.slot_num;
						resp2.board = resp.board;
						person_detail_updated(resp2);
						});
				},
		function() { close_detail(resp.pid); }, // cancel
		!button.disabled && !resp.busy && // delete
			function() {
				$.get("/people/predel", { pid: current_people_id },
						function(resp) {
							if (resp.blank) delete_person(resp.pid);
							else {
									local_confirm("Manifest",
										"Yes", "No",
										function() {
											delete_person(resp.pid);
											}, null,
										"Really? Delete '"
											+ resp.name + "'?");
									}
							},
						"json");
				}
		);
	}
function display_sheets(mode) {
	var sheet, i;
	for (i = 0; i < empty_loads.length; i++) {
		sheet = document.getElementById("load" + empty_loads[i]);
		sheet.style.display = mode;
		}
	}
function make_printable(aircraft_id) {
	var sheets = document.getElementById("sheets" + aircraft_id);
	sheets.className = "load-sheets-printable";
	display_sheets("none");
	window.print();
	display_sheets("block");
	sheets.className = "load-sheets";
	}
function search_key(ev) {
	if ((ev.keyCode == 13) && (cr_handler != null)) {
		cr_handler();
		cr_handler = null;
		}
	}
function mark_page(sel_text) {
	if (document.getElementById("selector") != null) {
		menu_preset = sel_text;
		build_app_menu();
		}
	}
function open_stats(aircraft_id) {
	var date = sheet_date(aircraft_id, 0);
	$("#stats-dialog").dialog({
		draggable: true,
		show: true,
		hide: true,
		title: "Load Statistics: " + date,
		closeOnEscape: true,
		height: "auto",
		width: "auto",
		resizable: false,
		autoResize: true,
		modal: false
		});
	$.get("/manifest/stats",
		{ date: date },
		function(resp) {
			var box = document.getElementById("stats-dialog");
			box.innerHTML = resp.html;
			$("#stats-dialog").dialog("open");
			}, "json");
	}
function log_pilot(menu, aircraft_id, load_num, selector) {
	$.get("/manifest/setpilot",
			{
				aircraft: aircraft_id,
				load: load_num,
				date: sheet_date(aircraft_id, 0),
				pilot: menu.options[menu.selectedIndex].value,
				selector: selector
				},
			function(resp) {
				update_load_weight(resp.aircraft, resp.load);
				}, "json");
	}
function load_fuel_change(box, aircraft_id, load_num) {
	var key = aircraft_id + "_" + load_num;
	document.getElementById("fobret" + key).value = box.value;
	set_load_fuel(box.value, aircraft_id, load_num);
	}
function set_load_fuel(fuel_amt, aircraft_id, load_num) {
	$.get("/manifest/setfuel",
			{
				aircraft: aircraft_id,
				load: load_num,
				date: sheet_date(aircraft_id, 0),
				value: fuel_amt
				},
			function(resp) {
				var key = resp.aircraft + "_" + resp.load;
				document.getElementById("fuelob" + key).value = resp.gals;
				update_load_weight(resp.aircraft, resp.load);
				}, "json");
	}
function update_load_weight(aircraft_id, load_num) {
	$.post("/manifest/updwt",
			{
				aircraft: aircraft_id,
				load: load_num,
				date: sheet_date(aircraft_id, 0)
				},
			function(resp) {
				var key = resp.aircraft + "_" + resp.load;
				var div = document.getElementById("loadwt" + key);
				if (div != null) {
					div.innerHTML = resp.wt_html;
					div = document.getElementById("cg" + key);
					div.innerHTML = resp.cg_html;
					}
				}, "json");
	}
function track_search(str) {
	search_buf = str;
	var div = document.getElementById("search-track-drop" +
					selected_aircraft_id);
	if (div == null) return;
	var html = "";
	if (search_buf != "")
		html += "<i>" + search_buf.split("").join("&zwnj;") + "</i>";
	div.innerHTML = html;
	return search_buf;
	}
function manifest_help() {
	Help.open_wiki('Manifest');
	}
function find_and_scroll(str, backward) {
	if (window.find(str, false, backward, true, false, false, false)) {
		window.getSelection().focusNode.parentNode.scrollIntoView(false);
		return true;
		}
	return false;
	}
function go_find(str, backward) {
	if (find_and_scroll(str, backward)) return;
	window.getSelection().removeAllRanges();
	find_and_scroll(str, backward);
	}
function hitkey(obj, event, search_mode) {
	var div = document.getElementById("sheets" + selected_aircraft_id);
	if (div == null) return true;
	if (slots_focused > 0) return true;
	var code = event.key;
	var grab = false;
	if (code == "ArrowDown") {
		scroll_one_step(1);
		grab = true;
		}
	else if (code == "ArrowUp") {
		scroll_one_step(-1);
		grab = true;
		}
	else if (code == "F1") {
		manifest_help();
		}
	if (search_mode) {
		if (code == "Escape") {
			track_search("");
			grab = true;
			}
		else if (code == "Delete") {
			track_search("");
			grab = true;
			}
		else if (code == "Backspace") {
			if (search_buf != "") {
				track_search(search_buf.substr(0, search_buf.length - 1));
				go_find(search_buf, false);
				}
			grab = true;
			}
		else if (code == "ArrowRight") {
			if (search_buf != "") go_find(search_buf, false);
			grab = true;
			}
		else if (code == "ArrowLeft") {
			if (search_buf != "") go_find(search_buf, true);
			grab = true;
			}
		else if (code.match(/^\w$/i) && !event.ctrlKey) {
			track_search(search_buf + code);
			go_find(search_buf, false);
			grab = true;
			}
		}
	return !grab;
	}
function people_note_add(pid) {
	$.get("/people/addnote", { pid: pid },
			function(resp) {
				var div = document.getElementById("people-notes" + pid);
				div.innerHTML = resp.html;
				}, "json"
			);
	}
function people_note_edit(box, note_id) {
	$.post("/people/ednote",
			{ nid: note_id, note: box.value },
			function(resp) {
				}, "json"
			);
	}
function people_note_del(note_id, person_id) {
	$.get("/people/delnote",
			{ nid: note_id, pid: person_id },
			function(resp) {
				var div = document.getElementById("people-notes" + person_id);
				div.innerHTML = resp.html;
				}, "json"
			);
	}
function people_note_hilite(checkbox, note_id, person_id) {
	$.post("/people/hlnote",
			{ nid: note_id,
				pid: person_id,
				state: (checkbox.checked ? 1 : 0) },
			function(resp) { }
			);
	}
function hide_manifest_header() {
	document.getElementById("header").style.display = "none";
	var sheets = document.getElementsByClassName("load-sheets");
	for (var i = 0; i < sheets.length; i++) {
		sheets[i].style.height = "70vh";
		sheets[i].style.maxHeight = "70vh";
		}
	sheets = document.getElementsByClassName("load-sheets-inline");
	for (var i = 0; i < sheets.length; i++) {
		sheets[i].style.height = "70vh";
		sheets[i].style.maxHeight = "70vh";
		}
	}
function detect_viz_api() {
	var event_name;
	var hidden = null;
	if (typeof document.hidden !== "undefined") {
		hidden = "hidden";
		event_name = "visibilitychange";
		}
	else if (typeof document.msHidden !== "undefined") {
		hidden = "msHidden";
		event_name = "msvisibilitychange";
		}
	else if (typeof document.webkitHidden !== "undefined") {
		hidden = "webkitHidden";
		event_name = "webkitvisibilitychange";
		}
	if (hidden == null) return;
	document.addEventListener(event_name,
		function(event) {
			event.preventDefault();
			event.stopPropagation();
			document_hidden = document[hidden];
			change_icon("/favicon.ico");
			binking = false;
			}, true);
	}
