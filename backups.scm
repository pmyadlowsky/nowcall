#! /usr/local/bin/gusher -p 3010
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))
(use-modules (ice-9 ftw))
(use-modules (ice-9 regex))
(use-modules (ice-9 rdelim))
(use-modules (ice-9 popen))
(use-modules (gusher cron))

(include "lib.scm")
(include "settings_lib.scm")

(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-backup-dir (setting-cache 'backup-dir))
(define env-backup-retention (setting-cache 'backup-retain))
(define sql-dump-dir "/vol/store/tmp")
(define frame (make-doc 'file "frame.html"))
(define page-body (make-doc 'file "backups.html"))
(define composite
	(make-doc (list frame page-body)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "Backups")
				(cons 'app "backups")
				(cons 'script "")
				(cons 'version nowcall-version)
				(cons 'content
					(fill-template (fetch-doc page-body) #t))))))
(define backup-pat
	(make-regexp "([0-9][0-9][0-9][0-9])([0-9][0-9])([0-9][0-9])([0-9][0-9])([0-9][0-9])([0-9][0-9])\\.sql\\.(bz2|gz)$"))
(define backups-table
	(let ([entry-row-tpt
				(deflate "<tr>
				<td class=\"tcell bcell [[TINT]]\">
				<button type=\"button\"
					class=\"chart-btn\"
					onclick=\"delentry('[[KEY]]')\"
					title=\"delete this snapshot\"><img
					src=\"/img/delete-16.png\" alt=\"delete\"></button>
				</td>
				<td class=\"tcell bcell [[TINT]]\">
				<button type=\"button\"
					class=\"chart-btn\"
					onclick=\"expentry('[[KEY]]')\"
					title=\"download this snapshot\"><img
				src=\"/img/download-16.png\" alt=\"download\"></button>
				</td>
				<td class=\"tcell bcell [[TINT]]\">
				<button type=\"button\"
					class=\"chart-btn\"
					onclick=\"restore('[[KEY]]')\"
					title=\"restore from this snapshot\"><img
				src=\"/img/reload-16.png\" alt=\"reload\"></button>
				</td>
				<td class=\"tcell [[TINT]]\">[[STAMP]]</td>
				<td class=\"tcell [[TINT]]\"
					style=\"text-align: right\">[[SIZE]]</td>
				<td class=\"ncell [[TINT]]\">[[NOTE]]</td>
				</tr>")])
		(define (entry-note key base-dir)
			(let ([path (format #f "~a/~a.txt" base-dir key)])
				(catch #t
					(lambda ()
						(call-with-input-file path
							(lambda (port) (read-line port))))
					(lambda (key . args) ""))))
		(define (file-size path)
			(if (file-exists? path)
				(let ([bytes (stat:size (stat path))])
					(cond
						[(< bytes 1000) (format #f "~dB" bytes)]
						[(< bytes 1000000)
							(format #f "~1,1fK" (/ bytes 1000.0))]
						[(< bytes 1000000000)
							(format #f "~1,1fM" (/ bytes 1000000.0))]
						[else
							(format #f "~1,1fG"
								(/ bytes 1000000000.0))])) "?"))
		(define (entry-row entry tint base-dir)
			(let* ([match (regexp-exec backup-pat entry)]
					[key
						(format #f "~a~a~a~a~a~a"
							(match:substring match 1)
							(match:substring match 2)
							(match:substring match 3)
							(match:substring match 4)
							(match:substring match 5)
							(match:substring match 6))])
				(fill-template entry-row-tpt #f
					(cons 'tint (if tint "tint-row" "plain-row"))
					(cons 'key key)
					(cons 'size
						(file-size (format #f "~a/~a" base-dir entry)))
					(cons 'stamp
						(format #f "~a/~a/~a ~a:~a:~a"
							(match:substring match 2)
							(match:substring match 3)
							(match:substring match 1)
							(match:substring match 4)
							(match:substring match 5)
							(match:substring match 6)))
					(cons 'note (entry-note key base-dir)))))
		(lambda ()
			(let ([tint #f]
					[dir (env-backup-dir)])
				(string-cat "\n"
					"<table>"
					(map
						(lambda (entry)
							(set! tint (not tint))
							(entry-row entry tint dir))
						(reverse
							(filter
								(lambda (entry)
									(regexp-exec backup-pat entry))
								(or (scandir dir) '()))))
					"</table>")))))
(define add-snapshot
	(let ([dump-cmd "/usr/bin/pg_dump -h database -c -U nowcall nowcall"]
			[compress-cmd "gzip"])
		(define (add-note note path-base)
			(unless (string=? note "")
				(call-with-output-file
					(format #f "~a.txt" path-base)
					(lambda (port) (format port "~a\n" note)))))
		(define (source-base source)
			(let ([target
						(format #f "~a/~a"
							(env-backup-dir)
							(time-format (time-now) "%Y%m%d%H%M%S"))])
				(if (eq? source 'from-dump)
					(system (format #f "~a > ~a.sql" dump-cmd target))
					(system (format #f "cp '~a' ~a.sql" source target)))
				target))
		(lambda (source note)
			(catch #t
				(lambda ()
					(let ([path-base (source-base source)])
						(system 
							(format #f "~a ~a.sql" compress-cmd path-base))
						(add-note note path-base)
						(list (cons 'status #t))))
				(lambda (key . args)
					(list
						(cons 'status #f)
						(cons 'msg
							(format #f "ERROR: ~s, ~s<br/>~a"
								key args
								"file system full or missing?"))))))))
(define deflater
	(let ([bz2-pat (make-regexp "\\.bz2$")]
			[gz-pat (make-regexp "\\.gz$")])
		(lambda (path)
			(format #f "~a ~a"
				(cond
					[(regexp-exec bz2-pat path) "bzcat"]
					[(regexp-exec gz-pat path) "zcat"]
					[else "cat"]) path))))
(define restore-from-dump
	(let ([restore-cmd
				"/usr/bin/psql >/vol/store/db_restore.log 2>&1 -f -\
					-h database nowcall nowcall"])
		(lambda (path)
			(let ([status
						(system
							(format #f "~a | ~a"
								(deflater path) restore-cmd))])
				(= status 0)))))
(define detect-path
	(let ()
		(define (try-path dir key suffix)
			(let ([path (format #f "~a/~a.sql.~a" dir key suffix)])
				(and (file-exists? path) path)))
		(lambda (key)
			(let ([dir (env-backup-dir)])
				(or
					(try-path dir key "gz")
					(try-path dir key "bz2"))))))
(define sweep
	; delete expired backup files
	(let ([query "select (current_date - interval '~d months') as old"]
			[base-pat (make-regexp "^([0-9]+)")])
		(define (cutoff-date)
			(let ([row
					(pg-one-row (sdb)
						(format #f query
							(max (env-backup-retention) 1)))])
				(time-epoch (pg-cell row 'old))))
		(define (old? entry-name cutoff)
			(< (stat:mtime (stat
					(format #f "~a/~a"
						(env-backup-dir) entry-name))) cutoff))
		(define (drop entry-name)
			(let* ([match (regexp-exec base-pat entry-name)]
					[note-path
						(format #f "~a/~a.txt"
							(env-backup-dir)
							(match:substring match 1))])
				(log-msg "rm ~a/~a" (env-backup-dir) entry-name)
				(delete-file
					(format #f "~a/~a" (env-backup-dir) entry-name))
				(when (access? note-path F_OK)
					(log-msg "rm ~a" note-path)
					(delete-file note-path))))
		(lambda ()
			(let ([cutoff (cutoff-date)])
				(for-each drop
					(scandir (env-backup-dir)
						(lambda (entry-name)
							(and
								(regexp-exec backup-pat entry-name)
								(old? entry-name cutoff)))))))))
(define (cksum file-path)
	(let* ([pipe (open-input-pipe (format #f "cksum ~a" file-path))]
			[out (read-line pipe 'trim)]
			[tokens (string-tokenize out)])
		(close-pipe pipe)
		(list
			(cons 'sum (to-i (car tokens)))
			(cons 'size (to-i (cadr tokens))))))
(define download-from-master
	(let ([wget-tpt "wget -O ~a 'https://~a/tmp/~a'"]
			[dbd-tpt "https://~a/backups/dbd"])
		(define (get-profile database-master)
			; generate SQL dump at master and return dump filename
			; checksum, operation status
			(let ([reply
						(http-get
							(format #f dbd-tpt database-master)
							(cons 'post
								(list
									(cons "s"
										(get-setting
											'shared-secret (sdb))))))])
				(cdr reply)))
		(define (write-note filebase)
			; create note for local copy of SQL dump
			; indicate that it's a dump of master database
			(let ([port
						(open-output-file (format #f "~a.txt" filebase))])
				(display "download from database master" port)
				(close-port port)))
		(define (compare-sums file-path checksum file-size)
			(let ([sums (cksum file-path)])
				(and
					(= (assq-ref sums 'size) file-size)
					(= (assq-ref sums 'sum) checksum))))
		(define (wget filename checksum file-size database-master)
			; download SQL dump from master and create note
			(let* ([filebase
						(format #f "~a/~a"
							(env-backup-dir)
							(time-format (time-now) "%Y%m%d%H%M%S"))]
					[sql-gzip (format #f "~a.sql.gz" filebase)])
				(and
					(= 0
						(system
							(format #f wget-tpt
								sql-gzip
								database-master filename)))
					(compare-sums sql-gzip checksum file-size)
					(write-note filebase) #t)))
		(lambda ()
			(let* ([database-master (get-setting 'db-master (sdb))]
					[profile (get-profile database-master)])
				(if (assq-ref profile 'status)
					(let ([filename (assq-ref profile 'file)])
						(list
							(cons 'status
								(wget filename
									(assq-ref profile 'sum)
									(assq-ref profile 'size)
									database-master))
							(cons 'msg
								(format #f
									"failed to download ~a" filename))))
					(list
						(cons 'status #f)
						(cons 'msg "failed to authenticate")))))))
(define (sql-dump-path entry-name)
	(format #f "~a/~a" sql-dump-dir entry-name))
(define sweep-dumps
	; delete expired SQL dump files
	(let ([dump-pat (make-regexp "^ncdbdump.*gz$")]
			[dump-lifetime 300])
		(define (old? entry-name)
			(> (time-diff
				(time-now)
				(time-at
					(stat:mtime
						(stat (sql-dump-path entry-name)))))
				dump-lifetime))
		(define (drop entry-name)
			(let ([path (sql-dump-path entry-name)])
				(log-msg "rm ~a" path)
				(delete-file path)))
		(define (dump-filter entry-name)
			(and
				(regexp-exec dump-pat entry-name)
				(old? entry-name)))
		(lambda ()
(log-msg "SWEEP DUMPS")
			(for-each drop
				(or (scandir sql-dump-dir dump-filter) '())))))

(log-to "/var/log/nowcall/backups.log")

(sweep-dumps)
(cron-add '() 0 (lambda (now) (sweep-dumps)))
(cron-add 3 13
	(lambda (now)
		(sweep)
		(add-snapshot 'from-dump "auto-backup")))

(http-html "/"
	(lambda (req)
		(if (authorized-for req "backups")
			(let ([files (query-value req 'upsnap_file)])
				(when (and files (not (string=? (cdr files) "")))
					; if file was uploaded, store it as a DB snapshot
					(add-snapshot
						(car files)
						(format #f "uploaded: ~a" (cdr files))))
				(fill-template (fetch-doc composite) #f
					(cons 'admin_check (admin-light req (sdb)))
					(cons 'dz_full_name (env-dz-full-name))))
			(redirect-html "/login/backups"))))
(http-json "/gdir"
	; display backups directory path
	(admin-gate "backups" (req)
		(list
			(cons 'dir (env-backup-dir))
			(cons 'db_master (get-setting 'db-master (sdb))))))
(http-json "/chkdir"
	; check write-accesibility of backups directory
	(admin-gate "backups" (req)
		(let ([dir (query-value req 'dir)]
				[success #f]
				[msg "missing, inaccessible or non-writable"])
			(catch #t
				(lambda ()
					(let* ([tmpfile (format #f "~a/test" dir)]
							[port (open-file tmpfile "w")])
						(close-port port)
						(delete-file tmpfile)
						(set! success #t)
						(set! msg "OK")))
				(lambda (key . args)
					(log-msg "CHKDIR: ~s, ~s" key args)))
			(list
				(cons 'can_write success)
				(cons 'msg msg)))))
(http-json "/list"
	; display backups chart
	(admin-gate "backups" (req)
		(list (cons 'table (backups-table)))))
(http-json "/make"
	; take snapshot
	(admin-gate "backups" (req)
		(let ([note (or (query-value req 'note) "")])
			(add-snapshot 'from-dump note))))
(http-json "/delete"
	(admin-gate "backups" (req)
		(let* ([key (query-value req 'key)]
				[path (detect-path key)])
			(when path
				(catch #t
					(lambda ()
						(delete-file path)
						(delete-file
							(format #f "~a/~a.txt" (env-backup-dir) key)))
					(lambda (key . args) #t)))
			(list (cons 'status #t)))))
(responder "/download"
	(admin-gate "backups" (req)
		(let* ([key (query-value req 'key)]
				[path (detect-path key)])
			(list
				"200 OK"
				(list
					(cons "content-type"
						"application/octet-stream")
					(cons "content-disposition"
						(format #f "attachment; filename=\"~a.sql\""
							key)))
				(if path
					(let* ([pipe
							(open-input-pipe (deflater path))]
							[sql (read-delimited "" pipe)])
						(close-port pipe)
						sql) "")))))
(http-json "/restore"
	(admin-gate "backups" (req)
		(let ([path (detect-path (query-value req 'key))])
			(if path
				(if (restore-from-dump path)
					(list (cons 'status #t))
					(list
						(cons 'status #f)
						(cons 'msg "restore may have failed; check log")))
				(list
					(cons 'status #f)
					(cons 'msg "backup file not found"))))))
(http-json "/mrdl"
	(admin-gate "backups" (req) (download-from-master)))
(define dump-db
	(let ([dump-cmd
			"pg_dump -h database -c -U nowcall nowcall | gzip - > ~a"])
		(define (cksum entry-name path)
			(let* ([pipe (open-input-pipe (format #f "cksum ~a" path))]
					[out (read-line pipe 'trim)])
				(close-pipe pipe)
				(let ([tokens (string-tokenize out)])
					(list
						(cons 'status #t)
						(cons 'sum (to-i (car tokens)))
						(cons 'size (to-i (cadr tokens)))
						(cons 'file entry-name)))))
		(lambda ()
			(let* ([entry
						(format #f "ncdbdump-~a.gz" (uuid-generate))]
					[full-path (sql-dump-path entry)])
				(system (format #f dump-cmd full-path))
				(cksum entry full-path)))))
(http-json "/dbd"
	(lambda (req)
		(let ([secret (or (query-value req 's) "")])
			(if (string=? secret (get-setting 'shared-secret (sdb)))
				(dump-db)
				(list (cons 'status #f))))))

(add-javascript-logger "backups")

(cron-start)
