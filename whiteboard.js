var last_stamp = 0.0;
var min_search_chars = 3;
var selected_person_id = 0;
var date_format = "";
var now_dragging = null;
function yymmdd(picker_id) {
	var date = $("#" + picker_id).datepicker("getDate");
	if (date == null) return "";
	return $.datepicker.formatDate("yy-mm-dd", date);
	}
function get_board_id(id) {
	var res = id.match(/^[a-z]+([0-9]+)/);
	if (res == null) return 0;
	return parseInt(res[1]);
	}
function drop_entry(ev, ui) {
	if (now_dragging == null) return;
	var src = get_board_id(now_dragging);
	now_dragging = null;
	$.get("/whiteboard/shuffle", {
			src: src,
			dst: get_board_id(ev.target.id)
			},
			function(resp) {
				if (resp.status) load_chart();
				else {
					local_alert("Whiteboard", resp.msg, 300,
						function() { load_chart(); });
					}
				}, "json");
	}
function load_chart() {
	$.get("/whiteboard/chart",
			{
				from: yymmdd("from-date"),
				to: yymmdd("to-date")
				},
			function(resp) {
				document.getElementById("chart").innerHTML = resp.html;
				$(".dragger").draggable({
					stack: ".dragger",
					start: function(ev, ui) {
								now_dragging = ev.target.id;
								}
					});
				$(".dragger").droppable({ drop: drop_entry });
				$("#spinner").hide();
				},
			"json");
	$("#spinner").show();
	}
function predelete_entry(obj, wb_id) {
	var student_name = obj.name;
	local_confirm("Whiteboard", "Yes", "No",
		function() {
			$.get("/whiteboard/drop", { id: wb_id },
					function(resp) { load_chart(); }, "json");
			}, null,
		"Drop " + student_name + " from whiteboard?");
	}
function media_change(board_id) {
	var menu = document.getElementById("media-" + board_id);
	$.get("/whiteboard/chmedia",
			{ bid: board_id, pid: menu.value },
			function(resp) {
				var box = document.getElementById("camera" + resp.bid);
				box.innerHTML = resp.html;
				}, "json");
	}
function instructor_change(ordinal, board_id) {
	var menu = document.getElementById("instructor-" +
							ordinal + "_" + board_id);
	$.get("/whiteboard/chinstr",
			{ bid: board_id, ord: ordinal, pid: menu.value },
			function(resp) { }, "json");
	}
function camera_change(board_id) {
	var menu = document.getElementById("camera-" + board_id);
	$.get("/whiteboard/chcam",
			{ bid: board_id, pid: menu.value },
			function(resp) { }, "json");
	}
function weight_change(box, person_id) {
	$.get("/whiteboard/chweight",
			{ pid: person_id, wt: box.value },
			function(resp) { }, "json");
	}
function jump_change(menu, board_id) {
	$.get("/whiteboard/chjump",
			{ jid: menu.value, bid: board_id },
			function(resp) {
				var box = document.getElementById("media" + resp.bid);
				box.innerHTML = resp.media;
				box = document.getElementById("instructor1_" + resp.bid);
				box.innerHTML = resp.instr1;
				box = document.getElementById("instructor2_" + resp.bid);
				box.innerHTML = resp.instr2;
				box = document.getElementById("camera" + resp.bid);
				box.innerHTML = resp.camera;
				}, "json");
	}
function post_menu(resp) {
	if (resp.status) {
		if (resp.menu == "") {
			local_alert("Boarding Error", "No loads available.");
			return;
			}
		var box = document.getElementById("load" + resp.bid);
		box.innerHTML = resp.menu;
		}
	else {
		local_alert("Boarding Error",
			"Can't manifest a whiteboard group created on a prior date."
			);
		}
	}
function board_group(board_id) {
	$.get("/whiteboard/validate", { bid: board_id },
			function(resp) {
				if (resp.status)
					$.get("/whiteboard/loads", { bid: resp.bid },
						post_menu, "json");
				else local_alert("Data Errors", resp.msg);
				}, "json");
	}
function unboard_group(board_id) {
	$.get("/whiteboard/scratch", { bid: board_id },
			function(resp) {
				if (!resp.status) {
					local_alert("Scratch Error",
						"Can't unmanifest a whiteboard group created on a prior date."
						);
					return;
					}
				if (resp.slots == null) {
					// couldn't locate group's manifest slots
					// (possibly unmanifested at Manifest app)
					load_chart();
					return;
					}
				$.post("/manifest/scratch",
						{ date: resp.date,
							load: resp.load,
							aircraft: resp.aircraft,
							slots: resp.slots.join(",") },
						function(resp2) {
							console.log("SCRATCHED " +
											JSON.stringify(resp2));
							}, "json");
				load_chart();
				}, "json");
	}
function charge_extra_item(board_id) {
	$.get("/whiteboard/extra", { bid: board_id },
			function(resp) {
				if (!resp.status) return;
				$.get("/manifest/chgextra", {
						jumper_id: resp.pid,
						item_id: resp.media,
						date: resp.date,
						slot: resp.slot_id
						},
					function(resp) { load_chart() }, "json");
				}, "json");
	}
function call_manifest(resp) {
	var ids = [];
	var roles = [];
	var extras = [ false, false ];
	ids.push(resp.student.id);
	roles.push(resp.student.role);
	if (resp.instructor1) {
		ids.push(resp.instructor1.id);
		roles.push(resp.instructor1.role);
		}
	if (resp.instructor2) {
		ids.push(resp.instructor2.id);
		roles.push(resp.instructor2.role);
		extras.push(false);
		}
	if (resp.camera) {
		ids.push(resp.camera.id);
		roles.push(resp.camera.role);
		extras.push(false);
		}
	var data = {
		ids: ids.join(","),
		roles: roles.join(","),
		extras: JSON.stringify(extras),
		dst_aircraft: resp.aircraft,
		src_aircraft: resp.aircraft,
		load: resp.load,
		slot: 0,
		date: resp.date,
		sd: 0 
		};
	$.post("/manifest/board", data,
			function(resp2) {
				if (resp2.status) {
					if (resp.has_extra) charge_extra_item(resp.bid);
					else load_chart();
					}
				else local_alert("Boarding Errors", resp2.msg);
				}, "json");
	load_chart();
	}
function select_load(menu, board_id) {
	$.get("/whiteboard/manifest",
			{ bid: board_id, load: menu.value },
			call_manifest, "json");
	}
function track() {
	$.get("/whiteboard/track", { },
			function(resp) {
				if (resp.msec > last_stamp) {
					last_stamp = resp.msec;
					load_chart();
					}
				}, "json");
	}
function dress_date(id, format) {
	$("#" + id).datepicker({
		dateFormat: format,
		altField: "#" + id + "-fmt",
		altFormat: "yy-mm-dd",
		firstDay: 1,
		changeYear: true
		});
	}
function dress(resp) {
	date_format = resp.date_format;
	dress_date("to-date", date_format);
	dress_date("from-date", date_format);
	min_search_chars = resp.min_search;
	}
function preset_dates() {
	var menu = document.getElementById("date-presets");
	var refresh = true;
	if (menu.value == "manual") {
		document.getElementById("from-date").value = "";
		document.getElementById("to-date").value = "";
		refresh = false;
		}
	else if (menu.value == "month") {
		var today = new Date();
		$("#from-date").datepicker("setDate",
			new Date(today.getFullYear(), today.getMonth(), 1));
		$("#to-date").datepicker("setDate",
			new Date(today.getFullYear(), today.getMonth() + 1, 0));
		}
	else {
		$.get("/whiteboard/dpreset", { key: menu.value },
				function(resp) {
					document.getElementById("from-date").value = resp.from;
					document.getElementById("to-date").value = resp.to;
					load_chart();
					}, "json");
		refresh = false;
		}
	if (refresh) load_chart();
	}
function post_button_decor(highlight) {
	var btn = document.getElementById("post-button");
	if (highlight) {
		btn.style.color = "green";
		btn.style.fontWeight = "bold";
		}
	else {
		btn.style.color = "";
		btn.style.fontWeight = "normal";
		}
	}
function clear_post_button() {
	document.getElementById("patron-select").value = "";
	post_button_decor(false);
	selected_person_id = 0;
	}
function arm_post_button(pid, name) {
	document.getElementById("patron-select").value = name;
	post_button_decor(true);
	selected_person_id = pid;
	}
function post_person() {
	if (selected_person_id == 0) {
		local_alert("Whiteboard",
			"Please select a patron to whiteboard. Start typing name to initiate search.", 300,
			function() {
				document.getElementById("patron-select").focus();
				});
		return;
		}
	$.get("/whiteboard/post", { pid: selected_person_id },
			function(resp) {
				load_chart();
				clear_post_button();
				}, "json");
	}
function close_detail(pid) {
	$.get("/people/close_det", { pid: pid },
			function(resp) {
				$("#people-dialog").dialog("close");
				if (resp.delete)
					$.get("/people/del", { pid: resp.pid },
						function() {}, "json");
				}, "json");
	}
function cancel_boarding(pid) {
	close_detail(pid);
	clear_post_button();
	}
function delete_person(pid) {
	$.get("/people/del", { pid: pid },
		function() { cancel_boarding(pid); }, "json");
	}
function load_person(resp) {
	var box = document.getElementById("people-dialog");
	if (!resp.status) {
		//box.innerHTML = unauthorized("white");
		cancel_boarding(resp.pid);
		return;
		}
	if (resp.busy) {
		local_alert("Record Busy", "This record is held by " + resp.holder
				+ ".<br/>Access is read-only till released.");
		}
	box.innerHTML = resp.html;
	var button = document.getElementById("people-del-button" + resp.pid);
	People.panel_config(resp.pid, date_format, resp.first_last, false,
		!resp.busy && //save
			function() {
				return People.update_detail(resp.pid,
					function(resp2) {
						if (!resp2.status) {
							local_alert("Update Failed", resp2.msg);
							return;
							}
						close_detail(resp2.pid);
						arm_post_button(resp2.pid, resp2.name);
						});
				},
		function() { cancel_boarding(resp.pid); }, //cancel
		!button.disabled && !resp.busy && // delete
			function() {
				$.get("/people/predel", { pid: selected_person_id },
						function(resp) {
							if (resp.blank) delete_person(resp.pid);
							else {
									local_confirm("Whiteboard",
										"Yes", "No",
										function() {
											delete_person(resp.pid);
											}, null,
										"Really? Delete '"
											+ resp.name + "'?");
									}
							},
						"json");
				}
		);
	}
function open_people_dialog(pid) {
	selected_person_id = pid;
	$("#people-dialog").dialog({
		//beforeClose: close_trap,
		draggable: true,
		show: true,
		hide: true,
		closeOnEscape: true,
		height: "auto",
		width: 1200,
		autoResize: false,
		resizable: true,
		modal: false
		});
	$("#people-dialog").dialog("option", "title", "People");
	$("#people-dialog").dialog("open");
	$.get("/people/detail", { pid: pid },
			function(resp) {
				load_person(resp);
				},
			"json");
	}
function select_person(event, ui) {
	if (ui.item.pid == 0) {
		// no match
		var m;
		var last_name = "";
		var first_name = "";
		var name = ui.item.value.trim();
		m = name.match(/^([^ ,]+)[ ]*,[ ]*([^ ]+)/);
		if (m != null) {
			last_name = m[1];
			first_name = m[2]
			}
		else {
			m = name.match(/^([^ ]+)[ ]+([^ ]+)/);
			if (m != null) {
				first_name = m[1];
				last_name = m[2];
				}
			}
console.log("NEW " + last_name + ", " + first_name);
		$.get("/people/new", {
				last_name: last_name,
				first_name: first_name,
				role: "FRIEND"
				},
			function(resp) {
				open_people_dialog(resp.pid);
				}, "json");
		}
	else {
		arm_post_button(ui.item.pid,
			ui.item.value.replace(/^[^A-Z]+/, ""));
		}
	return false;
	}
function init() {
	mark_page("Whiteboard");
	$.get("/whiteboard/dress", {},
			function(resp) {
				dress(resp);
				preset_dates();
				}, "json");
	$("#patron-select").autocomplete({
		source: "/manifest/search?allow_new=1",
		minLength: min_search_chars,
		select: select_person
		});
	setInterval(track, 5000);
	}
function logout() {
	$.get("/login/logout", {},
		function(resp) { window.location = "/whiteboard"; },
		"json");
	}
