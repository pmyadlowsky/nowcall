#! /usr/local/bin/gusher -p 3011
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))
(use-modules (ice-9 regex))
(use-modules (ice-9 popen))
(use-modules (srfi srfi-1))

(include "lib.scm")
(include "settings_lib.scm")
(include "email.scm")
(include "acct.scm")

(define env-clock-interval (setting-cache 'clock-interval))
(define env-date-format (setting-cache 'date-format))
(define env-name-order (setting-cache 'name-order))
(define env-time-format (setting-cache 'time-format))
(define env-dz-email-addr (setting-cache 'dz-email-addr))
(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-dz-phone (setting-cache 'dz-phone))
(define env-website (setting-cache 'website))
(define env-facebook-id (setting-cache 'facebook-id))
(define env-street-addr (setting-cache 'street-addr))
(define env-min-search-chars (setting-cache 'min-search-chars))
(define checklist-hot "orangered")
(define checklist-cold "black")
(define env-worker-id
	(request-cache
		(lambda (http-req) (session-get http-req 'person-id))))
(define env-manifest-worker
	; non-admin with manifest privileges
	(request-cache
		(let ([query
					"select who
						from roles
						where (who=~a)
						and (role='MANFST')
						limit 1"])
			(define (manifest-priv)
				(let ([row
							(pg-one-row (sdb) query
								(or (env-person-id) -1))])
					(and row (pg-cell row 'who))))
			(lambda (http-req)
				(and
					(not (admin-user http-req))
					(manifest-priv))))))
(define frame (make-doc 'file "frame.html"))
(define confirm-template
	(make-doc 'file "/vol/profile/reserve_confirm.html"))
(define script (make-doc 'file "reservations.js"))
(define page-body (make-doc 'file "reservations.html"))
(define composite
	(make-doc (list frame script page-body)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "Reservations")
				(cons 'app "reservations")
				(cons 'script (fetch-doc script))
				(cons 'version nowcall-version)
				(cons 'content (fetch-doc page-body))))))
(define (dollars cents)
	(let* ([dollars (quotient cents 100)]
			[cents (abs (remainder cents 100))])
		(if (= cents 0)
			(format #f "$~d" dollars)
			(format #f "$~d.~2,'0d" dollars cents))))
(define (format-date date-obj)
	(time-format date-obj
		(if (string=? (env-date-format) "m/d/yy") "%m/%d/%Y" "%Y-%m-%d")))
(define (product-menu-duple row)
	; for constructing product item menus
	(list
		(cons 'value (pg-cell row 'id))
		(cons 'label
			(format #f "~a - ~a"
				(pg-cell row 'name)
				(dollars (pg-cell row 'cash_price))))))
(define skydive-menu
	; skydive products
	(let ([query
				"select product_item.id as id,
						product_item.name as name,
						cash_price
					from product_item, product_category
					where reservable
					and (product_item.category=product_category.id)
					and ((product_category.internal_name='skydives')
							or product_item.right_seat)
					order by product_item.name"])
		(lambda ()
			(pg-map-rows (pg-exec (sdb) query) product-menu-duple))))
(define (skydive-menu-wrapped res-id jump-id)
	(html-select
		(format #f "res-jump~d" res-id)
		(format #f "onchange=\"select_skydive('~d')\"" res-id)
		jump-id
		(map
			(lambda (duple)
				(cons
					(assq-ref duple 'value)
					(assq-ref duple 'label)))
			(skydive-menu))))
(define default-media
	; label applied to media menu for no-selection option
	(let ([skydive-query
				"select media_included
					from product_item
					where (id=~a)"]
			[general-query
				"select media_included
					from product_item, product_category
					where reservable
					and (product_item.category=product_category.id)
					and (product_category.internal_name='skydives')
					order by product_item.seq
					limit 1"])
		(lambda (skydive)
			(if (pg-cell 
					(if (and skydive (not (null? skydive)))
						(pg-one-row (sdb) skydive-query skydive)
						(pg-one-row (sdb) general-query))
					'media_included) "included media" "no media"))))
(define media-menu
	; media products
	(let ([query
				"select product_item.id as id,
						product_item.name as name,
						cash_price
					from product_item, product_category
					where reservable
					and (product_item.category=product_category.id)
					and (product_category.internal_name='media')
					order by product_item.seq"])
		(lambda (skydive)
			(cons
				(list
					(cons 'value 0)
					(cons 'label (default-media skydive)))
				(pg-map-rows (pg-exec (sdb) query) product-menu-duple)))))
(define media-menu-wrapped
	(let ([skydive
				"select skydive
					from reservations
					where (id=~a)"])
		(define (get-skydive res-id)
			(pg-cell (pg-one-row (sdb) skydive res-id) 'skydive))
		(lambda (res-id media-id)
			(html-select
				(format #f "res-media~d" res-id)
				(format #f "onchange=\"select_media('~d')\"" res-id)
				media-id
				(map
					(lambda (duple)
						(cons
							(assq-ref duple 'value)
							(assq-ref duple 'label)))
					(media-menu (get-skydive res-id)))))))
(define parse-time
	; parse class time-slot expression to standard 24-hour hh:mm
	(let ([int-pat (make-regexp "^([0-9]+)$")]
			[intap-pat (make-regexp "^([0-9]+)([ap])")]
			[hm-pat (make-regexp "^([0-9]+):([0-9][0-9])$")]
			[hmap-pat (make-regexp "^([0-9]+):([0-9][0-9])([ap])")])
		(define (pat-test src pat handler)
			(let ([match (regexp-exec pat src)])
				(and match (handler match))))
		(define (hhmm hour-str minute meridian)
			(let ([hour (to-i hour-str)])
				(format #f "~d:~2,'0d"
					(cond
						[(= hour 12) 12]
						[(< hour 7) (+ hour 12)]
						[else
							(if (string=? meridian "p")
								(if (= hour 12) 12 (+ 12 hour))
								(if (= hour 12) 0 hour))])
					(to-i minute))))
		(lambda (time-slot)
			(or
				(pat-test time-slot hmap-pat
					(lambda (match)
						(hhmm (match:substring match 1)
							(match:substring match 2)
							(match:substring match 3))))
				(pat-test time-slot hm-pat
					(lambda (match)
						(hhmm (match:substring match 1)
							(match:substring match 2) "")))
				(pat-test time-slot intap-pat
					(lambda (match)
						(hhmm (match:substring match 1) 0
								(match:substring match 2))))
				(pat-test time-slot int-pat
					(lambda (match)
						(hhmm (match:substring match 1) 0 "")))))))
(define add-reservation-group
	(let ([query
				"insert into reservation_groups
						(jump_date, class_slot, created, updated)
					values (~a, ~a, ~a, ~a);
				select currval('reservations_seq') as id;"])
		(lambda (jump-date class-slot)
			(let ([now (time-now)])
				(pg-cell
					(pg-one-row (sdb) query jump-date
						class-slot now now) 'id)))))
(define set-group-contact
	(let ([query
				"update reservation_groups set
					contact=~a
					where (id=~a)"])
		(lambda (group-id reservation-id)
			(pg-exec (sdb) query reservation-id group-id))))
(define new-reservation
	(let ([query
				"insert into reservations
					(name, email, phone, skydive, media,
						person_id, created, updated,
						is_returning, group_id, discount,
						chk_age, chk_weight, chk_deposit,
						chk_waiver_fee, chk_audio_consent)
					values (~a, ~a, ~a, ~a, ~a,
								~a, ~a, ~a,
								~a, ~a, ~a,
								~a, ~a, ~a,
								~a, ~a);
				insert into reservations_log
					(res_id, stamp, staff, remark, paid)
					values (
						(select currval('reservations_seq')),
						~a, ~a, 'initial entry', ~a);
				select currval('reservations_seq') as id;"])
		(define (new-id name email phone skydive media
					student-id staff-id group-id discount paid
					chk-age chk-weight chk-deposit
					chk-waiver-fee chk-audio-consent)
			(let* ([now (time-now)]
					[row (pg-one-row (sdb) query
							; reservation
							name email phone skydive
							(if (= media 0) '() media)
							student-id now now
							(not (null? student-id))
							group-id discount
							chk-age chk-weight chk-deposit
							chk-waiver-fee chk-audio-consent
							; log entry
							now staff-id paid)])
				(and row (pg-cell row 'id))))
		(lambda (name email phone skydive media
					paid date slot student-id staff-id discount
					chk-age chk-weight chk-deposit
					chk-waiver-fee chk-audio-consent)
			(let ([now (time-now)]
					[group-id (add-reservation-group date slot)])
				(set-group-contact group-id
					(new-id name email phone skydive media
						student-id staff-id group-id
						discount paid
						chk-age chk-weight chk-deposit
						chk-waiver-fee chk-audio-consent))))))
(define valid-email
	(let ([pat (make-regexp "^.+@.+$")])
		(lambda (email)
			(and (not (string=? email "")) (regexp-exec pat email)))))
(define valid-phone
	(let ([pat (make-regexp "^[0-9\\-]+")])
		(lambda (phone)
			(and (not (string=? phone "")) (regexp-exec pat phone)))))
(define valid-date
	(let ([pat (make-regexp "^[0-9]+[-/][0-9]+[-/][0-9]+$")])
		(lambda (date) (regexp-exec pat date))))
(define (validate name email phone date)
	; validate data entry inputs
	; produce an error message if there are errors
	(let ([errors
				(filter identity
					(list
						(if (string=? name "") "customer's name" #f)
						(if (or (valid-email email) (valid-phone phone))
							#f
							"email address and/or phone number")
						(if (valid-date date) #f "jump date")))])
		(if (null? errors) (cons #t "")
			(cons #f
				(format #f "Please correct: ~a"
					(html-ul errors))))))
(define select-contact
	; select group contact from its list of reservees
	; (first reservee created)
	(let ([query
				"select id
					from reservations
					where (group_id=~a)
					order by created asc
					limit 1"]
			[update
				"update reservation_groups set
					contact=~a
					where (id=~a)"])
		(lambda (group-id)
			(let ([row (pg-one-row (sdb) query group-id)])
				(and row
					(pg-exec (sdb) update (pg-cell row 'id) group-id))))))
(define is-group
	(let ([query
				"select id from reservation_groups
					where (id=~a)"])
		(lambda (res-id) (pg-one-row (sdb) query res-id))))
(define group-id
	(let ([query
				"select coalesce(group_id,0) as gid
					from reservations
					where (id=~a)"])
		(lambda (reservation-id)
			(if (is-group reservation-id) reservation-id
				(let ([row (pg-one-row (sdb) query reservation-id)])
					(if row (pg-cell row 'gid) 0))))))
(define contact-id
	(let ([query
				"select contact
					from reservation_groups
					where (id=~a)"])
		(lambda (group-id)
			(pg-cell
				(pg-one-row (sdb) query group-id) 'contact))))
(define (is-contact reservation-id group-id)
	(let ([contact (contact-id group-id)])
		(and contact (= contact reservation-id))))
(define get-group-id
	(let ([query
				"select group_id from reservations where id=~a"])
		(lambda (reservation-id)
			(let ([row (pg-one-row (sdb) query reservation-id)])
				(if row (pg-cell row 'group_id) 0)))))
(define cancel-reservee
	; cancel reservee from reservation group
	; if reservee is only member, cancel entire group
	; if reservee was contact person, select new contact
	; return #t if group was canceled, #f otherwise
	(let ([delete
				"update whiteboard set
					reservation_id=null
					where (reservation_id=~a);
				delete from reservations_log where (res_id=~a);
				delete from reservations where (id=~a);"]
			[res-count
				"select count(*) as n
					from reservations
					where (group_id=~a)"]
			[mop-up
				"delete from reservations_log where (res_id=~a);
				delete from reservation_groups where (id=~a)"])
		(define (only-reservee group-id)
			(< (pg-cell (pg-one-row (sdb) res-count group-id) 'n) 2))
		(define (delete-reservee reservation-id)
			(pg-exec (sdb) delete reservation-id
				reservation-id reservation-id))
		(lambda (reservation-id)
			(let ([group-id (get-group-id reservation-id)])
				(if (only-reservee group-id)
					(begin
						(delete-reservee reservation-id)
						(pg-exec (sdb) mop-up group-id group-id) #t)
					(let ([new-contact
								(is-contact reservation-id group-id)])
						(delete-reservee reservation-id)
						(and new-contact
							(select-contact group-id)) #f))))))
(define cancel-reservation-group
	(let ([reservees
				"select id
					from reservations
					where (group_id=~a)"]
			[mop-up
				"delete from reservations_log where (res_id=~a);
				delete from reservation_groups where (id=~a)"])
		(lambda (group-id)
			(pg-each-row
				(pg-exec (sdb) reservees group-id)
				(lambda (row) (cancel-reservee (pg-cell row 'id))))
			(pg-exec (sdb) mop-up group-id group-id))))
(define get-cancellation-detail
	; for confirming group cancellation
	(let ([query
				"select name, jump_date
					from reservations, reservation_groups
					where (reservation_groups.id=~a)
					and (reservations.id=reservation_groups.contact)"])
		(lambda (group-id)
			(pg-one-row (sdb) query group-id))))
(define update-reservation
	(let ([query
				"update reservations set
					name=~a,
					email=~a,
					phone=~a,
					skydive=~a,
					media=~a,
					person_id=coalesce(person_id,~a),
					is_returning=(is_returning or ~a),
					updated=~a,
					discount=~a
					where (id=~a)"])
		(lambda (res-id name email phone skydive media
					person-id discount)
			(pg-exec (sdb) query
				name email phone skydive
				(if (= media 0) '() media)
				person-id
				(not (null? person-id))
				(time-now) discount res-id))))
(define on-whiteboard?
	; this reservation already on whiteboard?
	(let ([query
				"select id
					from whiteboard
					where (reservation_id=~a)"])
		(lambda (res-id) (pg-one-row (sdb) query res-id))))
(define group-on-whiteboard?
	(let ([query
				"select count(*) as n
					from reservations, whiteboard
					where (reservations.group_id=~a)
					and (whiteboard.reservation_id=reservations.id)"])
		(lambda (group-id)
			(let ([row (pg-one-row (sdb) query group-id)])
				(and row (> (pg-cell row 'n) 0))))))
(define total-prepayment
	(let ([log-query
				"select coalesce(sum(paid), 0) as total
					from reservations_log
					where (res_id=~a)"]
			[res-query
				"select paid
					from reservations
					where (id=~a)"])
		(lambda (reservation-id)
			(let ([row (pg-one-row (sdb) res-query reservation-id)])
				(+ (if row (pg-cell row 'paid) 0) ; backward compat
					(pg-cell
						(pg-one-row (sdb) log-query
							reservation-id) 'total))))))
(define reservation-due
	(let ([item-query
				"select cash_price
					from product_item
					where (id=~a)"]
			[products-query
				"select skydive, media, discount
					from reservations
					where (id=~a)"])
		(define (item-price item-id)
			(if (null? item-id) 0
				(let ([row (pg-one-row (sdb) item-query item-id)])
					(if row (pg-cell row 'cash_price) 0))))
		(lambda (reservation-id)
			(let ([row (pg-one-row (sdb) products-query reservation-id)])
				(if row
					(cons
						(total-prepayment reservation-id)
						(+ (item-price (pg-cell row 'skydive))
							(item-price (pg-cell row 'media))
							(- (pg-cell row 'discount)))) '(0 . 0))))))
(define checklist
	(let ([html
				(deflate "<div id=\"checklist[[RID]]\" class=\"checklist\">
					<div><input type=\"checkbox\"[[AGE_CHECKED]]
						onclick=\"update_checklist(this,[[RID]],'age')\"
						id=\"chk-age[[RID]]\"/>18 or older?</div>
					<div><input type=\"checkbox\"[[WEIGHT_CHECKED]]
						onclick=\"update_owl(this,[[RID]])\"
						id=\"chk-weight[[RID]]\"/>OWL?</div>
					<div><input type=\"checkbox\"[[DEPOSIT_CHECKED]]
						onclick=\"update_checklist(this,[[RID]],'deposit')\"
						id=\"chk-deposit[[RID]]\"/>explain payment/fee policy</div>
					<div><input type=\"checkbox\"[[WAIVER_FEE_CHECKED]]
						onclick=\"update_checklist(this,[[RID]],'waiver')\"
						id=\"chk-waiver-fee[[RID]]\"/>$5 waiver fee</div>
					<div><input type=\"checkbox\"[[AUDIO_CONSENT_CHECKED]]
						onclick=\"update_checklist(this,[[RID]],'audio')\"
						id=\"chk-audio-consent[[RID]]\"/>audio agreement</div>
					</div>")])
		(define (checked checklist-item)
			(if checklist-item " checked=\"checked\"" ""))
		(lambda (reservation-id checked-items)
			(fill-template html #f
				(cons 'rid reservation-id)
				(cons 'age_checked
					(checked (pg-cell checked-items 'chk_age)))
				(cons 'weight_checked
					(checked (pg-cell checked-items 'chk_weight)))
				(cons 'deposit_checked
					(checked (pg-cell checked-items 'chk_deposit)))
				(cons 'waiver_fee_checked
					(checked (pg-cell checked-items 'chk_waiver_fee)))
				(cons 'audio_consent_checked
					(checked
						(pg-cell checked-items 'chk_audio_consent)))))))
(define checklist-items
	(let ([query
				"select chk_age, chk_weight, chk_deposit,
						chk_waiver_fee, chk_audio_consent
					from reservations
					where (id=~a)"])
		(lambda (reservation-id)
			(pg-one-row (sdb) query reservation-id))))
(define (checklist-done? reservation-id)
	(let ([row (checklist-items reservation-id)])
		(and
			(pg-cell row 'chk_age)
			(pg-cell row 'chk_deposit)
			(pg-cell row 'chk_waiver_fee)
			(pg-cell row 'chk_audio_consent))))
(define (OWL? reservation-id)
	(let ([row (checklist-items reservation-id)])
		(pg-cell row 'chk_weight)))
(define (checklist-btn-title pending)
	(format #f "~a (toggle open/close)"
		(if pending "items pending" "items complete")))
(define reservee-detail
	(let ([html
				(deflate "<div style=\"background-color: [[COLOR]]\">
				<table style=\"width: 100%\">
				<tr>
				<td class=\"rjust\"><a title=\"Returning? Search by name...\"
					href=\"#\"
					onclick=\"return find_by_name('[[RID]]')\">Name</a>:</td>
				<td style=\"width: 15em\"><input type=\"text\"
					id=\"res-name[[RID]]\"[[NO_EDIT]]
					title=\"[[EDIT_HINT]]\"
					onkeypress=\"arm_update(this)\"
					onchange=\"update_res('[[RID]]')\"
					onfocus=\"live_name_search(this)\"
					value=\"[[NAME]]\"/></td>
				<td class=\"rjust\" style=\"width: 3em\">for a</td>
				<td>[[SKYDIVE_MENU]]</td>
				<td rowspan=\"3\">
				<input type=\"button\" class=\"form-btn\"
					value=\"Confirm\"[[DISABLE_CONFIRM]]
					id=\"confirm[[RID]]\"
					onclick=\"send_confirm('[[RID]]',[[GROUP_CONFIRM]])\"
					title=\"send confirmation email\"/>
				<input type=\"button\" class=\"form-btn\"
					onclick=\"people_detail('[[RID]]',false)\"
					title=\"open/create personal detail record\"
					value=\"Detail\"/>
				<input type=\"button\" class=\"form-btn\"
					id=\"wbd[[RID]]\"
					onclick=\"people_detail('[[RID]]',true)\"
					title=\"post this student to the whiteboard\"[[DISABLE_CHECKIN]]
					value=\"Whiteboard\"/>
				<br/><input type=\"button\" class=\"form-btn\"
					id=\"contact[[RID]]\"
					title=\"make this person the group contact\"
					value=\"Contact\"[[READ_ONLY]]
					onclick=\"make_contact('[[RID]]')\"/>
				<input type=\"button\" class=\"form-btn\"
					title=\"drop from this reservation\"
					value=\"Delete\"[[READ_ONLY]]
					onclick=\"delete_res('[[RID]]')\"/>
				<input type=\"button\" class=\"form-btn\"
					title=\"place this person on-hold\"
					value=\"Split\"[[READ_ONLY]][[DISABLE_SPLIT]]
					onclick=\"split_res('[[RID]]')\"/>
				<br/><input type=\"button\" class=\"form-btn\"
					id=\"checklist-btn[[RID]]\"[[CHECKLIST_STYLE]]
					title=\"[[CHECKLIST_BTN_TITLE]]\"
					value=\"Checklist\"[[READ_ONLY]]
					onclick=\"toggle_checklist('[[RID]]')\"/>
					[[CHECKLIST]]
				<br/>
				<span id=\"is-returning-slot[[RID]]\"
					style=\"font-weight: bold; color: green;
						display: [[RETURN_DISPLAY]]\">
				<input type=\"checkbox\" id=\"is-returning[[RID]]\"
					checked=\"checked\"[[READ_ONLY]]
					onclick=\"verify_dereturn('[[RID]]',this)\"/><abbr
			title=\"this is a returning student\">returning</abbr></span>
				</td>
				</tr>
				<tr><td class=\"rjust\"><a href=\"#\" title=\"Returning? Search by email...\" onclick=\"return find_by_email('[[RID]]')\">Email</a>:</td>
				<td><input type=\"text\" id=\"res-email[[RID]]\"
					onkeypress=\"arm_update(this)\"[[NO_EDIT]]
					title=\"[[EDIT_HINT]]\"
					onchange=\"update_res('[[RID]]')\"
					onfocus=\"live_email_search(this)\"
					value=\"[[EMAIL]]\"/></td>
				<td class=\"rjust\">with</td>
				<td>[[MEDIA_MENU]]</td>
				</tr>
				<tr>
				<tr><td class=\"rjust\"><a href=\"#\" title=\"Returning? Search by phone...\" onclick=\"return find_by_phone('[[RID]]')\">Phone</a>:</td>
				<td><input type=\"text\" id=\"res-phone[[RID]]\"
					onkeypress=\"arm_update(this)\"[[NO_EDIT]]
					title=\"[[EDIT_HINT]]\"
					onchange=\"update_res('[[RID]]')\"
					onfocus=\"live_phone_search(this)\"
					value=\"[[PHONE]]\"/></td>
				<td class=\"rjust\"><abbr
					title=\"pre-paid total to date\">paid</abbr></td>
				<td><span id=\"res-paid[[RID]]\">[[PAID]]</span>
				of <span id=\"res-total[[RID]]\">[[ITEM_TOTAL]]</span>;
				<abbr title=\"negative amount implies fee; e.g. no-show\">discount</abbr>
				<input type=\"text\" id=\"res-discount[[RID]]\"
					class=\"paid-box-edit\"
					onkeypress=\"arm_update(this)\"
					onchange=\"update_res('[[RID]]')\"
					value=\"[[DISCOUNT]]\"/>
				</td>
				</tr>
				</table>
				<div id=\"logrow[[RID]]\"
					class=\"log-block\">[[LOG]]</div>
				<div id=\"pplrow[[RID]]\"></div>
				</div>")]
			[reservee-count-query
				"select count(*) as n
					from reservations
					where (group_id=~a)"]
			[query
				"select name, email, phone, skydive, media,
						person_id, is_returning, discount
					from reservations
					where (reservations.id=~a)"])
		(define (group-of-one? group-id)
			(let ([row (pg-one-row (sdb) reservee-count-query group-id)])
				(< (pg-cell row 'n) 2)))
		(define (editable? read-only data-row)
			(and
				(not read-only)
				(null? (pg-cell data-row 'person_id))))
		(lambda (reservation-id group-id email
					divider background-color read-only)
			(let ([row (pg-one-row (sdb) query reservation-id)]
					[checklist-done (checklist-done? reservation-id)])
				(fill-template html #f
					(cons 'color background-color)
					(cons 'rid reservation-id)
					(cons 'return_display
						(if (pg-cell row 'is_returning) "inline" "none"))
					(cons 'edit_hint
						(if (editable? read-only row)
							"" "edit in personal detail panel"))
					(cons 'no_edit
						(if (editable? read-only row)
							"" " disabled=\"disabled\""))
					(cons 'read_only
						(if read-only " disabled=\"disabled\"" ""))
					(cons 'checklist
						(checklist reservation-id
							(checklist-items reservation-id)))
					(cons 'checklist_style
						(if checklist-done ""
							(format #f " style=\"color: ~a\""
								checklist-hot)))
					(cons 'checklist_btn_title
						(checklist-btn-title (not checklist-done)))
					(cons 'name (pg-cell row 'name))
					(cons 'email (pg-cell row 'email))
					(cons 'phone (pg-cell row 'phone))
					(cons 'paid
						(dollars (total-prepayment reservation-id)))
					(cons 'discount (dollars (pg-cell row 'discount)))
					(cons 'disable_checkin
						(if (on-whiteboard? reservation-id)
							" disabled=\"disabled\"" ""))
					(cons 'disable_split
						(if (group-of-one? group-id)
							" disabled=\"disabled\"" ""))
					(cons 'disable_confirm
						(if (or read-only (null? email))
							" disabled=\"disabled\"" ""))
					(cons 'group_confirm
						(or
							(and
								(is-contact reservation-id group-id)
								(> (group-size group-id) 1)
								"true") "false"))
					(cons 'log (res-log reservation-id))
					(cons 'item_total
						(dollars
							(cdr (reservation-due reservation-id))))
					(cons 'skydive_menu
						(skydive-menu-wrapped reservation-id
							(to-i (pg-cell row 'skydive))))
					(cons 'media_menu
						(media-menu-wrapped reservation-id
							(to-i (pg-cell row 'media)))))))))
(define group-detail
	(let ([reservees
				"select id, email
					from reservations
					where (group_id=~a)
					order by created asc"]
			[logrow
				(deflate "<div id=\"logrow[[RID]]\"
					class=\"log-block\">[[LOG]]</div>")]
			[divider #f]
			[tint #f])
		(define (reservee-block group-id row read-only)
			(let ([out
					(reservee-detail
						(pg-cell row 'id)
						group-id
						(pg-cell row 'email)
						divider
						(if tint "#ddddff" "#bbbbcc") read-only)])
				(set! divider #t)
				(set! tint (not tint))
				out))
		(define (log-row group-id)
			(fill-template logrow #f
				(cons 'rid group-id)
				(cons 'log (res-log group-id))))
		(lambda (group-id read-only)
			(set! divider #f)
			(set! tint #f)
			(string-cat "\n"
				(log-row group-id)
				(pg-map-rows
					(pg-exec (sdb) reservees group-id)
					(lambda (row)
						(reservee-block group-id row read-only)))))))
(define contacts-list
	(let ([email-href
				(deflate "<a href=\"mailto:~a?subject=Your skydive on ~a\"
					onclick=\"stop_prop(event)\">
					~a</a>")])
		(define (email-link addr date)
			(if (null? addr) '()
				(format #f email-href addr
					(if date (time-format date "%A %B %d") "???") addr)))
		(lambda (email-addr event-date phone-num)
			(string-cat ", "
				(filter (lambda (item) (not (null? item)))
					(list
						(email-link email-addr event-date) phone-num))))))
(define product-list
	(let ([item-query "select name from product_item where (id=~a)"])
		(define (product-item pid)
			(if (null? pid) #f
				(let ([row (pg-one-row (sdb) item-query pid)])
					(or (and row (pg-cell row 'name))
						(format #f "#~d" pid)))))
		(lambda (skydive media)
			(string-cat ", "
				(filter identity
					(list
						(product-item skydive)
						(product-item media)))))))
(define group-due
	(let ([res-query
				"select id
					from reservations
					where (group_id=~a)"])
		(define (accum pair sums)
			(cons (+ (car pair) (car sums)) (+ (cdr pair) (cdr sums))))
		(lambda (group-id)
			(fold accum '(0 . 0)
				(pg-map-rows
					(pg-exec (sdb) res-query group-id)
					(lambda (row)
						(reservation-due (pg-cell row 'id))))))))
(define has-notes
	; reservation group or any of its constituents include notes
	(let ([group-query
				(deflate "select count(*) as n
					from reservations_log
					where (res_id=~a)")]
			[res
				(deflate "select count(*) as n
					from reservations_log, reservations
					where (res_id=reservations.id)
					and (reservations.group_id=~a)")])
		(define (group-notes group-id)
			(> (pg-cell (pg-one-row (sdb) group-query group-id) 'n) 0))
		(define (reservation-notes group-id)
			(> (pg-cell (pg-one-row (sdb) res group-id) 'n) 0))
		(lambda (group-id)
			(and
				group-id
				(not (null? group-id))
				(or (group-notes group-id)
					(reservation-notes group-id))))))
(define group-contact
	(let ([query
				"select name, email, phone, jump_date, group_id
					from reservations, reservation_groups
					where (reservations.id=~a)
					and (group_id=reservation_groups.id)"]
			[notes-icon
				(deflate "<img src=\"/img/notepad-16.png\"
					title=\"has notes\" alt=\"note\"/>")]
			[owl-icon
				(deflate "<img src=\"/img/owl-icon.png\"
					title=\"over weight limit\" alt=\"checklist\"/>")]
			[ids-query
				"select reservations.id as id
					from reservations, reservation_groups
					where (reservation_groups.contact=~a)
					and (reservations.group_id=reservation_groups.id)"])
		(define (any-owls? contact-id)
			(let scan ([ids
						(pg-map-rows
							(pg-exec (sdb) ids-query contact-id)
							(lambda (row) (pg-cell row 'id)))])
				(cond
					[(null? ids) #f]
					[(OWL? (car ids)) #t]
					[else (scan (cdr ids))])))
		(lambda (contact-id)
			(if contact-id
				(let ([row (pg-one-row (sdb) query contact-id)])
					(format #f "~a~a~a, ~a"
						(if (has-notes (pg-cell row 'group_id))
							notes-icon "")
						(if (any-owls? contact-id) owl-icon "")
						(pg-cell row 'name)
						(contacts-list
							(pg-cell row 'email)
							(pg-cell row 'jump_date)
							(pg-cell row 'phone))))
				"MISSING CONTACT ID"))))
(define group-media-count
	(let ([media
				"select count(*) as n
					from reservations
					where (group_id=~a)
					and (media is not null)"]
			[skydive
				"select count(*) as n
					from reservations, product_item
					where (reservations.group_id=~a)
					and (product_item.id=reservations.skydive)
					and product_item.media_included"])
		(lambda (group-id)
			(+ (pg-cell (pg-one-row (sdb) media group-id) 'n)
				(pg-cell (pg-one-row (sdb) skydive group-id) 'n)))))
(define group-size
	(let ([query
				"select count(*) as n
					from reservations
					where (group_id=~a)"])
		(lambda (group-id)
			(pg-cell (pg-one-row (sdb) query group-id) 'n))))
(define (group-due-format due)
	(let ([amt-due (- (cdr due) (car due))])
		(format #f "<span class=\"~a\">~a / ~a</span>"
			(if (> amt-due 0) "amount-due" "amount-paid")
			(dollars amt-due)
			(dollars (cdr due)))))
(define (filter-active-hold select-active select-on-hold)
	; qualify reservations SQL query
	(cond
		[(and select-active select-on-hold) ""]
		[(and select-active (not select-on-hold)) " and (not hold)"]
		[(and (not select-active) select-on-hold) " and hold"]
		[else " and (1=0)"]))
(define chart-markers
	; compute booked and media counts for each
	; reservation date and class slot
	(let ([classes
				"select distinct jump_date, class_slot
					from reservation_groups, reservations
					where (jump_date >= ~a)[[ACTIVE]]
					and (reservations.group_id=reservation_groups.id)
					and ((class_slot >= ~a) or (jump_date > ~a))[[NAME]]
					order by jump_date asc, class_slot asc"]
			; for date in asc order change:
			;    "jump_date desc" -> "jump_date asc"
			;    "jump_date <= ~a" -> "jump_date >= ~a"
			;    "jump_date < ~a" -> "jump_date > ~a"
			[groups
				"select id
					from reservation_groups
					where (jump_date=~a)
					and (not hold)
					and (class_slot=~a)"])
		(define (group-counts group)
			(let ([id (pg-cell group 'id)])
				(cons
					(group-size id)
					(group-media-count id))))
		(define (accum pair sums)
			(cons (+ (car pair) (car sums)) (+ (cdr pair) (cdr sums))))
		(define (class-counts class)
			(fold accum '(0 . 0)
				(pg-map-rows
					(pg-exec (sdb) groups
						(pg-cell class 'jump_date)
						(pg-cell class 'class_slot))
					(lambda (group) (group-counts group)))))
		(lambda (start-date start-class name-clause
					select-active select-on-hold)
			(pg-map-rows
				(pg-exec (sdb)
					(fill-template classes #f
						(cons 'name name-clause)
						(cons 'active
							(filter-active-hold
								select-active select-on-hold)))
					start-date
					start-class start-date)
				(lambda (class)
					(list
						(time-format (pg-cell class 'jump_date) "%Y-%m-%d")
						(pg-cell class 'class_slot)
						(class-counts class)))))))
(define name-search
	(let ([text-search
				(deflate "and (to_tsvector(reservations.name) @@
					to_tsquery('~a'))")])
		(lambda (name-key)
			(if (string=? name-key "") ""
				(format #f text-search
					(string-cat " & " (string-tokenize name-key)))))))
(define group-signature
	(let ([query
				"select product_item.id as id
					from reservations, product_item
					where (reservations.group_id=~a)
					and (product_item.id=reservations.skydive)"]
			[tag-query
				"select tag
					from product_tags
					where (product=~a)"])
		(define (product-tags product-id)
			(pg-map-rows
				(pg-exec (sdb) tag-query product-id)
				(lambda (row) (string->symbol (pg-cell row 'tag)))))
		(define (jump-symbol row)
			(let scan ([tags (product-tags (pg-cell row 'id))])
				(cond
					[(null? tags) "?"]
					[(eq? (car tags) 'aff) "A"]
					[(eq? (car tags) 'tandem) "T"]
					[(eq? (car tags) 'recurrency) "R"]
					[(eq? (car tags) 'observer) "O"]
					[else (scan (cdr tags))])))
		(define (sort-count items)
			(let scan ([sorted
						(sort-list
							items
							(lambda (a b) (string<? a b)))]
					[count 0]
					[bag '("")])
				(cond
					[(null? sorted) (cddr (reverse(cons count bag)))]
					[(string=? (car sorted) (car bag))
						(scan (cdr sorted) (1+ count) bag)]
					[else
						(scan (cdr sorted) 1
							(cons (car sorted) (cons count bag)))])))
		(define (jump-sig counts)
			(let collect ([pairs counts]
					[bag ""])
				(cond
					[(null? pairs) (string-trim-both bag)]
					[(= (cadr pairs) 1)
						(collect (cddr pairs)
							(format #f "~a~a " bag (car pairs)))]
					[else
						(collect (cddr pairs)
							(format #f "~a~d~a "
								bag (cadr pairs) (car pairs)))])))
		(lambda (group-id)
			(jump-sig
				(sort-count
					(pg-map-rows (pg-exec (sdb) query group-id)
						jump-symbol))))))
(define class-slots
	(let ([first 420] ; 0700
			[last 1170]) ; 1930
		(define (minutes-list start step)
			(let walk ([bag (list first)])
				(if (>= (car bag) last) (reverse bag)
					(walk (cons (+ (car bag) step) bag)))))
		(lambda ()
			(map
				(lambda (minute)
					(let ([hhmmss
								(format #f "~2,'0d:~2,'0d:00"
									(quotient minute 60)
									(remainder minute 60))])
						(list
							(cons 'value hhmmss)
							(cons 'label
								(wall-clock-time hhmmss
									(env-time-format))))))
				(minutes-list first (env-clock-interval))))))
(define class-slot-menu
	(let ([menu-item "<option value=\"~a\"~a>~a</option>"])
		(lambda (preset)
			(string-cat "\n"
				(map
					(lambda (slot)
						(format #f menu-item
							(assq-ref slot 'value)
							(if (string=? (assq-ref slot 'value) preset)
								" selected=\"selected\"" "")
							(assq-ref slot 'label)))
					(class-slots))))))
(define tabulate-reservations
	; generate chart of reservation groups
	(let ([query
				(deflate "select distinct reservation_groups.id as id,
						class_slot, jump_date, contact, hold,
						reservation_groups.created
					from reservation_groups, reservations
					where (reservations.group_id=reservation_groups.id)~a~a~a~a
					order by jump_date asc, class_slot asc,
					reservation_groups.created asc")]
			[no-reservations
				(deflate "<div style=\"font-size: 115%;
					font-weight: bold\">No reservations found as filtered.</div>")]
			[row-form
				(deflate "
				<tr onmouseover=\"hilite(this)\"
					onmouseout=\"lolite(this,'[[BACKGROUND]]')\"
					style=\"background-color: [[BACKGROUND]]\">
				<td class=\"res-cell\"><input type=\"text\"
						id=\"ch-date[[RES_ID]]\"
						title=\"click to change reservation date\"
						onchange=\"date_change(this, '[[RES_ID]]')\"
						onfocus=\"popcal(this,'[[DATE_FMT]]')\"
						style=\"width: 7em\"
						value=\"[[DATE]]\"[[DISABLED]]/></td>
				<td class=\"res-cell\"><select[[DISABLED]]
						id=\"ch-slot[[RES_ID]]\"
						style=\"text-align: right\"
						onchange=\"slot_change(this, '[[RES_ID]]')\"
						title=\"select to change arrival time\"
						>[[SLOT_MENU]]</select></td>
				<td id=\"ch-count[[RES_ID]]\"
					onclick=\"group_detail('[[RES_ID]]')\"
					class=\"res-cell rjust\">[[GROUP_SIZE]]</td>
				<td id=\"ch-media[[RES_ID]]\"
					onclick=\"group_detail('[[RES_ID]]')\"
					class=\"res-cell rjust\">[[MEDIA]]</td>
				<td id=\"ch-contact[[RES_ID]]\"
					onclick=\"group_detail('[[RES_ID]]')\"
					class=\"res-cell\">[[CONTACT]]</td>
				<td id=\"ch-due[[RES_ID]]\"
					onclick=\"group_detail('[[RES_ID]]')\"
					title=\"due / total\"
					class=\"res-cell rjust\">[[DUE]]</td>
				<td class=\"res-cell cancel\">
					<input type=\"checkbox\"
						title=\"mark for batch delete\"
						id=\"batchdel[[RES_ID]]\"
						onclick=\"mark_for_delete(this,'[[RES_ID]]')\"/>
					<img onclick=\"can_res('[[RES_ID]]')\"
					title=\"cancel entire reservation\"
					src=\"/img/delete-16.png\"/></td>
				</tr>
				<tr id=\"drow[[RES_ID]]\" style=\"display: none\">
				<td id=\"dcell[[RES_ID]]\" colspan=\"7\"
					class=\"res-cell\"></td></tr>")]
			[marker-html
				(deflate "<tr><td class=\"hdr5\">~a</td>
				<td class=\"hdr5\">~a</td>
				<td class=\"marker-6\">Booked ~d</td>
				<td class=\"marker-5\">Media ~d</td>
				<td class=\"hdrl\">Contact</td>
				<td class=\"hdr8\">Due</td><td></td>
				</tr>")])
		(define (day-and-date row)
			(time-format (pg-cell row 'jump_date) "%a %m/%d"))
		(define (marker-row row date-change counts)
			(format #f marker-html
				(if date-change (day-and-date row) "")
				(if date-change "Time" "")
				(car counts) (cdr counts)))
		(define (section-header-obj name-key select-active select-on-hold)
			(let ([markers #f]
					[name-clause (name-search name-key)])
				(lambda (row date-change)
					(unless markers
						(set! markers
							(chart-markers 
								(pg-cell row 'jump_date)
								(pg-cell row 'class_slot)
								name-clause
								select-active select-on-hold)))
					(let ([tuple (if (null? markers) '() (car markers))])
						(cond
							[(null? markers) ""]
							[(not (marker-match row tuple)) ""]
							[else
								(set! markers (cdr markers))
								(marker-row row
									date-change (caddr tuple))])))))
		(define (product-item pid)
			(if (null? pid) #f
				(let ([row (pg-one-row (sdb) item-query pid)])
					(or (and row (pg-cell row 'name))
						(format #f "#~d" pid)))))
		(define (make-row row read-only)
			(let ([grp-id (pg-cell row 'id)])
				(fill-template row-form #f
					(cons 'background
						(if (pg-cell row 'hold) "gold" "white"))
					(cons 'res_id grp-id)
					(cons 'disabled
						(if read-only " disabled=\"disabled\"" ""))
					(cons 'date_fmt (env-date-format))
					(cons 'date
						(format-date (pg-cell row 'jump_date)))
					(cons 'slot_menu
						(class-slot-menu (pg-cell row 'class_slot)))
					(cons 'group_size (group-signature grp-id))
					(cons 'media (group-media-count grp-id))
					(cons 'due
						(group-due-format (group-due grp-id)))
					(cons 'contact
						(group-contact (pg-cell row 'contact))))))
		(define (make-query from-date to-date name-key
					select-active select-on-hold)
			(format #f query
				(filter-active-hold select-active select-on-hold)
				(if (string=? from-date "") ""
					(format #f " and (jump_date >= '~a')" from-date))
				(if (string=? to-date "") ""
					(format #f " and (jump_date <= '~a')" to-date))
				(name-search name-key)))
		(define (date-string row)
			(time-format (pg-cell row 'jump_date) "%Y-%m-%d"))
		(define (marker-match row marker)
			(and
				(string=? (car marker) (date-string row))
				(string=? (cadr marker)
					(pg-cell row 'class_slot))))
		(define (date-marker prev-date)
			(string-cat "\n"
				(if (string=? prev-date "") "" "</table></div>")
				"<div class=\"reservations-day rounded-corners-10\">"
				"<table style=\"width: 100%\">"))
		(define (block-header-obj)
			(let ([prev-date ""])
				(lambda (res-row)
					(let ([this-date (date-string res-row)]
							[marker (date-marker prev-date)])
						(if (string=? this-date prev-date)
							""
							(begin
								(set! prev-date this-date)
								marker))))))
		(define (headers row block-header section-header)
			(let ([block (block-header row)])
				(list
					block
					(section-header row
						(not (string=? block ""))))))
		(lambda (from-date to-date read-only name-key
					select-active select-on-hold)
			(let ([res
					(pg-exec (sdb)
						(make-query from-date to-date
							name-key select-active select-on-hold))]
					[date-change #f]
					[block-header (block-header-obj)]
					[section-header
						(section-header-obj name-key
							select-active select-on-hold)])
				(if (< (pg-tuples res) 1) no-reservations
					(string-cat "\n"
						(pg-map-rows res
							(lambda (row)
								(string-cat ""
									(headers row
										block-header section-header)
									(make-row row read-only))))))))))
(define (zero-is-null num)
	; regard numerical zero as null for database purposes
	(if (= num 0) '() num))
(define (null-search-result value label)
	(list
		(cons 'pid 0)
		(cons 'new #t)
		(cons 'value value)
		(cons 'label label)))
(define search-email
	; search people DB by email
	(let ([query
				"select id, email,
						coalesce(phone_mobile,phone_home) as phone
					from people
					where (lower(email) like ~a)"])
		(define (result-tuple row)
			(let* ([pid (pg-cell row 'id)]
					[email (pg-cell row 'email)])
				(list
					(cons 'label email)
					(cons 'value email)
					(cons 'new #f)
					(cons 'name (person-name pid (env-name-order) (sdb)))
					(cons 'pid pid)
					(cons 'phone (pg-cell row 'phone))
					(cons 'email (pg-cell row 'email)))))
		(lambda (term)
			(let ([res (pg-exec (sdb) query (format #f "~a%" term))])
				(if (< (pg-tuples res) 1)
					(list (null-search-result term "NEW RECORD"))
					(append
						(pg-map-rows res
							(lambda (row)
								(result-tuple row)))
						(list (null-search-result term "NEW RECORD"))))))))
(define search-phone
	; search people DB by phone number
	(let ([query
				(deflate "select id, email, phone_mobile, phone_home
					from people
					where (
						(regexp_replace(coalesce(phone_mobile,''),
							'[^0-9]', '', 'g') like ~a)
						or
						(regexp_replace(coalesce(phone_home,''),
							'[^0-9]', '', 'g') like ~a)
						)")]
			[non-digits (make-regexp "[^0-9]")])
		(define (digits-only str)
			(regexp-substitute/global #f non-digits str 'pre "" 'post))
		(define (phone-list home mobile)
			(string-cat ", "
				(filter (lambda (item) (not (null? item)))
					(list mobile home))))
		(define (result-tuple row)
			(let* ([pid (pg-cell row 'id)]
					[email (pg-cell row 'email)]
					[phones
						(phone-list
							(pg-cell row 'phone_home)
							(pg-cell row 'phone_mobile))])
				(list
					(cons 'label phones)
					(cons 'value phones)
					(cons 'new #f)
					(cons 'name (person-name pid (env-name-order) (sdb)))
					(cons 'pid pid)
					(cons 'phone phones)
					(cons 'email (pg-cell row 'email)))))
		(lambda (term)
			(let* ([d-term (format #f "~a%" (digits-only term))]
					[res (pg-exec (sdb) query d-term d-term)])
				(if (< (pg-tuples res) 1)
					(list (null-search-result term "NEW RECORD"))
					(append
						(pg-map-rows res
							(lambda (row) (result-tuple row)))
						(list (null-search-result term "NEW RECORD"))))))))
(define send-confirmation-email
	(let ([query-one
				"select email, name, skydive, media,
						reservation_groups.class_slot as class_slot,
						reservation_groups.jump_date as jump_date
					from reservations, reservation_groups
					where (reservations.id=~a)
					and (reservation_groups.id=reservations.group_id)"]
			[query-group
				"select email, name, skydive, media,
						reservation_groups.class_slot as class_slot,
						reservation_groups.jump_date as jump_date
					from reservations, reservation_groups
					where (group_id=~a)
					and (reservation_groups.id=reservations.group_id)
					order by
						(reservations.id != reservation_groups.contact) asc,
						reservations.created asc"]
			[item-query "select name from product_item where id=~a"]
			[student-row-tpt
				(deflate "<tr>
				<td class=\"tcell\">~a</td>
				<td class=\"tcell\">~a</td>
				<td class=\"tcell\">~a</td>
				<td class=\"tcell\">~a</td>
				</tr>")])
		(define (query-set reservation-id get-group)
			(pg-exec (sdb)
				(if get-group query-group query-one)
				(if get-group
					(group-id reservation-id)
					reservation-id)))
		(define (product-name pid)
			(let ([row (pg-one-row (sdb) item-query pid)])
				(if row (pg-cell row 'name) "???")))
		(define (product-items skydive media)
			(string-cat ", "
				(map
					(lambda (product-id) (product-name product-id))
					(filter (lambda (item) (not (null? item)))
						(list skydive media)))))
		(define (student-row row)
			(format #f student-row-tpt
				(pg-cell row 'name)
				(product-items
					(pg-cell row 'skydive)
					(pg-cell row 'media))
				(time-format (pg-cell row 'jump_date) "%b %d, %Y")
				(wall-clock-time (pg-cell row 'class_slot)
					(env-time-format))))
		(define (student-rows res)
			(let* ([email #f]
					[html
						(string-cat "\n"
							(pg-map-rows res
								(lambda (row)
									(unless email
										(set! email (pg-cell row 'email)))
									(student-row row))))])
				(cons email html)))
		(lambda (reservation-id group-email)
			(let* ([res (query-set reservation-id group-email)]
					[students (student-rows res)]
					[sender (env-dz-email-addr)]
					[bcc (bcc-list)])
				(send-email
					sender
					(append
						(list (car students))
						(if (null? bcc) '() (cdr bcc)))
					(fill-template (fetch-doc confirm-template) #f
						(cons 'resid reservation-id)
						(cons 'recipient (car students))
						(cons 'bcc
							(if (null? bcc) ""
								(format #f "\nBcc: ~a" (car bcc))))
						(cons 'dz_name (env-dz-full-name))
						(cons 'sender sender)
						(cons 'phone (env-dz-phone))
						(cons 'website (env-website))
						(cons 'facebook (env-facebook-id))
						(cons 'street_addr (env-street-addr))
						(cons 'student_rows (cdr students))))))))
(define delete-log-entry
	(let ([query "delete from reservations_log where id=~a"])
		(lambda (log-id)
			(pg-exec (sdb) query log-id))))
(define add-log
	(let ([query
				"insert into reservations_log (res_id, stamp,
					staff, remark) values (~a, ~a, ~a, ~a)"])
		(lambda (reservation-id staff-id remark)
			(pg-exec (sdb) query
				reservation-id
				(time-now)
				staff-id
				remark))))
(define on-hold
	(let ([query
				"select hold
					from reservation_groups
					where (id=~a)"])
		(lambda (reservation-id)
			(let ([row (pg-one-row (sdb) query reservation-id)])
				(and row (pg-cell row 'hold))))))
(define res-log
	(let ([query
				"select reservations_log.id as id,
						reservations_log.res_id as res_id,
						reservations_log.stamp as stamp,
						reservations_log.paid as paid,
						(people.first_name || ' '
							|| people.last_name) as name,
						remark
					from reservations_log, people
					where (res_id=~a)
					and (people.id=reservations_log.staff)
					order by reservations_log.stamp asc"]
			[table-header
				(deflate "<tr>
				<td class=\"log-hdr\" style=\"width: 11em\">logged</th>
				<td class=\"log-hdr\" style=\"width: 12em\">by</th>
				<td class=\"log-num-hdr\" style=\"width: 4em\">paid</th>
				<td class=\"log-hdr\">student note</th>
				<td style=\"width: 1em\"></th>
				</tr>")]
			[table-header-group
				(deflate "<tr>
				<td class=\"log-hdr\" style=\"width: 11em\">logged</th>
				<td class=\"log-hdr\" style=\"width: 12em\">by</th>
				<td class=\"log-hdr\">reservation note</th>
				<td style=\"width: 1em\"></th>
				</tr>")]
			[note-link
				(deflate "<img onclick=\"add_note('~d')\"
					title=\"add ~a note\"
					src=\"/img/notepad-16.png\"/>")]
			[new-res-link
				(deflate "<img src=\"/img/addperson-16.png\"
					title=\"add student to this reservation\"
					onclick=\"new_res('~d')\"/>")]
			[hold-link
				(deflate "<img src=\"/img/hold-16.png\"
					title=\"put this reservation on hold\"
					onclick=\"hold_reservation('~d')\"/>")]
			[activate-link
				(deflate "<img src=\"/img/activate-16.png\"
					title=\"activate this reservation\"
					onclick=\"activate_reservation('~d')\"/>")]
			[table-row
				(deflate "<tr>
				<td class=\"log-cell\">~a</td>
				<td class=\"log-cell\">~a</td>
				<td class=\"log-num-cell\"
					title=\"click to edit\"
					onclick=\"liquify_pay(this,'~d')\">~a</td>
				<td class=\"log-cell\"
					title=\"click to edit\"
					onclick=\"liquify_note(this,'~d')\">~a</td>
				<td class=\"log-cell\"
					onclick=\"delete_log('~d','~d')\"
					title=\"delete this log entry\"><img
					src=\"/img/delete-16.png\"/></td>
				</tr>")]
			[table-row-group
				(deflate "<tr>
				<td class=\"log-cell\">~a</td>
				<td class=\"log-cell\">~a</td>
				<td class=\"log-cell\"
					title=\"click to edit\"
					onclick=\"liquify_note(this,'~d')\">~a</td>
				<td class=\"log-cell\"
					onclick=\"delete_log('~d','~d')\"
					title=\"delete this log entry\"><img
					src=\"/img/delete-16.png\"/></td>
				</tr>")]
			[chrome-autofill-bait
				(deflate "<input style=\"display: none\" type=\"text\"
					name=\"chrome_bait1\"/>
					<input style=\"display: none\" type=\"password\"
					name=\"chrome_bait2\"/>")]
			[bang-pat (make-regexp "^ *!")]
			[super-note-wrap "<span class=\"super-note\">~a</span>"])
		(define (hilite? note)
			(if (regexp-exec bang-pat note)
				(format #f super-note-wrap note) note))
		(define (stamp-form)
			(if (string=? (env-time-format) "12")
				"%m/%d/%y %I:%M%p" "%m/%d/%y %H:%M"))
		(define (student-note-row row)
			(let ([log-id (pg-cell row 'id)]
					[paid (pg-cell row 'paid)])
				(format #f table-row
					(time-format (pg-cell row 'stamp) (stamp-form))
					(pg-cell row 'name)
					log-id
					(if (> paid 0) (dollars paid) "")
					log-id
					(hilite? (to-s (pg-cell row 'remark)))
					(pg-cell row 'res_id)
					log-id)))
		(define (group-note-row row)
			(let ([log-id (pg-cell row 'id)])
				(format #f table-row-group
					(time-format (pg-cell row 'stamp) (stamp-form))
					(pg-cell row 'name)
					log-id
					(to-s (pg-cell row 'remark))
					(pg-cell row 'res_id)
					log-id)))
		(lambda (reservation-id)
			(let ([res (pg-exec (sdb) query reservation-id)]
					[group-note (is-group reservation-id)])
				(string-cat "\n"
					(if (< (pg-tuples res) 1) ""
						(string-cat "\n"
							chrome-autofill-bait
							"<table style=\"width: 100%\">"
							(if group-note
								table-header-group
								table-header)
							(pg-map-rows res
								(if group-note
									group-note-row
									student-note-row))
							"</table>"))
					(format #f note-link reservation-id
						(if group-note "reservation" "student"))
					(if group-note
						(format #f new-res-link reservation-id) "")
					(if group-note
						(format #f
							(if (on-hold reservation-id)
								activate-link hold-link)
							reservation-id) ""))))))
(define update-remark
	(let ([query
				"update reservations_log set
					remark=~a
					where (id=~a);
				select res_id
					from reservations_log
					where (id=~a)"])
		(lambda (log-id remark)
			(let ([row (pg-one-row (sdb) query remark log-id log-id)])
				(pg-cell row 'res_id)))))
(define update-payment
	(let ([query
				"update reservations_log set
					paid=~a
					where (id=~a);
				select res_id
					from reservations_log
					where (id=~a)"])
		(lambda (log-id amount)
			(let ([row (pg-one-row (sdb) query amount log-id log-id)])
				(pg-cell row 'res_id)))))
(define chart-row
	; query reservation to refresh it after edit
	(let ([email-query
				"select email
					from reservations
					where (id=~a)"])
		(define (has-email-addr reserve-id)
			(let ([row (pg-one-row (sdb) email-query reserve-id)])
				(and row (not (null? (pg-cell row 'email))))))
		(lambda (reserve-id)
			(let ([gid (group-id reserve-id)])
				(list
					(cons 'id reserve-id)
					(cons 'gid gid)
					(cons 'has_email (has-email-addr reserve-id))
					(cons 'contact
						(group-contact (contact-id gid)))
					(cons 'booked (group-signature gid))
					(cons 'media (group-media-count gid))
					(cons 'due (group-due-format (group-due gid)))
					(cons 'total
						(dollars
							(cdr (reservation-due reserve-id)))))))))
(define change-contact
	; change reservation group contact person
	(let ([query
				(deflate "update reservation_groups set
					contact=~a
					from reservations
					where (reservation_groups.id=reservations.group_id)
					and (reservations.id=~a)")])
		(lambda (reservation-id)
			(pg-exec (sdb) query reservation-id reservation-id))))
(define blank-reservee
	(let ([query "insert into reservations (group_id) values (~a)"])
		(lambda (group-id) (pg-exec (sdb) query group-id))))
(define change-date
	; change reservation date
	(let ([query
				"update reservation_groups set
					jump_date=~a,
					updated=~a
					where (id=~a)"])
		(lambda (reservation-id jump-date)
			(pg-exec (sdb) query jump-date (time-now) reservation-id))))
(define change-slot
	; change reservation arrival time
	(let ([query
				"update reservation_groups set
					class_slot=~a,
					updated=~a
					where (id=~a)"])
		(lambda (reservation-id class-slot)
			(pg-exec (sdb) query class-slot (time-now) reservation-id)
			#t)))
(define reservation-dates
	(let ([query
			"select distinct jump_date as jdate
				from reservation_groups
				where (jump_date >= current_date)~a
				order by jump_date asc"])
		(lambda (select-active select-on-hold)
			(pg-map-rows
				(pg-exec (sdb)
					(format #f query
						(filter-active-hold
							select-active select-on-hold)))
				(lambda (row)
					(let ([jump-date (pg-cell row 'jdate)])
						(list
							(format-date jump-date)
							(format-date jump-date))))))))
(define set-hold
	(let ([query
				"update reservation_groups set
					hold=~a
					where (id=~a)"])
		(lambda (reservation-id state)
			(pg-exec (sdb) query state reservation-id))))
(define on-hold-count
	(let ([query
				"select count(*) as n
					from reservation_groups
					where hold"])
		(lambda () (pg-cell (pg-one-row (sdb) query) 'n))))
(define on-hold-reservations
	(let ([query
				(deflate "select distinct reservation_groups.id as id,
						class_slot, jump_date, contact,
						reservation_groups.created
					from reservation_groups, reservations
					where hold~a~a
					and (reservations.group_id=reservation_groups.id)
					order by jump_date desc, class_slot asc,
					reservation_groups.created asc")]
			[row-form
				(deflate "
				<tr onmouseover=\"hilite(this)\"
					onmouseout=\"lolite(this,'white')\"
					style=\"background-color: white\">
				<td class=\"res-cell hdr5\"><input type=\"text\"
						id=\"ch-date[[RES_ID]]\"
						title=\"click to change reservation date\"
						onchange=\"date_change(this, '[[RES_ID]]')\"
						onfocus=\"popcal(this,'[[DATE_FMT]]')\"
						style=\"width: 7em\"
						value=\"[[DATE]]\"/></td>
				<td class=\"res-cell hdr5\"><select
						id=\"ch-slot[[RES_ID]]\"
						style=\"text-align: right\"
						onchange=\"slot_change(this, '[[RES_ID]]')\"
						title=\"select to change arrival time\"
						>[[SLOT_MENU]]</select></td>
				<td id=\"ch-count[[RES_ID]]\"
					onclick=\"group_detail('[[RES_ID]]')\"
					style=\"width: 6em\"
					class=\"res-cell rjust\">[[GROUP_SIZE]]</td>
				<td id=\"ch-media[[RES_ID]]\"
					onclick=\"group_detail('[[RES_ID]]')\"
					style=\"width: 5em\"
					class=\"res-cell rjust\">[[MEDIA]]</td>
				<td id=\"ch-contact[[RES_ID]]\"
					onclick=\"group_detail('[[RES_ID]]')\"
					class=\"res-cell\">[[CONTACT]]</td>
				<td id=\"ch-due[[RES_ID]]\"
					onclick=\"group_detail('[[RES_ID]]')\"
					title=\"due / total\"
					style=\"width: 8em\"
					class=\"res-cell rjust\">[[DUE]]</td>
				<td class=\"res-cell cancel\">
					<input type=\"checkbox\"
						title=\"mark for batch delete\"
						id=\"batchdel[[RES_ID]]\"
						onclick=\"mark_for_delete(this,'[[RES_ID]]')\"/>
					<img
						onclick=\"can_res('[[RES_ID]]')\"
						title=\"cancel entire reservation\"
						src=\"/img/delete-16.png\"/></td>
				</tr>
				<tr id=\"drow[[RES_ID]]\" style=\"display: none\">
				<td id=\"dcell[[RES_ID]]\" colspan=\"7\"
					class=\"res-cell\"></td></tr>")]
			[block-header
				(deflate "<div class=\"reservations-day
					rounded-corners-10\" style=\"background-color: gold\">
				<div style=\"font-weight: bold; width: 100%; text-align: center; font-size: 115%; margin-bottom: 0.3em\">On Hold</div>
				<table style=\"width: 100%\">")]
			[block-trailer "</table></div>"]
			[no-reservations
				(deflate "<div style=\"font-size: 115%;
					font-weight: bold\">No reservations on hold.</div>")])
		(define (make-query from-date to-date)
			(format #f query
				(if (string=? from-date "") ""
					(format #f " and (jump_date >= '~a')" from-date))
				(if (string=? to-date "") ""
					(format #f " and (jump_date <= '~a')" to-date))))
		(define (make-row row)
			(let ([grp-id (pg-cell row 'id)])
				(fill-template row-form #f
					(cons 'res_id grp-id)
					(cons 'date_fmt (env-date-format))
					(cons 'date
						(format-date (pg-cell row 'jump_date)))
					(cons 'slot_menu
						(class-slot-menu (pg-cell row 'class_slot)))
					(cons 'group_size (group-signature grp-id))
					(cons 'media (group-media-count grp-id))
					(cons 'due
						(group-due-format (group-due grp-id)))
					(cons 'contact
						(group-contact (pg-cell row 'contact))))))
		(lambda (from-date to-date)
			(let ([res (pg-exec (sdb) (make-query from-date to-date))])
				(if (< (pg-tuples res) 1) no-reservations
					(string-cat "\n"
						block-header
						(pg-map-rows res
							(lambda (row)
								(make-row row))) block-trailer))))))
(define de-return
	; disassociate reservation from people record
	; (returning student)
	(let ([query
				"update reservations set
					person_id=NULL,
					is_returning='f'
					where (id=~a)"])
		(lambda (reservation-id)
			(pg-exec (sdb) query reservation-id))))
(define reservee-profile
	; - copy reservee's people ID to reservations.person_id
	; - copy email and phone from reservation to people record,
	;   but don't overwrite non-null values (mainly applies to
	;   new people records)
	; - make a new whiteboard record for this reservation if
	;   such doesn't already exist
	(let ([pid-update
				"update reservations set
					person_id=~a,
					updated=~a
					where (id=~a)
					and (person_id is null)"]
			[status-update
				"update people set
					new='f',
					updated=~a
					where (id=~a)"]
			[email-update
				"update people set
					email=(select email from reservations where id=~a),
					updated=~a
					where (id=~a)
					and (email is null)"]
			[phone-update
				"update people set
					phone_mobile=
						(select phone from reservations where id=~a),
					updated=~a
					where (id=~a)
					and (phone_mobile is null)"])
		(lambda (person-id res-id)
			(let ([stamp (time-now)])
				(pg-exec (sdb) pid-update person-id stamp res-id)
				(pg-exec (sdb) status-update stamp person-id)
				(pg-exec (sdb) email-update res-id stamp person-id)
				(pg-exec (sdb) phone-update res-id stamp person-id)))))
(define reservee-check-in
	(let ([credited-query
				"select acct_xact
					from reservations
					where (id=~a)"]
			[whiteboard-insert
				"insert into whiteboard (reservation_id, student,
						created, updated, skydive, media, seq)
					values (~a, ~a, ~a, ~a,
						(select skydive from reservations
							where (id=~a)),
						(select media from reservations
							where (id=~a)),
						(select (coalesce(max(seq),0) + 1)
							from whiteboard
							where created::date=~a::date))"]
			[set-xact-id
				"update reservations set
					acct_xact=~a
					where (id=~a)"])
		(define (acct-credited? res-id)
			(let ([row (pg-one-row (sdb) credited-query res-id)])
				(and row (not (null? (pg-cell row 'acct_xact))))))
		(define (acct-credit person-id res-id stamp)
			(let ([amt-paid (total-prepayment res-id)])
				(when (> amt-paid 0)
					(let ([xact-id
								(acct-charge-person person-id
									(account-id "general" (sdb))
									(- amt-paid) 0
									"Reservation Deposit"
									stamp #f "reservations"
									(env-worker-id) (sdb))])
						(pg-exec (sdb) set-xact-id xact-id res-id)))))
		(lambda (person-id res-id)
			(let ([stamp (time-now)])
				(unless (acct-credited? res-id)
					(acct-credit person-id res-id stamp))
				(unless (on-whiteboard? res-id)
					(pg-exec (sdb) whiteboard-insert
						res-id person-id stamp stamp
						res-id res-id stamp))))))
(define reservee-pid
	; Given a reservation ID, return the reservee's person ID,
	; or zero if non-existent, and reservee's name for the purpose
	; of creating a new people record when necessary (pid == 0).
	(let ([query
				"select name, person_id, id as rid
					from reservations
					where (id=~a)"]
			[role-query
				"select product_roles.role as role
					from product_roles, reservations
					where (reservations.id=~a)
					and (reservations.skydive is not null)
					and (product_roles.product=reservations.skydive)"])
		(define (null-is-zero pid) (if (null? pid) 0 pid))
		(define (get-role res-id)
			(let ([row (pg-one-row (sdb) role-query res-id)])
				(if row (pg-cell row 'role) "TAN")))
		(define (name-parts name)
			(if (null? name) '()
				(let ([parts (reverse (string-tokenize (to-s name)))])
					(if (null? parts) '()
						(list
							(cons 'last (car parts))
							(cons 'other
								(string-cat " " (cdr parts))))))))
		(lambda (res-id)
			(let ([row (pg-one-row (sdb) query res-id)])
				(append
					(name-parts (pg-cell row 'name))
					(list
						(cons 'pid
							(null-is-zero (pg-cell row 'person_id)))
						(cons 'role (get-role res-id))
						(cons 'rid (pg-cell row 'rid))))))))
(define person-ident
	; get person's name, email, phone for prefilling reservation
	; form
	(let ([query
				"select coalesce(goes_by, first_name) as first,
						last_name, coalesce(email,'') as email,
						coalesce(phone_mobile, phone_home, '') as phone
					from people
					where (id=~a)"])
		(lambda (person-id reservation-id)
			(let ([row (pg-one-row (sdb) query person-id)])
				(list
					(cons 'pid person-id)
					(cons 'rid reservation-id)
					(cons 'name
						(format #f "~a ~a"
							(pg-cell row 'first)
							(pg-cell row 'last_name)))
					(cons 'phone (pg-cell row 'phone))
					(cons 'email (pg-cell row 'email)))))))
(define match-person
	; selection link for matching records dialog (see match-panel)
	(let ([query
				"select coalesce(goes_by, first_name) as first,
					last_name, disambiguate, email,
					coalesce(phone_mobile, phone_home) as phone
					from people
					where (id=~a)"]
			[form
				(deflate "<li><a href=\"#\"
					onclick=\"return bind_to_record(~a,~a)\">~a
					~a~a~a~a</a></li>")])
		(lambda (person-id reservation-id)
			(let* ([row (pg-one-row (sdb) query person-id)]
					[disambiguate (pg-cell row 'disambiguate)]
					[email (pg-cell row 'email)]
					[phone (pg-cell row 'phone)])
				(format #f form
					person-id reservation-id
					(pg-cell row 'first)
					(pg-cell row 'last_name)
					(if (null? disambiguate) ""
						(format #f " [~a]" disambiguate))
					(if (null? email) "" (format #f ", ~a" email))
					(if (null? phone) "" (format #f ", ~a" phone)))))))
(define match-panel
	; dialog content for selecting from a list of possible
	; People database matches
	(let ([no-matches
				(deflate "<p style=\"margin-top: 2em\">No
					record matches '~a'.</p>")]
			[none
				(deflate
					"<li><a href=\"#\"
						onclick=\"return close_match_dialog()\">none of the above</a></li>")]
			[intro
				(deflate "<p>Select a matching record:</p>")])
		(lambda (pids reservation-id search-term)
			(if (null? pids)
				(format #f no-matches search-term)
				(string-cat "\n"
					intro
					"<ul>"
					(map
						(lambda (pid)
							(match-person pid reservation-id)) pids)
					none "</ul>")))))
(define keyed-dates
	; generate from/to dates (for search) from magic keywords
	(let ([all-dates-query
				"select min(jump_date) as d1,
						max(jump_date) as d2
					from reservation_groups where (1=1)~a"]
			[find4
				"select distinct jump_date
					from reservation_groups
					where (jump_date >= current_date)~a
					order by jump_date asc limit 4"]
			[latest-query
				"select coalesce(max(jump_date), current_date) as d1
					from reservation_groups~a"])
		(define (blank-null val) (if (null? val) "" (format-date val)))
		(define (all-dates select-active select-on-hold)
			(let ([row
					(pg-one-row (sdb)
						(format #f all-dates-query
							(filter-active-hold
								select-active select-on-hold)))])
				(cons (blank-null (pg-cell row 'd1))
					(blank-null (pg-cell row 'd2)))))
		(define (next-four select-active select-on-hold)
			(let ([dates
						(pg-map-rows
							(pg-exec (sdb)
								(format #f find4
									(filter-active-hold
										select-active select-on-hold)))
							(lambda (row) (pg-cell row 'jump_date)))])
				(if (null? dates)
					(cons (format-date (time-now)) "")
					(cons (format-date (car dates))
						(format-date (car (reverse dates)))))))
		(define (from-today)
			(cons (format-date (time-now)) ""))
		(lambda (key select-active select-on-hold)
			(cond
				[(string=? key "all")
					(all-dates select-active select-on-hold)]
				[(string=? key "4day")
					(next-four select-active select-on-hold)]
				[(string=? key "today") (from-today)]
				[else
					(let ([today (format-date (time-now))])
						(cons today today))]))))
(define on-whiteboard-msg "\
<p>
This person seems to be on the whiteboard. You'll need
to remove them from the board before deleting this reservation.
</p>")
(define group-on-whiteboard-msg "\
<p>
At least one person in this reservation group seems to be on
the whiteboard. You'll need to remove them from the board
before deleting this reservation group.
</p>")
(define get-reservee-name
	(let ([query "select name from reservations where id=~a"])
		(lambda (reservation-id)
			(let ([row (pg-one-row (sdb) query reservation-id)])
				(pg-cell row 'name)))))
(define fork-reservation
	(let ([res-detail-query
				"select reservation_groups.jump_date as date,
						reservation_groups.class_slot as slot
					from reservation_groups, reservations
					where (reservations.id=~a)
					and (reservations.group_id=reservation_groups.id)"]
			[group-id-query
				"select group_id
					from reservations
					where (id=~a)"]
			[group-detail-query
				"select reservations.name as name, jump_date, class_slot
					from reservation_groups, reservations
					where (reservation_groups.id=~a)
					and (reservation_groups.contact=reservations.id)"]
			[assign-group-query
				"update reservations set
					group_id=~a
					where (id=~a)"])
		(define (former-group-id res-id)
			(let ([row (pg-one-row (sdb) group-id-query res-id)])
				(pg-cell row 'group_id)))
		(define (make-group res-id)
			(let ([detail (pg-one-row (sdb) res-detail-query res-id)])
				(add-reservation-group
					(pg-cell detail 'date) (pg-cell detail 'slot))))
		(define (log-note group-id)
			(let ([row (pg-one-row (sdb) group-detail-query group-id)])
				(format #f "split from ~a, ~a, ~a"
					(pg-cell row 'name)
					(time-format (pg-cell row 'jump_date) "%Y-%m-%d")
					(pg-cell row 'class_slot))))
		(lambda (res-id admin)
			(let ([group-id (make-group res-id)]
					[old-group (former-group-id res-id)])
				(pg-exec (sdb) assign-group-query group-id res-id)
				(set-group-contact group-id res-id)
				(add-log res-id admin (log-note old-group))))))
(define (time-nao)
	; for testing date reckoning
	;(time-local 2017 9 2 12 0 0)
	(time-now)
	)
(define window
	; If it's Saturday, let non-admin manifest workers
	; see Sunday's reservations too. Otherwise, only today's.
	(let ([saturday 6])
		(lambda ()
			(let ([today (time-nao)])
				(time-format
					(if (= (time-wday today) saturday)
						(time-add today (* 24 3600))
						today) "%Y-%m-%d")))))
(define (weekend?)
	(let ([wday (time-wday (time-now))])
		(or (= wday 6) (= wday 0))))
(define set-checklist-item
	(let ([query
				"update reservations set
					~a='~a'
					where (id=~d)"])
		(define (launder column)
			(cond
				[(string=? column "age") "chk_age"]
				[(string=? column "weight") "chk_weight"]
				[(string=? column "deposit") "chk_deposit"]
				[(string=? column "waiver") "chk_waiver_fee"]
				[(string=? column "audio") "chk_audio_consent"]
				[else "chk_age"]))
		(lambda (reservation-id column state)
			(pg-exec (sdb)
				(format #f query
					(launder column)
					(if state "t" "f")
					reservation-id)))))
(define contact-name
	(let ([query
				"select reservations.name as name
					from reservations, reservation_groups
					where (reservation_groups.id=~a)
					and (reservations.id=reservation_groups.contact)"])
		(lambda (group-id)
			(let ([row (pg-one-row (sdb) query group-id)])
				(and row (pg-cell row 'name))))))
(define (admin-or-remote req)
	(or (admin-user req) (can-work-remotely? req)))
(define (read-only req)
	(not
		(or (admin-or-remote req)
			(and
				(env-on-dz-lan)
				(weekend?)))))

(log-to "/var/log/nowcall/reservations.log")

(http-html "/"
	(lambda (req)
		(if (authorized-for req "reservations")
			(fill-template (fetch-doc composite) #f
				(cons 'dz_full_name (env-dz-full-name))
				(cons 'admin_check (admin-light req (sdb))))
			(redirect-html "/login/reservations"))))
(http-json "/dress"
	; dress page according to system settings
	(admin-gate "reservations" (req)
		(list
			(cons 'date_format (env-date-format))
			(cons 'on_hold (on-hold-count))
			(cons 'search_length (env-min-search-chars))
			(cons 'skydive_menu (skydive-menu))
			(cons 'slot_menu (class-slots))
			(cons 'media_menu (media-menu #f)))))
(http-json "/savres"
	; create a new reservation
	(admin-gate "reservations" (req)
		(let ([name (query-value req 'name)]
				[email (query-value req 'email)]
				[student-id (zero-is-null (query-value-number req 'pid))]
				[phone (query-value req 'phone)]
				[skydive (query-value-number req 'jump)]
				[media (query-value-number req 'media)]
				[paid (query-value-number req 'paid)]
				[discount (query-value-number req 'discount)]
				[date (query-value req 'date)]
				[chk-age (query-value-boolean req 'chk_age)]
				[chk-weight (query-value-boolean req 'chk_weight)]
				[chk-deposit (query-value-boolean req 'chk_deposit)]
				[chk-waiver-fee (query-value-boolean req 'chk_waiver_fee)]
				[chk-audio-consent
					(query-value-boolean req 'chk_audio_consent)]
				[slot (query-value req 'slot)])
			(let ([status (validate name email phone date)])
				(if (car status)
					(begin
						(new-reservation
							name email phone skydive media paid date slot
							student-id (session-get req 'person-id)
							discount
							chk-age chk-weight chk-deposit
							chk-waiver-fee chk-audio-consent)
						(list
							(cons 'status #t)
							(cons 'date date)))
					(list
						(cons 'status #f)
						(cons 'msg (cdr status))))))))
(http-json "/onhold"
	(admin-gate "reservations" (req)
		(let ([from-date (to-s (query-value req 'from))]
				[to-date (to-s (query-value req 'to))]
				[admin (admin-or-remote req)])
			(list
				(cons 'html
					(on-hold-reservations
						(if admin from-date
							(time-format (time-nao) "%Y-%m-%d"))
						(if admin to-date (window))))))))
(http-json "/showres"
	; display reservations in table
	(admin-gate "reservations" (req)
		(let ([from-date (to-s (query-value req 'from))]
				[to-date (to-s (query-value req 'to))]
				[name-key (to-s (query-value req 'student))]
				[admin (admin-or-remote req)]
				[select-active (query-value-boolean req 'active)]
				[select-on-hold (query-value-boolean req 'on_hold)])
			(list
				(cons 'date_menu
					(reservation-dates select-active select-on-hold))
				(cons 'hold_count (on-hold-count))
				(cons 'read_only (read-only req))
				(cons 'html
					(tabulate-reservations
						(if admin from-date
							(time-format (time-nao) "%Y-%m-%d"))
						(if admin to-date (window))
						(read-only req)
						name-key select-active select-on-hold))))))
(http-json "/cangrp"
	; cancel reservation group, after confirmation
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)])
			(if (admin-or-remote req)
				(begin
					(cancel-reservation-group res-id)
					(list (cons 'status #t)))
				(list (cons 'status #f))))))
(http-json "/delres"
	; drop reservee from group, after confirmation
	(admin-gate "reservations" (req)
		(let* ([res-id (query-value-number req 'rid)]
				[gid (group-id res-id)]
				[refresh
					(and (admin-or-remote req) (cancel-reservee res-id))])
			(if refresh
				(list
					(cons 'status #t)
					(cons 'refresh #t))
				(append (chart-row (contact-id gid))
					(list
						(cons 'status #t)
						(cons 'gid gid)
						(cons 'refresh #f)
						(cons 'log (res-log gid))
						(cons 'html
							(group-detail gid (read-only req)))))))))
(http-json "/update"
	; edit/update a reservation
	(admin-gate "reservations" (req)
		(let ([name (query-value req 'name)]
				[res-id (query-value-number req 'rid)]
				[email (query-value req 'email)]
				[person-id (zero-is-null (query-value-number req 'pid))]
				[phone (query-value req 'phone)]
				[skydive (query-value-number req 'jump)]
				[media (query-value-number req 'media)]
				[discount (query-value-number req 'discount)])
			(update-reservation res-id name email phone
				skydive media person-id discount)
			(append (chart-row res-id)
				(list (cons 'status #t))))))
(http-json "/contact"
	; change reservation group contact
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)])
			(change-contact res-id)
			(append (chart-row res-id)
				(list (cons 'status #t))))))
(http-json "/srchemail"
	; search people DB by email addr, for auto-fill
	(admin-gate "reservations" (req)
		(let ([term (query-value req 'term)])
			(search-email
				(string-downcase (string-trim-both (to-s term)))))))
(http-json "/srchphone"
	; search people DB by phone number, for auto-fill
	(admin-gate "reservations" (req)
		(let ([term (query-value req 'term)])
			(search-phone (to-s term)))))
(http-json "/gdetail"
	; open detail panel
	(admin-gate "reservations" (req)
		(let ([grp-id (query-value-number req 'rid)])
			(list
				(cons 'gid grp-id)
				(cons 'log (res-log grp-id))
				(cons 'html
					(group-detail grp-id (read-only req)))))))
(http-json "/newres"
	; open detail panel
	(admin-gate "reservations" (req)
		(let ([grp-id (query-value-number req 'rid)])
			(blank-reservee grp-id)
			(list
				(cons 'gid grp-id)
				(cons 'log (res-log grp-id))
				(cons 'html
					(group-detail grp-id (read-only req)))))))
(http-json "/splitres"
	; move reservation to a new reservation group
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[admin (session-get req 'person-id)])
			(fork-reservation res-id admin)
			(list (cons 'status #t)))))
(http-json "/sendconfirm"
	; send a confirmation email, log the event
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[admin (session-get req 'person-id)]
				[send-group (query-value-boolean req 'group)])
			(if (send-confirmation-email res-id send-group)
				(begin
					(add-log res-id admin "sent confirmation email")
					(list
						(cons 'status #t)
						(cons 'id res-id)
						(cons 'log (res-log res-id))))
				(begin
					(log-msg "confirmation email failed: res #~d" res-id)
					(list
						(cons 'status #f)
						(cons 'msg
							"uh oh. Email didn't send. Call the geek")))))))
(http-json "/dellog"
	; delete log entry
	(admin-gate "reservations" (req)
		(let* ([res-id (query-value-number req 'rid)]
				[log-id (query-value-number req 'lid)]
				[gid (group-id res-id)])
			(delete-log-entry log-id)
			(list
				(cons 'id res-id)
				(cons 'gid gid)
				(cons 'due (group-due-format (group-due gid)))
				(cons 'total (dollars (total-prepayment res-id)))
				(cons 'contact (group-contact (contact-id gid)))
				(cons 'log (res-log res-id))))))
(http-json "/edlog"
	; edit log entry remark
	(admin-gate "reservations" (req)
		(let ([log-id (query-value-number req 'lid)]
				[remark (query-value req 'remark)]
				[reserve-id #f])
			(set! reserve-id (update-remark log-id remark))
			(list
				(cons 'id reserve-id)
				(cons 'log (res-log reserve-id))))))
(http-json "/edpay"
	; edit log pre-pay amount
	(admin-gate "reservations" (req)
		(let ([log-id (query-value-number req 'lid)]
				[amount (query-value-number req 'amount)]
				[reserve-id #f]
				[group-id #f])
			(set! reserve-id (update-payment log-id amount))
			(set! group-id (get-group-id reserve-id))
			(list
				(cons 'id reserve-id)
				(cons 'total (dollars (total-prepayment reserve-id)))
				(cons 'gid group-id)
				(cons 'due (group-due-format (group-due group-id)))
				(cons 'log (res-log reserve-id))))))
(http-json "/addnote"
	; edit log entry remark
	(admin-gate "reservations" (req)
		(let* ([res-id (query-value-number req 'rid)]
				[person (session-get req 'person-id)]
				[gid (group-id res-id)])
			(add-log res-id person "")
			(list
				(cons 'id res-id)
				(cons 'gid gid)
				(cons 'contact (group-contact (contact-id gid)))
				(cons 'log (res-log res-id))))))
(http-json "/redate"
	; change reservation date
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[res-date (query-value req 'date)])
			(change-date res-id res-date)
			(list (cons 'status #t)))))
(http-json "/reslot"
	; change reservation arrival time
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[res-slot (query-value req 'slot)])
			(if (change-slot res-id res-slot)
				(list (cons 'status #t))
				(list
					(cons 'status #f)
					(cons 'msg "invalid time value"))))))
(http-json "/chjump"
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[proposed-jump (query-value-number req 'jump)])
			(list
				(cons 'id res-id)
				(cons 'label (default-media proposed-jump))))))
(http-json "/hold"
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[hold (query-value-boolean req 'hold)])
			(set-hold res-id hold)
			(add-log res-id (session-get req 'person-id) "placed on hold")
			(list (cons 'status #t)))))
(http-json "/activate"
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[hold (query-value-boolean req 'hold)])
			(set-hold res-id hold)
			(add-log res-id
				(session-get req 'person-id) "restored from hold")
			(list (cons 'status #t)))))
(http-json "/dereturn"
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)])
			(de-return res-id)
			(list
				(cons 'status #t)
				(cons 'rid res-id)))))
(http-json "/getpid"
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)])
			(reservee-pid res-id))))
(http-json "/pplpanel"
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[person-id (query-value-number req 'pid)])
			(reservee-profile person-id res-id)
			(list
				(cons 'status #t)
				(cons 'rid res-id)
				(cons 'pid person-id)))))
(http-json "/checkin"
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[person-id (query-value-number req 'pid)])
			(reservee-check-in person-id res-id)
			(list
				(cons 'status #t)
				(cons 'rid res-id)
				(cons 'pid person-id)))))
(http-json "/matches"
	(admin-gate "reservations" (req)
		(let ([pids (json-decode (query-value req 'matches))]
				[res-id (query-value-number req 'rid)]
				[search-term (query-value req 'term)])
			(list
				(cons 'html
					(match-panel pids res-id search-term))
				(cons 'close (null? pids))))))
(http-json "/ident"
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[person-id (query-value-number req 'pid)])
			(person-ident person-id res-id))))
(http-json "/dpreset"
	(admin-gate "reservations" (req)
		(let ([dates
				(keyed-dates
					(to-s (query-value req 'key))
					(query-value-boolean req 'active)
					(query-value-boolean req 'on_hold))])
			(list
				(cons 'from (car dates))
				(cons 'to (cdr dates))))))
(http-json "/confcan"
	; prompt to confirm reservation cancellation
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[is-group (query-value-boolean req 'is_group)])
			(if is-group
				(let ([row (get-cancellation-detail res-id)])
					(if (and (admin-or-remote req) row)
						(list
							(cons 'detail #t)
							(cons 'rid res-id)
							(cons 'name (pg-cell row 'name))
							(cons 'date
								(time-format (pg-cell row 'jump_date)
									"%A %B %d")))
						(list (cons 'detail #f))))
				(list
					(cons 'rid res-id)
					(cons 'name (get-reservee-name res-id)))))))
(http-json "/upd_checklist"
	(admin-gate "reservations" (req)
		(let ([res-id (query-value-number req 'rid)]
				[column (query-value req 'col)]
				[state (query-value-boolean req 'state)]
				[checklist-done #f]
				[gid #f])
			(set-checklist-item res-id column state)
			(set! checklist-done (checklist-done? res-id))
			(set! gid (group-id res-id))
			(list
				(cons 'color
					(if checklist-done checklist-cold checklist-hot))
				(cons 'gid gid)
				(cons 'contact
					(group-contact (contact-id gid)))
				(cons 'title
					(checklist-btn-title (not checklist-done)))
				(cons 'btn_id
					(format #f "checklist-btn~d" res-id))))))
(http-json "/upd_owl"
	(admin-gate "reservations" (req)
		(let* ([res-id (query-value-number req 'rid)]
				[state (query-value-boolean req 'state)]
				[gid (group-id res-id)])
			(set-checklist-item res-id "weight" state)
			(list
				(cons 'gid gid)
				(cons 'contact
					(group-contact (contact-id gid)))))))
(http-json "/conf_batch_delete"
	(admin-gate "reservations" (req)
		(let ([rids (json-decode (query-value req 'ids))])
			(list
				(cons 'status #f)
				(cons 'ids rids)
				(cons 'names
					(map (lambda (rid) (contact-name rid)) rids))))))
(http-json "/do_batch_delete"
	(admin-gate "reservations" (req)
		(let ([rids (json-decode (query-value req 'ids))])
			(for-each
				(lambda (group-id) (cancel-reservation-group group-id))
				rids)
			(list (cons 'status #t)))))
(add-javascript-logger "reservations")
