#! /usr/local/bin/gusher -p 3003
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))

(include "lib.scm")
(include "settings_lib.scm")

(define env-date-format (setting-cache 'date-format))
(define env-name-order (setting-cache 'name-order))
(define page-body (make-doc 'file "mobman.html"))
(define script (make-doc 'file "mobman.js"))
(define composite
	(make-doc (list page-body script)
		(lambda ()
			(fill-template (fetch-doc page-body) #f
				(cons 'script (fetch-doc script))))))
(define active-aircraft
	; populate aircraft menu
	(let ([query
				"select id, nickname
					from aircraft
					where active
					order by rank asc"])
		(lambda ()
			(let ([date (show-date (time-now) (env-date-format))])
				(pg-map-rows
					(pg-exec (sdb) query)
					(lambda (row)
						(list
							(pg-cell row 'id)
							(format #f "~a - ~a"
								date
								(pg-cell row 'nickname)))))))))
(define my-slot?
	(let ([query
				"select passenger
					from load_slots, load_sheets
					where (load_sheets.aircraft_id=~a)
					and (load_sheets.load_date=current_date)
					and (load_sheets.load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (load_slots.passenger=~a)
					and (load_slots.slot_num=~a)"])
		(lambda (aircraft-id load-num slot-num)
			(and
				(env-person-id)
				(pg-one-row (sdb) query
					aircraft-id load-num (env-person-id) slot-num)))))
(define (mani-admin req) (authorized-for req "manifest")) ;SHARE
(define env-manifest-admin ; SHARE
	(request-cache (lambda (http-req) (mani-admin http-req))))
(define (open-for-manifest? load-status) ; SHARE
	(or (env-manifest-admin)
		(and (eq? load-status 'open) (env-on-dz-lan))))
(define env-logged-in
	(request-cache (lambda (http-req) (logged-in http-req))))
(define disable-form " disabled=\"disabled\"")
(define (observer-slot? slot-num) (= slot-num observer-slot))
(define (slot-num-tag slot-num)
	(if (observer-slot? slot-num) "rs" (to-s slot-num)))
(define load-slot-scratch
	; scratch button
	(let ([html
				(deflate "<td style=\"width: 2em\"><input type=\"button\"
					title=\"scratch\" class=\"scratch rounded-corners-5\"
					onclick=\"scratch('~d',~d,~d)\" value=\"x\"
					id=\"scratch~d_~d_~a\"~a/></td>")])
		(define (enabled? locked self-manifest
					aircraft-id load-num slot-num)
			(and
				;(not locked)
				self-manifest
				(my-slot? aircraft-id load-num slot-num)))
		(lambda (aircraft-id load-num slot-num locked self-manifest)
			(format #f html
				aircraft-id load-num slot-num
				aircraft-id load-num
				(slot-num-tag slot-num)
				(if (enabled? locked self-manifest
						aircraft-id load-num slot-num) "" disable-form)
				)
			)
		)
	)
(define passenger-products ; SHARE
	(let ([query
				"select distinct product_item.name,
						product_item.manifest_symbol as symbol,
						product_category.seq, product_item.seq,
						product_item.manifest_seq
					from roles, product_roles, product_item,
						product_category
					where (roles.who=~a)
					and (product_item.manifest_symbol is not null)
					and (roles.role=product_roles.role)
					and (product_roles.product=product_item.id)
					and (product_item.category=product_category.id)
					and (not product_item.divider)
					order by
						product_item.manifest_seq asc,
						product_category.seq asc,
						product_item.seq asc"])
		(lambda (pid)
			(pg-map-rows
				(pg-exec (sdb) query pid)
				(lambda (row) (pg-cell row 'symbol))))))
(define (role-menu preset roles) ; SHARE
	(if (null? roles)
		"<option value=\"\">-</option>"
		(string-cat "\n"
			(map
				(lambda (role)
					(format #f "<option value=\"~a\"~a>~a</option>"
						role
						(if (string=? role preset)
							" selected=\"selected\"" "")
						role)) roles))))
(define load-slot-role
	; slot role menu
	(let ([html
				(deflate "<td style=\"width: 3em\">
				<select class=\"role-option\"
					title=\"select role\"
					onchange=\"roll_role(this)\"
					id=\"role~d_~d_~d\"~a>~a</select></td>")])
		(define (disable? self-manifest products
					aircraft-id load-num slot-num)
			(or (not self-manifest)
				(< (length products) 1)
				(not (env-logged-in))
				(not (my-slot? aircraft-id load-num slot-num))))
		(lambda (role passenger aircraft-id load-num
					slot-num self-manifest)
			(let ([products
						(if (null? passenger) '()
							(passenger-products passenger))])
				(format #f html
					aircraft-id load-num slot-num
					(if (disable? self-manifest products
							aircraft-id load-num slot-num)
						disable-form "")
					(role-menu (if (null? role) "" role) products))))))
(define reserved-slot-form
	(let ([html
				(deflate "<div class=\"rounded-corners-5 filled-form\">
				<div id=\"fname0_~d_~d_~d\" class=\"dragger\">
				<div style=\"float: left\">~a</div>
				<div style=\"clear: both\"></div>
				</div>
				</div>")])
		(lambda (aircraft-id load-num slot-num)
			(format #f html
				aircraft-id load-num slot-num "<i>reserved</i>"))))
(define has-extras ; SHARE
	(let ([query
				"select count(*) as n
					from load_sheets, load_slots, slot_extras
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (slot_num=~a)
					and (slot_extras.slot_id=load_slots.id)"])
		(lambda (aircraft-id load-date load-num slot-num)
			(let ([row (pg-one-row (sdb) query
						aircraft-id load-date load-num slot-num)])
				(and row (> (pg-cell row 'n) 0))))))
(define filled-slot-form
	; make populated <div> for occupied passenger name slot
	; inter-load draggable unless reserved
	(let ([html
				(deflate "<div class=\"rounded-corners-5 filled-form\">
				<div id=\"fname~d_~d_~d_~a\"~a class=\"dragger\">
				<div style=\"float: left\">~a</div>
				<div style=\"float: left; font-weight: bold;
					font-size: 50%; color: red\">~a</div>
				<div style=\"float: right\">~a</div>
				<div style=\"clear: both\"></div></div></div>")]
			[video-icon
				"<img src=\"/img/vidicon.png\" alt=\"extras\"/>"])
		(lambda (aircraft-id load-num slot-num passenger has-extras)
			(let ([notes (cons "" "")])
				(format #f html
					passenger
					aircraft-id
					load-num
					(slot-num-tag slot-num)
					(cdr notes)
					(person-name passenger (env-name-order) (sdb))
					(car notes)
					(if has-extras video-icon ""))))))
(define blank-slot-form
	; make input text box for empty passenger name slot
	(let ([html
				(deflate "<input type=\"text\" id=\"bname~d_~d_~a\"
					value=\"~a\" class=\"blank-form\"
					onfocus=\"return board(this,~d,~d,~d,'~a')\"~a/>")])
		(lambda (aircraft-id load-num slot-num closed pre-fill)
			(format #f html
				aircraft-id load-num (slot-num-tag slot-num)
				pre-fill
				(or (env-person-id) 0) aircraft-id load-num
				(cond
					[(and (env-logged-in) (env-on-dz-lan)) "ok"]
					[(env-logged-in) "auth"]
					[else "none"])
				(if closed disable-form "")))))
(define (load-slot-name-content aircraft-id load-num slot-num passenger
					reserved has-extras self-manifest)
	(cond
		[reserved
			(reserved-slot-form aircraft-id load-num slot-num)]
		[(not (null? passenger))
			(filled-slot-form
				aircraft-id load-num slot-num
				passenger has-extras)]
		[else
			(blank-slot-form
				aircraft-id
				load-num slot-num
				(not (or (env-manifest-admin) self-manifest)) "")]))
(define load-slot-name
	; add name slot to load sheet, occupied or empty
	(let ([html
				(deflate "<td style=\"padding: 3px\"
					id=\"name~d_~d_~a\">~a</td>")])
		(lambda (passenger aircraft-id load-num slot-num
					reserved self-manifest has-extras)
			(format #f html
				aircraft-id load-num
				(slot-num-tag slot-num)
				(load-slot-name-content aircraft-id load-num
					slot-num passenger reserved has-extras
					self-manifest)))))
(define load-sheet-line
	; build one load sheet entry
	(let ([html
				(deflate "<tr id=\"slot[[AIRCRAFT_ID]]_[[LOAD]]_[[SLOT]]\"
				class=\"catcher\">
				<td id=\"row[[AIRCRAFT_ID]]_[[LOAD]]_[[SLOT]]\"
					class=\"ui-widget-content row_select\">[[SLOT]]</td>
				[[NAME]]
				[[SCRATCH]]
				[[ROLE]]
				</tr>")])
		(lambda (aircraft-id load-num slot passenger role reserved locked
					status has-extras)
			(let ([open-slots (open-for-manifest? status)])
(when (not (null? passenger))
(log-msg "AIRCRAFT ~s LOAD ~s SLOT ~s WHO ~s LOCKED ~s STATUS ~s OPEN ~s"
	aircraft-id load-num slot passenger locked status open-slots
	))
				(fill-template html #f
					(cons 'aircraft_id aircraft-id)
					(cons 'load load-num)
					(cons 'slot slot)
					(cons 'name
						(load-slot-name passenger
							aircraft-id load-num slot reserved
							open-slots has-extras))
					(cons 'scratch
						(load-slot-scratch aircraft-id load-num
							slot locked open-slots))
					(cons 'role
						(load-slot-role role passenger aircraft-id
							load-num slot open-slots)))))))
(define qualifier-button ; SHARE
	(let ([html
				(deflate "<input type=\"button\"
					value=\"~a\"
					class=\"rounded-corners-5\"
					style=\"width: 4em; background-color: ~a; font-weight: bold; color: white\"
					disabled=\"disabled\"/>")]
			[fuel-load-color "#daa520"]
			[hot-load-color "red"])
		(lambda (qualifier)
			(cond
				[(string=? qualifier "fuel")
					(format #f html "FUEL" fuel-load-color)]
				[(string=? qualifier "hot")
					(format #f html "HOT" hot-load-color)]
				[else ""]))))
(define qualifier-lite ; SHARE
	(let ([query
				"select qualifier
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=current_date)
					and (load_num=~a)"])
		(lambda (aircraft-id load-num)
			(let* ([row
					(pg-one-row (sdb) query aircraft-id load-num)])
				(qualifier-button
					(or (and row (pg-cell row 'qualifier)) "none"))))))
(define load-states ; SHARE
	(list
		(cons 'closed
			(list
				(cons 'title "VIEW")
				(cons 'color "#946d53")))
		(cons 'open
			(list
				(cons 'title "OPEN")
				(cons 'color "#7a9453")))
		(cons 'gone
			(list
				(cons 'title "GONE")
				(cons 'color "#6d5394")))))
(define (find-load-status button-label) ; SHARE
	(caar
		(filter
			(lambda (state)
				(string=? (assq-ref (cdr state) 'title) button-label))
			load-states)))
(define (load-status-color status) ; SHARE
	(assq-ref (assq-ref load-states status) 'color))
(define (load-id-key aircraft-id load-num)
	(format #f "~d_~d" aircraft-id load-num))
(define load-sheet
	(let ([sheet-top-tpt
				(deflate "<div id=\"load[[LOAD_ID]]\"
					class=\"load-sheet-mob rounded-corners-10\"
					style=\"border: 6px solid [[BGCOLOR]]\">\
				<table class=\"selectable\" style=\"border-spacing: 0px;
					background-color: white; width: 100%\">
				<tr id=\"toprow[[TOPROW_ID]]\"
					style=\"background-color: [[BGCOLOR]]\">
				<td colspan=\"3\">
				<div class=\"load-number\">[[LOAD_NUM]]</div>
				<div style=\"float: left; margin-right: 4px\"
					id=\"qualifier[[LOAD_ID]]\">[[QUALIFIER_BUTTON]]</div>
				<div style=\"clear: left\"></div>
				</td>
				<td class=\"role-header\">plan</td>
				</tr>")]
			[slots-query
				"select slot_num, passenger, role, reserved,
						locked, id
					from load_slots
					where (load_id=~a)
					order by slot_num"])
		(define (slots load-id aircraft load-num load-status)
			(pg-map-rows
				(pg-exec (sdb) slots-query load-id)
				(lambda (row)
					(load-sheet-line
						aircraft
						load-num
						(pg-cell row 'slot_num)
						(pg-cell row 'passenger)
						(pg-cell row 'role)
						(pg-cell row 'reserved)
						(pg-cell row 'locked)
						load-status
						(has-extras aircraft
							(time-now) load-num
							(pg-cell row 'slot_num))))))
		(define (sheet-top sheet-id load-num aircraft load-status)
			(fill-template sheet-top-tpt #t
				(cons 'load_id (load-id-key aircraft load-num))
				(cons 'toprow_id (load-id-key aircraft load-num))
				(cons 'bgcolor
					(load-status-color load-status))
				(cons 'load_num load-num)
				(cons 'qualifier_button
					(qualifier-lite aircraft load-num))))
		(lambda (sheet-id load-num aircraft load-status)
			(string-cat "\n"
				(sheet-top sheet-id load-num aircraft load-status)
				(slots sheet-id aircraft load-num load-status)
				"</table></div>"))))
(define load-sheets
	(let ([query
				"select id, load_num, status
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=current_date)
					order by load_num asc"])
		(lambda (aircraft)
			(string-cat "\n"
				(pg-map-rows
					(pg-exec (sdb) query aircraft)
					(lambda (row)
						(load-sheet
							(pg-cell row 'id)
							(pg-cell row 'load_num)
							aircraft
							(find-load-status (pg-cell row 'status)))))))))

(log-to "/var/log/nowcall/mobman.log")

(http-html "/"
	(lambda (req)
		(if (or (logged-in req) (env-on-dz-lan))
			(fill-template (fetch-doc composite) #f)
			(redirect-html "/login/mobman"))))
(http-json "/aircraft"
	(lambda (req)
		(list
			(cons 'aircraft (active-aircraft))
			(cons 'logged_in (logged-in req)))))
(http-json "/sheets"
	(lambda (req)
		(let ([aircraft (query-value-number req 'aircraft)]
				[user-id (logged-in req)])
			(if (or user-id (env-on-dz-lan))
				(list
					(cons 'status #t)
					(cons 'user (if user-id user-id -1))
					(cons 'aircraft aircraft)
					(cons 'sheets (load-sheets aircraft)))
				(redirect-html "/login/mobman")))))
