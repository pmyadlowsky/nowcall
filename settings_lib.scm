
(include "database.scm")

(define get-setting
	; get NowCall system setting
	(let ([query
				"select value, data_type
					from settings
					where (sym_name=~a)"])
		(lambda (key dbh)
			(let* ([row (pg-one-row dbh query (to-s key))]
					[value (and row (pg-cell row 'value))]
					[dtype (and row (pg-cell row 'data_type))])
				(and
					value
					(cond
						[(string=? dtype "b") (string=? value "t")]
						[(string=? dtype "i") (to-i value)]
						[(string=? dtype "f") (to-f value)]
						[(string=? dtype "s") (to-s value)]
						[else (to-s value)]))))))
(define set-setting
	; set NowCall system setting
	(let ([query
				"update settings set
					value=~a,
					stamp=current_timestamp
					where (sym_name=~a)"])
		(lambda (key value dbh)
			(pg-exec dbh query
				(cond
					[(boolean? value) (if value "t" "f")]
					[else (to-s value)])
				(to-s key)))))
(define (setting-cache setting-id)
	; create fluid and accessor for per-thread caching
	; of a NowCall system setting
	; ex: (define env-name-order (setting-cache 'name-order))
	(let ([cache (make-unbound-fluid)])
		(lambda ()
			(unless (fluid-bound? cache)
				(fluid-set! cache (get-setting setting-id (sdb))))
			(fluid-ref cache))))
