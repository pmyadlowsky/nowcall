#! /usr/local/bin/gusher -p 3010
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))
(use-modules (ice-9 regex))

(include "lib.scm")
(include "settings_lib.scm")

(define env-name-order (setting-cache 'name-order))
(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-time-format (setting-cache 'time-format))
(define env-date-format (setting-cache 'date-format))
(define sql-dump-dir "/vol/store/tmp")
(define frame (make-doc 'file "frame.html"))
(define page-body (make-doc 'file "squawk.html"))
(define composite
	(make-doc (list frame page-body)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "Squawk")
				(cons 'app "squawk")
				(cons 'script "")
				(cons 'version nowcall-version)
				(cons 'content
					(fill-template (fetch-doc page-body) #t))))))
(define log-squawk
	(let ([query
				"insert into squawk (id, squawker, application, descrip)
					values (~a, ~a, ~a, ~a)"]
			[idx-query "select nextval('squawk_seq') as id"])
		(define (next-id)
			(let ([row (pg-one-row (sdb) idx-query)])
				(and row (pg-cell row 'id))))
		(lambda (app-name descrip squawker)
			(let ([id (next-id)])
				(pg-exec (sdb) query id squawker app-name descrip) id))))
(define can-edit?
	(let ([query
				"select squawker
					from squawk
					where (id=~a)"])
		(define (my-squawk? squawk-id user)
			(let ([row (pg-one-row (sdb) query squawk-id)])
				(and row (= (pg-cell row 'squawker) user))))
		(lambda (squawk-id req)
			(or
				(admin-session? req)
				(my-squawk? squawk-id (logged-in req))))))
(define build-chart
	(let ([query
				"select id, application, stamp, descrip, squawker
					from squawk
					order by stamp desc"]
			[row-tpt
				(deflate "<tr>
					<td class=\"chart-cell-top\">[[DATE]] [[TIME]]</td>
					<td class=\"chart-cell-top\">[[WHO]]</td>
					<td class=\"chart-cell-top\">[[APP]]</td>
					<td class=\"text-box\"
						id=\"desc[[SID]]\">[[WHAT]]</td>
					<td class=\"chart-cell\">[[EDIT]]</td>
					<td class=\"chart-cell\">[[DELETE]]</td>
					</tr>")]
			[del-tpt
				(deflate "<img title=\"delete this squawk\"
					src=\"/img/delete-16.png\"
					onclick=\"delete_squawk(~d)\"
					alt=\"delete\"/>")]
			[edit-tpt
				(deflate "<img title=\"click to edit, click to save\"
					src=\"/img/notepad-16.png\"
					onclick=\"edit_squawk(~d)\"
					alt=\"edit\"/>")]
			[header
				(deflate "<tr>
					<th>Posted</th>
					<th>By</th>
					<th>App</th>
					<th>Description</th>
					<th></th>
					<th></th>
					</tr>")])
		(define (delete-button squawk-id) (format #f del-tpt squawk-id))
		(define (edit-button squawk-id) (format #f edit-tpt squawk-id))
		(define (chart-row row req dbh)
			(fill-template row-tpt #f
				(cons 'sid (pg-cell row 'id))
				(cons 'date
					(show-date (pg-cell row 'stamp) (env-date-format)))
				(cons 'time
					(time-format (pg-cell row 'stamp)
						(if (string=? (env-time-format) "12")
							"%I:%M:%S%p" "%H:%M:%S")))
				(cons 'who
					(person-name (pg-cell row 'squawker)
						(env-name-order) dbh))
				(cons 'app (pg-cell row 'application))
				(cons 'delete
					(if (can-edit? (pg-cell row 'id) req)
						(delete-button (pg-cell row 'id)) "&nbsp;"))
				(cons 'edit
					(if (can-edit? (pg-cell row 'id) req)
						(edit-button (pg-cell row 'id)) "&nbsp;"))
				(cons 'what (pg-cell row 'descrip))))
		(lambda (req)
			(let ([rows
						(pg-map-rows
							(pg-exec (sdb) query)
							(lambda (row) (chart-row row req (sdb))))])
				(if (= (length rows) 0)
					"No Squawks"
					(string-cat "\n"
						"<table>" header rows "</table>"))))))
(define delete-squawk
	(let ([query "delete from squawk where (id=~a)"])
		(lambda (squawk-id req)
			(when (can-edit? squawk-id req)
				(pg-exec (sdb) query squawk-id)))))
(define update-squawk
	(let ([update
				"update squawk set
					descrip=~a
					where (id=~a)"]
			[read-back
				"select descrip
					from squawk
					where (id=~a)"])
		(lambda (squawk-id text req)
			(when (can-edit? squawk-id req)
				(pg-exec (sdb) update text squawk-id)
				(let ([row (pg-one-row (sdb) read-back squawk-id)])
					(pg-cell row 'descrip))))))

(log-to "/var/log/nowcall/squawk.log")

(http-html "/"
	(lambda (req)
		(if (authorized-for req "squawk")
			(let ([files (query-value req 'upsnap_file)])
				(fill-template (fetch-doc composite) #f
					(cons 'admin_check (admin-light req (sdb)))
					(cons 'dz_full_name (env-dz-full-name))))
			(redirect-html "/login/squawk"))))
(http-json "/log"
	; log squawk
	(admin-gate "squawk" (req)
		(let ([app-name (query-value req 'app)]
				[descrip (query-value req 'descrip)])
			(log-squawk app-name descrip (logged-in req))
			(list (cons 'status #t)))))
(http-json "/new"
	; new squawk
	(admin-gate "squawk" (req)
		(let ([app-name (query-value req 'app)])
			(list
				(cons 'id
					(log-squawk app-name
						"Whaddup? Talk to me." (logged-in req)))))))
(http-json "/delete"
	; delete squawk
	(admin-gate "squawk" (req)
		(let ([squawk-id (query-value-number req 'id)])
			(delete-squawk squawk-id req)
			(list (cons 'status #t)))))
(http-json "/update"
	; update squawk
	(admin-gate "squawk" (req)
		(let ([squawk-id (query-value-number req 'id)]
				[text (query-value req 'text)])
			(list
				(cons 'text
					(update-squawk squawk-id text req))))))
(http-json "/chart"
	; log chart
	(admin-gate "squawk" (req)
		(list (cons 'html (build-chart req)))))

(add-javascript-logger "squawk")
