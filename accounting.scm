#! /usr/local/bin/gusher -p 3005
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 regex))
(use-modules (ice-9 format))
(use-modules (srfi srfi-1))

(include "lib.scm")
(include "acct.scm")
(include "settings_lib.scm")

(define env-dz-short-name (setting-cache 'dz-short-name))
(define env-name-order (setting-cache 'name-order))
(define env-date-format (setting-cache 'date-format))
(define env-sales-tax (setting-cache 'sales-tax))
(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-min-search-chars (setting-cache 'min-search-chars))

(define frame (make-doc 'file "frame.html"))
(define page-body (make-doc 'file "acct.html"))
(define main-script (make-doc 'file "acct.js"))
(define lib-script (make-doc 'file "docroot/js/acct_lib.js"))
(define scripts
	(make-doc (list main-script lib-script)
		(lambda ()
			(string-cat "\n"
				(fetch-doc lib-script)
				(fetch-doc main-script)))))
(define acct-menu
	; build pull-down account menu
	(let ([query
				"select id, display_name, name
					from acct
					where (not personal)
					order by not home_acct, display_name asc"])
		(define (menu-option row short-name)
			(list
				(to-s (pg-cell row 'id))
				(if (and
						(string=? (pg-cell row 'name) "general")
						(not (string=? short-name "")))
					short-name
					(pg-cell row 'display_name))))
		(lambda (with-special)
			(let ([res (pg-exec (sdb) query)]
					[short-name (env-dz-short-name)])
				(append
					(list (list "pacct" "Personal Acct"))
					(pg-map-rows res
						(lambda (row) (menu-option row short-name)))
					(if with-special
						(list (list "special" "Create Special")) '()))))))
(define composite
	(make-doc (list frame page-body scripts)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "Accounting")
				(cons 'app "accounting")
				(cons 'script (fetch-doc scripts))
				(cons 'version nowcall-version)
				(cons 'content
					(fill-template (fetch-doc page-body) #t))))))
(define (dollars cents)
	(if (not (number? cents)) "#####"
		(let ([abs-cents (abs cents)]
				[sign (if (< cents 0) "-" "")])
			(if (>= abs-cents 100000)
				(format #f "~a~d,~3,'0d.~2,'0d"
					sign
					(quotient abs-cents 100000)
					(quotient (remainder abs-cents 100000) 100)
					(remainder (remainder abs-cents 100000) 100))
				(format #f "~a~d.~2,'0d"
					sign
					(quotient abs-cents 100)
					(remainder abs-cents 100))))))
(define transaction-row-tpt "\
<tr>
<td class=\"chart-cell date-col [[TINT]]\">
<input type=\"text\" class=\"[[TINT]] cell-box[[DATE_CLASS]]\"
	id=\"date[[XACT_ID]]\"[[EDITABLE]]
	value=\"[[XACT_DATE]]\"
	onkeypress=\"Acct.editing(this,event)\"
	onchange=\"Acct.editing(this,event)\"/></td>
<td class=\"chart-cell [[TINT]] descr-col\">
<input type=\"text\" class=\"[[TINT]] cell-box\" id=\"descr[[XACT_ID]]\"
	value=\"[[DESC]]\"
	onkeypress=\"Acct.editing(this,event)\"/></td>
<td class=\"chart-cell xfer-col [[TINT]]\">
<input type=\"text\" class=\"[[TINT]]\" id=\"xfer[[XACT_ID]]\"
	readonly=\"readonly\"
	title=\"click to visit this account\"
	onclick=\"display_acct('[[XFER_ACCT]]')\"
	value=\"[[XFER]]\"/>
</td>
<td class=\"chart-cell [[TINT]] money-col\">
<input type=\"text\" class=\"num-cell cell-box [[TINT]]\"
	id=\"amount_in[[XACT_ID]]\"[[AMOUNT_IN_EDIT]]
	value=\"[[AMOUNT_IN]]\"
	onkeypress=\"Acct.editing(this,event)\"/></td>
<td class=\"chart-cell [[TINT]] money-col\">
<input type=\"text\" class=\"num-cell cell-box [[TINT]]\"
	id=\"amount_out[[XACT_ID]]\"[[AMOUNT_OUT_EDIT]]
	value=\"[[AMOUNT_OUT]]\"
	onkeypress=\"Acct.editing(this,event)\"/></td>
<td class=\"chart-cell [[TINT]] money-col\">
<input type=\"text\" class=\"num-cell cell-box [[TINT]]\"
	id=\"balance[[XACT_ID]]\"
	disabled=\"disabled\"
	style=\"color: [[BCOLOR]]\"
	value=\"[[BALANCE]]\"/></td>
<td class=\"chart-cell [[TINT]] del-col\" style=\"text-align: center\">
<input type=\"button\" value=\"x\" class=\"scratch\"[[DELETABLE]]
	onclick=\"Acct.delete_xact('[[XACT_ID]]')\">
</td>
</tr>")

(define account-name
	(let ([query
				"select display_name, personal,
						people.last_name as lname,
						people.first_name as fname,
						people.id as pid
					from acct, people
					where (acct.id=~a)
					and (acct.owner=people.id)"])
		(lambda (acct-id)
			(let ([row (pg-one-row (sdb) query acct-id)])
				(if (pg-cell row 'personal)
					(format #f "~a"
						(person-name (pg-cell row 'pid)
							(env-name-order) (sdb)))
					(pg-cell row 'display_name))))))
(define manifest-associated
	; verify that transaction is associated with a manifest event
	(let ([slot-query
				"select acct_xact_id
					from load_slots
					where (acct_xact_id=~a)
					limit 1"]
			[observer-query
				"select observer_xact
					from load_sheets
					where (observer_xact=~a)
					limit 1"])
		(lambda (transaction)
			(and (pg-cell transaction 'manifest)
				(or (pg-one-row (sdb) observer-query
						(pg-cell transaction 'xact_id))
					(pg-one-row (sdb) slot-query
						(pg-cell transaction 'xact_id)))))))
(define (adjusted-amount row)
	(+ (pg-cell row 'amount)
		(pg-cell row 'sales_tax)))
(define (transaction-row tint balance row)
	(let ([amount (adjusted-amount row)]
			[this-acct (pg-cell row 'this_acct)]
			[from-acct (pg-cell row 'from_acct)]
			[to-acct (pg-cell row 'to_acct)]
			[quantity (or (pg-cell row 'quantity) 1)]
			[manifest (manifest-associated row)]
			[xact-id (pg-cell row 'xact_id)])
		(fill-template transaction-row-tpt #f
			(cons 'tint (if tint "tint-row" "plain-row"))
			(cons 'ref (to-s (pg-cell row 'reference)))
			(cons 'xact_id xact-id)
			(cons 'xact_date
				(show-date (pg-cell row 'date_posted) (env-date-format)))
			(cons 'xfer_acct
				(if (= this-acct to-acct) from-acct to-acct))
			(cons 'xfer
				(if (= this-acct to-acct)
					(account-name from-acct)
					(account-name to-acct)))
			(cons 'amount_in
				(if (= this-acct to-acct)
					(dollars (* amount quantity)) ""))
			(cons 'amount_in_edit
				(if (= this-acct to-acct) "" " disabled=\"disabled\""))
			(cons 'amount_out
				(if (= this-acct from-acct)
					(dollars (* amount quantity)) ""))
			(cons 'amount_out_edit
				(if (= this-acct from-acct) "" " disabled=\"disabled\""))
			(cons 'desc
				(format #f "~a~a"
					(to-s (pg-cell row 'description))
					(if (> quantity 1) (format #f " [~d]" quantity) "")))
			(cons 'balance (dollars balance))
			(cons 'bcolor (if (< balance 0) "red" "black"))
			(cons 'date_class (if manifest "" " dp"))
			(cons 'editable (if manifest " disabled=\"disabled\"" ""))
			(cons 'deletable (if manifest " disabled=\"disabled\"" ""))
			(cons 'row_select ""))))
(define acct-balance
	(let ([query "select balance from acct where (id=~a)"])
		(lambda (acct)
			(let ([row (pg-one-row (sdb) query acct)])
				(if row (pg-cell row 'balance) 0)))))
(define build-chart
	(let ([query
				"select from_acct, to_acct, date_posted, amount,
						acct_xact.description as description,
						reference, entered, personal,
						display_name, product_id,
						manifest, quantity, sales_tax,
						acct.id as this_acct,
						acct_xact.id as xact_id,
						((from_acct in ([[WORKERS]]))) as from_worker,
						((to_acct in ([[WORKERS]]))) as to_worker,
						((amount + sales_tax) * quantity) *
							(case
								when (from_acct=~a) then 1
								else -1
								end) as amt
					from acct_xact, acct
					where (acct.id=~a)
					and ((acct_xact.from_acct=acct.id) or
							(acct_xact.to_acct=acct.id))
					order by date_posted desc, entered desc
					limit ~a"]
			[save-button
				"<input type=\"button\" class=\"acct-submit-btn\"
					id=\"save_btn\"
					value=\"Save\" disabled=\"disabled\"
					onclick=\"Acct.save_edits()\"/>"]
			[chart-header
				(deflate "<table id=\"chart-header\"><tr>
				<td class=\"account-hdr date-col\">Date</td>
				<td class=\"account-hdr descr-col\">Detail</td>
				<td class=\"account-hdr xfer-col\">Transfer Acct</td>
				<td class=\"account-hdr money-col\">In</td>
				<td class=\"account-hdr money-col\">Out</td>
				<td class=\"account-hdr money-col\">Balance</td>
				<td class=\"account-hdr del-col\">&nbsp;</td>
				</tr></table>")]
			[chart-wrap
				"<div id=\"chart-wrap\" class=\"ui-widget-content\">"]
			[chart-table
				"<table class=\"selectable\" id=\"chart-table\">"]
			[worker-query
				"select distinct acct.id as id
					from acct, app_auth
					where (app_auth.app in ('manifest','accounting'))
					and acct.personal
					and (acct.owner=app_auth.person_id)"]
			[empty-response
				"<p>no activity on this account</p>~a"]
			[delete-button
				(deflate "<p><input type=\"button\"
					value=\"Delete [~a]\"
					onclick=\"delete_account('~d')\"/></p>")]
			[personal-query
				"select (personal or home_acct) from acct where id=~a"])
		(define (manifest-workers)
			(pg-map-rows
				(pg-exec (sdb) worker-query)
				(lambda (row) (pg-cell row 'id))))
		(define (make-render start-balance admin)
			(let ([tint #f]
					[balance start-balance])
				(lambda (row)
					(if (and (pg-cell row 'to_worker) (not admin)) ""
						(let ([table-row
									(transaction-row tint
										(- balance) row)])
							(set! tint (not tint))
							(set! balance (- balance (pg-cell row 'amt)))
							table-row)))))
		(define (ledger-table res balance private-admin)
			(string-cat "\n"
				chart-header
				chart-wrap
				chart-table
				(reverse
					(pg-map-rows res
						(make-render balance private-admin)))
				"</table></div>" save-button))
		(define (dz-acct? acct-id)
			(let ([row (pg-one-row (sdb) personal-query acct-id)])
				(and row (not (pg-cell row 'personal)))))
		(define (empty-account acct-id)
			(format #f empty-response
				(if (dz-acct? acct-id)
					(format #f delete-button
						(account-name acct-id) acct-id) "")))
		(lambda (acct limit private-admin)
			(let ([res
					(pg-exec (sdb)
						(fill-template query #f
							(cons 'workers
								(string-cat ","
									"0"
									(manifest-workers))))
						acct acct
						(if (= limit 0) '() limit))])
				(if (< (pg-tuples res) 1)
					(empty-account acct)
					(ledger-table res
						(acct-balance acct) private-admin))))))
(define person-account
	(let ([query
				"select id
					from acct
					where personal
					and (owner=~a)"])
		(lambda (owner-id)
			(let ([row (pg-one-row (sdb) query owner-id)])
				(if row
					(pg-cell row 'id)
					(begin
						(acct-assure-cash-acct owner-id (sdb))
						(person-account owner-id)))))))
(define set-search-where
	; set where clause on basic search query
	(let ([base-query
				"select first_name, last_name, id, goes_by,
						upper(coalesce(goes_by, first_name)) as fname,
						upper(last_name) as lname, disambiguate,
						coalesce(phone_mobile,phone_home) as phone,
						email
					from people
					where ~a
					order by lname asc, fname asc"])
		(lambda (where-clause) (format #f base-query where-clause))))
(define search-query
	; general last-first-nick search
	(set-search-where "\
		((upper(last_name) like ~a) or
			(upper(goes_by) like ~a) or
			(upper(first_name) like ~a) or
			(upper(nickname) like ~a))"))
(define search-query-f
	; search on first name only
	(set-search-where "(upper(coalesce(goes_by, first_name)) like ~a)"))
(define search-query-l
	; search on last name only
	(set-search-where "(upper(last_name) like ~a)"))
(define search-query-fl
	; search on first-last, or last-first
	(set-search-where "\
		(upper(last_name) like ~a)
		and (upper(coalesce(goes_by,first_name)) like ~a)"))
(define (like-string src) (pg-format (sdb) (format #f "~a%" src)))
(define try-fname-lname
	(let ([pat (make-regexp "([A-Z]+)[ ]+([A-Z]+)")])
		(lambda (query)
			(let ([match (regexp-exec pat query)])
				(and match
					(format #f search-query-fl
						(like-string (match:substring match 2))
						(like-string (match:substring match 1))))))))
(define try-lname-fname
	(let ([pat (make-regexp "([A-Z]+)[ ]*,[ ]*([A-Z]+)")])
		(lambda (query)
			(let ([match (regexp-exec pat query)])
				(and match
					(format #f search-query-fl
						(like-string (match:substring match 1))
						(like-string (match:substring match 2))))))))
(define try-fname
	(let ([pat (make-regexp "([A-Z]+)[ ]+")])
		(lambda (query)
			(let ([match (regexp-exec pat query)])
				(and match
					(format #f search-query-f
				 		(like-string (match:substring match 1))))))))
(define try-lname
	(let ([pat (make-regexp "([A-Z]+)[ ]*,")])
		(lambda (query)
			(let ([match (regexp-exec pat query)])
				(and match
					(format #f search-query-l
						(like-string (match:substring match 1))))))))
(define (make-search-query query)
	(let ([upcase (string-upcase query)])
		(or
			(try-fname-lname upcase)
			(try-lname-fname upcase)
			(try-lname upcase)
			(try-fname upcase)
			(let ([term (like-string upcase)])
				(format #f search-query term term term term)))))
(define search-results
	(let ()
		(define (name-mark-up row)
			(let ([disambig (pg-cell row 'disambiguate)])
				(format #f "~a~a"
					(person-name (pg-cell row 'id) (env-name-order) (sdb))
					(if (null? disambig) ""
						(format #f " [~a]" disambig)))))
		(define (menu-entry row)
			(let ([pid (pg-cell row 'id)]
					[name (name-mark-up row)])
				(list
					(cons 'label name)
					(cons 'value name)
					(cons 'match #t)
					(cons 'pid pid)
					(cons 'phone (pg-cell row 'phone))
					(cons 'email (pg-cell row 'email))
					(cons 'acct_id
						(person-account pid)))))
		(define (no-match term)
			(list
				(list
					(cons 'pid 0)
					(cons 'match #f)
					(cons 'value term)
					(cons 'label "NO MATCH?"))))
		(lambda (term allow-new)
			(let ([res (pg-exec (sdb) (make-search-query term))])
				(if (< (pg-tuples res) 1)
					(no-match term)
					(append
						(pg-map-rows res
							(lambda (row) (menu-entry row)))
						(no-match term)))))))
(define latest-event
	; latest accont event time
	(let ([query
				"select (date_trunc('sec', latest_event)) as stamp
					from acct_track
					where acct_id=~a"])
		(lambda (acct)
			(if (= acct 0) 0
				(let* ([row (pg-one-row (sdb) query acct)]
						[stamp (pg-cell row 'stamp)])
					(if stamp (to-i (time-epoch stamp)) 0))))))
(define compute-balances
	; run through all transactions to compute account balances
	; (provided as tool, not called anywhere)
	(let ([query
				"update acct set balance=0;
				select from_acct, to_acct,
						((amount + sales_tax) * quantity) as amt
					from acct_xact
					order by entered asc"]
			[increment
				"update acct set
					balance=(balance + ~a)
					where (id=~a)"])
		(lambda ()
			(pg-each-row (pg-exec (sdb) query)
				(lambda (row)
					(let ([amount (pg-cell row 'amt)])
					(pg-exec (sdb) increment
						amount (pg-cell row 'from_acct))
					(pg-exec (sdb) increment
						(- amount) (pg-cell row 'to_acct))))))))
(define init-balances
	; if account balances are all zero, they probably need to be
	; initialized
	(let ([query
				"select balance from acct where (balance != 0) limit 1"])
		(lambda (force)
			(if (or force (not (pg-one-row (sdb) query)))
				(compute-balances)
				(log-msg "account balances initialized")))))
(define acct-balances
	; consult account balances to prepare lists of payables and receivables
	(let ([query
				"select people.id as pid, acct.balance as balance
					from people, acct
					where (acct.balance != 0)
					and (acct.owner=people.id)
					and (acct.personal)"])
		(define (flip tuple)
			(list
				(cons 'pid (pg-cell tuple 'pid))
				(cons 'balance (- (pg-cell tuple 'balance)))))
		(lambda ()
			(let sort-out
					([balances
						(pg-map-rows (pg-exec (sdb) query)
							(lambda (row) row))]
					[receivable '()]
					[payable '()])
				(cond
					[(null? balances)
						(list
							(cons 'recv receivable)
							(cons 'pay payable))]
					[(< (pg-cell (car balances) 'balance) 0)
						(sort-out (cdr balances)
							receivable
							(cons (flip (car balances)) payable))]
					[else
						(sort-out (cdr balances)
							(cons (car balances) receivable) payable)])))))
(define balances-table
	(let ([html
				(deflate "<tr><td class=\"chart-cell xfer-col\"
					onclick=\"display_acct(~d)\"
					title=\"click to visit this account\">~a</td>
				<td class=\"chart-cell num-cell money-col\">~a</td></tr>")])
		(define account-owned-by
			(let ([query
						"select id
							from acct
							where (owner=~a)
							and personal
							limit 1"])
				(lambda (pid)
					(let ([row (pg-one-row (sdb) query pid)])
						(pg-cell row 'id)))))
		(define (acct-name-balance tuple)
			(list
				(cons 'acct (account-owned-by (pg-cell tuple 'pid)))
				(cons 'name
					(person-name (pg-cell tuple 'pid)
						(env-name-order) (sdb)))
				(cons 'balance (pg-cell tuple 'balance))))
		(define (sort-by-name a b)
			(string<? (pg-cell a 'name) (pg-cell b 'name)))
		(lambda (balances)
			(if (null? balances) "<br/>none"
				(string-cat "\n"
					"<table>"
					(map
						(lambda (tuple)
							(format #f html
								(pg-cell tuple 'acct)
								(pg-cell tuple 'name)
								(dollars (pg-cell tuple 'balance))))
						(sort-list
							(map
								(lambda (tuple)
									(acct-name-balance tuple))
								balances)
							sort-by-name))
					"</table>")))))
(define balances-cache '())
(define (invalidate-balances) (set! balances-cache '()))
(define (invalid-balances?) (null? balances-cache))
(define balances
	(let ([html
				(deflate
				"<div><table><tr><td style=\"padding-left: 0.5em;
					vertical-align: top; width: 20em\">
				<b>Receivable</b>
				<div class=\"scroll-ledger\" id=\"receivable\">~a</div>
				</td><td style=\"padding-left: 0.5em;
					vertical-align: top; width: 20em\">
				<b>Payable</b>
				<div class=\"scroll-ledger\" id=\"payable\">~a</div>
				</td></tr></table></div>")])
		(lambda ()
			(when (null? balances-cache)
				(let ([balances (acct-balances)])
					(set! balances-cache
						(format #f html
							(balances-table (assq-ref balances 'recv))
							(balances-table
								(assq-ref balances 'pay))))))
			balances-cache)))
(define controls-tpt "\
<table>
<tr>
<td style=\"width: 6em\">Debit</td>
<td>
$<input type=\"text\" id=\"charge_amt\"
	onchange=\"Acct.recompute_tax(this)\"
	class=\"num-cell money-col add-trig\"/>
for</td>
<td>[[CHARGE_MENU]]</td>
<td>
 x <input id=\"charge_mult\"
		class=\"add-trig\" title=\"quantity\" type=\"text\"/>
</td>
<td>detail:</td>
<td>
<input id=\"charge_detail\"
		class=\"add-trig\" style=\"width: 24em\" type=\"text\"/>
tax: <input type=\"text\" id=\"sales_tax\"
	readonly=\"readonly\"
	class=\"num-cell tax-col add-trig\"/>
</td>
</tr>
<tr>
<td style=\"width: 6em\">Credit</td>
<td>
$<input type=\"text\" id=\"pay_amt\"
	class=\"num-cell money-col add-trig\"/>
for</td><td>
[[PAYOUT_MENU]]
</td><td>
 x <input id=\"pay_mult\"
		class=\"add-trig\" title=\"quantity\" type=\"text\"/>
</td>
<td>detail:</td>
<td><input id=\"pay_detail\"
		class=\"add-trig\" style=\"width: 24em\" type=\"text\"/>
</td>
</tr>
<tr>
<td style=\"width: 6em\">Balance</td>
<td>$<input type=\"text\" id=\"balance\"
	style=\"color: [[BALANCE_COLOR]]\"
	readonly=\"readonly\"
	value=\"[[BALANCE]]\"
	class=\"num-cell money-col\"/></td>
<td colspan=\"4\">
Transfer Acct:
<select id=\"xfer-acct\" onchange=\"select_xfer_acct()\"></select>
<input type=\"text\" id=\"person-acct-srch\"
	value=\"\"
	onfocus=\"live_xfer_search(this)\"
	disabled=\"disabled\"/>
<input type=\"hidden\" id=\"acct-xfer-id\" value=\"[[DZ_ACCT]]\"/>
<input type=\"hidden\" id=\"dzname-cache\" value=\"[[DZ_NAME]]\"/>
<input type=\"hidden\" id=\"dzacct-cache\" value=\"[[DZ_ACCT]]\"/>
<input type=\"button\" class=\"acct-submit-btn\" value=\"Add\"
	onclick=\"Acct.pay()\"/>
<input type=\"button\" class=\"acct-submit-btn\" value=\"Clear\"
	onclick=\"Acct.clear_controls()\"/>
</td>
</tr>
</table>")
(define (make-controls acct-id)
	(let ([balance (- (acct-balance acct-id))])
		(fill-template controls-tpt #f
			(cons 'dz_acct (dz-account))
			(cons 'dz_name (env-dz-short-name))
			(cons 'charge_menu (item-menu "charge_acct" #f))
			(cons 'payout_menu
				(item-menu "payment_acct" #t))
			(cons 'balance (dollars (abs balance)))
			(cons 'balance_color (if (< balance 0) "red" "black")))))
(define chart-reply
	(let ([acct-query
				"select home_acct, personal, balance
					from acct
					where (id=~a)"]
			[personals-query
				"select coalesce(people.goes_by, people.first_name,
							'Patron') as name,
						coalesce(people.email, '') as email
					from people, acct
					where (acct.id=~a)
					and acct.personal
					and (acct.owner=people.id)"]
			[stripe-query
				"select service_token as token
					from cards, acct
					where (acct.id=~a)
					and acct.personal
					and (acct.owner=cards.owner)"])
		(define owner-name
			(let ([query "select personal, owner from acct where (id=~a)"])
				(lambda (acct-id)
					(let ([row (pg-one-row (sdb) query acct-id)])
						(and row
							(if (pg-cell row 'personal)
								(person-name
									(pg-cell row 'owner)
									(env-name-order) (sdb)) #f))))))
		(define (stripe-id acct-id)
			(let ([row (pg-one-row (sdb) stripe-query acct-id)])
				(if row (pg-cell row 'token) "foo")))
		(define (personals acct-id)
			(let ([row (pg-one-row (sdb) personals-query acct-id)])
				(if row
					(cons (pg-cell row 'email) (pg-cell row 'name))
					(cons "" "Patron"))))
		(lambda (acct-id event-stamp limit admin)
			(let ([extras (pg-one-row (sdb) acct-query acct-id)]
					[personal (personals acct-id)])
				(list
					(cons 'status #t)
					(cons 'date_format (env-date-format))
					(cons 'acct_id acct-id)
					(cons 'owner (owner-name acct-id))
					(cons 'latest event-stamp)
					(cons 'customer (stripe-id acct-id))
					(cons 'balances (balances))
					(cons 'balance (pg-cell extras 'balance))
					(cons 'showpay (pg-cell extras 'personal))
					(cons 'cust_name (cdr personal))
					(cons 'authorized admin)
					(cons 'email (car personal))
					(cons 'chart
						(if (> acct-id 0)
							(build-chart acct-id limit admin) #f))
					(cons 'controls
						(if (pg-cell extras 'home_acct) ""
							(make-controls acct-id))))))))
(define (today) (time-format (time-now) "%Y-%m-%d"))
(define not-manifest #f)
(define (to-cents amount) (to-i (+ (* (to-f amount) 100) 0.5)))
(define xact-col-map
	(list
		(cons 'ref (cons "reference" to-s))
		(cons 'descr (cons "description" to-s))
		(cons 'amount_in (cons "total" to-cents))
		(cons 'amount_out (cons "total" to-cents))
		(cons 'date (cons "date_posted" to-s))))
(define xcat-edit-pat (make-regexp "^([^0-9]+)([0-9]+)"))
(define item-menu
	(let ([query
				"select product_category.id as cid,
						product_category.name as cname,
						product_item.id as pid,
						product_item.name as pname,
						product_item.cash_price
					from product_category, product_item
					where (product_item.category=product_category.id)
					and (not product_item.manifest_apropos)
					and (not product_item.divider)
					and ~a
					order by product_category.seq asc,
						product_item.seq asc"]
				[item-li "<li id=\"item~d\" style=\"width: 30em\">~a</li>"])
		(define (menu-item item)
			(if (eq? item 'null) ""
				(format #f item-li
					(pg-cell item 'pid)
					(pg-cell item 'pname))))
		(define (item-group category items)
			(if (null? items) #f
				(string-cat ""
					(format #f "<li>~a" category)
					(if (eq? (car items) 'null) "" "<ul>")
					(map menu-item (reverse items))
					(if (eq? (car items) 'null) "" "</ul>") "</li>")))
		(define (process-menu row prev-category items)
			(let ([category (pg-cell row 'cname)])
				(if (string=? category prev-category)
					(list
						(cons 'keep #f)
						(cons 'items (cons row items))
						(cons 'cat prev-category))
					(list
						(cons 'keep (item-group prev-category items))
						(cons 'items (list row))
						(cons 'cat category)))))
		(lambda (menu-id expenses)
			(string-cat "\n"
				(format #f "<ul id=\"~a\">" menu-id)
				"<li style=\"width: 5em\">Select...<ul>"
				(let ([res
							(pg-exec (sdb) (format #f query
								(if expenses "payout" "(not payout)")))]
						[prev-category ""]
						[items '()])
					(filter
						(lambda (item) item)
						(append
							(pg-map-rows res
								(lambda (row)
									(let ([piece
												(process-menu row
													prev-category items)])
										(set! items (assq-ref piece 'items))
										(set! prev-category
											(assq-ref piece 'cat))
										(assq-ref piece 'keep))))
							(list (item-group prev-category items)))))
				"</ul></li>"
				"</ul>"))))
(define get-charge
	(let ([query
				"select product_item.name as name,
						product_category.name as cname,
						cash_price, compute_balance,
						sales_tax, acct_transfer
					from product_item, product_category
					where (product_item.id=~a)
					and (product_item.category=product_category.id)
					and (not product_item.divider)"])
		(lambda (item-id) (pg-one-row (sdb) query item-id))))
(define dz-account
	; create if doesn't exist
	(let ([query
				"select id
					from acct
					where (name='general')"]
			[create
				"insert into acct (name, display_name, home_acct,
						personal, description)
					values ('general', 'DZ', 't', 'f',
							'main DZ account')"])
		(lambda ()
			(let ([row (pg-one-row (sdb) query)])
				(if row
					(pg-cell row 'id)
					(begin
						(pg-exec (sdb) create)
						(pg-cell (pg-one-row (sdb) query) 'id)))))))
(define my-account
	(let ([template
				(deflate "<div style=\"background-color: white;
					border: 1px solid gray; padding: 0.5em; width: 100%\">
					<div style=\"font-size: 115%;
						font-weight: bold\">~a</div>
					<div id=\"controls\" style=\"margin-top: 1em\">~a</div>
					<div id=\"chart\">~a</div>
					<script type=\"text/javascript\">
					console.log('my account');
					</script>
					</div>")])
		(lambda (acct-id admin)
			(format #f template
				(account-name acct-id)
				(make-controls acct-id)
				(build-chart acct-id 0 admin)))))
(define (clip-balance pay acct-id)
	(let ([balance (- (acct-balance acct-id))])
		(cond
			[(and pay (< balance 0)) (abs balance)]
			[(and (not pay) (> balance 0)) (abs balance)]
			[else 0])))
(define (sales-tax price)
	(to-i (+ (* price (/ (env-sales-tax) 100)) 0.5)))
(define set-proxy-person
	(let ([query
				"select owner
					from acct
					where (id=~a)
					and personal"])
		(lambda (acct-id req)
			(session-set req 'proxy-person-id
				(if (authorized-for req "accounting")
					(let ([row (pg-one-row (sdb) query acct-id)])
						(if row (pg-cell row 'owner) 0)) 0)))))
(define manifest-response
	(let ([accts-query "select owner from acct where (id=~a)"])
		(define (accts-owner acct-id)
			(let ([row (pg-one-row (sdb) accts-query acct-id)])
				(and row (pg-cell row 'owner))))
		(lambda (acct-id)
			(list
				(cons 'status #t)
				(cons 'owner (accts-owner acct-id))))))
(define special-acct-exists?
	(let ([query
				"select display_name from acct
					where (lower(display_name)=lower(~a))"])
		(lambda (acct-name)
			(let ([row (pg-one-row (sdb) query acct-name)])
				(and row (pg-cell row 'display_name))))))
(define create-special-account
	(let ([query
				"insert into acct (owner, name, personal,
						display_name, home_acct, description)
					values (~a, 'special', 'f',
						~a, 'f', ~a);
				select currval('acct_seq') as id;"]
			[owner-query
				"select owner
					from acct
					where home_acct"])
		(define (dz-owner)
			(let ([row (pg-one-row (sdb) owner-query)])
				(if row (pg-cell row 'owner) 0)))
		(lambda (name description)
			(pg-cell
				(pg-one-row (sdb) query
					(dz-owner) name description) 'id))))
(define delete-account
	(let ([query
			"delete from acct_track where acct_id=~a;
			delete from acct where id=~a"])
		(lambda (acct-id)
			(pg-exec (sdb) query acct-id acct-id))))
(define (update-pending acct-id refreshed admin limit)
	(let ([latest (latest-event acct-id)])
		(and
			(> latest refreshed)
			(begin
				(invalidate-balances)
				(chart-reply acct-id latest limit admin)))))

(log-to "/var/log/nowcall/accounting.log")

(init-balances #f)

(http-html "/"
	(lambda (req)
		(if (authorized-for req "accounting")
			(fill-template (fetch-doc composite) #f
				(cons 'dz_full_name (env-dz-full-name))
				(cons 'admin_check (admin-light req (sdb))))
			(redirect-html "/login/accounting"))))
(http-json "/menu"
	(admin-gate "accounting" (req)
		(let ([with-special (query-value-boolean req 'ws)])
		(list (cons 'options (acct-menu with-special))))))
(http-json "/chart"
	(admin-gate "accounting" (req)
		(let ([acct-id (query-value-number req 'acct)]
				[limit (query-value-number req 'limit)])
			(set-proxy-person acct-id req)
			(chart-reply acct-id (latest-event acct-id)
				limit (authorized-for req "accounting")))))
(http-json "/search"
	(admin-gate "accounting" (req)
		(let ([term (query-value req 'term)]
				[allow-new (query-value-boolean req 'allow_new)])
			(search-results term allow-new))))
(http-json "/selpers"
	(admin-gate "accounting" (req)
		(let ([pid (query-value-number req 'pid)])
			(list
				(cons 'acct_id (person-account pid))
				(cons 'pid pid)
				(cons 'name (person-name pid (env-name-order) (sdb)))))))
(http-json "/poll"
	; long-poll request handler
	(lambda (req)
		(let ([tag (query-value req 'tag)]
				[acct-id (query-value-number req 'acct)]
				[refreshed (query-value-number req 'time_ref)]
				[admin (admin-session? req)]
				[limit (query-value-number req 'limit)])
			(longpoller
				(lambda ()
					(update-pending acct-id refreshed
						admin limit)) tag))))
(http-json "/trip"
	(lambda (req)
		(log-msg "TRIP ~s" req)
		(transaction-alert)
		(list (cons 'status #t))))
(http-json "/dacct"
	; default account
	(admin-gate "accounting" (req)
		(list
			(cons 'acct (to-s (dz-account)))
			(cons 'search_length (env-min-search-chars)))))
(http-json "/cracct"
	; create special account
	(admin-gate "accounting" (req)
		(let* ([acct-name (query-value req 'name)]
				[acct-desc (query-value req 'desc)]
				[existing-name (special-acct-exists? acct-name)])
			(if existing-name
				(list
					(cons 'status #f)
					(cons 'msg
						(format #f "Special account '~a' already exists."
							existing-name)))
				(list
					(cons 'status #t)
					(cons 'acct
						(to-s
							(create-special-account
								acct-name acct-desc))))))))
(http-json "/delacct"
	; delete special account
	(admin-gate "accounting" (req)
		(let ([acct-id (query-value-number req 'id)])
			(delete-account acct-id)
			(list (cons 'status #t)))))
(http-json "/charge"
	; charge for service or merchandise
	(admin-gate "accounting" (req)
		(let ([acct-id (query-value-number req 'acct)]
				[product-id (query-value-number req 'pid)]
				[other-acct (query-value-number req 'xfer_acct)]
				[quantity (max (query-value-number req 'quantity) 1)]
				[detail (query-value req 'detail)]
				[sales-tax (to-i (query-value-number req 'sales_tax))]
				[manifest (query-value-boolean req 'manifest)]
				[amount (query-value-number req 'amt)]
				[limit (query-value-number req 'limit)])
			(acct-charge acct-id other-acct amount
				quantity sales-tax product-id detail (today)
				not-manifest "" "accounting"
				(session-get req 'person-id) (sdb))
			(invalidate-balances)
			(if manifest
				(manifest-response acct-id)
				(chart-reply acct-id
					(latest-event acct-id) limit
					(authorized-for req "accounting"))))))
(http-json "/pay"
	; pay for service or merchandise
	(admin-gate "accounting" (req)
		(let ([acct-id (query-value-number req 'acct)]
				[product-id (query-value-number req 'pid)]
				[other-acct (query-value-number req 'xfer_acct)]
				[manifest (query-value-boolean req 'manifest)]
				[quantity (max (query-value-number req 'quantity) 1)]
				[detail (query-value req 'detail)]
				[amount (query-value-number req 'amt)]
				[limit (query-value-number req 'limit)])
			(acct-charge other-acct acct-id amount quantity
				0 product-id detail (today) not-manifest ""
				"accounting" (session-get req 'person-id) (sdb))
			(invalidate-balances)
			(if manifest
				(manifest-response acct-id)
				(chart-reply acct-id
					(latest-event acct-id) limit
					(authorized-for req "accounting"))))))
(http-json "/delxact"
	(admin-gate "accounting" (req)
		(let ([acct-id (query-value-number req 'acct)]
				[manifest (query-value-boolean req 'manifest)]
				[xact-id (query-value-number req 'xact)]
				[limit (query-value-number req 'limit)])
			(delete-transaction xact-id (sdb))
			(invalidate-balances)
			(if manifest
				(manifest-response acct-id)
				(chart-reply acct-id
					(latest-event acct-id) limit
					(authorized-for req "accounting"))))))
(http-json "/edits"
	(admin-gate "accounting" (req)
		(let ([edits (string-split (query-value req 'edits) #\^)]
				[vals (string-split (query-value req 'values) #\^)])
			(for-each
				(lambda (edit value)
					(let* ([match (regexp-exec xcat-edit-pat edit)]
							[col-map 
								(assq-ref xact-col-map
									(string->symbol
										(match:substring match 1)))])
						(update-transaction
							(to-i (match:substring match 2))
							(car col-map)
							((cdr col-map) value) (sdb))))
				edits vals)
			(invalidate-balances)
			(list (cons 'status #t)))))
(http-json "/setcharge"
	(admin-gate "accounting" (req)
		(let* ([item-id (query-value-number req 'item_id)]
				[row (get-charge item-id)]
				[cash-price (or (and row (pg-cell row 'cash_price)) 0)]
				[pay (query-value-boolean req 'pay)]
				[acct-balance-payment
					(and row (pg-cell row 'compute_balance))]
				[acct-id (query-value-number req 'acct)])
			(list
				(cons 'amount
					(dollars
						(if acct-balance-payment
							(clip-balance pay acct-id) cash-price)))
				(cons 'sales_tax
					(if (or pay (not (pg-cell row 'sales_tax)))
						""
						(dollars (sales-tax cash-price))))
				(cons 'fee 0)
				(cons 'pid item-id)
				(cons 'acct_xfer (pg-cell row 'acct_transfer))
				(cons 'detail
					(format #f "~a: ~a"
						(to-s (pg-cell row 'cname))
						(to-s (pg-cell row 'name))))))))
(http-json "/salestax"
	(admin-gate "accounting" (req)
		(let ([cash-price (query-value-number req 'price)])
			(list (cons 'tax (dollars (sales-tax cash-price)))))))
(http-json "/myaccount"
	(admin-gate "accounting" (req)
		(let* ([person-id (query-value-number req 'pid)]
				[acct-id (person-account person-id)])
			(list
				(cons 'status #t)
				(cons 'html
					(my-account acct-id (admin-session? req)))
				(cons 'acct acct-id)))))
(add-javascript-logger "accounting")
