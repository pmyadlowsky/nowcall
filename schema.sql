--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.slot_extras_old DROP CONSTRAINT slot_extras_item_id_fkey;
ALTER TABLE ONLY public.slot_extras_old DROP CONSTRAINT slot_extras_acct_xact_fkey;
ALTER TABLE ONLY public.slot_extras DROP CONSTRAINT slot_extras2_slot_id_fkey;
ALTER TABLE ONLY public.slot_extras DROP CONSTRAINT slot_extras2_item_id_fkey;
ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_who_fkey;
ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_role_fkey;
ALTER TABLE ONLY public.reservations DROP CONSTRAINT reservations_skydive_fkey;
ALTER TABLE ONLY public.reservations DROP CONSTRAINT reservations_media_fkey;
ALTER TABLE ONLY public.reservations_log DROP CONSTRAINT reservations_log_staff_fkey;
ALTER TABLE ONLY public.reg_tickets DROP CONSTRAINT reg_tickets_person_id_fkey;
ALTER TABLE ONLY public.product_roles DROP CONSTRAINT product_roles_role_fkey;
ALTER TABLE ONLY public.product_roles DROP CONSTRAINT product_roles_product_fkey;
ALTER TABLE ONLY public.product_item DROP CONSTRAINT prod_item_category_fkey;
ALTER TABLE ONLY public.reservations DROP CONSTRAINT pidfk;
ALTER TABLE ONLY public.app_auth DROP CONSTRAINT person_fk;
ALTER TABLE ONLY public.people_log DROP CONSTRAINT people_log_person_fkey;
ALTER TABLE ONLY public.people_log DROP CONSTRAINT people_log_author_fkey;
ALTER TABLE ONLY public.load_slots DROP CONSTRAINT load_slots_passenger_fkey;
ALTER TABLE ONLY public.load_slots DROP CONSTRAINT load_slots_load_id_fkey;
ALTER TABLE ONLY public.load_slots DROP CONSTRAINT load_slots_acct_xact_id_fkey;
ALTER TABLE ONLY public.load_sheets DROP CONSTRAINT load_sheets2_pilot_fkey;
ALTER TABLE ONLY public.load_sheets DROP CONSTRAINT load_sheets2_copilot_fkey;
ALTER TABLE ONLY public.load_sheets DROP CONSTRAINT load_sheets2_aircraft_id_fkey;
ALTER TABLE ONLY public.reservations DROP CONSTRAINT grpfk;
ALTER TABLE ONLY public.aircraft DROP CONSTRAINT aircraft_fuel_type_fkey;
ALTER TABLE ONLY public.acct_xact DROP CONSTRAINT acct_xact_to_acct_fkey;
ALTER TABLE ONLY public.acct_xact DROP CONSTRAINT acct_xact_from_acct_fkey;
ALTER TABLE ONLY public.acct_track DROP CONSTRAINT acct_track_acct_id_fkey;
ALTER TABLE ONLY public.acct DROP CONSTRAINT acct_owner_fkey;
DROP INDEX public.slot_extras_aircraft_load_date_load_num_slot_num_idx;
DROP INDEX public.roles_who_idx;
DROP INDEX public.people_nname_idx;
DROP INDEX public.people_lname_idx;
DROP INDEX public.people_fname_idx;
DROP INDEX public.load_sheets_aircraft_load_date_load_num_slot_num_idx;
DROP INDEX public.load_call_idx;
DROP INDEX public.app_auth_person_id_idx;
DROP INDEX public.acct_track_acct_id_idx;
DROP INDEX public._kv_index;
ALTER TABLE ONLY public.settings DROP CONSTRAINT settings_pkey;
ALTER TABLE ONLY public.role_key DROP CONSTRAINT role_key_pkey;
ALTER TABLE ONLY public.reservations DROP CONSTRAINT reservations_pkey;
ALTER TABLE ONLY public.reservations_log DROP CONSTRAINT reservations_log_pkey;
ALTER TABLE ONLY public.reservation_groups DROP CONSTRAINT reservation_groups_pkey;
ALTER TABLE ONLY public.product_item DROP CONSTRAINT prod_item_pkey;
ALTER TABLE ONLY public.product_category DROP CONSTRAINT prod_category_pkey;
ALTER TABLE ONLY public.people DROP CONSTRAINT people_pkey;
ALTER TABLE ONLY public.people_log DROP CONSTRAINT people_log_pkey;
ALTER TABLE ONLY public.load_slots DROP CONSTRAINT load_slots_pkey;
ALTER TABLE ONLY public.load_sheets DROP CONSTRAINT load_sheets2_pkey;
ALTER TABLE ONLY public.fuel_type DROP CONSTRAINT fuel_type_pkey;
ALTER TABLE ONLY public.aircraft DROP CONSTRAINT aircraft_pkey;
ALTER TABLE ONLY public.acct_xact DROP CONSTRAINT acct_xact_pkey;
ALTER TABLE ONLY public.acct DROP CONSTRAINT acct_pkey;
DROP TABLE public.slot_extras_old;
DROP TABLE public.slot_extras;
DROP TABLE public.settings;
DROP TABLE public.roles;
DROP TABLE public.role_key;
DROP TABLE public.reservations_log;
DROP TABLE public.reservations;
DROP TABLE public.reservation_groups;
DROP TABLE public.reg_tickets;
DROP TABLE public.product_roles;
DROP TABLE public.product_item;
DROP TABLE public.product_category;
DROP TABLE public.people_log;
DROP SEQUENCE public.people_log_seq;
DROP TABLE public.people_inactive;
DROP TABLE public.people;
DROP TABLE public.page_touch;
DROP TABLE public.load_slots;
DROP SEQUENCE public.load_slots_seq;
DROP TABLE public.load_sheets_old;
DROP TABLE public.load_sheets;
DROP SEQUENCE public.load_sheets_seq;
DROP TABLE public.load_calls;
DROP TABLE public.fuel_type;
DROP TABLE public.app_auth;
DROP TABLE public.aircraft;
DROP SEQUENCE public.aircraft_seq;
DROP TABLE public.acct_xact;
DROP TABLE public.acct_track;
DROP TABLE public.acct;
DROP SEQUENCE public.common_seq;
DROP TABLE public._kv_;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: _kv_; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE _kv_ (
    namespace character varying,
    key character varying,
    value text
);


ALTER TABLE public._kv_ OWNER TO nowcall;

--
-- Name: common_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE common_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.common_seq OWNER TO nowcall;

--
-- Name: acct; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE acct (
    id integer DEFAULT nextval('common_seq'::regclass) NOT NULL,
    owner integer,
    name character varying,
    created timestamp without time zone DEFAULT now(),
    personal boolean DEFAULT false,
    display_name character varying,
    counter_sales boolean DEFAULT false,
    terminal boolean DEFAULT false,
    home_acct boolean DEFAULT false,
    balance integer DEFAULT 0
);


ALTER TABLE public.acct OWNER TO nowcall;

--
-- Name: acct_track; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE acct_track (
    acct_id integer,
    latest_event timestamp without time zone
);


ALTER TABLE public.acct_track OWNER TO nowcall;

--
-- Name: acct_xact; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE acct_xact (
    id integer DEFAULT nextval('common_seq'::regclass) NOT NULL,
    from_acct integer NOT NULL,
    to_acct integer NOT NULL,
    entered timestamp without time zone DEFAULT now(),
    date_posted date DEFAULT ('now'::text)::date,
    amount integer NOT NULL,
    description character varying,
    reference character varying,
    updated timestamp without time zone DEFAULT now(),
    manifest boolean DEFAULT false,
    card_fee integer DEFAULT 0,
    fee_active boolean DEFAULT false,
    quantity integer DEFAULT 1,
    sales_tax integer DEFAULT 0,
    payment_in_goods boolean,
    product_id integer DEFAULT 0
);


ALTER TABLE public.acct_xact OWNER TO nowcall;

--
-- Name: aircraft_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE aircraft_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aircraft_seq OWNER TO nowcall;

--
-- Name: aircraft; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE aircraft (
    make character varying,
    model character varying,
    tail_num character varying NOT NULL,
    nickname character varying,
    capacity integer,
    fuel_cycle integer,
    active boolean DEFAULT true,
    created timestamp without time zone DEFAULT now(),
    updated timestamp without time zone,
    id integer DEFAULT nextval('aircraft_seq'::regclass) NOT NULL,
    rank integer DEFAULT 1,
    empty_weight double precision,
    basic_arm double precision,
    fuel_arm double precision,
    fuel_capacity double precision,
    fuel_burn double precision,
    pilot_arm double precision,
    avg_payload_arm double precision,
    fuel_type character varying,
    notes text
);


ALTER TABLE public.aircraft OWNER TO nowcall;

--
-- Name: app_auth; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE app_auth (
    app character varying NOT NULL,
    person_id integer NOT NULL
);


ALTER TABLE public.app_auth OWNER TO nowcall;

--
-- Name: fuel_type; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE fuel_type (
    symbol character varying NOT NULL,
    name character varying,
    density double precision,
    seq integer
);


ALTER TABLE public.fuel_type OWNER TO nowcall;

--
-- Name: load_calls; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE load_calls (
    load_date date,
    aircraft character varying,
    load_num integer,
    call_time timestamp without time zone,
    tic timestamp without time zone,
    detail character varying(8) DEFAULT 'shut'::character varying,
    detail_change timestamp without time zone DEFAULT now(),
    abort boolean DEFAULT false,
    aircraft_id integer
);


ALTER TABLE public.load_calls OWNER TO nowcall;

--
-- Name: load_sheets_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE load_sheets_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.load_sheets_seq OWNER TO nowcall;

--
-- Name: load_sheets; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE load_sheets (
    id integer DEFAULT nextval('load_sheets_seq'::regclass) NOT NULL,
    aircraft_id integer NOT NULL,
    load_date date NOT NULL,
    load_num integer NOT NULL,
    stamp timestamp without time zone,
    pilot integer,
    copilot integer,
    fuel_at_takeoff double precision,
    status character varying DEFAULT 'closed'::character varying,
    qualifier character varying DEFAULT 'shut'::character varying,
    qualifier_change timestamp without time zone,
    call_time timestamp without time zone,
    call_tic timestamp without time zone,
    call_abort boolean DEFAULT false,
    departure timestamp without time zone
);


ALTER TABLE public.load_sheets OWNER TO nowcall;

--
-- Name: load_sheets_old; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE load_sheets_old (
    aircraft character varying,
    load_date date NOT NULL,
    load_num integer NOT NULL,
    slot_num integer NOT NULL,
    stamp timestamp without time zone,
    passenger integer,
    role character varying(16),
    altitude character(2),
    reserved boolean DEFAULT false,
    locked boolean DEFAULT false,
    status character varying(10) DEFAULT 'closed'::character varying,
    transaction integer,
    has_extras boolean DEFAULT false,
    aircraft_id integer
);


ALTER TABLE public.load_sheets_old OWNER TO nowcall;

--
-- Name: load_slots_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE load_slots_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.load_slots_seq OWNER TO nowcall;

--
-- Name: load_slots; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE load_slots (
    id integer DEFAULT nextval('load_slots_seq'::regclass) NOT NULL,
    load_id integer NOT NULL,
    slot_num integer NOT NULL,
    stamp timestamp without time zone,
    passenger integer,
    role character varying,
    reserved boolean DEFAULT false,
    locked boolean DEFAULT false,
    acct_xact_id integer
);


ALTER TABLE public.load_slots OWNER TO nowcall;

--
-- Name: page_touch; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE page_touch (
    aircraft character varying,
    load_date date,
    stamp timestamp without time zone,
    aircraft_id integer
);


ALTER TABLE public.page_touch OWNER TO nowcall;

--
-- Name: people; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE people (
    id integer DEFAULT nextval('common_seq'::regclass) NOT NULL,
    last_name character varying,
    first_name character varying,
    uspa_id character varying(10),
    license character varying(16),
    nickname character varying,
    active boolean DEFAULT true,
    created timestamp without time zone DEFAULT now(),
    updated timestamp without time zone DEFAULT now(),
    uspa_expires date,
    last_repack date,
    street1 character varying,
    street2 character varying,
    city character varying,
    state character varying,
    country character varying,
    zipcode character varying,
    phone_home character varying,
    phone_mobile character varying,
    email character varying,
    emerg_contact character varying,
    birth date,
    emerg_phone character varying,
    waiver_date date,
    new boolean DEFAULT true,
    disambiguate character varying,
    page_key character varying,
    login_name character varying,
    login_password character varying,
    goes_by character varying,
    busy integer,
    gear_rental timestamp without time zone,
    last_repack_2 date,
    exit_weight integer
);


ALTER TABLE public.people OWNER TO nowcall;

--
-- Name: people_inactive; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE people_inactive (
    id integer,
    last_name character varying,
    first_name character varying,
    uspa_id character varying(10),
    license character varying(16),
    nickname character varying,
    active boolean,
    created timestamp without time zone,
    updated timestamp without time zone,
    uspa_expires date,
    last_repack date,
    street1 character varying,
    street2 character varying,
    city character varying,
    state character varying,
    country character varying,
    zipcode character varying,
    phone_home character varying,
    phone_mobile character varying,
    email character varying,
    emerg_contact character varying,
    birth date,
    emerg_phone character varying,
    waiver_date date,
    busy boolean,
    new boolean,
    disambiguate character varying,
    page_key character varying,
    login_name character varying,
    login_password character varying
);


ALTER TABLE public.people_inactive OWNER TO nowcall;

--
-- Name: people_log_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE people_log_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.people_log_seq OWNER TO nowcall;

--
-- Name: people_log; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE people_log (
    id integer DEFAULT nextval('people_log_seq'::regclass) NOT NULL,
    person integer NOT NULL,
    stamp timestamp without time zone NOT NULL,
    note text,
    author integer NOT NULL,
    highlight boolean DEFAULT false
);


ALTER TABLE public.people_log OWNER TO nowcall;

--
-- Name: product_category; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE product_category (
    id integer NOT NULL,
    name character varying,
    description character varying,
    seq integer
);


ALTER TABLE public.product_category OWNER TO nowcall;

--
-- Name: product_item; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE product_item (
    id integer NOT NULL,
    category integer,
    seq integer,
    name character varying,
    cash_price integer,
    credit_price integer,
    description character varying,
    payout boolean DEFAULT false,
    manifest_symbol character varying(10),
    manifest_apropos boolean DEFAULT true,
    divider boolean DEFAULT false,
    notes text,
    compute_balance boolean DEFAULT false,
    sales_tax boolean DEFAULT false,
    acct_transfer boolean DEFAULT false,
    manifest_extra boolean DEFAULT false,
    offer_extras boolean DEFAULT false,
    reservable boolean DEFAULT false,
    media_included boolean DEFAULT false,
    manifest_seq integer
);


ALTER TABLE public.product_item OWNER TO nowcall;

--
-- Name: product_roles; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE product_roles (
    product integer,
    role character varying(6)
);


ALTER TABLE public.product_roles OWNER TO nowcall;

--
-- Name: reg_tickets; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE reg_tickets (
    key character varying,
    email character varying,
    person_id integer,
    stamp timestamp without time zone DEFAULT now()
);


ALTER TABLE public.reg_tickets OWNER TO nowcall;

--
-- Name: reservation_groups; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE reservation_groups (
    id integer DEFAULT nextval('common_seq'::regclass) NOT NULL,
    jump_date date NOT NULL,
    created timestamp without time zone DEFAULT now(),
    updated timestamp without time zone DEFAULT now(),
    contact integer,
    class_slot time without time zone,
    hold boolean DEFAULT false
);


ALTER TABLE public.reservation_groups OWNER TO nowcall;

--
-- Name: reservations; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE reservations (
    id integer DEFAULT nextval('common_seq'::regclass) NOT NULL,
    group_id integer NOT NULL,
    name character varying,
    email character varying,
    phone character varying,
    skydive integer,
    media integer,
    paid integer DEFAULT 0,
    created timestamp without time zone DEFAULT now(),
    updated timestamp without time zone DEFAULT now(),
    person_id integer
);


ALTER TABLE public.reservations OWNER TO nowcall;

--
-- Name: reservations_log; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE reservations_log (
    id integer DEFAULT nextval('common_seq'::regclass) NOT NULL,
    res_id integer NOT NULL,
    stamp timestamp without time zone,
    staff integer,
    remark text
);


ALTER TABLE public.reservations_log OWNER TO nowcall;

--
-- Name: role_key; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE role_key (
    key character varying(6) NOT NULL,
    description character varying,
    seq integer DEFAULT 1 NOT NULL,
    stat_name character varying,
    apropos_name character varying,
    payout boolean DEFAULT false
);


ALTER TABLE public.role_key OWNER TO nowcall;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE roles (
    who integer,
    role character varying(6)
);


ALTER TABLE public.roles OWNER TO nowcall;

--
-- Name: settings; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE settings (
    sym_name character varying NOT NULL,
    long_name character varying,
    value character varying,
    seq integer,
    data_type character(1),
    stamp timestamp without time zone DEFAULT now(),
    description text
);


ALTER TABLE public.settings OWNER TO nowcall;

--
-- Name: slot_extras; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE slot_extras (
    acct_xact_id integer,
    item_id integer NOT NULL,
    slot_id integer NOT NULL
);


ALTER TABLE public.slot_extras OWNER TO nowcall;

--
-- Name: slot_extras_old; Type: TABLE; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE TABLE slot_extras_old (
    acct_xact integer,
    item_id integer,
    aircraft character varying,
    load_date date,
    load_num integer,
    slot_num integer,
    aircraft_id integer
);


ALTER TABLE public.slot_extras_old OWNER TO nowcall;

--
-- Name: acct_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY acct
    ADD CONSTRAINT acct_pkey PRIMARY KEY (id);


--
-- Name: acct_xact_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY acct_xact
    ADD CONSTRAINT acct_xact_pkey PRIMARY KEY (id);


--
-- Name: aircraft_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY aircraft
    ADD CONSTRAINT aircraft_pkey PRIMARY KEY (id);


--
-- Name: fuel_type_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY fuel_type
    ADD CONSTRAINT fuel_type_pkey PRIMARY KEY (symbol);


--
-- Name: load_sheets2_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY load_sheets
    ADD CONSTRAINT load_sheets2_pkey PRIMARY KEY (id);


--
-- Name: load_slots_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY load_slots
    ADD CONSTRAINT load_slots_pkey PRIMARY KEY (id);


--
-- Name: people_log_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY people_log
    ADD CONSTRAINT people_log_pkey PRIMARY KEY (id);


--
-- Name: people_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY people
    ADD CONSTRAINT people_pkey PRIMARY KEY (id);


--
-- Name: prod_category_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY product_category
    ADD CONSTRAINT prod_category_pkey PRIMARY KEY (id);


--
-- Name: prod_item_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY product_item
    ADD CONSTRAINT prod_item_pkey PRIMARY KEY (id);


--
-- Name: reservation_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY reservation_groups
    ADD CONSTRAINT reservation_groups_pkey PRIMARY KEY (id);


--
-- Name: reservations_log_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY reservations_log
    ADD CONSTRAINT reservations_log_pkey PRIMARY KEY (id);


--
-- Name: reservations_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY reservations
    ADD CONSTRAINT reservations_pkey PRIMARY KEY (id);


--
-- Name: role_key_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY role_key
    ADD CONSTRAINT role_key_pkey PRIMARY KEY (key);


--
-- Name: settings_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall; Tablespace: 
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (sym_name);


--
-- Name: _kv_index; Type: INDEX; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE INDEX _kv_index ON _kv_ USING btree (namespace, key);


--
-- Name: acct_track_acct_id_idx; Type: INDEX; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE INDEX acct_track_acct_id_idx ON acct_track USING btree (acct_id);


--
-- Name: app_auth_person_id_idx; Type: INDEX; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE INDEX app_auth_person_id_idx ON app_auth USING btree (person_id);


--
-- Name: load_call_idx; Type: INDEX; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE INDEX load_call_idx ON load_calls USING btree (load_date, aircraft, load_num);


--
-- Name: load_sheets_aircraft_load_date_load_num_slot_num_idx; Type: INDEX; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE INDEX load_sheets_aircraft_load_date_load_num_slot_num_idx ON load_sheets_old USING btree (aircraft, load_date, load_num, slot_num);


--
-- Name: people_fname_idx; Type: INDEX; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE INDEX people_fname_idx ON people USING btree (upper((first_name)::text));


--
-- Name: people_lname_idx; Type: INDEX; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE INDEX people_lname_idx ON people USING btree (upper((last_name)::text));


--
-- Name: people_nname_idx; Type: INDEX; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE INDEX people_nname_idx ON people USING btree (upper((nickname)::text));


--
-- Name: roles_who_idx; Type: INDEX; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE INDEX roles_who_idx ON roles USING btree (who);


--
-- Name: slot_extras_aircraft_load_date_load_num_slot_num_idx; Type: INDEX; Schema: public; Owner: nowcall; Tablespace: 
--

CREATE INDEX slot_extras_aircraft_load_date_load_num_slot_num_idx ON slot_extras_old USING btree (aircraft, load_date, load_num, slot_num);


--
-- Name: acct_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY acct
    ADD CONSTRAINT acct_owner_fkey FOREIGN KEY (owner) REFERENCES people(id);


--
-- Name: acct_track_acct_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY acct_track
    ADD CONSTRAINT acct_track_acct_id_fkey FOREIGN KEY (acct_id) REFERENCES acct(id);


--
-- Name: acct_xact_from_acct_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY acct_xact
    ADD CONSTRAINT acct_xact_from_acct_fkey FOREIGN KEY (from_acct) REFERENCES acct(id);


--
-- Name: acct_xact_to_acct_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY acct_xact
    ADD CONSTRAINT acct_xact_to_acct_fkey FOREIGN KEY (to_acct) REFERENCES acct(id);


--
-- Name: aircraft_fuel_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY aircraft
    ADD CONSTRAINT aircraft_fuel_type_fkey FOREIGN KEY (fuel_type) REFERENCES fuel_type(symbol);


--
-- Name: grpfk; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY reservations
    ADD CONSTRAINT grpfk FOREIGN KEY (group_id) REFERENCES reservation_groups(id);


--
-- Name: load_sheets2_aircraft_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY load_sheets
    ADD CONSTRAINT load_sheets2_aircraft_id_fkey FOREIGN KEY (aircraft_id) REFERENCES aircraft(id);


--
-- Name: load_sheets2_copilot_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY load_sheets
    ADD CONSTRAINT load_sheets2_copilot_fkey FOREIGN KEY (copilot) REFERENCES people(id);


--
-- Name: load_sheets2_pilot_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY load_sheets
    ADD CONSTRAINT load_sheets2_pilot_fkey FOREIGN KEY (pilot) REFERENCES people(id);


--
-- Name: load_slots_acct_xact_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY load_slots
    ADD CONSTRAINT load_slots_acct_xact_id_fkey FOREIGN KEY (acct_xact_id) REFERENCES acct_xact(id);


--
-- Name: load_slots_load_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY load_slots
    ADD CONSTRAINT load_slots_load_id_fkey FOREIGN KEY (load_id) REFERENCES load_sheets(id);


--
-- Name: load_slots_passenger_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY load_slots
    ADD CONSTRAINT load_slots_passenger_fkey FOREIGN KEY (passenger) REFERENCES people(id);


--
-- Name: people_log_author_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY people_log
    ADD CONSTRAINT people_log_author_fkey FOREIGN KEY (author) REFERENCES people(id);


--
-- Name: people_log_person_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY people_log
    ADD CONSTRAINT people_log_person_fkey FOREIGN KEY (person) REFERENCES people(id);


--
-- Name: person_fk; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY app_auth
    ADD CONSTRAINT person_fk FOREIGN KEY (person_id) REFERENCES people(id);


--
-- Name: pidfk; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY reservations
    ADD CONSTRAINT pidfk FOREIGN KEY (person_id) REFERENCES people(id);


--
-- Name: prod_item_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY product_item
    ADD CONSTRAINT prod_item_category_fkey FOREIGN KEY (category) REFERENCES product_category(id);


--
-- Name: product_roles_product_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY product_roles
    ADD CONSTRAINT product_roles_product_fkey FOREIGN KEY (product) REFERENCES product_item(id);


--
-- Name: product_roles_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY product_roles
    ADD CONSTRAINT product_roles_role_fkey FOREIGN KEY (role) REFERENCES role_key(key);


--
-- Name: reg_tickets_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY reg_tickets
    ADD CONSTRAINT reg_tickets_person_id_fkey FOREIGN KEY (person_id) REFERENCES people(id);


--
-- Name: reservations_log_staff_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY reservations_log
    ADD CONSTRAINT reservations_log_staff_fkey FOREIGN KEY (staff) REFERENCES people(id);


--
-- Name: reservations_media_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY reservations
    ADD CONSTRAINT reservations_media_fkey FOREIGN KEY (media) REFERENCES product_item(id);


--
-- Name: reservations_skydive_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY reservations
    ADD CONSTRAINT reservations_skydive_fkey FOREIGN KEY (skydive) REFERENCES product_item(id);


--
-- Name: roles_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_role_fkey FOREIGN KEY (role) REFERENCES role_key(key);


--
-- Name: roles_who_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_who_fkey FOREIGN KEY (who) REFERENCES people(id);


--
-- Name: slot_extras2_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY slot_extras
    ADD CONSTRAINT slot_extras2_item_id_fkey FOREIGN KEY (item_id) REFERENCES product_item(id);


--
-- Name: slot_extras2_slot_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY slot_extras
    ADD CONSTRAINT slot_extras2_slot_id_fkey FOREIGN KEY (slot_id) REFERENCES load_slots(id);


--
-- Name: slot_extras_acct_xact_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY slot_extras_old
    ADD CONSTRAINT slot_extras_acct_xact_fkey FOREIGN KEY (acct_xact) REFERENCES acct_xact(id);


--
-- Name: slot_extras_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY slot_extras_old
    ADD CONSTRAINT slot_extras_item_id_fkey FOREIGN KEY (item_id) REFERENCES product_item(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

