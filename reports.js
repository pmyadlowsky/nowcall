var date_format = "";
var min_search_length = 3;

function date_preset(menu) {
	var preset = menu.value;
	var today = new Date();
	if (preset == "today") {
		$("#from_date").datepicker("setDate", today);
		$("#to_date").datepicker("setDate", today);
		}
	else if (preset == "clear") {
		document.getElementById("from_date").value = "";
		document.getElementById("to_date").value = "";
		}
	else if (preset == "week") {
		var day = new Date(today.getFullYear(),
						today.getMonth(), today.getDate(), 10, 0, 0, 0);
		while (!day.toString().match(/Mon /)) {
			day = new Date(day.getTime() - 24 * 3600 * 1000);
			}
		$("#from_date").datepicker("setDate", day);
		$("#to_date").datepicker("setDate", today);
		}
	else if (preset == "month") {
		$("#from_date").datepicker("setDate",
					new Date(today.getFullYear(),
						today.getMonth(),
						1, 12, 0, 0, 0));
		$("#to_date").datepicker("setDate", today);
		}
	else if (preset == "q1") {
		$("#from_date").datepicker("setDate",
					new Date(today.getFullYear(),
						0, 1, 12, 0, 0, 0));
		$("#to_date").datepicker("setDate",
					new Date(today.getFullYear(),
						2, 31, 12, 0, 0, 0));
		}
	else if (preset == "q2") {
		$("#from_date").datepicker("setDate",
					new Date(today.getFullYear(),
						3, 1, 12, 0, 0, 0));
		$("#to_date").datepicker("setDate",
					new Date(today.getFullYear(),
						5, 30, 12, 0, 0, 0));
		}
	else if (preset == "q3") {
		$("#from_date").datepicker("setDate",
					new Date(today.getFullYear(),
						6, 1, 12, 0, 0, 0));
		$("#to_date").datepicker("setDate",
					new Date(today.getFullYear(),
						8, 30, 12, 0, 0, 0));
		}
	else if (preset == "q4") {
		$("#from_date").datepicker("setDate",
					new Date(today.getFullYear(),
						9, 1, 12, 0, 0, 0));
		$("#to_date").datepicker("setDate",
					new Date(today.getFullYear(),
						11, 31, 12, 0, 0, 0));
		}
	else if (preset == "year") {
		$("#from_date").datepicker("setDate",
					new Date(today.getFullYear(),
						0, 1, 12, 0, 0, 0));
		$("#to_date").datepicker("setDate", today);
		}
	else if (preset == "lyear") {
		var year = today.getFullYear() - 1;
		$("#from_date").datepicker("setDate",
					new Date(year,
						 0,  1, 12,  0, 0, 0));
		$("#to_date").datepicker("setDate",
					new Date(year,
						11, 31, 23, 59, 0, 0));
		}
	}
function select_person(event, ui) {
	document.getElementById("customer").value = ui.item.label;
	document.getElementById("customer-id").value = ui.item.pid;
	return true;
	}
function live_search(box) {
	box.value = "";
	$("#" + box.id).autocomplete({
		source: "/accounting/search",
		minLength: min_search_length,
		select: select_person
		});
	}
function report_handler(resp) {
	if (resp.status) {
		document.getElementById("report").innerHTML = resp.html;
		$("#spinner").hide();
		}
	else {
		$("#spinner").hide();
		local_alert("Query Incomplete", resp.msg);
		}
	}
function open_report(key) {
	var body = document.getElementById(key);
	if ((body.style.display == "none") ||
			(body.style.display == "")) body.style.display = "block";
	else body.style.display = "none";
	}
function read_date(prefix) {
	var date = $("#" + prefix + "_date").datepicker("getDate");
	if (date == null) return "";
	return $.datepicker.formatDate("yy-mm-dd", date);
	}
function parse_tags() {
	var src = document.getElementById("tags").value.trim();
	if (src == "") return "[]";
	return JSON.stringify(src.split(/\s+/));
	}
function run_report(key, spreadsheet) {
	var to_date = read_date("to");
	var from_date = read_date("from");
	var jumped_this_year =
		document.getElementById("jumped-this-year").checked;
	var customer = document.getElementById("customer-id").value;
	var temp_memb = document.getElementById("temp-memberships").checked;
	if (spreadsheet) {
		var url = "/reports/" + key + "_ss?";
		url += "to_date=" + escape(to_date);
		url += "&from_date=" + escape(from_date);
		url += "&customer=" + escape(customer);
		url += "&jty=" + (jumped_this_year ? "1" : "0")
		url += "&tmp=" + (temp_memb ? "1" : "0")
		url += "&tags=" + escape(parse_tags());
		window.location=url;
		}
	else {
		$.post("/reports/" + key, {
			from_date: from_date,
			to_date: to_date,
			jty: (jumped_this_year ? "1" : "0"),
			tmp: (temp_memb ? "1" : "0"),
			customer: document.getElementById("customer-id").value,
			tags: parse_tags()
			}, report_handler, "json");
		$("#spinner").show();
		}
	}
function toggle_tag_menu() {
	var div = document.getElementById("tag-menu");
	if (div.style.display == "none") {
		$.get("/products/alltags", {},
				function(resp) {
					div.style.display = "block";
					div.innerHTML = resp.html;
					}, "json");
		}
	else div.style.display = "none";
	}
function add_tag(tag) {
	var box = document.getElementById("tags");
	if ((box.value != "") && (box.value.match(/\s$/) == null))
		box.value += " ";
	box.value += tag;
	box.scrollLeft = box.scrollWidth;
	}
function load_tags(set_id) {
	$.get("/reports/loadtags", { preset: set_id },
			function(resp) {
				document.getElementById("tags").value = resp.tags;
				}, "json");
	}
function set_selected(menu) {
	load_tags(menu.value);
	document.getElementById("tag-remove").style.display =
		((parseInt(menu.value) >= 0) ? "inline" : "none");
	}
function drop_preset_tag() {
	var menu = document.getElementById("tag-sets");
	var text = menu.options[menu.selectedIndex].text;
	local_confirm("Tag Presets", "Yes", "No",
		function() {
			$.get("/reports/droppreset", { preset: menu.value },
					function(resp) {
						load_tag_presets(resp.next_id);
						}, "json");
			}, null,
		"Really? Drop '" + text + "'?");
	}
function load_tag_presets(preset) {
	$.get("/reports/loadpresets", {},
			function(resp) {
				if (resp.tags == null) return;
				var menu = document.getElementById("tag-sets");
				var option;
				menu.options.length = 0;
				option = new Option("", "-1");
				menu.add(option);
				for (var i = 0; i < resp.tags.length; i++) {
					option = new Option(resp.tags[i][1],
									resp.tags[i][0]);
					option.selected = (preset == resp.tags[i][0]);
					menu.add(option);
					}
				if (preset != null) load_tags(preset);
				}, "json");
	}
function close_preset_form() {
	document.getElementById("preset-name-box").style.display = "none";
	}
function open_preset_form() {
	document.getElementById("preset-name-box").style.display = "block";
	}
function save_tag_preset() {
	var tags = document.getElementById("tags").value.trim();
	var name = document.getElementById("preset-name").value.trim();
	if ((tags == "") || (name == "")) {
		close_preset_form();
		return
		}
	$.post("/reports/addpreset", { tags: tags, name: name },
			function(resp) {
				close_preset_form();
				load_tag_presets(resp.set_id);
				}, "json");
	}
function logout_handler(resp) {
	window.location = "/manifest";
	}
function logout() {
	$.get("/login/logout", {}, logout_handler, "json");
	}
function init_stage2(resp) {
	date_format = resp.format;
	min_search_length = resp.search_length;
	$("input.dp").datepicker({
		dateFormat: date_format,
		altFormat: "yy-mm-dd",
		setDate: new Date(Date.now),
		firstDay: 1,
		changeYear: true
		});
	load_tag_presets(null);
	}
function init() {
	mark_page("Reports");
	$.get("/reports/datefmt", {}, init_stage2, "json");
	}
