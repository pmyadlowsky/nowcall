#! /usr/bin/ruby

require "rubygems"
require "json"

container = ARGV[0]

json = `docker 2>/dev/null inspect #{container}`
begin
	state = JSON.parse(json)
rescue Exception => exc
	exit 1
	end
exit 1 unless state.class == Array
exit 1 if state.empty?
exit (state[0]['State']['Running'] ? 0 : 1)
