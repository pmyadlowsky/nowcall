#! /usr/bin/ruby

require "rubygems"
require "listen"
require "fileutils"

Hopper = "/hopper"
Inbox = "#{Hopper}/inbox"
Old = 3600 # sec

def log(msg)
	$logfile.puts "#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}: #{msg}"
	$logfile.flush
	$stdout.puts "#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}: #{msg}"
	end

def scrub
	dir = "#{Hopper}/done"
	Dir::entries(dir).each do |entry|
		next unless entry =~ /\.ods$/
		fullpath = "#{dir}/#{entry}"
		if (Time.now - File::mtime(fullpath)) > Old
			log "retire #{fullpath}"
			File::unlink(fullpath)
			end
		end
	end

def convert(filepath)
	log "convert #{filepath}..."
	system("/usr/local/bin/convert #{Hopper} #{filepath}")
	log "...done"
	FileUtils::rm_f(filepath)
	end

FileUtils::mkdir_p(Inbox)
FileUtils::chmod_R(0777, Hopper)
$logfile = File.new("/var/log/docproc.log", "a")

Thread::start do
	loop do
		scrub
		sleep 60
		end
	end

listener = Listen::to(Inbox, only: /\.csv$/) do |mod, add, rem|
	next unless add && (add.size > 0)
	add.each do |path|
		Thread.new { convert(path) }
		end
	end
listener.start
log "converter listening..."
sleep
