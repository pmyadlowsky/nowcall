--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: product_category; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY product_category (id, name, description, seq, internal_name) FROM stdin;
1	Skydives	\N	1	skydives
2	Gear	\N	2	\N
3	Rigging	\N	3	\N
12	Instruction	\N	4	\N
13	Photo/Video	\N	5	media
14	Packing	\N	6	\N
15	Food & Drink	\N	8	\N
16	Swag	\N	9	\N
10	Misc	\N	10	\N
17	Payment	\N	11	\N
18	DZ Operations	\N	7	\N
\.


--
-- Data for Name: product_item; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY product_item (id, category, seq, name, cash_price, credit_price, description, payout, manifest_symbol, manifest_apropos, divider, notes, compute_balance, sales_tax, acct_transfer, manifest_extra, offer_extras) FROM stdin;
44	14	25	Tandem	1600	\N	\N	t	\N	f	f	\N	f	f	f	f	f
45	15	26	Dinner	1000	\N	\N	f	\N	f	f	\N	f	f	f	f	f
46	15	27	Lunch	1000	\N	\N	f	\N	f	f	\N	f	f	f	f	f
55	15	28	Snacks	75	\N	\N	f	\N	f	f	\N	f	f	f	f	f
3	3	3	Inspection & Repack	5000	5200	\N	f	\N	f	f	\N	f	f	f	f	f
66	15	30	Water	50	\N	\N	f	\N	f	f	\N	f	f	f	f	f
43	14	24	AFF	800	\N	\N	t	\N	f	f	\N	f	f	f	f	f
40	13	5	Video Dubbing	500	\N	\N	t	\N	f	f	\N	f	f	f	f	f
41	13	6	Video Editing	5000	\N	\N	t	\N	f	f	\N	f	f	f	f	f
53	16	2	T-Shirt	2000	\N	\N	f	\N	f	f	\N	f	t	f	f	f
54	16	5	T-Shirt Long Sleeve	2500	\N	\N	f	\N	f	f	\N	f	t	f	f	f
50	16	7	Fleece Jacket	3000	\N	\N	f	\N	f	f	\N	f	t	f	f	f
51	16	8	Sweatshirt	3700	\N	\N	f	\N	f	f	\N	f	t	f	f	f
13	1	18	AFF: Category A	34500	\N	\N	f	CAT-A	t	f	\N	f	f	f	f	f
23	1	19	AFF: Category B	22900	\N	\N	f	CAT-B	t	f	\N	f	f	f	f	f
24	1	20	AFF: Category C-1	22900	\N	\N	f	CAT-C1	t	f	\N	f	f	f	f	f
6	1	1	Up-Jumper, Full Altitude	2600	2700	\N	f	FUN-HI	t	f	\N	f	f	f	f	f
78	1	29	A-License Check Dive	9500	\N	\N	f	CK-DIVE	t	f	\N	f	f	f	f	f
77	1	21	AFF: Category C-2	19900	\N	\N	f	CAT-C2	t	f	\N	f	f	f	f	f
7	1	2	Up-Jumper, Low Altitude	1800	1900	\N	f	FUN-LO	t	f	\N	f	f	f	f	f
14	1	3	Up-Jumper Recurrency Training	14000	15000	\N	f	RECUR	t	f	\N	f	f	f	f	f
71	1	4	Jump Ticket	2600	2700	\N	f	\N	f	f	\N	f	f	f	f	f
11	1	11	Tandem: Value	19900	\N	\N	f	TAN-VAL	t	f	\N	f	f	f	f	f
63	1	12	Tandem: Living Social	16500	\N	\N	f	TAN-LS	t	f	\N	f	f	f	f	f
38	13	7	Still Photography	2000	\N	\N	t	STILLS	t	f	\N	f	f	f	f	f
64	1	14	Tandem: Return	14900	\N	\N	f	TAN-RET	t	f	\N	f	f	f	f	f
25	1	22	AFF: Category D	19900	\N	\N	f	CAT-D	t	f	\N	f	f	f	f	f
26	1	24	AFF: Category E	19900	\N	\N	f	CAT-E	t	f	\N	f	f	f	f	f
67	1	6	\N	\N	\N	\N	f	\N	t	t	\N	f	f	f	f	f
9	1	8	Tandem: Premium Package	32400	\N	\N	f	TAN-PRM	t	f	\N	f	f	f	f	f
27	1	26	AFF: Category F	9500	\N	\N	f	CAT-F	t	f	\N	f	f	f	f	f
89	1	10	Tandem Military	22900	\N	\N	f	TAN-MIL	t	f	\N	f	f	f	f	f
28	1	27	AFF: Category G	9500	\N	\N	f	CAT-G	t	f	\N	f	f	f	f	f
68	1	16	\N	\N	\N	\N	f	\N	t	t	\N	f	f	f	f	f
12	1	17	AFF: Deluxe First Jump	44400	\N	\N	f	AFF-AX	t	f	\N	f	f	f	f	f
29	1	28	AFF: Category H	9500	\N	\N	f	CAT-H	t	f	\N	f	f	f	f	f
69	1	30	\N	\N	\N	\N	f	\N	t	t	\N	f	f	f	f	f
79	1	31	Beginner Solo	6600	\N	\N	f	SOLO	t	f	\N	f	f	f	f	f
30	1	32	Coached	9500	\N	\N	f	COACHED	t	f	\N	f	f	f	f	f
93	17	5	Dropzone Discount	0	\N	\N	t	\N	f	f	\N	f	f	f	f	f
72	17	1	Balance Due (cash)	100	\N	\N	t	\N	f	f	\N	t	f	f	f	f
80	17	2	Balance Due (credit card)	100	\N	\N	t	\N	f	f	\N	t	f	f	f	f
91	17	4	Transfer Credit	0	\N	\N	f	\N	f	f	\N	f	f	t	f	f
90	17	3	Transfer Debt	0	\N	\N	t	\N	f	f	\N	t	f	t	f	f
73	17	6	Jump Ticket	2600	\N	\N	t	\N	f	f	\N	f	f	f	f	f
74	17	7	Staff Payout	0	\N	\N	f	\N	f	f	\N	t	f	f	f	f
8	1	7	Tandem: Deluxe Package	34800	\N	\N	f	TAN-DLX	t	f	\N	f	f	f	f	f
81	17	8	Gift Certificate	24900	\N	\N	t	\N	f	f	\N	f	f	f	f	f
82	17	9	Cloud 9	24900	\N	\N	t	\N	f	f	\N	f	f	f	f	f
83	17	10	Groupon	15000	\N	\N	t	\N	f	f	\N	f	f	f	f	f
84	17	11	Living Social	16500	\N	\N	t	\N	f	f	\N	f	f	f	f	f
85	17	12	Adrenaline 365	24900	\N	\N	t	\N	f	f	\N	f	f	f	f	f
37	13	1	DVD Package	9900	\N	\N	f	\N	f	f	DVD and stills	f	f	f	t	f
39	13	3	Stills Only	7500	\N	\N	f	\N	f	f	\N	f	f	f	t	f
70	1	15	Tandem Over Weight Limit Fee	2000	\N	\N	f	\N	f	f	\N	f	f	f	t	f
10	1	9	Tandem: Regular	24900	\N	\N	f	TAN-REG	t	f	\N	f	f	f	f	t
62	1	13	Tandem: Groupon	15000	\N	\N	f	TAN-GPN	t	f	\N	f	f	f	f	t
32	12	1	Tandem	4000	\N	\N	t	TI	t	f	\N	f	f	f	f	f
33	12	2	Tandem, Over Weight Limit (200-230)	6000	\N	\N	t	TI-OWL1	t	f	\N	f	f	f	f	f
99	18	46	Aviation - Flight	1000	\N	\N	t	\N	f	f	\N	f	f	f	f	f
95	12	3	Tandem, Over Weight Limit (230-250)	8000	\N	\N	t	TI-OWL2	t	f	\N	f	f	f	f	f
96	12	4	Tandem, Over Weight Limit (250-up)	10000	\N	\N	t	TI-OWL3	t	f	\N	f	f	f	f	f
104	18	51	Reservations	1000	\N	\N	t	\N	f	f	\N	f	f	f	f	f
34	12	5	AFF	4500	\N	\N	t	AFFI	t	f	\N	f	f	f	f	f
35	12	6	First Jump Course	3500	\N	\N	t	\N	f	f	\N	f	f	f	f	f
36	12	7	Coaching	2000	\N	\N	t	COACH	t	f	\N	f	f	f	f	f
102	14	49	Packing Class - Teach	2500	\N	\N	t	\N	f	f	\N	f	f	f	f	f
98	18	45	Maintenance	7500	\N	\N	t	\N	f	f	per day	f	f	f	f	f
42	13	4	Video Only	2000	\N	\N	t	VID	t	f	\N	f	f	f	f	f
97	18	44	Manifest	1000	\N	\N	t	\N	f	f	per hour	f	f	f	f	f
101	14	48	Packing Class - Attend	2500	\N	\N	f	\N	f	f	\N	f	f	f	f	f
105	18	52	Staff Scheduling	10000	\N	\N	t	\N	f	f	\N	f	f	f	f	f
22	10	10	Observer Ride	3500	\N	\N	f	OBS	t	f	\N	f	f	f	f	f
103	13	50	DVD Package Payout	4000	\N	\N	t	FULLDVD	t	f	\N	f	f	f	f	f
\.


--
-- Data for Name: product_roles; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY product_roles (product, role) FROM stdin;
7	UNL
7	LIC
14	UNL
14	LIC
38	VID
78	STU
78	UNL
13	STU
23	STU
24	STU
77	STU
25	STU
26	STU
27	STU
28	STU
29	STU
8	TAN
10	TAN
62	TAN
11	TAN
63	TAN
64	TAN
12	STU
32	TI
34	AFFI
36	C
36	AFFI
6	UNL
6	LIC
9	TAN
30	STU
30	UNL
79	STU
79	UNL
89	TAN
33	TI
95	TI
96	TI
42	VID
103	VID
22	TAN
22	STU
22	OBS
22	UNL
22	LIC
22	FRIEND
22	C
\.


--
-- PostgreSQL database dump complete
--

