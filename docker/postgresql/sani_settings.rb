#! /usr/bin/ruby

# Read dropzone settings from development DB and generate a generic,
# sanitized build script.

Query = "select sym_name, long_name, seq, data_type, description, internal from settings"

Insert = "insert into settings (sym_name, long_name, seq, data_type, description, internal)"

def desc(description)
	return "NULL" if description.nil?
	return "'#{description.gsub("'", "''")}'"
	end

container = ARGV[0]
database = ARGV[1]
role = ARGV[2]

puts "delete from settings;"
IO::popen("docker exec -it #{container} psql -c \"#{Query}\" -P \"pager=off\" -A -t #{database} #{role}", "r") do |pipe|
	pipe.readlines.each do |line|
		parts = line.strip.split("|")
		puts Insert
		puts "\tvalues ('#{parts[0]}','#{parts[1]}',#{parts[2].to_i},'#{parts[3]}',#{desc(parts[4])},'#{parts[5]}');"
		end
	end

