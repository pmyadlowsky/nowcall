
-- generic admin account, password "admin"

-- Use this account to create a real user account with
-- admin privileges, then drop this one.

insert into people (new, last_name, first_name,
					login_name, login_password, page_key)
	values ('f', 'Administrator', 'Administrator', 'admin',
				'8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',
				'ADMIN-ADMIN-000000000001');
insert into roles (who, role)
	select id, 'FRIEND'
		from people
		where (login_name='admin');
insert into app_auth (app, person_id)
	select 'admin', id
		from people
		where (login_name='admin');
insert into app_auth (app, person_id)
	select 'settings', id
		from people
		where (login_name='admin');
insert into app_auth (app, person_id)
	select 'aircraft', id
		from people
		where (login_name='admin');
insert into app_auth (app, person_id)
	select 'backups', id
		from people
		where (login_name='admin');
insert into app_auth (app, person_id)
	select 'reports', id
		from people
		where (login_name='admin');
insert into app_auth (app, person_id)
	select 'people', id
		from people
		where (login_name='admin');
insert into app_auth (app, person_id)
	select 'products', id
		from people
		where (login_name='admin');
insert into app_auth (app, person_id)
	select 'accounting', id
		from people
		where (login_name='admin');
insert into app_auth (app, person_id)
	select 'manifest', id
		from people
		where (login_name='admin');
