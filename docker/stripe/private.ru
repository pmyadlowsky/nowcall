require "json"
require "stripe"
require "cgi"
require "pg"

DefaultDZName = "dropzone"

def db_conn
	begin
		PG::connect({
			:host => "database",
			:dbname => "nowcall",
			:user => "nowcall"
			})
	rescue Exception => exc
		false
		end
	end

def get_setting(conn, key, default)
	query = <<-END
		select value
			from settings
			where (sym_name='#{key}')
		END
	begin
		res = conn.exec(query)
		key = (res.getvalue(0, 0) || default).strip
		res.clear
		key
	rescue Exception => exc
		default
		end
	end

def private_key(conn)
	key = get_setting(conn, "stripe-private-key", "")
	(key.length > 0) && key
	end

def dz_name(conn)
	get_setting(conn, "dz-full-name", DefaultDZName)
	end

def fee(conn, base_amount)
	stripe_rate = get_setting(conn, "credit-card-rate", "0.0").to_f / 100.0
	stripe_flat = get_setting(conn, "credit-card-flat", "0.0").to_f * 100.0
	((base_amount * stripe_rate + stripe_flat) /
			(1.0 - stripe_rate)).round
	end

def person_name(conn, person_id)
	query = <<-END
	select last_name, coalesce(goes_by, first_name) as first_name
		from people
		where (id=#{person_id})
		END
	res = conn.exec(query)
	name = res.getvalue(0, 1) + " " + res.getvalue(0, 0)
	res.clear
	name
	end

def customer(conn, token, email, person_id, card_brand, card_last4)
	brand = card_brand.gsub("'", "''")
	query = <<-END
	select service_token
		from cards
		where (owner=#{person_id})
		END
	res = conn.exec(query)
	if res.num_tuples > 0
		id = res.getvalue(0, 0)
		res.clear
		cust = Stripe::Customer.retrieve(id)
		cust.sources.create({:source => token})
		return id
		end
	res.clear
	customer = Stripe::Customer.create(
		:source => token,
		:email => email,
		:description => person_name(conn, person_id)
		)
	query = <<-END
	insert into cards (owner, service_token, brand, last4)
		values (#{person_id}, '#{customer.id}',
			'#{brand}', '#{card_last4}')
		END
	conn.exec(query)
	customer.id
	end

def issue_charge(conn, customer_id, amount, dropzone_name)
	begin
		total = amount + fee(conn, amount)
		charge = Stripe::Charge.create(
			:amount => total,
			:currency => "usd",
			:customer => customer_id,
			:description => dropzone_name + " account credit")
		{ :success => true, :charge_id => charge.id }
	rescue Exception => exc
		{ :success => false, :msg => exc.inspect }
		end
	end

def request_data(env)
	data = {}
	if env['REQUEST_METHOD'] == "POST"
		query = env['rack.input'].read || ""
	else
		query = env['QUERY_STRING'] || ""
		end
	query.split("&").each do |item|
		key, value = item.split("=", 2)
		data[key] = CGI::unescape(value)
		end
	data
	end

def person_account(conn, person_id)
	query = <<-END
	select id
		from acct
		where (owner=#{person_id})
		and personal
		END
	res = conn.exec(query)
	id = (res.num_tuples > 0) && res.getvalue(0, 0)
	res.clear
	return id
	end

def dz_account(conn)
	query = <<-END
	select id
		from acct
		where (name='general')
		and (not personal)
		END
	res = conn.exec(query)
	id = (res.num_tuples > 0) && res.getvalue(0, 0)
	res.clear
	return id
	end

def mark_acct(conn, acct_id)
	update = <<-END
		update acct_track set
			latest_event=current_timestamp
			where (acct_id=#{acct_id})
		END
	insert = <<-END
		insert into acct_track (acct_id, latest_event)
			values (#{acct_id}, current_timestamp)
		END
	res = conn.exec(update)
	if (res.cmd_tuples < 1)
		res.clear
		res = conn.exec(insert)
		res.clear
		end
	end

def log_transaction(conn, amount, person_id, reference)
	person_acct = person_account(conn, person_id)
	dz_acct = dz_account(conn)
	now = Time.now.strftime("%Y-%m-%d %H:%M:%S")
	query = <<-END
		insert into acct_xact (from_acct, to_acct, entered,
				amount, description, reference, updated)
			values (#{dz_acct},
					#{person_acct},
					'#{now}',
					#{amount},
					'online customer account credit',
					'Stripe::#{reference}',
					'#{now}');
		update acct set
			balance=(balance + #{amount})
			where (id=#{dz_acct});
		update acct set
			balance=(balance - #{amount})
			where (id=#{person_acct});
		END
	res = conn.exec(query)
	res.clear
	mark_acct(conn, person_acct)
	mark_acct(conn, dz_acct)
	end

def cards(conn, person_id)
	query = <<-END
	select service_token
		from cards
		where (owner=#{person_id})
		END
	res = conn.exec(query)
	if res.num_tuples < 1
		res.clear
		return []
		end
	cust = res.getvalue(0, 0)
	res.clear
	objs = Stripe::Customer.retrieve(cust).sources.all(:object => "card")
	list = []
	now = Time.now
	month = now.month
	year = now.year
	objs.each do |card|
		next unless card.cvc_check == "pass"
		expire = "#{card.exp_month}/#{card.exp_year}"
		expired = (card.exp_year < year) ||
			((card.exp_year == year) && (card.exp_month < month))
		list << {
			:id => card.id,
			:label => "#{card.brand}#{card.last4} #{expire}",
			:expire => expire,
			:expired => expired
			}
		end
	list
	end

def handler(env)
	rcode = "200"
	headers = {
		"content-type" => "text/json",
		}
	resp = { "status" => false, "msg" => "" }
	unless conn = db_conn
		resp['msg'] = "unable to reach NowCall database"
		return [rcode, headers, [resp.to_json]]
		end
	key = private_key(conn)
	unless key
		conn.close
		resp['msg'] = "missing transaction key"
		return [rcode, headers, [resp.to_json]]
		end
	data = request_data(env)
	Stripe.api_key = key
	resp['status'] = true
	resp['cards'] = cards(conn, data['pid'].to_i)
	conn.close
	[rcode, headers, [resp.to_json]]
	end

run Proc.new{ |env| handler(env) }
