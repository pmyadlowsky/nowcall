require "json"
require "stripe"
require "cgi"
require "pg"
require "net/http"

DefaultDZName = "dropzone"
StripeRate = "2.9"
StripeFlat = "0.30"

def db_conn
	begin
		PG::connect({
			:host => "database",
			:dbname => "nowcall",
			:user => "nowcall"
			})
	rescue Exception => exc
		false
		end
	end

def get_setting(conn, key, default)
	query = <<-END
		select value
			from settings
			where (sym_name='#{key}')
		END
	begin
		res = conn.exec(query)
		key = (res.getvalue(0, 0) || default).strip
		res.clear
		key
	rescue Exception => exc
		default
		end
	end

def private_key(conn)
	key = get_setting(conn, "stripe-private-key", "")
	(key.length > 0) && key
	end

def person_id(conn, env)
	if env['HTTP_COOKIE'] =~ /GUSHERID=([0-9a-f]+)/
		session_id = $1
	else
		return nil
		end
	query = <<-END
	select profile
		from sessions
		where (key='#{session_id}')
		END
	out = nil
	res = conn.exec(query)
	if res.num_tuples > 0
		begin
			data = JSON::parse(res.getvalue(0, 0))
			out = {}
			out[:patron_id] = data['person-id']
			out[:proxy] = false
			out[:operator] = data['person-id'].to_i
			proxy = data['proxy-person-id']
			if proxy && (proxy.to_i != 0)
				out[:patron_id] = proxy
				out[:proxy] = true
				end
		rescue Exception => exc
			end
		end
	res.clear
	return out
	end

def dz_name(conn)
	get_setting(conn, "dz-full-name", DefaultDZName)
	end

def person_name(conn, person_id)
	query = <<-END
	select last_name, coalesce(goes_by, first_name) as first_name
		from people
		where (id=#{person_id})
		END
	res = conn.exec(query)
	name = res.getvalue(0, 1) + " " + res.getvalue(0, 0)
	res.clear
	name
	end

def get_service_token(conn, person_id)
	query = <<-END
	select service_token
		from cards
		where (owner=#{person_id.to_i})
		END
	res = conn.exec(query)
	if res.num_tuples < 1
		res.clear
		return nil
		end
	token = res.getvalue(0, 0)
	res.clear
	token
	end

def sql_safe(string)
	trim = (string || "").strip
	return "NULL" if trim.empty?
	"'#{trim.gsub("'", "''")}'"
	end

def drop_customer(conn, owner)
	query = <<-END
	select service_token
		from cards
		where (owner=#{owner.to_i})
		END
	res = conn.exec(query)
	res.each do |row|
		cust = Stripe::Customer.retrieve(row['service_token'])
		cust.delete
		end
	res.clear
	query = "delete from cards where (owner=#{owner.to_i})"
	conn.exec(query)
	end

def drop_cards(env)
	conn = db_conn
	person = person_id(conn, env)
	if person
		drop_customer(conn, person[:patron_id])
		end
	conn.close
	{ "status" => true }
	end

def customer(conn, token, email, person_id, card_brand, card_last4, keep)
	cust_id = get_service_token(conn, person_id)
	if cust_id
		begin
			token_obj = Stripe::Token.retrieve(token)
			cards = Stripe::Customer.retrieve(cust_id).sources.all(
						:object => "card")
			cards.each do |card|
				if card.fingerprint == token_obj.card.fingerprint
					return { :id => cust_id }
					end
				end
			cust = Stripe::Customer.retrieve(cust_id)
			cust.sources.create({:source => token})
			return { :id => cust_id }
		rescue Exception => exc
			return { :err => exc.message, :id => false }
			end
		end
	begin
		customer = Stripe::Customer.create(
			:source => token,
			:email => email,
			:description => person_name(conn, person_id)
			)
	rescue Exception => exc
		return { :err => exc.message, :id => false }
		end
	if keep
		query = <<-END
		insert into cards (owner, service_token, brand, last4)
			values (#{person_id}, '#{customer.id}',
				#{sql_safe(card_brand)}, '#{card_last4}')
			END
		conn.exec(query)
		end
	{ :id => customer.id }
	end

def issue_charge(conn, customer_id, total, dropzone_name)
	begin
		charge = Stripe::Charge.create(
			:amount => total,
			:currency => "usd",
			:customer => customer_id,
			:description => dropzone_name + " account credit")
		{ :success => true, :charge_id => charge.id }
	rescue Exception => exc
		{ :success => false, :msg => exc.inspect }
		end
	end

def pay_cash(env)
	out = { 'status' => false }
	conn = db_conn
	person = person_id(conn, env)
	unless person[:proxy]
		out['msg'] = "Online cash payments not permitted."
		conn.close
		return out
		end
	data = post_data(env)
	out['status'] = true
	log_transaction(conn, data['amount'].to_i, person[:patron_id], "",
				"cash payment, DZ staff", "accounting", person[:operator])
	conn.close
	out
	end

def pay_offline_card(env)
	out = { 'status' => false }
	conn = db_conn
	person = person_id(conn, env)
	unless person[:proxy]
		out['msg'] = "Offline card payments not permitted."
		conn.close
		return out
		end
	data = post_data(env)
	out['status'] = true
	log_transaction(conn, data['amount'].to_i, person[:patron_id], "",
				"offline card payment, DZ staff",
				"accounting", person[:operator])
	conn.close
	out
	end

def charge_card(env)
	# charge registered card
	data = post_data(env)
	card_id = data['card']
	amount = data['amount'].to_i
	fee = data['fee'].to_i
	conn = db_conn
	person = person_id(conn, env)
	pid = (person ? person[:patron_id] : nil)
	customer_id = get_service_token(conn, pid)
	out = { 'status' => false }
	unless customer_id
		out['msg'] = "no customer ID:#{pid}"
		conn.close
		return out
		end
	begin
		charge = Stripe::Charge.create(
			:amount => amount + fee,
			:currency => "usd",
			:customer => customer_id,
			:source => card_id,
			:description => dz_name(conn) + " account credit")
		profile = trans_profile(person)
		log_transaction(conn, amount, pid, charge.id,
					profile[:description], profile[:source],
					profile[:id])
		out['status'] = true
	rescue Exception => exc
		out['msg'] = exc.message
		end
	conn.close
	out
	end

def post_data(env)
	data = {}
	env['rack.input'].read.split("&").each do |item|
		key, value = item.split("=", 2)
		data[key] = CGI::unescape(value)
		end
	data
	end

def person_account(conn, person_id)
	query = <<-END
	select id
		from acct
		where (owner=#{person_id})
		and personal
		END
	res = conn.exec(query)
	id = (res.num_tuples > 0) && res.getvalue(0, 0)
	res.clear
	return id
	end

def dz_account(conn)
	query = <<-END
	select id
		from acct
		where (name='general')
		and (not personal)
		END
	res = conn.exec(query)
	id = (res.num_tuples > 0) && res.getvalue(0, 0)
	res.clear
	return id
	end

def mark_acct(conn, acct_id)
	update = <<-END
		update acct_track set
			latest_event=current_timestamp
			where (acct_id=#{acct_id})
		END
	insert = <<-END
		insert into acct_track (acct_id, latest_event)
			values (#{acct_id}, current_timestamp)
		END
	res = conn.exec(update)
	if (res.cmd_tuples < 1)
		res.clear
		res = conn.exec(insert)
		res.clear
		end
	uri = URI("http://app-accounting:8080/trip")
	Net::HTTP.get(uri)
	end

def log_transaction(conn, amount, person_id, charge_id,
					description, source_type, source_id)
	person_acct = person_account(conn, person_id)
	dz_acct = dz_account(conn)
	now = Time.now.strftime("%Y-%m-%d %H:%M:%S")
	sql_charge = (charge_id.empty? ? "NULL" : "'#{charge_id}'")
	query = <<-END
		insert into acct_xact (from_acct, to_acct, entered,
				amount, description, charge_id,
				source_type, source_id, updated)
			values (#{dz_acct},
					#{person_acct},
					'#{now}',
					#{amount},
					#{sql_safe(description)},
					#{sql_charge},
					'#{source_type}',
					#{source_id == 0 ? "NULL" : source_id},
					'#{now}');
		update acct set
			balance=(balance + #{amount})
			where (id=#{dz_acct});
		update acct set
			balance=(balance - #{amount})
			where (id=#{person_acct});
		END
	res = conn.exec(query)
	res.clear
	mark_acct(conn, person_acct)
	mark_acct(conn, dz_acct)
	end

def cards(env)
	conn = db_conn
	data = post_data(env)
	if data['custid']
		cust = data['custid']
	else
		cust = get_service_token(conn, person_id(conn, env)[:patron_id])
		end
	unless cust
		conn.close
		return []
		end
	key = private_key(conn)
	conn.close
	return [] unless key
	Stripe.api_key = key
	begin
		objs = Stripe::Customer.retrieve(cust).sources.all(
					:object => "card")
	rescue Exception => exc
		return []
		end
	list = []
	now = Time.now
	month = now.month
	year = now.year
	objs.each do |card|
		next if card.cvc_check == "fail"
		expire = "#{card.exp_month}/#{card.exp_year}"
		expired = (card.exp_year < year) ||
			((card.exp_year == year) && (card.exp_month < month))
		list << {
			'id' => card.id,
			'label' => "#{card.brand}-#{card.last4} #{expire}",
			'expired' => expired
			}
		end
	list
	end

def check_svc
	conn = db_conn
	query = <<-END
	select sym_name
		from settings
		where (value is not null)
			and ((sym_name='stripe-public-key')
				or (sym_name='stripe-private-key'))
		END
	res = conn.exec(query)
	keys = (res.num_tuples >= 2)
	res.clear
	unless keys
		conn.close
		return { "status" => false, "err" => "keys" }
		end
	Stripe.api_key = private_key(conn)
	conn.close
	begin
		obj = Stripe::Balance.retrieve
		{ "status" => true }
	rescue Exception => exc
		{ "status" => false, "err" => "connection" }
		end
	end

def payment_config
	conn = db_conn
	pkey = private_key(conn)
	out = {
		"public_key" => get_setting(conn, "stripe-public-key", ""),
		"logo" => "/img/vsc_logo.png",
		"dz_short_name" => get_setting(conn, "dz-short-name", "DZ"),
		"stripe_rate" =>
			(pkey ? StripeRate :
				get_setting(conn, "credit-card-rate", "0.0")).to_f,
		"stripe_flat" =>
			(pkey ? StripeFlat :
			 	get_setting(conn, "credit-card-flat", "0.0")).to_f
		}
	conn.close
	out
	end

def trans_profile(person)
	profile = {}
	if person[:proxy]
		profile[:description] = "card payment, DZ staff"
		profile[:source] = "accounting"
		profile[:id] = person[:operator]
	else
		profile[:description] = "card payment, patron"
		profile[:source] = "patron"
		profile[:id] = person[:patron_id]
		end
	return profile
	end

def get_charge_id(conn, xact_id)
	query = <<-END
	select charge_id
		from acct_xact
		where (id=#{xact_id})
		and (charge_id is not null)
		END
	res = conn.exec(query)
	if res.num_tuples < 1
		res.clear
		return nil
		end
	cid = res.getvalue(0, 0)
	res.clear
	cid
	end

def cancel_charge(env)
	data = post_data(env)
	conn = db_conn
	ch_id = get_charge_id(conn, data['xact_id'].to_i)
	key = private_key(conn)
	conn.close
	return { 'status' => true } unless ch_id
	Stripe.api_key = key
	out = { 'status' => false }
	begin
		refund = Stripe::Refund.create(:charge => ch_id)
		out['status'] = true
	rescue Exception => exc
		out['msg'] = exc.message
		end
	out
	end

def handler(env)
	path = env['PATH_INFO']
	if path == "/cards"
		return ["200",
					{ "content-type" => "text/json" },
					[{'opts' => cards(env)}.to_json]]
		end
	if path == "/card"
		return ["200",
					{ "content-type" => "text/json" },
					[charge_card(env).to_json]]
		end
	if path == "/config"
		return ["200",
					{ "content-type" => "text/json" },
					[payment_config.to_json]]
		end
	if path == "/cash"
		return ["200",
					{ "content-type" => "text/json" },
					[pay_cash(env).to_json]]
		end
	if path == "/offline"
		return ["200",
					{ "content-type" => "text/json" },
					[pay_offline_card(env).to_json]]
		end
	if path == "/cancel"
		return ["200",
					{ "content-type" => "text/json" },
					[cancel_charge(env).to_json]]
		end
	if path == "/check"
		return ["200",
					{ "content-type" => "text/json" },
					[check_svc.to_json]]
		end
	if path == "/drop"
		return ["200",
					{ "content-type" => "text/json" },
					[drop_cards(env).to_json]]
		end
	# new card processing follows:
	# path == "/"
	rcode = "200"
	headers = {
		"content-type" => "text/json",
		}
	resp = { "status" => false, "msg" => "", "info" => env.inspect }
	unless conn = db_conn
		resp['msg'] = "unable to reach NowCall database"
		return [rcode, headers, [resp.to_json]]
		end
	key = private_key(conn)
	unless key
		conn.close
		resp['msg'] = "missing transaction key"
		return [rcode, headers, [resp.to_json]]
		end
	data = post_data(env)
	person = person_id(conn, env)
	pid = person[:patron_id]
	resp['pid'] = pid
	unless pid
		conn.close
		resp['msg'] = "can't determine user's identity"
		return [rcode, headers, [resp.to_json]]
		end
	amount = data['amount'].to_i
	fee = data['fee'].to_i
	Stripe.api_key = key
	drop_customer(conn, pid)
	cust = customer(conn, data['token'], data['email'],
					pid, data['card_brand'], data['card_last4'],
					(data['keep'].to_i > 0))
	if cust[:err]
		resp['msg'] = cust[:err]
	else
		res = issue_charge(conn, cust[:id], amount + fee, dz_name(conn))
		if res[:success]
			profile = trans_profile(person)
			log_transaction(conn, amount, pid, res[:charge_id],
						profile[:description], profile[:source],
						profile[:id])
			resp['status'] = true
		else
			resp['msg'] = res[:msg]
			end
		end
	conn.close
	[rcode, headers, [resp.to_json]]
	end

run Proc.new{ |env| handler(env) }
