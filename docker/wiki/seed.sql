--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: media_type; Type: TYPE; Schema: public; Owner: nowcall
--

CREATE TYPE media_type AS ENUM (
    'UNKNOWN',
    'BITMAP',
    'DRAWING',
    'AUDIO',
    'VIDEO',
    'MULTIMEDIA',
    'OFFICE',
    'TEXT',
    'EXECUTABLE',
    'ARCHIVE'
);


ALTER TYPE media_type OWNER TO nowcall;

--
-- Name: add_interwiki(text, integer, smallint); Type: FUNCTION; Schema: public; Owner: nowcall
--

CREATE FUNCTION add_interwiki(text, integer, smallint) RETURNS integer
    LANGUAGE sql
    AS $_$
 INSERT INTO interwiki (iw_prefix, iw_url, iw_local) VALUES ($1,$2,$3);
 SELECT 1;
 $_$;


ALTER FUNCTION public.add_interwiki(text, integer, smallint) OWNER TO nowcall;

--
-- Name: page_deleted(); Type: FUNCTION; Schema: public; Owner: nowcall
--

CREATE FUNCTION page_deleted() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
 DELETE FROM recentchanges WHERE rc_namespace = OLD.page_namespace AND rc_title = OLD.page_title;
 RETURN NULL;
 END;
 $$;


ALTER FUNCTION public.page_deleted() OWNER TO nowcall;

--
-- Name: ts2_page_text(); Type: FUNCTION; Schema: public; Owner: nowcall
--

CREATE FUNCTION ts2_page_text() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
 IF TG_OP = 'INSERT' THEN
 NEW.textvector = to_tsvector(NEW.old_text);
 ELSIF NEW.old_text != OLD.old_text THEN
 NEW.textvector := to_tsvector(NEW.old_text);
 END IF;
 RETURN NEW;
 END;
 $$;


ALTER FUNCTION public.ts2_page_text() OWNER TO nowcall;

--
-- Name: ts2_page_title(); Type: FUNCTION; Schema: public; Owner: nowcall
--

CREATE FUNCTION ts2_page_title() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
 IF TG_OP = 'INSERT' THEN
 NEW.titlevector = to_tsvector(REPLACE(NEW.page_title,'/',' '));
 ELSIF NEW.page_title != OLD.page_title THEN
 NEW.titlevector := to_tsvector(REPLACE(NEW.page_title,'/',' '));
 END IF;
 RETURN NEW;
 END;
 $$;


ALTER FUNCTION public.ts2_page_title() OWNER TO nowcall;

--
-- Name: archive_ar_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE archive_ar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE archive_ar_id_seq OWNER TO nowcall;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: archive; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE archive (
    ar_id integer DEFAULT nextval('archive_ar_id_seq'::regclass) NOT NULL,
    ar_namespace smallint NOT NULL,
    ar_title text NOT NULL,
    ar_text text,
    ar_page_id integer,
    ar_parent_id integer,
    ar_sha1 text DEFAULT ''::text NOT NULL,
    ar_comment text,
    ar_user integer,
    ar_user_text text NOT NULL,
    ar_timestamp timestamp with time zone NOT NULL,
    ar_minor_edit smallint DEFAULT 0 NOT NULL,
    ar_flags text,
    ar_rev_id integer,
    ar_text_id integer,
    ar_deleted smallint DEFAULT 0 NOT NULL,
    ar_len integer,
    ar_content_model text,
    ar_content_format text
);


ALTER TABLE archive OWNER TO nowcall;

--
-- Name: bot_passwords; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE bot_passwords (
    bp_user integer NOT NULL,
    bp_app_id text NOT NULL,
    bp_password text NOT NULL,
    bp_token text NOT NULL,
    bp_restrictions text NOT NULL,
    bp_grants text NOT NULL
);


ALTER TABLE bot_passwords OWNER TO nowcall;

--
-- Name: category_cat_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE category_cat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE category_cat_id_seq OWNER TO nowcall;

--
-- Name: category; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE category (
    cat_id integer DEFAULT nextval('category_cat_id_seq'::regclass) NOT NULL,
    cat_title text NOT NULL,
    cat_pages integer DEFAULT 0 NOT NULL,
    cat_subcats integer DEFAULT 0 NOT NULL,
    cat_files integer DEFAULT 0 NOT NULL,
    cat_hidden smallint DEFAULT 0 NOT NULL
);


ALTER TABLE category OWNER TO nowcall;

--
-- Name: categorylinks; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE categorylinks (
    cl_from integer NOT NULL,
    cl_to text NOT NULL,
    cl_sortkey text,
    cl_timestamp timestamp with time zone NOT NULL,
    cl_sortkey_prefix text DEFAULT ''::text NOT NULL,
    cl_collation text DEFAULT 0 NOT NULL,
    cl_type text DEFAULT 'page'::text NOT NULL
);


ALTER TABLE categorylinks OWNER TO nowcall;

--
-- Name: change_tag_ct_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE change_tag_ct_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE change_tag_ct_id_seq OWNER TO nowcall;

--
-- Name: change_tag; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE change_tag (
    ct_id integer DEFAULT nextval('change_tag_ct_id_seq'::regclass) NOT NULL,
    ct_rc_id integer,
    ct_log_id integer,
    ct_rev_id integer,
    ct_tag text NOT NULL,
    ct_params text
);


ALTER TABLE change_tag OWNER TO nowcall;

--
-- Name: externallinks_el_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE externallinks_el_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE externallinks_el_id_seq OWNER TO nowcall;

--
-- Name: externallinks; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE externallinks (
    el_id integer DEFAULT nextval('externallinks_el_id_seq'::regclass) NOT NULL,
    el_from integer NOT NULL,
    el_to text NOT NULL,
    el_index text NOT NULL
);


ALTER TABLE externallinks OWNER TO nowcall;

--
-- Name: filearchive_fa_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE filearchive_fa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE filearchive_fa_id_seq OWNER TO nowcall;

--
-- Name: filearchive; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE filearchive (
    fa_id integer DEFAULT nextval('filearchive_fa_id_seq'::regclass) NOT NULL,
    fa_name text NOT NULL,
    fa_archive_name text,
    fa_storage_group text,
    fa_storage_key text,
    fa_deleted_user integer,
    fa_deleted_timestamp timestamp with time zone NOT NULL,
    fa_deleted_reason text,
    fa_size integer NOT NULL,
    fa_width integer NOT NULL,
    fa_height integer NOT NULL,
    fa_metadata bytea DEFAULT '\x'::bytea NOT NULL,
    fa_bits smallint,
    fa_media_type text,
    fa_major_mime text DEFAULT 'unknown'::text,
    fa_minor_mime text DEFAULT 'unknown'::text,
    fa_description text NOT NULL,
    fa_user integer,
    fa_user_text text NOT NULL,
    fa_timestamp timestamp with time zone,
    fa_deleted smallint DEFAULT 0 NOT NULL,
    fa_sha1 text DEFAULT ''::text NOT NULL
);


ALTER TABLE filearchive OWNER TO nowcall;

--
-- Name: image; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE image (
    img_name text NOT NULL,
    img_size integer NOT NULL,
    img_width integer NOT NULL,
    img_height integer NOT NULL,
    img_metadata bytea DEFAULT '\x'::bytea NOT NULL,
    img_bits smallint,
    img_media_type text,
    img_major_mime text DEFAULT 'unknown'::text,
    img_minor_mime text DEFAULT 'unknown'::text,
    img_description text NOT NULL,
    img_user integer,
    img_user_text text NOT NULL,
    img_timestamp timestamp with time zone,
    img_sha1 text DEFAULT ''::text NOT NULL
);


ALTER TABLE image OWNER TO nowcall;

--
-- Name: imagelinks; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE imagelinks (
    il_from integer NOT NULL,
    il_from_namespace integer DEFAULT 0 NOT NULL,
    il_to text NOT NULL
);


ALTER TABLE imagelinks OWNER TO nowcall;

--
-- Name: interwiki; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE interwiki (
    iw_prefix text NOT NULL,
    iw_url text NOT NULL,
    iw_local smallint NOT NULL,
    iw_trans smallint DEFAULT 0 NOT NULL,
    iw_api text DEFAULT ''::text NOT NULL,
    iw_wikiid text DEFAULT ''::text NOT NULL
);


ALTER TABLE interwiki OWNER TO nowcall;

--
-- Name: ipblocks_ipb_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE ipblocks_ipb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ipblocks_ipb_id_seq OWNER TO nowcall;

--
-- Name: ipblocks; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE ipblocks (
    ipb_id integer DEFAULT nextval('ipblocks_ipb_id_seq'::regclass) NOT NULL,
    ipb_address text,
    ipb_user integer,
    ipb_by integer NOT NULL,
    ipb_by_text text DEFAULT ''::text NOT NULL,
    ipb_reason text NOT NULL,
    ipb_timestamp timestamp with time zone NOT NULL,
    ipb_auto smallint DEFAULT 0 NOT NULL,
    ipb_anon_only smallint DEFAULT 0 NOT NULL,
    ipb_create_account smallint DEFAULT 1 NOT NULL,
    ipb_enable_autoblock smallint DEFAULT 1 NOT NULL,
    ipb_expiry timestamp with time zone NOT NULL,
    ipb_range_start text,
    ipb_range_end text,
    ipb_deleted smallint DEFAULT 0 NOT NULL,
    ipb_block_email smallint DEFAULT 0 NOT NULL,
    ipb_allow_usertalk smallint DEFAULT 0 NOT NULL,
    ipb_parent_block_id integer
);


ALTER TABLE ipblocks OWNER TO nowcall;

--
-- Name: iwlinks; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE iwlinks (
    iwl_from integer DEFAULT 0 NOT NULL,
    iwl_prefix text DEFAULT ''::text NOT NULL,
    iwl_title text DEFAULT ''::text NOT NULL
);


ALTER TABLE iwlinks OWNER TO nowcall;

--
-- Name: job_job_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE job_job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE job_job_id_seq OWNER TO nowcall;

--
-- Name: job; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE job (
    job_id integer DEFAULT nextval('job_job_id_seq'::regclass) NOT NULL,
    job_cmd text NOT NULL,
    job_namespace smallint NOT NULL,
    job_title text NOT NULL,
    job_timestamp timestamp with time zone,
    job_params text NOT NULL,
    job_random integer DEFAULT 0 NOT NULL,
    job_attempts integer DEFAULT 0 NOT NULL,
    job_token text DEFAULT ''::text NOT NULL,
    job_token_timestamp timestamp with time zone,
    job_sha1 text DEFAULT ''::text NOT NULL
);


ALTER TABLE job OWNER TO nowcall;

--
-- Name: l10n_cache; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE l10n_cache (
    lc_lang text NOT NULL,
    lc_key text NOT NULL,
    lc_value bytea NOT NULL
);


ALTER TABLE l10n_cache OWNER TO nowcall;

--
-- Name: langlinks; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE langlinks (
    ll_from integer NOT NULL,
    ll_lang text,
    ll_title text
);


ALTER TABLE langlinks OWNER TO nowcall;

--
-- Name: log_search; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE log_search (
    ls_field text NOT NULL,
    ls_value text NOT NULL,
    ls_log_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE log_search OWNER TO nowcall;

--
-- Name: logging_log_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE logging_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE logging_log_id_seq OWNER TO nowcall;

--
-- Name: logging; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE logging (
    log_id integer DEFAULT nextval('logging_log_id_seq'::regclass) NOT NULL,
    log_type text NOT NULL,
    log_action text NOT NULL,
    log_timestamp timestamp with time zone NOT NULL,
    log_user integer,
    log_namespace smallint NOT NULL,
    log_title text NOT NULL,
    log_comment text,
    log_params text,
    log_deleted smallint DEFAULT 0 NOT NULL,
    log_user_text text DEFAULT ''::text NOT NULL,
    log_page integer
);


ALTER TABLE logging OWNER TO nowcall;

--
-- Name: module_deps; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE module_deps (
    md_module text NOT NULL,
    md_skin text NOT NULL,
    md_deps text NOT NULL
);


ALTER TABLE module_deps OWNER TO nowcall;

--
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE user_user_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_user_id_seq OWNER TO nowcall;

--
-- Name: mwuser; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE mwuser (
    user_id integer DEFAULT nextval('user_user_id_seq'::regclass) NOT NULL,
    user_name text NOT NULL,
    user_real_name text,
    user_password text,
    user_newpassword text,
    user_newpass_time timestamp with time zone,
    user_token text,
    user_email text,
    user_email_token text,
    user_email_token_expires timestamp with time zone,
    user_email_authenticated timestamp with time zone,
    user_touched timestamp with time zone,
    user_registration timestamp with time zone,
    user_editcount integer,
    user_password_expires timestamp with time zone
);


ALTER TABLE mwuser OWNER TO nowcall;

--
-- Name: objectcache; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE objectcache (
    keyname text,
    value bytea DEFAULT '\x'::bytea NOT NULL,
    exptime timestamp with time zone NOT NULL
);


ALTER TABLE objectcache OWNER TO nowcall;

--
-- Name: oldimage; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE oldimage (
    oi_name text NOT NULL,
    oi_archive_name text NOT NULL,
    oi_size integer NOT NULL,
    oi_width integer NOT NULL,
    oi_height integer NOT NULL,
    oi_bits smallint,
    oi_description text,
    oi_user integer,
    oi_user_text text NOT NULL,
    oi_timestamp timestamp with time zone,
    oi_metadata bytea DEFAULT '\x'::bytea NOT NULL,
    oi_media_type text,
    oi_major_mime text DEFAULT 'unknown'::text,
    oi_minor_mime text DEFAULT 'unknown'::text,
    oi_deleted smallint DEFAULT 0 NOT NULL,
    oi_sha1 text DEFAULT ''::text NOT NULL
);


ALTER TABLE oldimage OWNER TO nowcall;

--
-- Name: page_page_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE page_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE page_page_id_seq OWNER TO nowcall;

--
-- Name: page; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE page (
    page_id integer DEFAULT nextval('page_page_id_seq'::regclass) NOT NULL,
    page_namespace smallint NOT NULL,
    page_title text NOT NULL,
    page_restrictions text,
    page_is_redirect smallint DEFAULT 0 NOT NULL,
    page_is_new smallint DEFAULT 0 NOT NULL,
    page_random numeric(15,14) DEFAULT random() NOT NULL,
    page_touched timestamp with time zone,
    page_links_updated timestamp with time zone,
    page_latest integer NOT NULL,
    page_len integer NOT NULL,
    page_content_model text,
    page_lang text,
    titlevector tsvector
);


ALTER TABLE page OWNER TO nowcall;

--
-- Name: page_props; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE page_props (
    pp_page integer NOT NULL,
    pp_propname text NOT NULL,
    pp_value text NOT NULL,
    pp_sortkey double precision
);


ALTER TABLE page_props OWNER TO nowcall;

--
-- Name: page_restrictions_pr_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE page_restrictions_pr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE page_restrictions_pr_id_seq OWNER TO nowcall;

--
-- Name: page_restrictions; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE page_restrictions (
    pr_id integer DEFAULT nextval('page_restrictions_pr_id_seq'::regclass) NOT NULL,
    pr_page integer NOT NULL,
    pr_type text NOT NULL,
    pr_level text NOT NULL,
    pr_cascade smallint NOT NULL,
    pr_user integer,
    pr_expiry timestamp with time zone
);


ALTER TABLE page_restrictions OWNER TO nowcall;

--
-- Name: text_old_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE text_old_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE text_old_id_seq OWNER TO nowcall;

--
-- Name: pagecontent; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE pagecontent (
    old_id integer DEFAULT nextval('text_old_id_seq'::regclass) NOT NULL,
    old_text text,
    old_flags text,
    textvector tsvector
);


ALTER TABLE pagecontent OWNER TO nowcall;

--
-- Name: pagelinks; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE pagelinks (
    pl_from integer NOT NULL,
    pl_from_namespace integer DEFAULT 0 NOT NULL,
    pl_namespace smallint NOT NULL,
    pl_title text NOT NULL
);


ALTER TABLE pagelinks OWNER TO nowcall;

--
-- Name: profiling; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE profiling (
    pf_count integer DEFAULT 0 NOT NULL,
    pf_time double precision DEFAULT 0 NOT NULL,
    pf_memory double precision DEFAULT 0 NOT NULL,
    pf_name text NOT NULL,
    pf_server text
);


ALTER TABLE profiling OWNER TO nowcall;

--
-- Name: protected_titles; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE protected_titles (
    pt_namespace smallint NOT NULL,
    pt_title text NOT NULL,
    pt_user integer,
    pt_reason text,
    pt_timestamp timestamp with time zone NOT NULL,
    pt_expiry timestamp with time zone,
    pt_create_perm text DEFAULT ''::text NOT NULL
);


ALTER TABLE protected_titles OWNER TO nowcall;

--
-- Name: querycache; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE querycache (
    qc_type text NOT NULL,
    qc_value integer NOT NULL,
    qc_namespace smallint NOT NULL,
    qc_title text NOT NULL
);


ALTER TABLE querycache OWNER TO nowcall;

--
-- Name: querycache_info; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE querycache_info (
    qci_type text,
    qci_timestamp timestamp with time zone
);


ALTER TABLE querycache_info OWNER TO nowcall;

--
-- Name: querycachetwo; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE querycachetwo (
    qcc_type text NOT NULL,
    qcc_value integer DEFAULT 0 NOT NULL,
    qcc_namespace integer DEFAULT 0 NOT NULL,
    qcc_title text DEFAULT ''::text NOT NULL,
    qcc_namespacetwo integer DEFAULT 0 NOT NULL,
    qcc_titletwo text DEFAULT ''::text NOT NULL
);


ALTER TABLE querycachetwo OWNER TO nowcall;

--
-- Name: recentchanges_rc_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE recentchanges_rc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recentchanges_rc_id_seq OWNER TO nowcall;

--
-- Name: recentchanges; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE recentchanges (
    rc_id integer DEFAULT nextval('recentchanges_rc_id_seq'::regclass) NOT NULL,
    rc_timestamp timestamp with time zone NOT NULL,
    rc_cur_time timestamp with time zone,
    rc_user integer,
    rc_user_text text NOT NULL,
    rc_namespace smallint NOT NULL,
    rc_title text NOT NULL,
    rc_comment text,
    rc_minor smallint DEFAULT 0 NOT NULL,
    rc_bot smallint DEFAULT 0 NOT NULL,
    rc_new smallint DEFAULT 0 NOT NULL,
    rc_cur_id integer,
    rc_this_oldid integer NOT NULL,
    rc_last_oldid integer NOT NULL,
    rc_type smallint DEFAULT 0 NOT NULL,
    rc_source text NOT NULL,
    rc_patrolled smallint DEFAULT 0 NOT NULL,
    rc_ip cidr,
    rc_old_len integer,
    rc_new_len integer,
    rc_deleted smallint DEFAULT 0 NOT NULL,
    rc_logid integer DEFAULT 0 NOT NULL,
    rc_log_type text,
    rc_log_action text,
    rc_params text
);


ALTER TABLE recentchanges OWNER TO nowcall;

--
-- Name: redirect; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE redirect (
    rd_from integer NOT NULL,
    rd_namespace smallint NOT NULL,
    rd_title text NOT NULL,
    rd_interwiki text,
    rd_fragment text
);


ALTER TABLE redirect OWNER TO nowcall;

--
-- Name: revision_rev_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE revision_rev_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE revision_rev_id_seq OWNER TO nowcall;

--
-- Name: revision; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE revision (
    rev_id integer DEFAULT nextval('revision_rev_id_seq'::regclass) NOT NULL,
    rev_page integer,
    rev_text_id integer,
    rev_comment text,
    rev_user integer NOT NULL,
    rev_user_text text NOT NULL,
    rev_timestamp timestamp with time zone NOT NULL,
    rev_minor_edit smallint DEFAULT 0 NOT NULL,
    rev_deleted smallint DEFAULT 0 NOT NULL,
    rev_len integer,
    rev_parent_id integer,
    rev_sha1 text DEFAULT ''::text NOT NULL,
    rev_content_model text,
    rev_content_format text
);


ALTER TABLE revision OWNER TO nowcall;

--
-- Name: site_identifiers; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE site_identifiers (
    si_site integer NOT NULL,
    si_type text NOT NULL,
    si_key text NOT NULL
);


ALTER TABLE site_identifiers OWNER TO nowcall;

--
-- Name: site_stats; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE site_stats (
    ss_row_id integer NOT NULL,
    ss_total_edits integer DEFAULT 0,
    ss_good_articles integer DEFAULT 0,
    ss_total_pages integer DEFAULT '-1'::integer,
    ss_users integer DEFAULT '-1'::integer,
    ss_active_users integer DEFAULT '-1'::integer,
    ss_admins integer DEFAULT '-1'::integer,
    ss_images integer DEFAULT 0
);


ALTER TABLE site_stats OWNER TO nowcall;

--
-- Name: sites_site_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE sites_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sites_site_id_seq OWNER TO nowcall;

--
-- Name: sites; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE sites (
    site_id integer DEFAULT nextval('sites_site_id_seq'::regclass) NOT NULL,
    site_global_key text NOT NULL,
    site_type text NOT NULL,
    site_group text NOT NULL,
    site_source text NOT NULL,
    site_language text NOT NULL,
    site_protocol text NOT NULL,
    site_domain text NOT NULL,
    site_data text NOT NULL,
    site_forward smallint NOT NULL,
    site_config text NOT NULL
);


ALTER TABLE sites OWNER TO nowcall;

--
-- Name: tag_summary_ts_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE tag_summary_ts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tag_summary_ts_id_seq OWNER TO nowcall;

--
-- Name: tag_summary; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE tag_summary (
    ts_id integer DEFAULT nextval('tag_summary_ts_id_seq'::regclass) NOT NULL,
    ts_rc_id integer,
    ts_log_id integer,
    ts_rev_id integer,
    ts_tags text NOT NULL
);


ALTER TABLE tag_summary OWNER TO nowcall;

--
-- Name: templatelinks; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE templatelinks (
    tl_from integer NOT NULL,
    tl_from_namespace integer DEFAULT 0 NOT NULL,
    tl_namespace smallint NOT NULL,
    tl_title text NOT NULL
);


ALTER TABLE templatelinks OWNER TO nowcall;

--
-- Name: transcache; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE transcache (
    tc_url text NOT NULL,
    tc_contents text NOT NULL,
    tc_time timestamp with time zone NOT NULL
);


ALTER TABLE transcache OWNER TO nowcall;

--
-- Name: updatelog; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE updatelog (
    ul_key text NOT NULL,
    ul_value text
);


ALTER TABLE updatelog OWNER TO nowcall;

--
-- Name: uploadstash_us_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE uploadstash_us_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE uploadstash_us_id_seq OWNER TO nowcall;

--
-- Name: uploadstash; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE uploadstash (
    us_id integer DEFAULT nextval('uploadstash_us_id_seq'::regclass) NOT NULL,
    us_user integer,
    us_key text,
    us_orig_path text,
    us_path text,
    us_props bytea,
    us_source_type text,
    us_timestamp timestamp with time zone,
    us_status text,
    us_chunk_inx integer,
    us_size integer,
    us_sha1 text,
    us_mime text,
    us_media_type media_type,
    us_image_width integer,
    us_image_height integer,
    us_image_bits smallint
);


ALTER TABLE uploadstash OWNER TO nowcall;

--
-- Name: user_former_groups; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE user_former_groups (
    ufg_user integer,
    ufg_group text NOT NULL
);


ALTER TABLE user_former_groups OWNER TO nowcall;

--
-- Name: user_groups; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE user_groups (
    ug_user integer,
    ug_group text NOT NULL
);


ALTER TABLE user_groups OWNER TO nowcall;

--
-- Name: user_newtalk; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE user_newtalk (
    user_id integer NOT NULL,
    user_ip text,
    user_last_timestamp timestamp with time zone
);


ALTER TABLE user_newtalk OWNER TO nowcall;

--
-- Name: user_properties; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE user_properties (
    up_user integer,
    up_property text NOT NULL,
    up_value text
);


ALTER TABLE user_properties OWNER TO nowcall;

--
-- Name: valid_tag; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE valid_tag (
    vt_tag text NOT NULL
);


ALTER TABLE valid_tag OWNER TO nowcall;

--
-- Name: watchlist_wl_id_seq; Type: SEQUENCE; Schema: public; Owner: nowcall
--

CREATE SEQUENCE watchlist_wl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE watchlist_wl_id_seq OWNER TO nowcall;

--
-- Name: watchlist; Type: TABLE; Schema: public; Owner: nowcall
--

CREATE TABLE watchlist (
    wl_id integer DEFAULT nextval('watchlist_wl_id_seq'::regclass) NOT NULL,
    wl_user integer NOT NULL,
    wl_namespace smallint DEFAULT 0 NOT NULL,
    wl_title text NOT NULL,
    wl_notificationtimestamp timestamp with time zone
);


ALTER TABLE watchlist OWNER TO nowcall;

--
-- Data for Name: archive; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY archive (ar_id, ar_namespace, ar_title, ar_text, ar_page_id, ar_parent_id, ar_sha1, ar_comment, ar_user, ar_user_text, ar_timestamp, ar_minor_edit, ar_flags, ar_rev_id, ar_text_id, ar_deleted, ar_len, ar_content_model, ar_content_format) FROM stdin;
\.


--
-- Name: archive_ar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('archive_ar_id_seq', 1, false);


--
-- Data for Name: bot_passwords; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY bot_passwords (bp_user, bp_app_id, bp_password, bp_token, bp_restrictions, bp_grants) FROM stdin;
\.


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY category (cat_id, cat_title, cat_pages, cat_subcats, cat_files, cat_hidden) FROM stdin;
\.


--
-- Name: category_cat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('category_cat_id_seq', 1, false);


--
-- Data for Name: categorylinks; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY categorylinks (cl_from, cl_to, cl_sortkey, cl_timestamp, cl_sortkey_prefix, cl_collation, cl_type) FROM stdin;
\.


--
-- Data for Name: change_tag; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY change_tag (ct_id, ct_rc_id, ct_log_id, ct_rev_id, ct_tag, ct_params) FROM stdin;
\.


--
-- Name: change_tag_ct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('change_tag_ct_id_seq', 1, false);


--
-- Data for Name: externallinks; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY externallinks (el_id, el_from, el_to, el_index) FROM stdin;
\.


--
-- Name: externallinks_el_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('externallinks_el_id_seq', 1, false);


--
-- Data for Name: filearchive; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY filearchive (fa_id, fa_name, fa_archive_name, fa_storage_group, fa_storage_key, fa_deleted_user, fa_deleted_timestamp, fa_deleted_reason, fa_size, fa_width, fa_height, fa_metadata, fa_bits, fa_media_type, fa_major_mime, fa_minor_mime, fa_description, fa_user, fa_user_text, fa_timestamp, fa_deleted, fa_sha1) FROM stdin;
\.


--
-- Name: filearchive_fa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('filearchive_fa_id_seq', 1, false);


--
-- Data for Name: image; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY image (img_name, img_size, img_width, img_height, img_metadata, img_bits, img_media_type, img_major_mime, img_minor_mime, img_description, img_user, img_user_text, img_timestamp, img_sha1) FROM stdin;
\.


--
-- Data for Name: imagelinks; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY imagelinks (il_from, il_from_namespace, il_to) FROM stdin;
\.


--
-- Data for Name: interwiki; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY interwiki (iw_prefix, iw_url, iw_local, iw_trans, iw_api, iw_wikiid) FROM stdin;
acronym	http://www.acronymfinder.com/~/search/af.aspx?string=exact&Acronym=$1	0	0		
advogato	http://www.advogato.org/$1	0	0		
arxiv	http://www.arxiv.org/abs/$1	0	0		
c2find	http://c2.com/cgi/wiki?FindPage&value=$1	0	0		
cache	http://www.google.com/search?q=cache:$1	0	0		
commons	https://commons.wikimedia.org/wiki/$1	0	0	https://commons.wikimedia.org/w/api.php	
dictionary	http://www.dict.org/bin/Dict?Database=*&Form=Dict1&Strategy=*&Query=$1	0	0		
doi	http://dx.doi.org/$1	0	0		
drumcorpswiki	http://www.drumcorpswiki.com/$1	0	0	http://drumcorpswiki.com/api.php	
dwjwiki	http://www.suberic.net/cgi-bin/dwj/wiki.cgi?$1	0	0		
elibre	http://enciclopedia.us.es/index.php/$1	0	0	http://enciclopedia.us.es/api.php	
emacswiki	http://www.emacswiki.org/cgi-bin/wiki.pl?$1	0	0		
foldoc	http://foldoc.org/?$1	0	0		
foxwiki	http://fox.wikis.com/wc.dll?Wiki~$1	0	0		
freebsdman	http://www.FreeBSD.org/cgi/man.cgi?apropos=1&query=$1	0	0		
gentoo-wiki	http://gentoo-wiki.com/$1	0	0		
google	http://www.google.com/search?q=$1	0	0		
googlegroups	http://groups.google.com/groups?q=$1	0	0		
hammondwiki	http://www.dairiki.org/HammondWiki/$1	0	0		
hrwiki	http://www.hrwiki.org/wiki/$1	0	0	http://www.hrwiki.org/w/api.php	
imdb	http://www.imdb.com/find?q=$1&tt=on	0	0		
kmwiki	http://kmwiki.wikispaces.com/$1	0	0		
linuxwiki	http://linuxwiki.de/$1	0	0		
lojban	http://mw.lojban.org/papri/$1	0	0		
lqwiki	http://wiki.linuxquestions.org/wiki/$1	0	0		
meatball	http://www.usemod.com/cgi-bin/mb.pl?$1	0	0		
mediawikiwiki	https://www.mediawiki.org/wiki/$1	0	0	https://www.mediawiki.org/w/api.php	
memoryalpha	http://en.memory-alpha.org/wiki/$1	0	0	http://en.memory-alpha.org/api.php	
metawiki	http://sunir.org/apps/meta.pl?$1	0	0		
metawikimedia	https://meta.wikimedia.org/wiki/$1	0	0	https://meta.wikimedia.org/w/api.php	
mozillawiki	http://wiki.mozilla.org/$1	0	0	https://wiki.mozilla.org/api.php	
mw	https://www.mediawiki.org/wiki/$1	0	0	https://www.mediawiki.org/w/api.php	
oeis	http://oeis.org/$1	0	0		
openwiki	http://openwiki.com/ow.asp?$1	0	0		
pmid	https://www.ncbi.nlm.nih.gov/pubmed/$1?dopt=Abstract	0	0		
pythoninfo	http://wiki.python.org/moin/$1	0	0		
rfc	https://tools.ietf.org/html/rfc$1	0	0		
s23wiki	http://s23.org/wiki/$1	0	0	http://s23.org/w/api.php	
seattlewireless	http://seattlewireless.net/$1	0	0		
senseislibrary	http://senseis.xmp.net/?$1	0	0		
shoutwiki	http://www.shoutwiki.com/wiki/$1	0	0	http://www.shoutwiki.com/w/api.php	
squeak	http://wiki.squeak.org/squeak/$1	0	0		
tmbw	http://www.tmbw.net/wiki/$1	0	0	http://tmbw.net/wiki/api.php	
tmnet	http://www.technomanifestos.net/?$1	0	0		
theopedia	http://www.theopedia.com/$1	0	0		
twiki	http://twiki.org/cgi-bin/view/$1	0	0		
uncyclopedia	http://en.uncyclopedia.co/wiki/$1	0	0	http://en.uncyclopedia.co/w/api.php	
unreal	http://wiki.beyondunreal.com/$1	0	0	http://wiki.beyondunreal.com/w/api.php	
usemod	http://www.usemod.com/cgi-bin/wiki.pl?$1	0	0		
wiki	http://c2.com/cgi/wiki?$1	0	0		
wikia	http://www.wikia.com/wiki/$1	0	0		
wikibooks	https://en.wikibooks.org/wiki/$1	0	0	https://en.wikibooks.org/w/api.php	
wikidata	https://www.wikidata.org/wiki/$1	0	0	https://www.wikidata.org/w/api.php	
wikif1	http://www.wikif1.org/$1	0	0		
wikihow	http://www.wikihow.com/$1	0	0	http://www.wikihow.com/api.php	
wikinfo	http://wikinfo.co/English/index.php/$1	0	0		
wikimedia	https://wikimediafoundation.org/wiki/$1	0	0	https://wikimediafoundation.org/w/api.php	
wikinews	https://en.wikinews.org/wiki/$1	0	0	https://en.wikinews.org/w/api.php	
wikipedia	https://en.wikipedia.org/wiki/$1	0	0	https://en.wikipedia.org/w/api.php	
wikiquote	https://en.wikiquote.org/wiki/$1	0	0	https://en.wikiquote.org/w/api.php	
wikisource	https://wikisource.org/wiki/$1	0	0	https://wikisource.org/w/api.php	
wikispecies	https://species.wikimedia.org/wiki/$1	0	0	https://species.wikimedia.org/w/api.php	
wikiversity	https://en.wikiversity.org/wiki/$1	0	0	https://en.wikiversity.org/w/api.php	
wikivoyage	https://en.wikivoyage.org/wiki/$1	0	0	https://en.wikivoyage.org/w/api.php	
wikt	https://en.wiktionary.org/wiki/$1	0	0	https://en.wiktionary.org/w/api.php	
wiktionary	https://en.wiktionary.org/wiki/$1	0	0	https://en.wiktionary.org/w/api.php	
\.


--
-- Data for Name: ipblocks; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY ipblocks (ipb_id, ipb_address, ipb_user, ipb_by, ipb_by_text, ipb_reason, ipb_timestamp, ipb_auto, ipb_anon_only, ipb_create_account, ipb_enable_autoblock, ipb_expiry, ipb_range_start, ipb_range_end, ipb_deleted, ipb_block_email, ipb_allow_usertalk, ipb_parent_block_id) FROM stdin;
\.


--
-- Name: ipblocks_ipb_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('ipblocks_ipb_id_seq', 1, false);


--
-- Data for Name: iwlinks; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY iwlinks (iwl_from, iwl_prefix, iwl_title) FROM stdin;
\.


--
-- Data for Name: job; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY job (job_id, job_cmd, job_namespace, job_title, job_timestamp, job_params, job_random, job_attempts, job_token, job_token_timestamp, job_sha1) FROM stdin;
\.


--
-- Name: job_job_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('job_job_id_seq', 1, false);


--
-- Data for Name: l10n_cache; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY l10n_cache (lc_lang, lc_key, lc_value) FROM stdin;
\.


--
-- Data for Name: langlinks; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY langlinks (ll_from, ll_lang, ll_title) FROM stdin;
\.


--
-- Data for Name: log_search; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY log_search (ls_field, ls_value, ls_log_id) FROM stdin;
\.


--
-- Data for Name: logging; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY logging (log_id, log_type, log_action, log_timestamp, log_user, log_namespace, log_title, log_comment, log_params, log_deleted, log_user_text, log_page) FROM stdin;
\.


--
-- Name: logging_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('logging_log_id_seq', 1, false);


--
-- Data for Name: module_deps; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY module_deps (md_module, md_skin, md_deps) FROM stdin;
mediawiki.legacy.shared	vector|en	["resources/src/mediawiki.legacy/images/ajax-loader.gif","resources/src/mediawiki.legacy/images/spinner.gif"]
mediawiki.skinning.interface	vector|en	["resources/src/mediawiki.skinning/images/magnify-clip-ltr.png","resources/src/mediawiki.skinning/images/magnify-clip-ltr.svg","resources/src/mediawiki.skinning/images/magnify-clip-rtl.png","resources/src/mediawiki.skinning/images/magnify-clip-rtl.svg"]
skins.vector.styles	vector|en	["resources/src/mediawiki.less/mediawiki.mixins.animation.less","resources/src/mediawiki.less/mediawiki.mixins.less","resources/src/mediawiki.less/mediawiki.mixins.rotation.less","skins/Vector/components/common.less","skins/Vector/components/externalLinks.less","skins/Vector/components/footer.less","skins/Vector/components/navigation.less","skins/Vector/components/personalMenu.less","skins/Vector/components/search.less","skins/Vector/components/tabs.less","skins/Vector/components/watchstar.less","skins/Vector/images/arrow-down-focus-icon.png","skins/Vector/images/arrow-down-focus-icon.svg","skins/Vector/images/arrow-down-icon.png","skins/Vector/images/arrow-down-icon.svg","skins/Vector/images/bullet-icon.png","skins/Vector/images/bullet-icon.svg","skins/Vector/images/external-link-ltr-icon.png","skins/Vector/images/external-link-ltr-icon.svg","skins/Vector/images/page-fade.png","skins/Vector/images/portal-break.png","skins/Vector/images/search-fade.png","skins/Vector/images/search-ltr.png","skins/Vector/images/search-ltr.svg","skins/Vector/images/tab-break.png","skins/Vector/images/tab-current-fade.png","skins/Vector/images/tab-normal-fade.png","skins/Vector/images/unwatch-icon-hl.png","skins/Vector/images/unwatch-icon-hl.svg","skins/Vector/images/unwatch-icon.png","skins/Vector/images/unwatch-icon.svg","skins/Vector/images/user-icon.png","skins/Vector/images/user-icon.svg","skins/Vector/images/watch-icon-hl.png","skins/Vector/images/watch-icon-hl.svg","skins/Vector/images/watch-icon-loading.png","skins/Vector/images/watch-icon-loading.svg","skins/Vector/images/watch-icon.png","skins/Vector/images/watch-icon.svg","skins/Vector/screen-hd.less","skins/Vector/screen.less","skins/Vector/variables.less"]
mediawiki.action.view.postEdit	vector|en	["resources/src/mediawiki.action/images/green-checkmark.png"]
\.


--
-- Data for Name: mwuser; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY mwuser (user_id, user_name, user_real_name, user_password, user_newpassword, user_newpass_time, user_token, user_email, user_email_token, user_email_token_expires, user_email_authenticated, user_touched, user_registration, user_editcount, user_password_expires) FROM stdin;
0	Anonymous		\N	\N	\N	\N	\N	\N	\N	\N	2017-03-24 08:15:32.384317-04	2017-03-24 08:15:32.384317-04	\N	\N
1	Admin		:pbkdf2:sha512:30000:64:Ak89VH1ezEODClJWyV1+jw==:4BJz2bAr6IzN/HSHhfKPvgXVtxB+yF3T6GQ63YPHx3XK/Ot0/Ct8uWr6XW4HsLdBUl/wR4BltFYQFgSj82NLsA==		\N	acaa7371c2ec7bb387c2554a706c48bb			\N	\N	2017-03-24 08:15:39-04	2017-03-24 08:15:32-04	0	\N
\.


--
-- Data for Name: objectcache; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY objectcache (keyname, value, exptime) FROM stdin;
\.


--
-- Data for Name: oldimage; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY oldimage (oi_name, oi_archive_name, oi_size, oi_width, oi_height, oi_bits, oi_description, oi_user, oi_user_text, oi_timestamp, oi_metadata, oi_media_type, oi_major_mime, oi_minor_mime, oi_deleted, oi_sha1) FROM stdin;
\.


--
-- Data for Name: page; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY page (page_id, page_namespace, page_title, page_restrictions, page_is_redirect, page_is_new, page_random, page_touched, page_links_updated, page_latest, page_len, page_content_model, page_lang, titlevector) FROM stdin;
1	0	Main_Page		0	1	0.69713919415300	2017-03-24 08:15:33-04	\N	1	717	wikitext	\N	'main':1 'page':2
\.


--
-- Name: page_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('page_page_id_seq', 1, true);


--
-- Data for Name: page_props; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY page_props (pp_page, pp_propname, pp_value, pp_sortkey) FROM stdin;
\.


--
-- Data for Name: page_restrictions; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY page_restrictions (pr_id, pr_page, pr_type, pr_level, pr_cascade, pr_user, pr_expiry) FROM stdin;
\.


--
-- Name: page_restrictions_pr_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('page_restrictions_pr_id_seq', 1, false);


--
-- Data for Name: pagecontent; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY pagecontent (old_id, old_text, old_flags, textvector) FROM stdin;
1	<strong>MediaWiki has been installed.</strong>\n\nConsult the [https://meta.wikimedia.org/wiki/Help:Contents User's Guide] for information on using the wiki software.\n\n== Getting started ==\n* [https://www.mediawiki.org/wiki/Special:MyLanguage/Manual:Configuration_settings Configuration settings list]\n* [https://www.mediawiki.org/wiki/Special:MyLanguage/Manual:FAQ MediaWiki FAQ]\n* [https://lists.wikimedia.org/mailman/listinfo/mediawiki-announce MediaWiki release mailing list]\n* [https://www.mediawiki.org/wiki/Special:MyLanguage/Localisation#Translation_resources Localise MediaWiki for your language]\n* [https://www.mediawiki.org/wiki/Special:MyLanguage/Manual:Combating_spam Learn how to combat spam on your wiki]	utf-8	'/mailman/listinfo/mediawiki-announce':35 '/wiki/help:contents':9 '/wiki/special:mylanguage/localisation#translation_resources':42 '/wiki/special:mylanguage/manual:combating_spam':50 '/wiki/special:mylanguage/manual:configuration_settings':24 '/wiki/special:mylanguage/manual:faq':30 'combat':54 'configur':25 'consult':5 'faq':32 'get':20 'guid':12 'inform':14 'instal':4 'languag':47 'learn':51 'list':27,39 'lists.wikimedia.org':34 'lists.wikimedia.org/mailman/listinfo/mediawiki-announce':33 'localis':43 'mail':38 'mediawiki':1,31,36,44 'meta.wikimedia.org':8 'meta.wikimedia.org/wiki/help:contents':7 'releas':37 'set':26 'softwar':19 'spam':55 'start':21 'use':16 'user':10 'wiki':18,58 'www.mediawiki.org':23,29,41,49 'www.mediawiki.org/wiki/special:mylanguage/localisation#translation_resources':40 'www.mediawiki.org/wiki/special:mylanguage/manual:combating_spam':48 'www.mediawiki.org/wiki/special:mylanguage/manual:configuration_settings':22 'www.mediawiki.org/wiki/special:mylanguage/manual:faq':28
\.


--
-- Data for Name: pagelinks; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY pagelinks (pl_from, pl_from_namespace, pl_namespace, pl_title) FROM stdin;
\.


--
-- Data for Name: profiling; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY profiling (pf_count, pf_time, pf_memory, pf_name, pf_server) FROM stdin;
\.


--
-- Data for Name: protected_titles; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY protected_titles (pt_namespace, pt_title, pt_user, pt_reason, pt_timestamp, pt_expiry, pt_create_perm) FROM stdin;
\.


--
-- Data for Name: querycache; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY querycache (qc_type, qc_value, qc_namespace, qc_title) FROM stdin;
\.


--
-- Data for Name: querycache_info; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY querycache_info (qci_type, qci_timestamp) FROM stdin;
\.


--
-- Data for Name: querycachetwo; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY querycachetwo (qcc_type, qcc_value, qcc_namespace, qcc_title, qcc_namespacetwo, qcc_titletwo) FROM stdin;
\.


--
-- Data for Name: recentchanges; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY recentchanges (rc_id, rc_timestamp, rc_cur_time, rc_user, rc_user_text, rc_namespace, rc_title, rc_comment, rc_minor, rc_bot, rc_new, rc_cur_id, rc_this_oldid, rc_last_oldid, rc_type, rc_source, rc_patrolled, rc_ip, rc_old_len, rc_new_len, rc_deleted, rc_logid, rc_log_type, rc_log_action, rc_params) FROM stdin;
\.


--
-- Name: recentchanges_rc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('recentchanges_rc_id_seq', 1, false);


--
-- Data for Name: redirect; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY redirect (rd_from, rd_namespace, rd_title, rd_interwiki, rd_fragment) FROM stdin;
\.


--
-- Data for Name: revision; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY revision (rev_id, rev_page, rev_text_id, rev_comment, rev_user, rev_user_text, rev_timestamp, rev_minor_edit, rev_deleted, rev_len, rev_parent_id, rev_sha1, rev_content_model, rev_content_format) FROM stdin;
1	1	1		0	MediaWiki default	2017-03-24 08:15:33-04	0	0	717	0	6wzmf83s5t2n0ntohe3bmgww4dea1u3	\N	\N
\.


--
-- Name: revision_rev_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('revision_rev_id_seq', 1, true);


--
-- Data for Name: site_identifiers; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY site_identifiers (si_site, si_type, si_key) FROM stdin;
\.


--
-- Data for Name: site_stats; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY site_stats (ss_row_id, ss_total_edits, ss_good_articles, ss_total_pages, ss_users, ss_active_users, ss_admins, ss_images) FROM stdin;
1	0	0	0	0	-1	-1	0
\.


--
-- Data for Name: sites; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY sites (site_id, site_global_key, site_type, site_group, site_source, site_language, site_protocol, site_domain, site_data, site_forward, site_config) FROM stdin;
\.


--
-- Name: sites_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('sites_site_id_seq', 1, false);


--
-- Data for Name: tag_summary; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY tag_summary (ts_id, ts_rc_id, ts_log_id, ts_rev_id, ts_tags) FROM stdin;
\.


--
-- Name: tag_summary_ts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('tag_summary_ts_id_seq', 1, false);


--
-- Data for Name: templatelinks; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY templatelinks (tl_from, tl_from_namespace, tl_namespace, tl_title) FROM stdin;
\.


--
-- Name: text_old_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('text_old_id_seq', 1, true);


--
-- Data for Name: transcache; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY transcache (tc_url, tc_contents, tc_time) FROM stdin;
\.


--
-- Data for Name: updatelog; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY updatelog (ul_key, ul_value) FROM stdin;
filearchive-fa_major_mime-patch-fa_major_mime-chemical.sql	\N
image-img_major_mime-patch-img_major_mime-chemical.sql	\N
oldimage-oi_major_mime-patch-oi_major_mime-chemical.sql	\N
user_groups-ug_group-patch-ug_group-length-increase-255.sql	\N
user_former_groups-ufg_group-patch-ufg_group-length-increase-255.sql	\N
user_properties-up_property-patch-up_property.sql	\N
patch-textsearch_bug66650.sql	\N
PingBack	c1653b00c0ff72bf3023c0942bdf1ed3
\.


--
-- Data for Name: uploadstash; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY uploadstash (us_id, us_user, us_key, us_orig_path, us_path, us_props, us_source_type, us_timestamp, us_status, us_chunk_inx, us_size, us_sha1, us_mime, us_media_type, us_image_width, us_image_height, us_image_bits) FROM stdin;
\.


--
-- Name: uploadstash_us_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('uploadstash_us_id_seq', 1, false);


--
-- Data for Name: user_former_groups; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY user_former_groups (ufg_user, ufg_group) FROM stdin;
\.


--
-- Data for Name: user_groups; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY user_groups (ug_user, ug_group) FROM stdin;
1	sysop
1	bureaucrat
\.


--
-- Data for Name: user_newtalk; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY user_newtalk (user_id, user_ip, user_last_timestamp) FROM stdin;
\.


--
-- Data for Name: user_properties; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY user_properties (up_user, up_property, up_value) FROM stdin;
\.


--
-- Name: user_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('user_user_id_seq', 1, true);


--
-- Data for Name: valid_tag; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY valid_tag (vt_tag) FROM stdin;
\.


--
-- Data for Name: watchlist; Type: TABLE DATA; Schema: public; Owner: nowcall
--

COPY watchlist (wl_id, wl_user, wl_namespace, wl_title, wl_notificationtimestamp) FROM stdin;
\.


--
-- Name: watchlist_wl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nowcall
--

SELECT pg_catalog.setval('watchlist_wl_id_seq', 1, false);


--
-- Name: archive_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY archive
    ADD CONSTRAINT archive_pkey PRIMARY KEY (ar_id);


--
-- Name: bot_passwords_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY bot_passwords
    ADD CONSTRAINT bot_passwords_pkey PRIMARY KEY (bp_user, bp_app_id);


--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (cat_id);


--
-- Name: change_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY change_tag
    ADD CONSTRAINT change_tag_pkey PRIMARY KEY (ct_id);


--
-- Name: externallinks_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY externallinks
    ADD CONSTRAINT externallinks_pkey PRIMARY KEY (el_id);


--
-- Name: filearchive_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY filearchive
    ADD CONSTRAINT filearchive_pkey PRIMARY KEY (fa_id);


--
-- Name: image_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_pkey PRIMARY KEY (img_name);


--
-- Name: interwiki_iw_prefix_key; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY interwiki
    ADD CONSTRAINT interwiki_iw_prefix_key UNIQUE (iw_prefix);


--
-- Name: ipblocks_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY ipblocks
    ADD CONSTRAINT ipblocks_pkey PRIMARY KEY (ipb_id);


--
-- Name: job_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_pkey PRIMARY KEY (job_id);


--
-- Name: log_search_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY log_search
    ADD CONSTRAINT log_search_pkey PRIMARY KEY (ls_field, ls_value, ls_log_id);


--
-- Name: logging_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY logging
    ADD CONSTRAINT logging_pkey PRIMARY KEY (log_id);


--
-- Name: mwuser_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY mwuser
    ADD CONSTRAINT mwuser_pkey PRIMARY KEY (user_id);


--
-- Name: mwuser_user_name_key; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY mwuser
    ADD CONSTRAINT mwuser_user_name_key UNIQUE (user_name);


--
-- Name: objectcache_keyname_key; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY objectcache
    ADD CONSTRAINT objectcache_keyname_key UNIQUE (keyname);


--
-- Name: page_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY page
    ADD CONSTRAINT page_pkey PRIMARY KEY (page_id);


--
-- Name: page_props_pk; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY page_props
    ADD CONSTRAINT page_props_pk PRIMARY KEY (pp_page, pp_propname);


--
-- Name: page_restrictions_pk; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY page_restrictions
    ADD CONSTRAINT page_restrictions_pk PRIMARY KEY (pr_page, pr_type);


--
-- Name: page_restrictions_pr_id_key; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY page_restrictions
    ADD CONSTRAINT page_restrictions_pr_id_key UNIQUE (pr_id);


--
-- Name: pagecontent_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY pagecontent
    ADD CONSTRAINT pagecontent_pkey PRIMARY KEY (old_id);


--
-- Name: querycache_info_qci_type_key; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY querycache_info
    ADD CONSTRAINT querycache_info_qci_type_key UNIQUE (qci_type);


--
-- Name: recentchanges_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY recentchanges
    ADD CONSTRAINT recentchanges_pkey PRIMARY KEY (rc_id);


--
-- Name: revision_rev_id_key; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY revision
    ADD CONSTRAINT revision_rev_id_key UNIQUE (rev_id);


--
-- Name: site_stats_ss_row_id_key; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY site_stats
    ADD CONSTRAINT site_stats_ss_row_id_key UNIQUE (ss_row_id);


--
-- Name: sites_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY sites
    ADD CONSTRAINT sites_pkey PRIMARY KEY (site_id);


--
-- Name: tag_summary_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY tag_summary
    ADD CONSTRAINT tag_summary_pkey PRIMARY KEY (ts_id);


--
-- Name: transcache_tc_url_key; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY transcache
    ADD CONSTRAINT transcache_tc_url_key UNIQUE (tc_url);


--
-- Name: updatelog_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY updatelog
    ADD CONSTRAINT updatelog_pkey PRIMARY KEY (ul_key);


--
-- Name: uploadstash_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY uploadstash
    ADD CONSTRAINT uploadstash_pkey PRIMARY KEY (us_id);


--
-- Name: valid_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY valid_tag
    ADD CONSTRAINT valid_tag_pkey PRIMARY KEY (vt_tag);


--
-- Name: watchlist_pkey; Type: CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY watchlist
    ADD CONSTRAINT watchlist_pkey PRIMARY KEY (wl_id);


--
-- Name: archive_name_title_timestamp; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX archive_name_title_timestamp ON archive USING btree (ar_namespace, ar_title, ar_timestamp);


--
-- Name: archive_user_text; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX archive_user_text ON archive USING btree (ar_user_text);


--
-- Name: category_pages; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX category_pages ON category USING btree (cat_pages);


--
-- Name: category_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX category_title ON category USING btree (cat_title);


--
-- Name: change_tag_log_tag; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX change_tag_log_tag ON change_tag USING btree (ct_log_id, ct_tag);


--
-- Name: change_tag_rc_tag; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX change_tag_rc_tag ON change_tag USING btree (ct_rc_id, ct_tag);


--
-- Name: change_tag_rev_tag; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX change_tag_rev_tag ON change_tag USING btree (ct_rev_id, ct_tag);


--
-- Name: change_tag_tag_id; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX change_tag_tag_id ON change_tag USING btree (ct_tag, ct_rc_id, ct_rev_id, ct_log_id);


--
-- Name: cl_from; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX cl_from ON categorylinks USING btree (cl_from, cl_to);


--
-- Name: cl_sortkey; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX cl_sortkey ON categorylinks USING btree (cl_to, cl_sortkey, cl_from);


--
-- Name: externallinks_from_to; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX externallinks_from_to ON externallinks USING btree (el_from, el_to);


--
-- Name: externallinks_index; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX externallinks_index ON externallinks USING btree (el_index);


--
-- Name: fa_dupe; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX fa_dupe ON filearchive USING btree (fa_storage_group, fa_storage_key);


--
-- Name: fa_name_time; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX fa_name_time ON filearchive USING btree (fa_name, fa_timestamp);


--
-- Name: fa_notime; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX fa_notime ON filearchive USING btree (fa_deleted_timestamp);


--
-- Name: fa_nouser; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX fa_nouser ON filearchive USING btree (fa_deleted_user);


--
-- Name: fa_sha1; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX fa_sha1 ON filearchive USING btree (fa_sha1);


--
-- Name: il_from; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX il_from ON imagelinks USING btree (il_to, il_from);


--
-- Name: img_sha1; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX img_sha1 ON image USING btree (img_sha1);


--
-- Name: img_size_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX img_size_idx ON image USING btree (img_size);


--
-- Name: img_timestamp_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX img_timestamp_idx ON image USING btree (img_timestamp);


--
-- Name: ipb_address_unique; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX ipb_address_unique ON ipblocks USING btree (ipb_address, ipb_user, ipb_auto, ipb_anon_only);


--
-- Name: ipb_parent_block_id; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX ipb_parent_block_id ON ipblocks USING btree (ipb_parent_block_id);


--
-- Name: ipb_range; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX ipb_range ON ipblocks USING btree (ipb_range_start, ipb_range_end);


--
-- Name: ipb_user; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX ipb_user ON ipblocks USING btree (ipb_user);


--
-- Name: iwl_from; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX iwl_from ON iwlinks USING btree (iwl_from, iwl_prefix, iwl_title);


--
-- Name: iwl_prefix_from_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX iwl_prefix_from_title ON iwlinks USING btree (iwl_prefix, iwl_from, iwl_title);


--
-- Name: iwl_prefix_title_from; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX iwl_prefix_title_from ON iwlinks USING btree (iwl_prefix, iwl_title, iwl_from);


--
-- Name: job_cmd_namespace_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX job_cmd_namespace_title ON job USING btree (job_cmd, job_namespace, job_title);


--
-- Name: job_cmd_token; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX job_cmd_token ON job USING btree (job_cmd, job_token, job_random);


--
-- Name: job_cmd_token_id; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX job_cmd_token_id ON job USING btree (job_cmd, job_token, job_id);


--
-- Name: job_sha1; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX job_sha1 ON job USING btree (job_sha1);


--
-- Name: job_timestamp_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX job_timestamp_idx ON job USING btree (job_timestamp);


--
-- Name: l10n_cache_lc_lang_key; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX l10n_cache_lc_lang_key ON l10n_cache USING btree (lc_lang, lc_key);


--
-- Name: langlinks_lang_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX langlinks_lang_title ON langlinks USING btree (ll_lang, ll_title);


--
-- Name: langlinks_unique; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX langlinks_unique ON langlinks USING btree (ll_from, ll_lang);


--
-- Name: logging_page_id_time; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX logging_page_id_time ON logging USING btree (log_page, log_timestamp);


--
-- Name: logging_page_time; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX logging_page_time ON logging USING btree (log_namespace, log_title, log_timestamp);


--
-- Name: logging_times; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX logging_times ON logging USING btree (log_timestamp);


--
-- Name: logging_type_name; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX logging_type_name ON logging USING btree (log_type, log_timestamp);


--
-- Name: logging_user_text_time; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX logging_user_text_time ON logging USING btree (log_user_text, log_timestamp);


--
-- Name: logging_user_text_type_time; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX logging_user_text_type_time ON logging USING btree (log_user_text, log_type, log_timestamp);


--
-- Name: logging_user_time; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX logging_user_time ON logging USING btree (log_timestamp, log_user);


--
-- Name: logging_user_type_time; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX logging_user_type_time ON logging USING btree (log_user, log_type, log_timestamp);


--
-- Name: ls_log_id; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX ls_log_id ON log_search USING btree (ls_log_id);


--
-- Name: md_module_skin; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX md_module_skin ON module_deps USING btree (md_module, md_skin);


--
-- Name: new_name_timestamp; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX new_name_timestamp ON recentchanges USING btree (rc_new, rc_namespace, rc_timestamp);


--
-- Name: objectcacache_exptime; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX objectcacache_exptime ON objectcache USING btree (exptime);


--
-- Name: oi_name_archive_name; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX oi_name_archive_name ON oldimage USING btree (oi_name, oi_archive_name);


--
-- Name: oi_name_timestamp; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX oi_name_timestamp ON oldimage USING btree (oi_name, oi_timestamp);


--
-- Name: oi_sha1; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX oi_sha1 ON oldimage USING btree (oi_sha1);


--
-- Name: page_len_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX page_len_idx ON page USING btree (page_len);


--
-- Name: page_main_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX page_main_title ON page USING btree (page_title text_pattern_ops) WHERE (page_namespace = 0);


--
-- Name: page_mediawiki_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX page_mediawiki_title ON page USING btree (page_title text_pattern_ops) WHERE (page_namespace = 8);


--
-- Name: page_project_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX page_project_title ON page USING btree (page_title text_pattern_ops) WHERE (page_namespace = 4);


--
-- Name: page_props_propname; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX page_props_propname ON page_props USING btree (pp_propname);


--
-- Name: page_random_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX page_random_idx ON page USING btree (page_random);


--
-- Name: page_talk_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX page_talk_title ON page USING btree (page_title text_pattern_ops) WHERE (page_namespace = 1);


--
-- Name: page_unique_name; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX page_unique_name ON page USING btree (page_namespace, page_title);


--
-- Name: page_user_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX page_user_title ON page USING btree (page_title text_pattern_ops) WHERE (page_namespace = 2);


--
-- Name: page_utalk_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX page_utalk_title ON page USING btree (page_title text_pattern_ops) WHERE (page_namespace = 3);


--
-- Name: pagelink_unique; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX pagelink_unique ON pagelinks USING btree (pl_from, pl_namespace, pl_title);


--
-- Name: pagelinks_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX pagelinks_title ON pagelinks USING btree (pl_title);


--
-- Name: pf_name_server; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX pf_name_server ON profiling USING btree (pf_name, pf_server);


--
-- Name: pp_propname_page; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX pp_propname_page ON page_props USING btree (pp_propname, pp_page);


--
-- Name: pp_propname_sortkey_page; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX pp_propname_sortkey_page ON page_props USING btree (pp_propname, pp_sortkey, pp_page) WHERE (pp_sortkey IS NOT NULL);


--
-- Name: protected_titles_unique; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX protected_titles_unique ON protected_titles USING btree (pt_namespace, pt_title);


--
-- Name: querycache_type_value; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX querycache_type_value ON querycache USING btree (qc_type, qc_value);


--
-- Name: querycachetwo_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX querycachetwo_title ON querycachetwo USING btree (qcc_type, qcc_namespace, qcc_title);


--
-- Name: querycachetwo_titletwo; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX querycachetwo_titletwo ON querycachetwo USING btree (qcc_type, qcc_namespacetwo, qcc_titletwo);


--
-- Name: querycachetwo_type_value; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX querycachetwo_type_value ON querycachetwo USING btree (qcc_type, qcc_value);


--
-- Name: rc_cur_id; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX rc_cur_id ON recentchanges USING btree (rc_cur_id);


--
-- Name: rc_ip; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX rc_ip ON recentchanges USING btree (rc_ip);


--
-- Name: rc_name_type_patrolled_timestamp; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX rc_name_type_patrolled_timestamp ON recentchanges USING btree (rc_namespace, rc_type, rc_patrolled, rc_timestamp);


--
-- Name: rc_namespace_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX rc_namespace_title ON recentchanges USING btree (rc_namespace, rc_title);


--
-- Name: rc_timestamp; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX rc_timestamp ON recentchanges USING btree (rc_timestamp);


--
-- Name: rc_timestamp_bot; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX rc_timestamp_bot ON recentchanges USING btree (rc_timestamp) WHERE (rc_bot = 0);


--
-- Name: redirect_ns_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX redirect_ns_title ON redirect USING btree (rd_namespace, rd_title, rd_from);


--
-- Name: rev_text_id_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX rev_text_id_idx ON revision USING btree (rev_text_id);


--
-- Name: rev_timestamp_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX rev_timestamp_idx ON revision USING btree (rev_timestamp);


--
-- Name: rev_user_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX rev_user_idx ON revision USING btree (rev_user);


--
-- Name: rev_user_text_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX rev_user_text_idx ON revision USING btree (rev_user_text);


--
-- Name: revision_unique; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX revision_unique ON revision USING btree (rev_page, rev_id);


--
-- Name: si_key; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX si_key ON site_identifiers USING btree (si_key);


--
-- Name: si_site; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX si_site ON site_identifiers USING btree (si_site);


--
-- Name: si_type_key; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX si_type_key ON site_identifiers USING btree (si_type, si_key);


--
-- Name: site_domain; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX site_domain ON sites USING btree (site_domain);


--
-- Name: site_forward; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX site_forward ON sites USING btree (site_forward);


--
-- Name: site_global_key; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX site_global_key ON sites USING btree (site_global_key);


--
-- Name: site_group; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX site_group ON sites USING btree (site_group);


--
-- Name: site_language; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX site_language ON sites USING btree (site_language);


--
-- Name: site_protocol; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX site_protocol ON sites USING btree (site_protocol);


--
-- Name: site_source; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX site_source ON sites USING btree (site_source);


--
-- Name: site_type; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX site_type ON sites USING btree (site_type);


--
-- Name: tag_summary_log_id; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX tag_summary_log_id ON tag_summary USING btree (ts_log_id);


--
-- Name: tag_summary_rc_id; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX tag_summary_rc_id ON tag_summary USING btree (ts_rc_id);


--
-- Name: tag_summary_rev_id; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX tag_summary_rev_id ON tag_summary USING btree (ts_rev_id);


--
-- Name: templatelinks_from; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX templatelinks_from ON templatelinks USING btree (tl_from);


--
-- Name: templatelinks_unique; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX templatelinks_unique ON templatelinks USING btree (tl_namespace, tl_title, tl_from);


--
-- Name: ts2_page_text; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX ts2_page_text ON pagecontent USING gin (textvector);


--
-- Name: ts2_page_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX ts2_page_title ON page USING gin (titlevector);


--
-- Name: ufg_user_group; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX ufg_user_group ON user_former_groups USING btree (ufg_user, ufg_group);


--
-- Name: us_key_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX us_key_idx ON uploadstash USING btree (us_key);


--
-- Name: us_timestamp_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX us_timestamp_idx ON uploadstash USING btree (us_timestamp);


--
-- Name: us_user_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX us_user_idx ON uploadstash USING btree (us_user);


--
-- Name: user_email_token_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX user_email_token_idx ON mwuser USING btree (user_email_token);


--
-- Name: user_groups_unique; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX user_groups_unique ON user_groups USING btree (ug_user, ug_group);


--
-- Name: user_newtalk_id_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX user_newtalk_id_idx ON user_newtalk USING btree (user_id);


--
-- Name: user_newtalk_ip_idx; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX user_newtalk_ip_idx ON user_newtalk USING btree (user_ip);


--
-- Name: user_properties_property; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX user_properties_property ON user_properties USING btree (up_property);


--
-- Name: user_properties_user_property; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX user_properties_user_property ON user_properties USING btree (up_user, up_property);


--
-- Name: wl_user; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX wl_user ON watchlist USING btree (wl_user);


--
-- Name: wl_user_namespace_title; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE UNIQUE INDEX wl_user_namespace_title ON watchlist USING btree (wl_namespace, wl_title, wl_user);


--
-- Name: wl_user_notificationtimestamp; Type: INDEX; Schema: public; Owner: nowcall
--

CREATE INDEX wl_user_notificationtimestamp ON watchlist USING btree (wl_user, wl_notificationtimestamp);


--
-- Name: page_deleted; Type: TRIGGER; Schema: public; Owner: nowcall
--

CREATE TRIGGER page_deleted AFTER DELETE ON page FOR EACH ROW EXECUTE PROCEDURE page_deleted();


--
-- Name: ts2_page_text; Type: TRIGGER; Schema: public; Owner: nowcall
--

CREATE TRIGGER ts2_page_text BEFORE INSERT OR UPDATE ON pagecontent FOR EACH ROW EXECUTE PROCEDURE ts2_page_text();


--
-- Name: ts2_page_title; Type: TRIGGER; Schema: public; Owner: nowcall
--

CREATE TRIGGER ts2_page_title BEFORE INSERT OR UPDATE ON page FOR EACH ROW EXECUTE PROCEDURE ts2_page_title();


--
-- Name: archive_ar_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY archive
    ADD CONSTRAINT archive_ar_user_fkey FOREIGN KEY (ar_user) REFERENCES mwuser(user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: categorylinks_cl_from_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY categorylinks
    ADD CONSTRAINT categorylinks_cl_from_fkey FOREIGN KEY (cl_from) REFERENCES page(page_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: externallinks_el_from_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY externallinks
    ADD CONSTRAINT externallinks_el_from_fkey FOREIGN KEY (el_from) REFERENCES page(page_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filearchive_fa_deleted_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY filearchive
    ADD CONSTRAINT filearchive_fa_deleted_user_fkey FOREIGN KEY (fa_deleted_user) REFERENCES mwuser(user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filearchive_fa_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY filearchive
    ADD CONSTRAINT filearchive_fa_user_fkey FOREIGN KEY (fa_user) REFERENCES mwuser(user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: image_img_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_img_user_fkey FOREIGN KEY (img_user) REFERENCES mwuser(user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: imagelinks_il_from_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY imagelinks
    ADD CONSTRAINT imagelinks_il_from_fkey FOREIGN KEY (il_from) REFERENCES page(page_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipblocks_ipb_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY ipblocks
    ADD CONSTRAINT ipblocks_ipb_by_fkey FOREIGN KEY (ipb_by) REFERENCES mwuser(user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipblocks_ipb_parent_block_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY ipblocks
    ADD CONSTRAINT ipblocks_ipb_parent_block_id_fkey FOREIGN KEY (ipb_parent_block_id) REFERENCES ipblocks(ipb_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipblocks_ipb_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY ipblocks
    ADD CONSTRAINT ipblocks_ipb_user_fkey FOREIGN KEY (ipb_user) REFERENCES mwuser(user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: langlinks_ll_from_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY langlinks
    ADD CONSTRAINT langlinks_ll_from_fkey FOREIGN KEY (ll_from) REFERENCES page(page_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: logging_log_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY logging
    ADD CONSTRAINT logging_log_user_fkey FOREIGN KEY (log_user) REFERENCES mwuser(user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oldimage_oi_name_fkey_cascaded; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY oldimage
    ADD CONSTRAINT oldimage_oi_name_fkey_cascaded FOREIGN KEY (oi_name) REFERENCES image(img_name) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oldimage_oi_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY oldimage
    ADD CONSTRAINT oldimage_oi_user_fkey FOREIGN KEY (oi_user) REFERENCES mwuser(user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_props_pp_page_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY page_props
    ADD CONSTRAINT page_props_pp_page_fkey FOREIGN KEY (pp_page) REFERENCES page(page_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_restrictions_pr_page_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY page_restrictions
    ADD CONSTRAINT page_restrictions_pr_page_fkey FOREIGN KEY (pr_page) REFERENCES page(page_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pagelinks_pl_from_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY pagelinks
    ADD CONSTRAINT pagelinks_pl_from_fkey FOREIGN KEY (pl_from) REFERENCES page(page_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: protected_titles_pt_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY protected_titles
    ADD CONSTRAINT protected_titles_pt_user_fkey FOREIGN KEY (pt_user) REFERENCES mwuser(user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: recentchanges_rc_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY recentchanges
    ADD CONSTRAINT recentchanges_rc_user_fkey FOREIGN KEY (rc_user) REFERENCES mwuser(user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: redirect_rd_from_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY redirect
    ADD CONSTRAINT redirect_rd_from_fkey FOREIGN KEY (rd_from) REFERENCES page(page_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: revision_rev_page_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY revision
    ADD CONSTRAINT revision_rev_page_fkey FOREIGN KEY (rev_page) REFERENCES page(page_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: revision_rev_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY revision
    ADD CONSTRAINT revision_rev_user_fkey FOREIGN KEY (rev_user) REFERENCES mwuser(user_id) ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;


--
-- Name: templatelinks_tl_from_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY templatelinks
    ADD CONSTRAINT templatelinks_tl_from_fkey FOREIGN KEY (tl_from) REFERENCES page(page_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_former_groups_ufg_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY user_former_groups
    ADD CONSTRAINT user_former_groups_ufg_user_fkey FOREIGN KEY (ufg_user) REFERENCES mwuser(user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_groups_ug_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY user_groups
    ADD CONSTRAINT user_groups_ug_user_fkey FOREIGN KEY (ug_user) REFERENCES mwuser(user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_newtalk_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY user_newtalk
    ADD CONSTRAINT user_newtalk_user_id_fkey FOREIGN KEY (user_id) REFERENCES mwuser(user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_properties_up_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY user_properties
    ADD CONSTRAINT user_properties_up_user_fkey FOREIGN KEY (up_user) REFERENCES mwuser(user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: watchlist_wl_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nowcall
--

ALTER TABLE ONLY watchlist
    ADD CONSTRAINT watchlist_wl_user_fkey FOREIGN KEY (wl_user) REFERENCES mwuser(user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

