var Payment = (function(){
	var checkout_obj;
	var reader_state = 0;
	var need_swipe_listener = true;
	var stripe_public_key = "";
	var stripe_rate_fee = 0.0;
	var stripe_flat_fee = 0.0;
	var cardno_buf = "";
	var cardno_concealed = "";
	var stripe_js_callback = function(resp){};
	var use_checkout = true;
	var compute_fee =
			function(amount) {
				if (isNaN(amount) || (amount == 0.0)) return 0;
				return Math.round(((amount * stripe_rate_fee +
						stripe_flat_fee) / (1 - stripe_rate_fee)) * 100);
				};
	var require_checkout = function(state) {
		use_checkout = state;
		}
	var stripe_checkout_callback = function(token, callback) {
		var errs = [];
		var amount = parseFloat(document.getElementById("pay-amount").value);
		var remember;
		var card_rem = document.getElementById("card-remember");
		if (card_rem != null) remember = card_rem.checked;
		else remember = false;
		if (token.card.address_zip_check != "pass")
			errs.push("zipcode check failed");
		if (token.card.cvc_check != "pass")
			errs.push("CVC security code check failed");
		if (isNaN(amount) || (amount == 0.0))
			errs.push("invalid charge amount");
		if (errs.length > 0) {
			var msg = "Sorry, there were errors with charging card " + 
						token.card.brand + token.card.last4 + ":\r\n\r\n- ";
			msg += errs.join("\r\n- ") + "\r\n";
			alert(msg);
			return;
			}
		$.post("/payment", {
				token: token.id,
				amount: Math.round(amount * 100),
				fee: compute_fee(amount),
				card_brand: token.card.brand,
				card_last4: token.card.last4,
				keep: (remember ? 1 : 0),
				email: token.email
				}, callback, "json");
		};
	var config_stripe_response = function(pubkey, logo, dzname,
							rate_fee, flat_fee, callback) {
		if (use_checkout) {
			checkout_obj = StripeCheckout.configure({
				key: pubkey,
				image: logo,
				name: dzname,
				description: "credit patron's account",
				allowRememberMe: false,
				zipCode: true,
				token: function(token) {
						stripe_checkout_callback(token, callback);
						},
				locale: "auto"});
			window.addEventListener("popstate",
					function() { checkout_obj.close(); });
			}
		stripe_public_key = pubkey;
		stripe_rate_fee = rate_fee;
		stripe_flat_fee = flat_fee;
		stripe_js_callback = callback;
		return;
		};
	var config_stripe = function(callback) {
		$.get("/payment/config", {},
				function(resp) {
					config_stripe_response(
						resp.public_key,
						resp.logo,
						resp.dz_short_name,
						resp.stripe_rate / 100.0,
						resp.stripe_flat,
						callback
						);
					}, "json");
		};
	var wipe_form = function(set_focus) {
		var box = document.getElementById("card-swipe-cardno");
		box.value = "";
		cardno_concealed = "";
		cardno_buf = "";
		reader_state = 0;
		document.getElementById("card-swipe-name").value = "";
		document.getElementById("card-swipe-month").value = "";
		document.getElementById("card-swipe-year").value = "";
		document.getElementById("card-swipe-cvc").value = "";
		document.getElementById("card-swipe-zip").value = "";
		document.getElementById("card-remember").checked = false;
		if (set_focus) box.focus();
		}
	var parse_track = function(track) {
		var parts = track.split("^");
		var errors = [];
		if (parts.length != 3) {
			errors.push("invalid track data");
			}
		var card_num = parts[0].trim();
		if (card_num.match(/^\d+$/) == null) {
			errors.push("invalid card number");
			}
		var cust_name = parts[1].trim();
		var other = parts[2];
		var exp = other.match(/^(\d\d)(\d\d)/);
		if (exp == null) {
			errors.push("invalid expiration date");
			}
		var data = {};
		if (errors.length > 0) {
			data.error_html = "Please correct:<ul><li>" +
				errors.join("</li><li>") +
				"</li></ul>";
			data.exp_year = "";
			data.exp_month = "";
			data.card_num = "";
			data.cust_name = "";
			}
		else {
			data.error_html = "";
			data.exp_year = exp[1];
			data.exp_month = exp[2];
			data.card_num = card_num;
			data.cust_name = cust_name;
			}
		return data;
		}
	var show_swipe_errors = function(title, html) {
		local_alert(title, html);
		}
	var last4 = function(cardno) {
		var leader = cardno.length - 4;
		if (leader < 0) leader = 0;
		var out = "";
		for (var i = 0; i < leader; i++) out += "*";
		out += cardno.substring(leader);
		return out;
		}
	var populate_card_form = function(data) {
		document.getElementById("card-swipe-cardno").value =
			last4(data.card_num);
		cardno_concealed = data.card_num;
		document.getElementById("card-swipe-name").value = data.cust_name;
		document.getElementById("card-swipe-month").value = data.exp_month;
		document.getElementById("card-swipe-year").value = data.exp_year;
		window.setTimeout(
				function() {
					document.getElementById("card-swipe-cvc").focus();
					reader_state = 0;
					if (data.error_html != "")
						show_swipe_errors("Card Entry Errors",
										data.error_html);
					}, 1500);
		}
	var prep_for_swipe = function() {
		wipe_form(true);
		var buf = "";
		var card_data = null;
		if (!need_swipe_listener) return;
		var box;
		need_swipe_listener = false;
		box = document.getElementById("card-swipe-cvc");
		box.addEventListener("keypress",
				function(event) {
					// setTimeout to force this function to bottom of
					// chain
					window.setTimeout(function() {
						var cvc = document.getElementById("card-swipe-cvc");
						if (cvc.value.trim().length == 3) {
							document.getElementById("card-swipe-zip").focus();
							}
						});
					});
		box = document.getElementById("card-swipe-cardno");
		box.addEventListener("keypress",
				function(event) {
					var key = event.key;
					if (reader_state == 0) {
						cardno_buf = "";
						cardno_concealed = "";
						if (key == "%") {
							reader_state = 1; // probable card entry
							event.preventDefault();
							}
						else if (key == "B") {
							reader_state = 2; // missing start sentinel?
							event.preventDefault();
							}
						else {
							reader_state = 3; // probable manual entry
							cardno_concealed += key;
							}
						}
					else if (reader_state == 1) {
						if (key == "B") {
							reader_state = 2; // likely card entry
							event.preventDefault();
							}
						else {
							reader_state = 3; // manual entry
							cardno_concealed += key;
							}
						}
					else if (reader_state == 2) {
						if ((key == "?") || (cardno_buf.length > 52)) {
								// end-of-track
							event.preventDefault();
							card_data = parse_track(cardno_buf);
							cardno_buf = "";
							populate_card_form(card_data);
							reader_state = 4; // ignore remaining
							}
						else {
							cardno_buf += key;
							event.preventDefault();
							}
						}
					else if (reader_state == 3) {
						cardno_concealed += key;
						}
					else if (reader_state == 4) {
						event.preventDefault();
						}
					});
		}
	var show_swipe_group = function(reveal) {
		reveal = reveal && !use_checkout;
		var rows = document.getElementsByClassName("swipe-group");
		if (rows.length == 0) return;
		for (var i = 0; i < rows.length; i++) {
			rows[i].style.display = (reveal ? "table-row" : "none");
			}
		if (reveal) prep_for_swipe();
		};
	var load_card_menu = function(customer_id, allow_cash) {
		var data = {};
		if (customer_id != "") data.custid = customer_id;
		$.post("/payment/cards", data,
			function(resp) {
				var menu = document.getElementById("charge-to");
				var option;
				menu.options.length = 0;
				for (var i = 0; i < resp.opts.length; i++) {
					option = new Option(resp.opts[i].label,
											resp.opts[i].id);
					option.disabled = resp.opts[i].expired;
					menu.add(option);
					}
				option = new Option("New Card", "new");
				menu.add(option);
				if (allow_cash) {
					option = new Option("Offline Card", "offline");
					menu.add(option);
					option = new Option("Check/Cash", "cash");
					menu.add(option);
					}
				show_fee();
				menu.onchange =
					function() {
						show_swipe_group(menu.value == "new");
						show_fee();
						}
				show_swipe_group(menu.options[0].value == "new");
				},
			"json");
		};
	var charge_card = function(amount, email) {
		var errors = [];
		if (stripe_public_key == "") {
			errors.push("payment service not configured");
			}
		var cardno = cardno_concealed.trim();
		cardno_concealed = "";
		cardno = cardno.replace(/[ \-]/g, "");
		if (!cardno.match(/^\d+/)) {
			errors.push("Card number missing or invalid: '" + cardno + "'");
			}
		var exp_month = document.getElementById("card-swipe-month").value.trim();
		if (!exp_month.match(/^\d\d?$/)) {
			errors.push("Expiration month missing or invalid: '"
				+ exp_month + "'");
			}
		var exp_year = document.getElementById("card-swipe-year").value.trim();
		if (!exp_year.match(/^\d+$/)) {
			errors.push("Expiration year missing or invalid: '"
				+ exp_year + "'");
			}
		var cvc = document.getElementById("card-swipe-cvc").value.trim();
		if (!cvc.match(/^\d+$/)) {
			errors.push("CVC code missing or invalid: '" + cvc + "'");
			}
		var zip = document.getElementById("card-swipe-zip").value.trim();
		if (!zip.match(/^\d+$/)) {
			errors.push("Zipcode missing or invalid: '" + zip + "'");
			}
		var remember =
			document.getElementById("card-remember").checked;
		if (errors.length > 0) {
			show_swipe_errors("Card Entry Errors",
				"Please correct:<ul><li>" +
				errors.join("</li><li>") +
				"</li></ul>");
			return;
			}
		Stripe.setPublishableKey(stripe_public_key);
		Stripe.card.createToken({
			number: cardno,
			cvc: cvc,
			exp_month: exp_month,
			exp_year: exp_year,
			address_zip: zip
			}, function(status, resp) {
					if (resp.error) {
						show_swipe_errors("Card Error", resp.error.message);
						return;
						}
					$.post("/payment", {
							token: resp.id,
							amount: Math.round(amount * 100),
							fee: compute_fee(amount),
							card_brand: "",
							card_last4: "",
							keep: (remember ? 1 : 0),
							email: email
							}, function(resp) {
								stripe_js_callback(resp);
								if ((resp.status != null) && resp.status)
									wipe_form(false);
								}, "json");
					});
		}
	var pay_up = function(card_response) {
		var amount = parseFloat(document.getElementById("pay-amount").value);
		if (isNaN(amount) || (amount == 0.0)) {
			alert("Please enter a charge amount.");
			return;
			}
		var fee;
		if (waive_fee()) fee = 0.0;
		else {
			var fee_text = document.getElementById("pay-fee").value;
			fee = parseFloat(fee_text.replace("$", ""));
			}
		if (isNaN(fee)) {
			alert("Invalid fee value. This shouldn't happen.");
			return;
			}
		var charge_to = document.getElementById("charge-to");
		var total = Math.round((amount + fee) * 100)
		var email = document.getElementById("pay-email").value;
		if (charge_to.value == "new") {
			if (use_checkout) {
				checkout_obj.open({
					email: email,
					amount: total
					});
				}
			else charge_card(amount, email);
			}
		else if (charge_to.value == "cash") {
			$.post("/payment/cash",
					{ amount: Math.round(amount * 100) },
					card_response, "json");
			}
		else if (charge_to.value == "offline") {
			$.post("/payment/offline",
					{ amount: Math.round(amount * 100),
						fee: Math.round(fee * 100) },
					card_response, "json");
			}
		else {
			var msg = "<p>You're about to charge $" +
				pennies_dollars(total) +
				" to card '" +
				charge_to.options[charge_to.selectedIndex].text +
				"'.</p><p>Go ahead?</p>";
			local_confirm("Charge Confirmation", "Yes", "No",
					function() {
						$.post("/payment/card",
								{ card: charge_to.value,
									fee: Math.round(fee * 100),
									amount: Math.round(amount * 100) },
								card_response, "json");
						}, null, msg);
			}
		};
	var pennies_dollars = function(pennies) {
		var dollars = Math.floor(pennies / 100);
		var cents = pennies % 100;
		return dollars + "." + (cents < 10 ? "0" : "") + cents;
		};
	var waive_fee = function() {
		var cb = document.getElementById("waive-fee");
		return ((cb != null) && cb.checked);
		}
	var show_fee = function() {
		var amt_box = document.getElementById("pay-amount");
		var fee_box = document.getElementById("pay-fee");
		var btn = document.getElementById("pay-button");
		if (amt_box.value.trim() == "") {
			fee_box.value = "";
			btn.value = "Submit Charge"
			return;
			}
		var amount = parseFloat(amt_box.value);
		var method = document.getElementById("charge-to").value
		var fee;
		var cash_pay = (method == "cash");
		if (cash_pay || waive_fee()) fee = 0;
		else fee = compute_fee(amount);
		fee_box.value = "$" + pennies_dollars(fee);
		var fee_waive = document.getElementById("waive-row");
		if (fee_waive != null)
			fee_waive.style.display = (cash_pay ? "none" : "table-row");
		btn.value = (cash_pay ? "Accept" : "Charge");
		btn.value += " $" + pennies_dollars(fee + Math.round(amount * 100));
		};
	var set_amount = function(amount) {
		var box = document.getElementById("pay-amount");
		if (amount == 0) {
			box.value = "";
			var cb = document.getElementById("waive-fee");
			if (cb != null) cb.checked = false;
			}
		else box.value = pennies_dollars(amount);
		show_fee();
		};
	var set_email = function(addr) {
		var box = document.getElementById("pay-email");
		box.value = addr;
		};
	return {
		config_stripe: config_stripe,
		load_card_menu: load_card_menu,
		pay_up: pay_up,
		set_amount: set_amount,
		set_email: set_email,
		show_fee: show_fee,
		wipe_form: wipe_form,
		require_checkout: require_checkout
		};
	}());
