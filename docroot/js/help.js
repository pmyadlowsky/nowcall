var Help = (function() {
	var help_text = "";
	var help_app = "";
	var help_title = "";
	var help_height = "";
	var squawking_on = "";
	var open_help = function(app, title, height) {
		help_app = app;
		help_title = title;
		help_height = height;
		if (help_text == "") {
			$.get("/" + app + "/help", {},
					function(resp) {
						help_text = resp.html;
						open_help(help_app, help_title, help_height);
						},
					"json");
			return;
			}
		$("#help-dialog").dialog({
			draggable: true,
			show: true,
			hide: true,
			title: help_title + " (ESC to dismiss)",
			closeOnEscape: true,
			height: help_height,
			width: "auto",
			autoResize: true,
			modal: false
			});
		var box = document.getElementById("help-dialog");
		box.innerHTML = help_text;
		$("#help-dialog").dialog("open");
		};
	var open_wiki = function(title) {
		var url = "/wiki/index.php?title=NowCall:" + title;
		window.open(url, "wiki", "menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes");
		};
	var open_squawk = function(app_name) {
		var capped = app_name.charAt(0).toUpperCase() + app_name.slice(1);
		$("#squawk-dialog").dialog({
			draggable: true,
			show: true,
			hide: true,
			title: capped + " Squawk",
			closeOnEscape: true,
			width: "auto",
			autoResize: true,
			modal: false
			});
		squawking_on = app_name;
		$("#squawk-dialog").dialog("open");
		}
	var close_squawk = function() {
		$("#squawk-dialog").dialog("close");
		}
	var log_squawk = function() {
		var box = document.getElementById("squawk-text");
		if (box == null) return;
		var descrip = box.value.trim();
		if (descrip == "") return;
		$.get("/squawk/log",
				{
					descrip: descrip,
					app: squawking_on
					},
				function(resp) {
					squawking_on = "";
					close_squawk();
					},
				"json");
		}
	return {
		open_help: open_help,
		open_wiki: open_wiki,
		open_squawk: open_squawk,
		close_squawk: close_squawk,
		log_squawk: log_squawk
		};
	}());
function hide_header() {
	document.getElementById("header").style.display = "none";
	}
