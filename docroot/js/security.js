var Security = (function(){
	var set_icon = function(icon) {
		var obj = document.getElementById("security-tag");
		if (obj != null) obj.innerHTML = icon;
		}
	var clear_form = function() {
		document.getElementById("sec-password").value = "";
		document.getElementById("sec-errors").innerHTML = "";
		}
	var send_password = function(dialog_id) {
		$.post("/login/lock",
				{ lock: "1",
					password: document.getElementById("sec-password").value
					},
				function(resp) {
					if (resp.error == "") {
						clear_form();
						set_icon(resp.icon);
						$("#" + dialog_id).dialog("close");
						window.location.reload(true);
						}
					else {
						document.getElementById("sec-password").value = "";
						document.getElementById("sec-errors").innerHTML =
							resp.error;
						}
					}, "json");
		}
	var arm_keypress = function(dialog) {
		$(dialog).keypress(
			function(ev) {
				if (ev.keyCode == $.ui.keyCode.ENTER)
					send_password(dialog.id);
				});
		}
	var secure_session = function(lock) {
		if (lock) {
			$("#lock-dialog").dialog({
				title: "Lock Session",
				modal: true,
				closeText: "hide",
				closeOnEscape: true,
				dialogClass: "my-alert",
				open: function() { arm_keypress(this) },
				buttons: [
					{
						text: "Submit",
						click: function() { send_password(this.id); }
						},
					{
						text: "Cancel",
						click:
							function() {
								clear_form();
								$(this).dialog("close");
								}
						}]});
			}
		else {
			$.post("/login/lock", { lock: "0", password: "" },
					function(resp) {
						set_icon(resp.icon);
						window.location.reload(true);
						}, "json");
			}
		}
	return {
		secure_session: secure_session
		};
	}());
