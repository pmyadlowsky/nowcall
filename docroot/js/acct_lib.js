var Acct = (function(){
	var current_account = 0;
	var manifest_context = false;
	var charge_fee = "0";
	var charge_product_id = 0;
	var payment_product_id = 0;
	var acct_edited = Array();
	
	/*
	external functions defined in their own contexts,
	called here:
	
	- get_ledger_limit: how many transactions to display 
	- render_accounts(resp): display account ledger(s)
	- enable_acct_tracking(bool): enable refresh polling
	*/
	
	var get_current_account = function() {
		return current_account;
		};
	var set_current_account = function(acct) {
		current_account = acct;
		};
	var set_manifest_context = function(state) {
		manifest_context = state;
		};
	var norm_amount = function(el_name) {
		var amt_raw = "0" + document.getElementById(el_name).value;
		return Math.floor(parseFloat(amt_raw.replace(",", "")) * 100 + 0.5);
		};
	var clear_controls = function() {
		charge_fee = "0";
		document.getElementById("charge_amt").value = "";
		document.getElementById("charge_mult").value = "";
		document.getElementById("charge_detail").value = "";
		document.getElementById("pay_amt").value = "";
		document.getElementById("pay_mult").value = "";
		document.getElementById("pay_detail").value = "";
		document.getElementById("sales_tax").value = "";
		var xfer_acct = document.getElementById("xfer-acct");
		xfer_acct.selectedIndex = 0;
		xfer_acct.disabled = true;
		var acct_srch  = document.getElementById("person-acct-srch");
		acct_srch.value = "";
		acct_srch.disabled = true;
		document.getElementById("acct-xfer-id").value = 
				document.getElementById("dzacct-cache").value;
		};
	var recompute_tax = function(box) {
		var tax_box = document.getElementById("sales_tax");
		if (tax_box.value != "") {
			$.get("/accounting/salestax",
					 { price: norm_amount(box.id) },
					function(resp) {
						tax_box.value = resp.tax;
						},
					"json");
			}
		};
	var delete_xact = function(xact_id) {
		$.get("/manifest/dropxtra", { xact_id: xact_id },
				function(resp) {
					$.post("/payment/cancel", { xact_id: xact_id },
						function(resp2) {
							if (!resp2.status) {
								alert(resp2.msg);
								return;
								}
							$.get("/accounting/delxact", {
									acct: current_account,
									manifest: manifest_context,
									limit: get_ledger_limit(),
									xact: xact_id
									}, render_accounts, "json");
							}, "json");
					}, "json");
		};
	var charge = function(pay_response) {
		var amount = norm_amount("charge_amt")
		if (amount < 1) {
			if (pay_response == null)
				local_alert("Data Errors",
					"Please specify a debit or credit amount.");
			else render_accounts(pay_response);
			return;
			}
		var detail = document.getElementById("charge_detail").value.trim();
		if (detail == "") {
			local_alert("Data Errors", "Please select a debit item.");
			return;
			}
		var sales_tax = norm_amount("sales_tax");
		$.post("/accounting/charge", {
					amt: amount,
					quantity: document.getElementById("charge_mult").value,
					fee: charge_fee,
					sales_tax: sales_tax,
					manifest: manifest_context,
					limit: get_ledger_limit(),
					acct: current_account,
					pid: charge_product_id,
					xfer_acct: document.getElementById("acct-xfer-id").value,
					detail: detail
					}, render_accounts, "json");
		};
	var pay = function() {
		var amount = norm_amount("pay_amt")
		if (amount < 1) {
			charge(null);
			return;
			}
		var detail = document.getElementById("pay_detail").value.trim();
		if (detail == "") {
			local_alert("Data Errors", "Please select a credit item.");
			return;
			}
		$.post("/accounting/pay", {
					amt: amount,
					quantity: document.getElementById("pay_mult").value,
					manifest: manifest_context,
					limit: get_ledger_limit(),
					acct: current_account,
					pid: payment_product_id,
					xfer_acct: document.getElementById("acct-xfer-id").value,
					detail: detail
					}, charge, "json");
		};
	var activate_fee = function(chkbox, xact_id) {
		$.get("/accounting/setfee", {
					xact: xact_id,
					acct: current_account,
					manifest: manifest_context,
					limit: get_ledger_limit(),
					activate: (chkbox.checked ? 1 : 0)
					}, render_accounts, "json");
		};
	var activate_xfer = function(activate) {
		if (!activate) return;
		var menu = document.getElementById("xfer-acct");
		menu.disabled = false;
		if (menu.value == "pacct") {
			var box = document.getElementById("person-acct-srch");
			box.disabled = false;
			box.focus();
			}
		};
	var set_charge_item = function(event, ui) {
		var selection = ui.item[0].textContent.trim();
		if (/\n/.test(selection)) return;
		var selection_id = parseInt(ui.item[0].id.replace("item", ""));
		if (isNaN(selection_id)) return;
		$.post("/accounting/setcharge", {
				acct: current_account,
				pay: false,
				item_id: selection_id
				},
			function(resp) {
				charge_product_id = resp.pid;
				var charge_amt = document.getElementById("charge_amt");
				if (charge_amt.value == "") charge_amt.value = resp.amount;
				$("#charge_amt").focus();
				$("#charge_amt").select();
				document.getElementById("charge_detail").value = resp.detail;
				document.getElementById("charge_mult").value = "1";
				charge_fee = resp.fee;
				document.getElementById("sales_tax").value = resp.sales_tax;
				activate_xfer(resp.acct_xfer);
				},
			"json");
		};
	var set_payment_item = function(event, ui) {
		var selection = ui.item[0].textContent.trim();
		if (/\n/.test(selection)) return;
		var selection_id = parseInt(ui.item[0].id.replace("item", ""));
		if (isNaN(selection_id)) return;
		$.post("/accounting/setcharge", {
				acct: current_account,
				pay: true,
				item_id: selection_id
				},
			function(resp) {
				payment_product_id = resp.pid;
				var pay_amt = document.getElementById("pay_amt");
				if (pay_amt.value == "") pay_amt.value = resp.amount;
				$("#pay_amt").focus();
				$("#pay_amt").select();
				document.getElementById("pay_detail").value = resp.detail;
				document.getElementById("pay_mult").value = "1";
				activate_xfer(resp.acct_xfer);
				},
			"json");
		};
	var configure_product_item_menus = function() {
		if (document.getElementById("charge_acct") != null) {
			$("#charge_acct").menu({
				select: set_charge_item
				});
			$("#payment_acct").menu({
				select: set_payment_item
				});
			}
		};
	var editing = function(box, ev) {
		enable_acct_tracking(false);
		document.getElementById("save_btn").disabled = false;
		if (acct_edited.indexOf(box.id) < 0) acct_edited.unshift(box.id);
		if (ev.keyCode == 13) save_edits();
		};
	function save_edits() {
		var values = Array();
		var fields = Array();
		var cell;
		for (var i = 0; i < acct_edited.length; i++) {
				cell = document.getElementById(acct_edited[i]);
				if (cell == null) continue;
				fields.push(acct_edited[i]);
				values.push(cell.value);
				}
		acct_edited = Array();
		if (fields.length < 1) return;
		$.post("/accounting/edits", {
					edits: fields.join("^"),
					values: values.join("^")
					},
				function(resp) {
					enable_acct_tracking(true);
					document.getElementById("save_btn").disabled = true;
					},
				"json");
		};
	return {
		clear_controls: clear_controls,
		recompute_tax: recompute_tax,
		delete_xact: delete_xact,
		pay: pay,
		activate_fee: activate_fee,
		editing: editing,
		save_edits: save_edits,
		configure_product_item_menus: configure_product_item_menus,
		get_current_account: get_current_account,
		set_current_account: set_current_account,
		set_manifest_context: set_manifest_context
		};
	}());
