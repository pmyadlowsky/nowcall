
function local_alert(title, html, width = 300, post_func = null) {
	$("<div>" + html + "</div>").dialog({
		title: title,
		modal: true,
		width: width,
		closeText: "hide",
		closeOnEscape: true,
		dialogClass: "my-alert",
		buttons: [{
				text: "OK",
				click: function(){
							$(this).dialog("close");
							if (post_func != null) post_func();
							}
				}]
		});
	}
function local_confirm(title, yes_btn, no_btn,
							pos_response, neg_response, html) {
	$("<div>" + html + "</div>").dialog({
		title: title,
		modal: true,
		closeText: "hide",
		closeOnEscape: true,
		dialogClass: "my-alert",
		buttons: [{
				text: yes_btn,
				click: function() {
							$(this).dialog("close");
							if (pos_response != null) pos_response();
							}
				},
			{
				text: no_btn,
				click: function() {
							$(this).dialog("close");
							if (neg_response != null) neg_response();
							}
				}]
		});
	}
