var LongPoll = (function() {
	var busy = false;
	var lost_tags = [];
	var track_tag = 1;
	var fallback_timer = null;
	var fallback_timeout = 0; // msec
	var skip1 = false;
	var payload_handler = null;
	var poll_url_path = null;
	var restart_url_path = null;
	var time_reference = null;
	var responder = function(resp, stamp, user_data) {
		if (resp.tag in lost_tags) {
			delete lost_tags[resp.tag];
			console.log("discarded tag " + resp.tag);
			return;
			}
		if (fallback_timer != null) {
			clearTimeout(fallback_timer);
			fallback_timer = null;
			}
		track_tag += 1;
		if (!resp.timeout) {
			if (skip1) skip1 = false;
			else {
				time_reference = resp.time_ref;
console.log("TIME REF " + time_reference);
				if (payload_handler != null)
					payload_handler(resp.payload);
				}
			}
		busy = false;
		poll(user_data);
		};
	var poll = function(user_data = {}) {
		if (poll_url_path == null) {
			console.log("LongPoll: no polling URL set");
			return;
			}
		if (busy) return;
		busy = true;
		var now = new Date();
		var data;
		if (typeof user_data === "function") data = user_data();
		else data = user_data;
		data.tag = track_tag;
		if (time_reference == null)
			time_reference = (new Date().getTime()) / 1000.0;
		data.time_ref = time_reference;
		$.post(poll_url_path, data,
				function(resp) {
					responder(resp, now.getTime(), user_data);
					}, "json");
		if (fallback_timeout > 0) {
			// Set backup timer to declare long poll request dead
			// in case it never returns or is very late.
			fallback_timer = setTimeout(
				function() {
					lost_tags[track_tag + ""] = "x";
					track_tag = track_tag += 1;
					local_alert("Slow Response",
						"<p>Page auto-updates seem stuck." +
						"Reload with 'OK'.</p>", 300,
						function() { location.reload(true); });
					}, fallback_timeout);
			}
		};
	var skip_one = function() {
		skip1 = true;
		};
	var set_payload_handler = function(func) {
		// func(payload_object)
		payload_handler = func;
		};
	var set_poll_url = function(url) {
		poll_url_path = url;
		}
	var set_fallback_timeout = function(sec) {
		fallback_timeout = Math.round(sec * 1000);
		}
	return {
		poll: poll,
		set_payload_handler: set_payload_handler,
		set_poll_url: set_poll_url,
		set_fallback_timeout: set_fallback_timeout,
		skip_one: skip_one
		};
	}());
