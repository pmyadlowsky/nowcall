function build_app_menu() {
	$.get("/index/menu", {},
		function(resp) {
			var menu = document.getElementById("selector");
			var option, item;
			menu.options.length = 0;
			if (resp.menu == null) {
				menu.style.display = "none";
				return;
				}
			for (var i = 0; i < resp.menu.length; i++) {
				item = resp.menu[i];
				option = new Option(item[1], item[0]);
				menu.add(option);
				option.defaultSelected = (item[1] == menu_preset);
				option.addEventListener("contextmenu",
					function(evnt) {
						window.open(evnt.target.value, "_blank");
						evnt.preventDefault();
						}, false);
/*
				option.addEventListener("click",
					function(evnt) {
						window.location = evnt.target.value;
						evnt.preventDefault();
						}, false);
*/
				}
			},
		"json");
	}
