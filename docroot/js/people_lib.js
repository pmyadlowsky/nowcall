var People = (function() {
	var save_func = null;
	var date_pat1 = /^\d+\/\d+\/\d\d(\d\d)?$/;
	var date_pat2 = /^\d\d(\d\d)?\-\d+\-\d+$/;
	var do_save = function(box) {
		if (save_func != null) save_func();
		color_label(box, "black", "normal");
		return false;
		}
	var remind_check_email = function(id, checkbox) {
		var email = document.getElementById(id).value;
		if (checkbox.checked && (email == ""))
			local_alert("Warning", "no email address on file");
		do_save(checkbox);
		return true;
		};
	var enable_rating = function(id, enable) {
		var cb = document.getElementById("people-" + id);
		cb.disabled = !enable;
		if (!enable) cb.checked = false;
		};
	var ratings = function(pid) {
		var role1 = $("input:radio[name=status" + pid + "]:checked").val();
		var enable = (role1 == "LIC");
		enable_rating("affi" + pid, enable);
		enable_rating("ti" + pid, enable);
		enable_rating("vid" + pid, enable);
		enable_rating("coach" + pid, enable);
		enable_rating("pilot" + pid, true);
		enable_rating("maint" + pid, true);
		enable_rating("manifest" + pid, true);
		if (save_func != null) save_func();
		};
	var panel_assign_button = function(btn_name, person_id, func) {
		var id = "people-" + btn_name + "-button" + person_id;
		var btn = document.getElementById(id);
		if (btn != null) {
			if (func) btn.addEventListener("click", func);
			btn.disabled = !func;
			}
		};
	var date_save = function(date, picker) {
		if (save_func != null) save_func();
		};
	var panel_config = function(pid, date_format, first_last, admin,
					save_function, cancel_function, delete_function) {
		$("#people-birth" + pid).datepicker({
			dateFormat: date_format,
			altFormat: "yy-mm-dd",
			firstDay: 1,
			yearRange: "-90:-10",
			onSelect: date_save,
			changeYear: true
			});
		$("#people-waiver-date" + pid).datepicker({
			dateFormat: date_format,
			altFormat: "yy-mm-dd",
			firstDay: 1,
			yearRange: "-10:+01",
			onSelect: date_save,
			changeYear: true
			});
		$("#people-gearrent" + pid).datepicker({
			dateFormat: date_format,
			altFormat: "yy-mm-dd",
			firstDay: 1,
			yearRange: "-10:+01",
			onSelect: date_save,
			changeYear: true
			});
		$("#people-uspa-expires" + pid).datepicker({
			dateFormat: date_format,
			altFormat: "yy-mm-dd",
			firstDay: 1,
			onSelect: date_save,
			changeYear: true
			});
		$("#people-last-repack" + pid).datepicker({
			dateFormat: date_format,
			altFormat: "yy-mm-dd",
			firstDay: 1,
			yearRange: "-10:+01",
			changeYear: true,
			onSelect: date_save
			});
		$("#people-last-repack-2" + pid).datepicker({
			dateFormat: date_format,
			altFormat: "yy-mm-dd",
			firstDay: 1,
			yearRange: "-10:+01",
			onSelect: date_save,
			changeYear: true
			});
		document.getElementById("people-authblock" + pid).style.display =
			(admin ? "block" : "none");
		var name_slot_id = "people-";
		if (first_last) name_slot_id += "first-name" + pid;
		else name_slot_id += "last-name" + pid;
		var name_slot = document.getElementById(name_slot_id);
		if (name_slot.value == "") {
			name_slot.focus();
			name_slot.select();
			}
		save_func = (save_function ? save_function : null);
		panel_assign_button("save", pid,
			save_function && function() {
				if (save_function()) cancel_function();
				});
		panel_assign_button("cancel", pid, cancel_function);
		panel_assign_button("del", pid, delete_function);
		};
	var cell_value = function(id, column) {
		var box = document.getElementById("people-" + column + id);
		if (box == null) return "";
		return box.value.trim();
		};
	var checkbox_value = function(id, column) {
		var cb = document.getElementById("people-" + column + id);
		if (cb == null) return 0;
		return (cb.checked ? 1 : 0);
		};
	var valid_date = function(date_string) {
		if (!date_pat1.test(date_string) &&
					!date_pat2.test(date_string)) {
			return false;
			}
		try {
			return !isNaN(Date.parse(date_string));
			}
		catch(err) {
			return false;
			}
		}
	var update_detail = function(pid, callback) {
		var data;
		var role1 = $("input:radio[name=status" + pid + "]:checked").val();
		var last_name = cell_value(pid, "last-name");
		var first_name = cell_value(pid, "first-name");
		var uspa_id = cell_value(pid, "uspa-id");
		var uspa_expires = cell_value(pid, "uspa-expires");
		var birthdate = cell_value(pid, "birth");
		var waiver_date = cell_value(pid, "waiver-date");
		var last_repack = cell_value(pid, "last-repack");
		var last_repack2 = cell_value(pid, "last-repack-2");
		var errs = Array();
		if ((role1 === undefined) ||
				(last_name == "") ||
				(first_name == "") ||
				((last_name == "NEW") && (first_name == "PERSON"))) {
			errs.push("Please provide at least first name, last name and a status setting.");
			}
		if (uspa_id.length > 0) {
			if (uspa_expires.length == 0) {
				errs.push("Please provide a membership expiration date.");
				}
			else if (!valid_date(uspa_expires)) {
				errs.push("Invalid USPA expiration date");
				}
			}
		if ((birthdate.length > 0) && !valid_date(birthdate)) {
			errs.push("Invalid birth date");
			}
		if ((waiver_date.length > 0) && !valid_date(waiver_date)) {
			errs.push("Invalid waiver date");
			}
		if ((last_repack.length > 0) && !valid_date(last_repack)) {
			errs.push("Invalid repack date");
			}
		if ((last_repack2.length > 0) && !valid_date(last_repack2)) {
			errs.push("Invalid 2nd-rig repack date");
			}
		if (errs.length > 0) {
			var msg = "Please correct:<ul><li>" +
				errs.join("</li><li>") + "</li></ul>";
			local_alert("Data Entry Errors", msg);
			return false;
			}
		data = {
			pid: pid,
			last_name: cell_value(pid, "last-name"),
			first_name: cell_value(pid, "first-name"),
			uspa_id: uspa_id,
			license: cell_value(pid, "license"),
			nickname: cell_value(pid, "nickname"),
			goes_by: cell_value(pid, "goes-by"),
			disambiguate: cell_value(pid, "disambiguate"),
			active: checkbox_value(pid, "active"),
			exit_weight: cell_value(pid, "exit-weight"),
			uspa_expires: uspa_expires,
			last_repack: last_repack,
			last_repack_2: last_repack2,
			street1: cell_value(pid, "street1"),
			street2: cell_value(pid, "street2"),
			city: cell_value(pid, "city"),
			state: cell_value(pid, "state"),
			country: cell_value(pid, "country"),
			zipcode: cell_value(pid, "zipcode"),
			phone_home: cell_value(pid, "phone-home"),
			phone_mobile: cell_value(pid, "phone-mobile"),
			email: cell_value(pid, "email"),
			remind: checkbox_value(pid, "remind"),
			emerg_contact: cell_value(pid, "emerg-contact"),
			birth: birthdate,
			emerg_phone: cell_value(pid, "emerg-phone"),
			waiver_date: waiver_date,
			gear_rental: cell_value(pid, "gearrent"),
			role1: role1,
			affi: checkbox_value(pid, "affi"),
			ti: checkbox_value(pid, "ti"),
			vid: checkbox_value(pid, "vid"),
			coach: checkbox_value(pid, "coach"),
			pilot: checkbox_value(pid, "pilot"),
			maint: checkbox_value(pid, "maint"),
			manifest: checkbox_value(pid, "manifest"),
			packer: checkbox_value(pid, "packer"),
			username: cell_value(pid, "username"),
			password: cell_value(pid, "password"),
			passtouch: cell_value(pid, "passtouch"),
			auth_admin: checkbox_value(pid, "auth-admin"),
			auth_manifest: checkbox_value(pid, "auth-manifest"),
			auth_accounting: checkbox_value(pid, "auth-accounting"),
			auth_people: checkbox_value(pid, "auth-people"),
			auth_products: checkbox_value(pid, "auth-products"),
			auth_aircraft: checkbox_value(pid, "auth-aircraft"),
			auth_backups: checkbox_value(pid, "auth-backups"),
			auth_settings: checkbox_value(pid, "auth-settings"),
			auth_reports: checkbox_value(pid, "auth-reports"),
			auth_reservations: checkbox_value(pid, "auth-reservations"),
			auth_whiteboard: checkbox_value(pid, "auth-whiteboard"),
			auth_schedule: checkbox_value(pid, "auth-schedule"),
			auth_remote: checkbox_value(pid, "auth-remote")
			};
		$.post("/people/update_det", data, callback, "json");
		return true;
		};
	var show_repack_due = function(box, cycle, date_format) {
		if (box.value == "") return;
		var date = $.datepicker.parseDate(date_format, box.value);
		var due_date = new Date(date.getTime() + cycle * 24 * 3600 * 1000);
		box.title = "next due " + $.datepicker.formatDate("m/d/yy", due_date);
		};
	var color_label = function(box, color, weight) {
		var label = document.getElementById(box.id + "-label");
		if (label == null) return;
		label.style.color = color;
		label.style.fontWeight = weight;
		}
	var warn_save = function(box, event) {
		if (event.keyCode == 13) return do_save(box);
		if (event.keyCode == 9) return true;
		color_label(box, "green", "normal");
		return true;
		};
	return {
		remind_check_email: remind_check_email,
		ratings: ratings,
		panel_config: panel_config,
		panel_assign_button: panel_assign_button,
		update_detail: update_detail,
		show_repack_due: show_repack_due,
		warn_save: warn_save,
		do_save: do_save
		};
	}());
