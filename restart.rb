#! /usr/bin/ruby

def find_launcher(app)
	`grep -l "/#{app}" sh_*`.strip
	end

target = (ARGV[0] || "").strip

if target.empty?
	puts "gimme a target"
	exit
	end

`ps ax|grep "/gusher"`.split(/\n/).each do |line|
	next if line =~ /SCREEN/
	next unless line =~ /\/(#{target}.*\.scm)/
	app = $1
	launcher = find_launcher(app)
	print "restart #{app}? [y] "
	resp = $stdin.gets
	next if resp =~ /n/i
	line =~ /^\s*(\d+)/
	pid = $1
	cmd = "kill #{pid}; ./#{launcher}"
	puts cmd
	system(cmd)
	end
