select max(id)+1 as start_id from acct;
create sequence acct_seq start start-id;
alter table acct alter id set default nextval('acct_seq');

select max(id)+1 as start_id from acct_xact;
create sequence acct_xact_seq start start-id;
alter table acct_xact alter id set default nextval('acct_xact_seq');

select max(id)+1 as start_id from people;
create sequence people_seq start start-id;
alter table people alter id set default nextval('people_seq');

select max(id)+1 as start_id from reservation_groups;
create sequence reservation_groups_seq start start-id;
alter table reservation_groups alter id set default nextval('reservation_groups_seq');

select max(id)+1 as start_id from reservations;
create sequence reservations_seq start start-id;
alter table reservations alter id set default nextval('reservations_seq');

select max(id)+1 as start_id from reservations_log;
create sequence reservations_log_seq start start-id;
alter table reservations_log alter id set default nextval('reservations_log_seq');

alter table acct add column description text;
