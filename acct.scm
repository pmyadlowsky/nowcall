
(include "longpoll.scm")

(define account-id
	(let ([query "select id from acct where name=~a"])
		(lambda (name dbh)
			(let ([row (pg-one-row dbh query name)])
				(pg-cell row 'id)))))
(define update-balances
	(let ([query
				"select from_acct, to_acct,
						((amount + sales_tax) * quantity) as amt
					from acct_xact
					where (id=~a)"]
			[increment
				"update acct set
					balance=(balance + ~a)
					where (id=~a)"]
			[mutex (make-mutex)])
		(lambda (xact-id polarity dbh)
			(lock-mutex mutex)
			(let* ([row (pg-one-row dbh query xact-id)]
					[amount (and row (* polarity (pg-cell row 'amt)))])
				(when amount
					(pg-exec dbh increment
						amount (pg-cell row 'from_acct))
					(pg-exec dbh increment
						(- amount) (pg-cell row 'to_acct))))
			(unlock-mutex mutex))))
(define acct-assure-cash-acct
	; if personal account exists, return its ID
	; otherwise, create account and return ID
	(let ([check-acct
				"select id
					from acct
					where owner=~a
					and personal"]
			[create-acct
				"insert into acct (owner, name, personal, description)
					values (~a, 'cash', 't', 'patron personal account')"]
			[mutex (make-mutex)])
		(define (get-acct-id pid dbh)
			(let ([row (pg-one-row dbh check-acct pid)])
				(and row (pg-cell row 'id))))
		(define (make-acct pid dbh)
			(pg-exec dbh create-acct pid)
			(get-acct-id pid dbh))
		(lambda (pid dbh)
			(let ([cache #f])
				(lock-mutex mutex)
					(set! cache
						(or (get-acct-id pid dbh) (make-acct pid dbh)))
				(unlock-mutex mutex) cache))))
(define acct-condvar (make-condition-variable))
(define long-poll-interval 10)
(define longpoller
	(longpoll-responder acct-condvar long-poll-interval))
(define (transaction-alert)
	; fire a long-poll interrupt to broadcast something newsworthy
	(broadcast-condition-variable acct-condvar))
(define upsert-acct-track
	(let ([update-query
				"update acct_track set
					latest_event=current_timestamp
					where (acct_id=~a)"]
			[insert-query
				"insert into acct_track (acct_id, latest_event)
					values (~a, current_timestamp)"]
			[mutex (make-mutex)])
		(lambda (acct dbh)
			(lock-mutex mutex)
			(let ([res (pg-exec dbh update-query acct)])
				(when (< (pg-cmd-tuples res) 1)
					(pg-clear res)
					(pg-exec dbh insert-query acct)))
			(transaction-alert)
			(unlock-mutex mutex))))
(define update-xact-track
	(let ([query
				"select from_acct, to_acct
					from acct_xact
					where (id=~a)"])
		(lambda (xact-id dbh)
			(let ([row (pg-one-row dbh query xact-id)])
				(if row
					(begin
						(upsert-acct-track (pg-cell row 'from_acct) dbh)
						(upsert-acct-track (pg-cell row 'to_acct) dbh))
					(log-msg "XACT AWOL: ~s" xact-id))))))
(define delete-transaction
	(let ([query "update reservations set
					acct_xact=null
					where (acct_xact=~a);
				delete from acct_xact where (id=~a)"])
		(lambda (xact-id dbh)
			(update-balances xact-id -1 dbh)
(log-msg "trace delete: ~s" xact-id)
			(update-xact-track xact-id dbh)
			(pg-exec dbh query xact-id xact-id))))
(define update-transaction-total
	(let ([query
				"select coalesce(amount,0) as amount,
						coalesce(sales_tax,0) as tax
					from acct_xact
					where (id=~a)"]
			[update
				"update acct_xact set
					amount=~a,
					sales_tax=~a
					where (id=~a)"])
		(define (pro-rate total amount tax)
			(let ([sum (+ amount tax)])
				(if (= sum 0)
					total
					(to-i
						(round
							(* total
								(/ (to-f amount) (to-f sum))))))))
		(lambda (xact-id new-total dbh)
			(let ([row (pg-one-row dbh query xact-id)])
				(when row
					(let ([amount (pg-cell row 'amount)]
							[tax (pg-cell row 'tax)]
							[new-amount 0])
						(set! new-amount
							(pro-rate new-total amount tax))
						(pg-exec dbh update
							new-amount
							(- new-total new-amount) xact-id)))))))
(define update-transaction
	(let ([query
				"update acct_xact set
					~a=~a,
					updated=~a
					where (id=~d)"])
		(lambda (xact-id col val dbh)
			(update-balances xact-id -1 dbh)
			(if (string=? col "total")
				(update-transaction-total xact-id val dbh)
				(pg-exec dbh
					(format #f query
						col (pg-format dbh val)
						(pg-format dbh (time-now)) xact-id)))
			(update-balances xact-id 1 dbh)
(log-msg "trace update: ~s" (list xact-id col val))
			(update-xact-track xact-id dbh))))
(define acct-charge
	(let ([query
				"insert into acct_xact (id, from_acct, to_acct, amount,
						description, date_posted, manifest, reference,
						quantity, sales_tax,
						source_type, source_id,
						product_id, entered, updated)
					values (~a, ~a, ~a, ~a,
								~a, ~a, ~a, ~a,
								~a, ~a,
								~a, ~a,
								~a, ~a, ~a)"]
			[get-next "select nextval('acct_xact_seq') as seq"])
		(define (get-next-id dbh)
			(pg-cell (pg-one-row dbh get-next) 'seq))
		(lambda (from-acct to-acct amount quantity
					sales-tax product-id description date manifest
					reference source-type source-id dbh)
			(let ([now (time-now)]
					[xact-id (get-next-id dbh)]
					[adj-from-acct (if (>= amount 0) from-acct to-acct)]
					[adj-to-acct (if (>= amount 0) to-acct from-acct)])
				(pg-exec dbh query
					xact-id
					adj-from-acct
					adj-to-acct
					(abs amount)
					description
					date
					manifest
					reference
					quantity
					sales-tax
					source-type
					(if (= source-id 0) '() source-id)
					product-id now now)
				(update-balances xact-id 1 dbh)
				(upsert-acct-track from-acct dbh)
				(upsert-acct-track to-acct dbh)
				xact-id))))
(define acct-charge-person
	(let ([not-new
				"update people set
					new='f'
					where (id=~a)"])
		(lambda (pid mirror-acct amount
					product-id description date
					manifest source-type source-id dbh)
			(let ([acct (acct-assure-cash-acct pid dbh)])
				(pg-exec dbh not-new pid)
				(and acct
					(acct-charge acct
						mirror-acct amount 1 0 product-id description
						date manifest "" source-type source-id dbh))))))
