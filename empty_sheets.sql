select load_sheets.aircraft, aircraft.capacity, load_date, load_num, count(*)
	from load_sheets, aircraft
	where (passenger is null)
	and (load_sheets.aircraft=aircraft.tail_num)
	group by aircraft, aircraft.capacity, load_date, load_num
	order by load_date, aircraft, load_num desc
