#! /usr/local/bin/gusher -p 3009
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))
(use-modules (ice-9 regex))
(use-modules (srfi srfi-1))
(use-modules (ice-9 common-list))

(include "lib.scm")
(include "settings_lib.scm")
(include "weight_balance.scm")
(include "spreadsheet.scm")

(define env-repack-cycle (setting-cache 'repack-cycle))
(define env-date-format (setting-cache 'date-format))
(define env-name-order (setting-cache 'name-order))
(define env-dz-short-name (setting-cache 'dz-short-name))
(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-min-search-chars (setting-cache 'min-search-chars))

(define ods-conversion #t)

(define frame (make-doc 'file "frame.html"))
(define script (make-doc 'file "reports.js"))
(define page-body (make-doc 'file "reports.html"))
(define composite
	(make-doc (list frame script page-body)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "Reports")
				(cons 'app "reports")
				(cons 'script (fetch-doc script))
				(cons 'version nowcall-version)
				(cons 'content
					(fill-template (fetch-doc page-body) #t))))))
(define (csv-response spreadsheet)
	(let ([mime-type (car spreadsheet)]
			[charset (cadr spreadsheet)]
			[suffix (caddr spreadsheet)]
			[content (cadddr spreadsheet)])
		(list
			"200 OK"
			(list
				(cons "content-type"
					(format #f "~a; charset=~a" mime-type charset))
				(cons "content-length" (to-s (string-length content)))
				(cons "content-disposition"
					(format #f "attachment; filename=\"nowcall-report.~a\""
						suffix))) content)))
(define (pack-spreadsheet title headers rows)
	(append
		(list (list title))
		(if (< (length headers) 1) '() (list headers)) rows))
(define (build-spreadsheet title headers rows)
	(render-spreadsheet
		(pack-spreadsheet title headers rows) ods-conversion))
(define (build-composite-spreadsheet sheet . sheets)
	(render-spreadsheet
		(append sheet (apply append sheets)) ods-conversion))
(define spreadsheet-link
	(let ([html
				(deflate "<a href=\"#\"
					onclick=\"run_report('~a', true); return false\">\
				<img alt=\"spreadsheet\" title=\"download spreadsheet\"
					src=\"/img/ssheet.png\"/></a>")])
		(lambda (report-key) (format #f html report-key))))
(define (table-row row cell-class)
	(string-cat ""
		"<tr>"
		(map
			(lambda (cell)
				(format #f "<td class=\"~a~a\">~a</td>"
					cell-class
					(if (string? cell) ""
						(if (assq-ref cell 'class)
							(format #f " ~a" (assq-ref cell 'class)) ""))
					(if (string? cell) cell
						(or (assq-ref cell 'data) ""))))
			row)
		"</tr>"))
(define (date-or-today date-string)
	(if (string=? date-string "")
		(time-format (time-now) "%Y-%m-%d")
		date-string))
(define (build-table title headers report-key rows)
	(string-cat "\n"
		(format #f "<div class=\"report-title\">~a</div>" title)
		(format #f "<div>~a</div>"
			(if (= (length rows) 0) "nothing found"
				(format #f "~d record~a ~a"
					(length rows)
					(if (= (length rows) 1) "" "s")
					(spreadsheet-link report-key))))
		"<table>"
		(if (or (= (length rows) 0) (< (length headers) 1)) ""
			(table-row headers "report-hdr-cell"))
		(map (lambda (row) (table-row row "report-data-cell")) rows)
		"</table>"))
(define reserve-out-of-date
	(let ([query
				"select last_name, first_name, last_repack, id
					from people
					where active
					and (last_repack is not null)
					and ((last_repack + interval '~d days') < (date ~~a))
					order by last_name asc, first_name asc"])
		(lambda (date)
			(let ([res (pg-exec (sdb)
							(format #f query (env-repack-cycle))
							(date-or-today date))])
				(pg-map-rows res
					(lambda (row)
						(list
							(pg-cell row 'id)
							(person-name (pg-cell row 'id)
								(env-name-order) (sdb))
							(show-date
								(pg-cell row 'last_repack)
								(env-date-format)))))))))
(define waiver-expires
	; waiver is considered expired if it's not
	; for the current year
	(let ([query
				"select last_name, first_name, waiver_date, id
					from people
					where active
					and (waiver_date is not null)
					and (date_part('year',waiver_date) <
							date_part('year', current_date))
					order by last_name asc, first_name asc"])
		(lambda (date)
			(let ([res (pg-exec (sdb) query
							(date-or-today date))])
				(pg-map-rows res
					(lambda (row)
						(list
							(pg-cell row 'id)
							(person-name (pg-cell row 'id)
								(env-name-order) (sdb))
							(show-date
								(pg-cell row 'waiver_date)
								(env-date-format)))))))))
(define uspa-expires
	(let ([query
				"select last_name, first_name, uspa_expires, id
					from people
					where active
					and (uspa_expires is not null)
					and (uspa_expires < (date ~a))
					order by last_name asc, first_name asc"]
			[tmp-query
				"select last_name, first_name, uspa_expires, id
					from people
					where active
					and (license is null)
					and (uspa_expires is not null)
					and (uspa_id ilike 't%')
					order by last_name asc, first_name asc"])
		(define (run-query  date temporary-memb)
			(if temporary-memb
				(pg-exec (sdb) tmp-query)
				(pg-exec (sdb) query (date-or-today date))))
		(lambda (date temporary-memb)
			(let ([res (run-query date temporary-memb)])
				(pg-map-rows res
					(lambda (row)
						(list
							(pg-cell row 'id)
							(person-name (pg-cell row 'id)
								(env-name-order) (sdb))
							(show-date
								(pg-cell row 'uspa_expires)
								(env-date-format)))))))))
(define customer-skydives-query "\
select load_sheets.load_date as date,
			people.last_name as last_name,
			people.first_name as first_name,
			aircraft.nickname as aircraft,
			aircraft.tail_num as tail_num,
			load_sheets.load_num as load_num,
			load_slots.role as role,
			people.id as pid
	from load_sheets, load_slots, people, aircraft
	where (people.id=~d)~a~a
	and (load_slots.passenger=people.id)
	and (load_slots.load_id=load_sheets.id)
	and (aircraft.id=load_sheets.aircraft_id)
	order by load_sheets.load_date asc,
		aircraft.nickname asc, load_sheets.load_num asc")
(define (right-justified data spreadsheet)
	(if spreadsheet (to-s data)
		(list
			(cons 'data (to-s data))
			(cons 'class "report-cell-right"))))
(define (customer-skydives from-date to-date customer spreadsheet)
	(let ([res
				(pg-exec (sdb)
					(format #f customer-skydives-query
						customer
						(if (string=? from-date "") ""
							(format #f " and (load_date >= '~a')"
								from-date))
						(if (string=? to-date "") ""
							(format #f " and (load_date <= '~a')"
								to-date))))])
		(pg-map-rows res
			(lambda (row)
				(list
					(show-date (pg-cell row 'date) (env-date-format))
					(person-name (pg-cell row 'pid)
						(env-name-order) (sdb))
					(format #f "~a [~a]"
						(pg-cell row 'aircraft)
						(pg-cell row 'tail_num))
					(right-justified (pg-cell row 'load_num) spreadsheet)
					(pg-cell row 'role))))))
(define (dollars cents)
	(let ([abs-cents (abs cents)]
			[sign (if (< cents 0) "-" "")])
		(if (>= abs-cents 100000)
			(format #f "~a~d,~3,'0d.~2,'0d"
				sign
				(quotient abs-cents 100000)
				(quotient (remainder abs-cents 100000) 100)
				(remainder (remainder abs-cents 100000) 100))
			(format #f "~a~d.~2,'0d"
				sign
				(quotient abs-cents 100)
				(remainder abs-cents 100)))))
(define (debit-amount db-row)
	(* (pg-cell db-row 'quantity)
		(+
			(pg-cell db-row 'amount)
			(pg-cell db-row 'sales_tax))))
(define customer-acct-query "\
select people.last_name as last_name,
		people.first_name as first_name,
		acct_xact.date_posted as date,
		acct_xact.amount as amount,
		acct_xact.description as description,
		acct_xact.sales_tax as sales_tax,
		acct_xact.quantity as quantity,
		(acct_xact.from_acct = acct.id) as debit,
		people.id as pid
	from people, acct, acct_xact
	where (people.id=~d)~a~a
	and (acct.owner=people.id)
	and ((acct_xact.from_acct=acct.id) or (acct_xact.to_acct=acct.id))
	order by acct_xact.date_posted asc, acct_xact.entered asc")
(define (customer-acct from-date to-date customer spreadsheet)
	(let ([res
				(pg-exec (sdb)
					(format #f customer-acct-query
						customer
						(if (string=? from-date "") ""
							(format #f " and (date_posted >= '~a')"
								from-date))
						(if (string=? to-date "") ""
							(format #f " and (date_posted <= '~a')"
								to-date))))]
			[balance 0])
		(pg-map-rows res
			(lambda (row)
				(list
					(show-date (pg-cell row 'date) (env-date-format))
					(person-name (pg-cell row 'pid)
						(env-name-order) (sdb))
					(pg-cell row 'description)
					(if (pg-cell row 'debit) ;debit
						(let ([amt (debit-amount row)])
							(set! balance (+ balance amt))
							(right-justified (dollars amt)
								spreadsheet)) "")
					(if (pg-cell row 'debit) ;credit
						""
						(let ([amt (pg-cell row 'amount)])
							(set! balance (- balance amt))
							(right-justified (dollars amt) spreadsheet)))
					(right-justified (dollars balance) spreadsheet))))))
(define products-match
	; find products matching any or all (depending on mode)
	; of the given keyword tags
	(let ([pos-query
				"with counts as
					(select count(*) as n,
						product_tags.product as item
						from product_tags
						where (product_tags.tag in (~a))
						group by product_tags.product)
				select item from counts where (~a)"]
			[neg-query
				"select distinct product
					from product_tags
					where (product_tags.tag in (~a))"]
			[all-products-query
				"select distinct product from product_tags"]
			[neg-pat (make-regexp "^!(.+)")])
		(define (in-list tags)
			(string-cat ","
				(map
					(lambda (tag)
						(pg-format (sdb) (string-downcase tag))) tags)))
		(define (pos-match tags op)
			(if (null? tags) '()
				(pg-map-rows
					(pg-exec (sdb)
						(format #f pos-query
							(in-list tags)
							(if (eq? op 'and)
								(format #f "n >= ~d" (length tags))
								"n > 0")))
					(lambda (row) (pg-cell row 'item)))))
		(define (neg-match tags)
			(if (null? tags) '()
				(pg-map-rows
					(pg-exec (sdb)
						(format #f neg-query (in-list tags)))
					(lambda (row) (pg-cell row 'product)))))
		(define (all-products)
			(pg-map-rows
				(pg-exec (sdb) all-products-query)
				(lambda (row) (pg-cell row 'product))))
		(define (tag-lists tags)
			(let scan ([tagset tags]
					[pos '()]
					[neg '()])
				(if (null? tagset) (cons pos neg)
					(let ([match (regexp-exec neg-pat (car tagset))])
						(scan (cdr tagset)
							(if match pos (cons (car tagset) pos))
							(if match
								(cons (match:substring match 1) neg)
								neg))))))
		(lambda (tags op)
			(if (null? tags) '()
				(let* ([tagsets (tag-lists tags)]
						[pos (car tagsets)]
						[neg (cdr tagsets)])
					(if (null? pos)
						(set-difference
							(all-products)
							(neg-match neg))
						(set-difference
							(pos-match pos op)
							(neg-match neg))))))))
(define (products-clause tags)
	(if (null? tags) ""
		(let ([matches (products-match tags 'and)])
			(format #f " and (acct_xact.product_id in (~a))"
				(if (null? matches) "-1" (string-cat "," matches))))))
(define sales-tax-by-date
	(let ([header
				(list
					"Date"
					"Description"
					"Unit Price"
					"Quantity"
					"Subtotal"
					"Sales Tax"
					"Total")]
			[query
				"select date_posted, amount, sales_tax,
						acct_xact.description as description, quantity
					from acct_xact, acct
					where (to_acct=acct.id)~a
					and (acct.name='general')
					and (sales_tax > 0)
					and (date_posted >= '~a')
					and (date_posted <= '~a')
					order by date_posted asc, entered asc"])
		(define (chart-row row spreadsheet)
			(let* ([quantity (pg-cell row 'quantity)]
					[subtotal (* quantity (pg-cell row 'amount))]
					[tax (* quantity (pg-cell row 'sales_tax))]
					[total (+ subtotal tax)])
				(list
					(list
						(time-format
							(pg-cell row 'date_posted) "%m/%d/%Y")
						(pg-cell row 'description)
						(right-justified
							(dollars (pg-cell row 'amount)) spreadsheet)
						(right-justified quantity spreadsheet)
						(right-justified (dollars subtotal) spreadsheet)
						(right-justified (dollars tax) spreadsheet)
						(right-justified (dollars total) spreadsheet))
					subtotal tax total)))
		(define (sums rows spreadsheet)
			(let loop ([data rows]
					[grand-total 0]
					[sub-total 0]
					[tax-total 0])
				(if (null? data)
					(list "" "" "" ""
						(right-justified (dollars sub-total) spreadsheet)
						(right-justified (dollars tax-total) spreadsheet)
						(right-justified
							(dollars grand-total) spreadsheet))
					(loop
						(cdr data)
						(+ grand-total (cadddr (car data)))
						(+ sub-total (cadr (car data)))
						(+ tax-total (caddr (car data)))))))
		(lambda (from-date to-date tags spreadsheet)
			(let* ([chart
						(pg-map-rows
							(pg-exec (sdb)
								(format #f query
									(products-clause tags)
									from-date to-date))
							(lambda (row) (chart-row row spreadsheet)))]
					[data
						(append (map car chart)
							(list (sums chart spreadsheet)))]
					[title
						(format #f "Sales Tax by Date, ~a - ~a"
							from-date to-date)])
				(if spreadsheet
					(pack-spreadsheet title header data)
					(build-table title header "taxsales" data))))))
(define sales-tax-by-item
	(let ([header
				(list
					"Description"
					"Quantity"
					"Subtotal"
					"Sales Tax"
					"Total")]
			[query 
				"select acct_xact.description as description,
							sum(quantity) as quantity,
							sum(quantity * amount) as subtotal,
							sum(quantity * sales_tax) as tax,
							(sum(quantity * amount) +
								sum(quantity * sales_tax)) as total
					from acct_xact, acct
					where (to_acct=acct.id)~a
					and (acct.name='general')
					and (sales_tax > 0)
					and (date_posted >= '~a')
					and (date_posted <= '~a')
					group by acct_xact.description
					order by acct_xact.description asc"])
		(define (chart-row row spreadsheet)
			(list
				(list
					(pg-cell row 'description)
					(right-justified (pg-cell row 'quantity) spreadsheet)
					(right-justified
						(dollars (pg-cell row 'subtotal)) spreadsheet)
					(right-justified
						(dollars (pg-cell row 'tax)) spreadsheet)
					(right-justified
						(dollars (pg-cell row 'total)) spreadsheet))
				(pg-cell row 'subtotal)
				(pg-cell row 'tax)
				(pg-cell row 'total)))
		(define (sums rows spreadsheet)
			(let loop ([data rows]
					[grand-total 0]
					[sub-total 0]
					[tax-total 0])
				(if (null? data)
					(list "" ""
						(right-justified (dollars sub-total) spreadsheet)
						(right-justified (dollars tax-total) spreadsheet)
						(right-justified
							(dollars grand-total) spreadsheet))
					(loop
						(cdr data)
						(+ grand-total (cadddr (car data)))
						(+ sub-total (cadr (car data)))
						(+ tax-total (caddr (car data)))))))
		(lambda (from-date to-date tags spreadsheet)
			(let* ([chart
						(pg-map-rows
							(pg-exec (sdb)
								(format #f query
									(products-clause tags)
									from-date to-date))
							(lambda (row) (chart-row row spreadsheet)))]
					[data
						(append (map car chart)
							(list (sums chart spreadsheet)))]
					[title
						(format #f "Sales Tax by Item, ~a - ~a"
							from-date to-date)])
				(if spreadsheet
					(pack-spreadsheet title header data)
					(build-table title header "taxsales" data))))))
(define staff-name
	(let ([query "select owner from acct where (id=~a)"])
		(lambda (acct-id)
			(let ([row (pg-one-row (sdb) query acct-id)])
				(person-name (pg-cell row 'owner)
					(env-name-order) (sdb))))))
(define payouts-by-date
	(let ([query "select date_posted, to_acct,
						(amount * quantity) as amount
					from acct_xact, acct, product_item
					where (acct.name='general')
					and (from_acct=acct.id)
					and (acct_xact.product_id=product_item.id)
					and product_item.wage
					and (date_posted >= ~a)
					and (date_posted <= ~a)
					order by date_posted asc"])
		(define (one-row row)
			(list
				(time-format (pg-cell row 'date_posted) "%Y-%m-%d")
				(staff-name (pg-cell row 'to_acct))
				(pg-cell row 'amount)))
		(define (total rows sum)
			(if (null? rows) sum
				(total (cdr rows) (+ sum (caddr (car rows))))))
		(define (date-name-sort row1 row2)
			(let ([date1 (car row1)]
					[name1 (cadr row1)]
					[date2 (car row2)]
					[name2 (cadr row2)])
				(if (string=? date1 date2)
					(string<? name1 name2)
					(string<? date1 date2))))
		(lambda (from-date to-date)
			(let ([data
						(sort-list
							(pg-map-rows
								(pg-exec (sdb) query from-date to-date)
								(lambda (row) (one-row row)))
							date-name-sort)])
				(append
					data
					(list (list "" "" (total data 0))))))))
(define payouts-by-name
	(let ([query "select to_acct, sum(amount * quantity) as amount
					from acct_xact, acct, product_item
					where (acct.name='general')
					and (from_acct=acct.id)
					and (acct_xact.product_id=product_item.id)
					and product_item.wage
					and (date_posted >= ~a)
					and (date_posted <= ~a)
					group by to_acct"])
		(define (one-row row)
			(list
				(staff-name (pg-cell row 'to_acct))
				(pg-cell row 'amount)))
		(define (total rows sum)
			(if (null? rows) sum
				(total (cdr rows) (+ sum (cadr (car rows))))))
		(lambda (from-date to-date)
			(let ([data
						(sort-list
							(pg-map-rows
								(pg-exec (sdb) query from-date to-date)
								(lambda (row) (one-row row)))
							(lambda (row1 row2)
								(string<? (car row1) (car row2))))])
				(append
					data
					(list (list "" (total data 0))))))))
(define untaxed-sales
	(let ([query "select sum(quantity) as quantity,
						sum(quantity * amount) as total,
						regexp_replace(acct_xact.description,
							', load [0-9]+.*$', '') as detail
					from acct_xact, acct
					where (sales_tax=0)~a
					and (acct.name='general')
					and (to_acct=acct.id)
					and (acct_xact.description not like '%Staff Payout%')
					and (date_posted >= '~a')
					and (date_posted <= '~a')
					group by regexp_replace(acct_xact.description,
						', load [0-9]+.*$', '')
					order by regexp_replace(acct_xact.description,
						', load [0-9]+.*$', '')"])
		(define (one-row row)
			(list
				(pg-cell row 'quantity)
				(pg-cell row 'total)
				(pg-cell row 'detail)))
		(define (total rows sum)
			(if (null? rows) sum
				(total (cdr rows) (+ sum (cadr (car rows))))))
		(lambda (from-date to-date tags)
			(let ([data
					(pg-map-rows
						(pg-exec (sdb)
							(format #f query
								(products-clause tags)
								from-date to-date))
						one-row)
						]
					)
				(append
					data
					(list (list "" (total data 0) "")))))))
(define (date-qual date form)
	(if (string=? date "") ""
		(pg-query (sdb) form (list date))))
(define weight-balance
	(let ([slot-count
				"select id
					from load_slots
					where (load_slots.load_id=~a)
					and (passenger is not null) limit 1"]
			[load-query-tpt
				"select distinct load_id as id,
						load_date, load_num, tail_num
					from load_slots, load_sheets, aircraft
					where
					(load_sheets.aircraft_id=aircraft.id)[[START]][[END]]
					and (load_sheets.id=load_slots.load_id)
					and (passenger is not null)
					order by load_date, tail_num, load_num"]
			[load-detail-query
				"select load_date, aircraft.tail_num as aircraft,
						load_sheets.load_num as load_num
					from load_sheets, aircraft
					where (load_sheets.id=~a)
					and (load_sheets.aircraft_id=aircraft.id)"])
		(define (load-detail load-id)
			(pg-one-row (sdb) load-detail-query load-id))
		(define (qualified-value pair)
			(format #f "~1,1f~a"
				(to-f (cdr pair))
				(if (car pair) "" "?")))
		(define (cg moment-pair weight-pair)
			(let ([moment (cdr moment-pair)]
					[weight (cdr weight-pair)])
				(if (= weight 0) "N/A"
					(format #f "~1,1f~a"
						(/ (to-f moment) weight)
						(if (and (car moment-pair) (car weight-pair))
							"" "?")))))
		(define (load-row load-id)
			(let* ([detail (load-detail load-id)]
					[pilot (pilot-weight-balance load-id (sdb))]
					[fuel (fuel-weight-balance load-id (sdb))]
					[empty (empty-weight-balance load-id (sdb))]
					[payload (payload-weight-balance load-id (sdb))]
					[total-weight
						(sum-weights
							(list
							(car empty)
							(car fuel)
							(car pilot)
							(car payload)))]
					[total-moment
						(sum-weights
							(list
							(cdr empty)
							(cdr fuel)
							(cdr pilot)
							(cdr payload)))])
				(list
					(time-format (pg-cell detail 'load_date) "%Y-%m-%d")
					(pg-cell detail 'aircraft)
					(to-s (pg-cell detail 'load_num))
					(qualified-value (car empty))
					(qualified-value (car fuel))
					(qualified-value (car pilot))
					(qualified-value (car payload))
					(qualified-value total-weight)
					(qualified-value total-moment)
					(cg total-moment total-weight))))
		(define (load-query start-date end-date)
			(fill-template load-query-tpt #f
				(cons 'start
					(date-qual start-date " and (load_date >= ~a)"))
				(cons 'end
					(date-qual end-date " and (load_date <= ~a)"))))
		(lambda (start-date end-date)
			(map
				(lambda (load-id) (load-row load-id))
				(pg-map-rows
					(pg-exec (sdb)
						(load-query start-date end-date)
						start-date end-date)
					(lambda (row) (pg-cell row 'id)))))))
(define jumped-this-year
	; Screen list of tuples to select those associated with people
	; who have made at least one jump in the current year.
	; First element of tuple is people.id.
	(let ([query
				"select load_slots.id
					from load_sheets, load_slots, people
					where (date_part('year', load_sheets.load_date)
							= date_part('year', current_date))
					and (load_slots.load_id=load_sheets.id)
					and (load_slots.passenger=~a)
					limit 1"])
		(define (jump-check person-id)
			; has jumper had at least one jump in the current year?
			(pg-one-row (sdb) query person-id))
		(lambda (screen tuples)
			(map (lambda (tuple) (cdr tuple))
				(if screen
					(filter
						(lambda (tuple)
							(jump-check (car tuple))) tuples)
					tuples)))))
(define resolve-match
	(let ()
		(define (reduce reduction resolved)
			(cond
				[(null? resolved) '()]
				[(< (length resolved) 2) (car resolved)]
				[else (apply reduction resolved)]))
		(lambda (reduction operands)
			(let resolve ([ops operands]
					[resolved '()])
				(cond
					[(null? ops) (reduce reduction resolved)]
					[(string? (caar ops))
						(log-msg "resolve tags ~s" (car ops))
						(resolve (cdr ops)
							(cons (products-match (car ops) 'and)
								resolved))]
					[(eq? (caar ops) 'or)
						(log-msg "resolve-or ~s" (cdar ops))
						(resolve (cdr ops)
							(cons
								(resolve-match union
									(cdar ops)) resolved))]
					[(eq? (caar ops) 'and)
						(log-msg "resolve-and ~s" (cdar ops))
						(resolve (cdr ops)
							(cons
								(resolve-match intersection
									(cdar ops)) resolved))]
					[else
						(log-msg "resolve-other ~s" (car ops))
						(resolve (cdr ops)
							(cons (car ops) resolved))])))))
(define product-sales
	(let ([query
				"select date_posted, amount,
						acct_xact.description as description,
						quantity, to_acct as acct,
						product_item.name as product
					from acct_xact, product_item
					where (product_item.id=acct_xact.product_id)~a
					and (date_posted >= '~a')
					and (date_posted <= '~a')
					order by date_posted asc, entered asc"]
			[acct-query
				"select owner, name, personal
					from acct
					where (id=~a)"]
			[headers
				(list
					"Date" "Acct" "Description" "Value" "Quantity"
					"Total")]
			[title "Product Sales"])
		(define (acct-name acct dz-name)
			(let ([row (pg-one-row (sdb) acct-query acct)])
				(if (pg-cell row 'personal)
					(person-name (pg-cell row 'owner)
						(env-name-order) (sdb))
					dz-name)))
		(define (html-row row dz-name spreadsheet)
			(let ([amount (pg-cell row 'amount)]
					[qty (pg-cell row 'quantity)])
				(list
					(show-date (pg-cell row 'date_posted)
						(env-date-format))
					(acct-name (pg-cell row 'acct) dz-name)
					(pg-cell row 'description)
					(right-justified (dollars amount) spreadsheet)
					(right-justified qty spreadsheet)
					(right-justified
						(dollars (* amount qty)) spreadsheet))))
		(define (totals rows)
			(let sum ([data rows]
					[sums '(0 . 0)])
				(if (null? data) sums
					(sum (cdr data)
						(cons
							(+ (car sums)
								(pg-cell (car data) 'quantity))
							(+ (cdr sums)
								(*
									(pg-cell (car data) 'amount)
									(pg-cell (car data) 'quantity))))))))
		(define (dressed-data raw-data spreadsheet)
			(let ([sums (totals raw-data)])
				(append
					(map
						(lambda (row)
							(html-row row
								(env-dz-short-name) spreadsheet))
						raw-data)
					(list
						(list "" "" "" ""
							(right-justified (car sums) spreadsheet)
							(right-justified
								(dollars (cdr sums)) spreadsheet))))))
		(lambda (from-date to-date tags spreadsheet)
			(let ([rows
						(pg-map-rows
							(pg-exec (sdb)
								(format #f query
									(products-clause tags)
									from-date to-date)))])
				(if spreadsheet
					(pack-spreadsheet title headers
						(dressed-data rows #t))
					(build-table title headers "sales"
						(dressed-data rows #f)))))))
(define drop-tag-preset
	(let ([delete "delete from tag_sets where (id=~a)"]
			[next-id-query
				"select id, name
					from tag_sets
					order by name asc"])
		(define (next-id preset-id)
			(let ([res (pg-exec (sdb) next-id-query)])
				(let scan ([row (pg-next-row res)]
							[found #f])
					(cond
						[(not row) '()]
						[found (pg-clear res) (pg-cell row 'id)]
						[(= preset-id (pg-cell row 'id))
							(scan (pg-next-row res) #t)]
						[else (scan (pg-next-row res) #f)]))))
		(lambda (preset-id)
			(let ([id (next-id preset-id)])
				(pg-exec (sdb) delete preset-id) id))))
(define get-tag-set
	(let ([query "select tags from tag_sets where id=~a"])
		(lambda (tag-id)
			(let ([row (pg-one-row (sdb) query tag-id)])
				(if row (pg-cell row 'tags) "")))))
(define add-tag-preset
	(let ([id-query "select nextval('tag_set_seq') as id"]
			[insert
				"insert into tag_sets (id, name, tags)
					values (~a, ~a, ~a)"])
		(lambda (preset-name tags)
			(let ([id (pg-cell (pg-one-row (sdb) id-query) 'id)])
				(pg-exec (sdb) insert id preset-name tags) id))))
(define tag-presets
	(let ([query "select id, name from tag_sets"])
		(lambda ()
			(pg-map-rows
				(pg-exec (sdb) query)
				(lambda (row)
					(list
						(pg-cell row 'id)
						(pg-cell row 'name)))))))
(define (memberships-title date temporary-memb)
	(if temporary-memb
		"USPA Temporary Memberships"
		(format #f "USPA Memberships Expired as of ~a" date)))

(log-to "/var/log/nowcall/reports.log")

(http-html "/"
	(lambda (req)
		(if (authorized-for req "reports")
			(fill-template (fetch-doc composite) #f
				(cons 'admin_check (admin-light req (sdb)))
				(cons 'dz_full_name (env-dz-full-name)))
			(redirect-html "/login/reports"))))
(http-json "/datefmt"
	(lambda (req)
		(list
			(cons 'format (env-date-format))
			(cons 'search_length (env-min-search-chars)))))
(http-json "/resvood"
	(admin-gate "reports" (req)
		(let ([date (date-or-today (query-value req 'to_date))]
				[jumped (query-value-boolean req 'jty)])
			(list
				(cons 'status #t)
				(cons 'html
					(build-table
						(format #f "Reserves Out of Date as of ~a" date)
						(list "Jumper" "Last Repack")
						"resvood"
						(jumped-this-year jumped
							(reserve-out-of-date date))))))))
(responder "/resvood_ss"
	(admin-gate "reports" (req)
		(let* ([date (date-or-today (query-value req 'to_date))]
				[jumped (query-value-boolean req 'jty)])
			(csv-response
				(build-spreadsheet
					(format #f "Reserves Out of Date as of ~a" date)
					(list "Jumper" "Last Repack")
					(jumped-this-year jumped
						(reserve-out-of-date date)))))))
(http-json "/uspaexp"
	(admin-gate "reports" (req)
		(let ([date (date-or-today (query-value req 'to_date))]
				[jumped (query-value-boolean req 'jty)]
				[tmp-memb (query-value-boolean req 'tmp)])
			(list
				(cons 'status #t)
				(cons 'html
					(build-table
						(memberships-title date tmp-memb)
						(list "Jumper" "Expiration")
						"uspaexp"
						(jumped-this-year jumped
							(uspa-expires date tmp-memb))))))))
(responder "/uspaexp_ss"
	(admin-gate "reports" (req)
		(let ([date (date-or-today (query-value req 'to_date))]
				[jumped (query-value-boolean req 'jty)]
				[tmp-memb (query-value-boolean req 'tmp)])
			(csv-response
				(build-spreadsheet
					(memberships-title date tmp-memb)
					(list "Jumper" "Expiration")
					(jumped-this-year jumped
						(uspa-expires date tmp-memb)))))))
(http-json "/waiver"
	(admin-gate "reports" (req)
		(let ([date (date-or-today (query-value req 'to_date))]
				[jumped (query-value-boolean req 'jty)])
			(list
				(cons 'status #t)
				(cons 'html
					(build-table
						(format #f
							"DZ Waivers Expired as of ~a" date)
						(list "Jumper" "Expiration")
						"waiver"
						(jumped-this-year jumped
							(waiver-expires date))))))))
(responder "/waiver_ss"
	(admin-gate "reports" (req)
		(let ([date (date-or-today (query-value req 'to_date))]
				[jumped (query-value-boolean req 'jty)])
			(csv-response
				(build-spreadsheet
					(format #f "DZ Waivers Expired as of ~a" date)
					(list "Jumper" "Expiration")
					(jumped-this-year jumped
						(waiver-expires date)))))))
(http-json "/custjump"
	(admin-gate "reports" (req)
		(let ([from-date (query-value req 'from_date)]
				[to-date (query-value req 'to_date)]
				[customer (query-value-number req 'customer)])
			(if (= customer 0)
				(list
					(cons 'status #f)
					(cons 'msg "Please select a customer."))
				(list
					(cons 'status #t)
					(cons 'html
						(build-table
							"Customer Skydives"
							(list "Date" "Jumper" "Aircraft"
								"Load#" "Activity")
							"custjump"
							(customer-skydives from-date
								to-date customer #f))))))))
(responder "/custjump_ss"
	(admin-gate "reports" (req)
		(let ([from-date (query-value req 'from_date)]
				[to-date (query-value req 'to_date)]
				[customer (query-value-number req 'customer)])
			(csv-response
				(build-spreadsheet
					"Customer Skydives"
					(list "Date" "Jumper" "Aircraft" "Load#" "Activity")
					(customer-skydives from-date to-date
						customer #t))))))
(http-json "/custacct"
	(admin-gate "reports" (req)
		(let ([from-date (query-value req 'from_date)]
				[to-date (query-value req 'to_date)]
				[customer (query-value-number req 'customer)])
			(if (= customer 0)
				(list
					(cons 'status #f)
					(cons 'msg "Please select a customer."))
				(list
					(cons 'status #t)
					(cons 'html
						(build-table
							"Customer Account Transactions"
							(list "Date" "Customer" "Detail"
								"Debit" "Credit" "Balance")
							"custacct"
							(customer-acct from-date
								to-date customer #f))))))))
(responder "/custacct_ss"
	(admin-gate "reports" (req)
		(let ([from-date (query-value req 'from_date)]
				[to-date (query-value req 'to_date)]
				[customer (query-value-number req 'customer)])
			(csv-response
				(build-spreadsheet
					"Customer Account Transactions"
					(list "Date" "Customer" "Detail"
						"Debit" "Credit" "Balance")
					(customer-acct from-date to-date customer #t))))))
(http-json "/taxsales"
	(admin-gate "reports" (req)
		(let ([to-date (date-or-today (query-value req 'to_date))]
				[tags (json-decode (query-value req 'tags))]
				[from-date (date-or-today (query-value req 'from_date))])
			(list
				(cons 'status #t)
				(cons 'html
					(format #f "<p>~a</p><p>~a</p>"
						(sales-tax-by-item from-date to-date tags #f)
						(sales-tax-by-date from-date
							to-date tags #f)))))))
(responder "/taxsales_ss"
	(admin-gate "reports" (req)
		(let ([to-date (date-or-today (query-value req 'to_date))]
				[tags (json-decode (query-value req 'tags))]
				[from-date (date-or-today (query-value req 'from_date))])
			(csv-response
				(build-composite-spreadsheet
					(sales-tax-by-item from-date to-date tags #t)
					(sales-tax-by-date from-date to-date tags #t))))))
(http-json "/payouts"
	(admin-gate "reports" (req)
		(let* ([to-date (date-or-today (query-value req 'to_date))]
				[from-date (date-or-today (query-value req 'from_date))])
			(list
				(cons 'status #t)
				(cons 'html
					(format #f "<p>~a</p><p>~a</p>"
						(build-table
							(format #f "Staff Payouts by Name, ~a - ~a"
								from-date to-date)
							(list "Name" "Amount")
							"payouts"
							(map
								(lambda (row) 
									(list
										(car row)
										(right-justified
											(dollars (cadr row)) #f)))
								(payouts-by-name from-date to-date)))
						(build-table
							(format #f "Staff Payouts by Date, ~a - ~a"
								from-date to-date)
							(list "Date" "Name" "Amount")
							"payouts"
							(map
								(lambda (row)
									(list
										(car row)
										(cadr row)
										(right-justified
											(dollars (caddr row)) #f)))
								(payouts-by-date
									from-date to-date)))))))))
(responder "/payouts_ss"
	(admin-gate "reports" (req)
		(let* ([to-date (date-or-today (query-value req 'to_date))]
				[from-date (date-or-today (query-value req 'from_date))])
			(csv-response
				(build-composite-spreadsheet
					(pack-spreadsheet
						(format #f "Staff Payouts by Name, ~a - ~a"
							from-date to-date)
						(list "Name" "Amount")
						(map
							(lambda (row)
								(list
									(car row)
									(dollars (cadr row))))
							(payouts-by-name from-date to-date)))
					(pack-spreadsheet
						(format #f "Staff Payouts by Date, ~a"
							from-date to-date)
						(list "Date" "Name" "Amount")
						(map
							(lambda (row)
								(list
									(car row)
									(cadr row)
									(dollars (caddr row))))
							(payouts-by-date from-date to-date))))))))
(http-json "/notaxsales"
	(admin-gate "reports" (req)
		(let ([to-date (date-or-today (query-value req 'to_date))]
				[tags (json-decode (query-value req 'tags))]
				[from-date (date-or-today (query-value req 'from_date))])
			(list
				(cons 'status #t)
				(cons 'html
					(build-table
						(format #f "Untaxed Sales, ~a - ~a"
							from-date to-date)
						(list "Quantity" "Total" "Description")
						"notaxsales"
						(map
							(lambda (row)
								(list
									(right-justified (car row) #f)
									(right-justified
										(dollars (cadr row)) #f)
									(caddr row)))
							(untaxed-sales from-date to-date
								tags))))))))
(responder "/notaxsales_ss"
	(admin-gate "reports" (req)
		(let ([to-date (date-or-today (query-value req 'to_date))]
				[tags (json-decode (query-value req 'tags))]
				[from-date (date-or-today (query-value req 'from_date))])
			(csv-response
				(build-spreadsheet
					(format #f "Untaxed Sales, ~a - ~a"
						from-date to-date)
					(list "Quantity" "Total" "Description")
					(map
						(lambda (row)
							(list
								(to-s (car row))
								(dollars (cadr row))
								(caddr row)))
						(untaxed-sales from-date to-date tags)))))))
(http-json "/wtbal"
	(admin-gate "reports" (req)
		(let* ([from-date (query-value req 'from_date)]
				[to-date (query-value req 'to_date)])
			(list
				(cons 'status #t)
				(cons 'html
					(build-table
						"Weight/Balance"
						(list "Date" "Aircraft" "Load"
							"Empty Weight" "Fuel Weight" "Pilot Weight"
							"Payload Weight" "Total Weight"
							"Total Moment" "CG")
						"wtbal"
						(map
							(lambda (row)
								(append
									(list (car row) (cadr row))
									(map
										(lambda (item)
											(right-justified item #f))
										(cddr row))))
							(weight-balance from-date to-date))))))))
(responder "/wtbal_ss"
	(admin-gate "reports" (req)
		(let* ([from-date (query-value req 'from_date)]
				[to-date (query-value req 'to_date)])
			(csv-response
				(build-spreadsheet
					"Weight/Balance"
					(list "Date" "Aircraft" "Load"
						"Empty Weight" "Fuel Weight" "Pilot Weight"
						"Payload Weight" "Total Weight"
						"Total Moment" "CG")
					(weight-balance from-date to-date))))))
(http-json "/sales"
	(admin-gate "reports" (req)
		(let ([from-date (date-or-today (query-value req 'from_date))]
				[to-date (date-or-today (query-value req 'to_date))]
				[tags (json-decode (query-value req 'tags))])
			(list
				(cons 'status #t)
				(cons 'html
					(product-sales from-date to-date tags #f))))))
(responder "/sales_ss"
	(admin-gate "reports" (req)
		(let ([from-date (date-or-today (query-value req 'from_date))]
				[to-date (date-or-today (query-value req 'to_date))]
				[tags (json-decode (query-value req 'tags))])
			(csv-response
				(build-composite-spreadsheet
					(product-sales from-date to-date tags #t))))))
(http-json "/loadpresets"
	(admin-gate "reports" (req)
		(list
			(cons 'tags (tag-presets)))))
(http-json "/addpreset"
	(admin-gate "reports" (req)
		(let ([preset-name (query-value req 'name)]
				[tags (query-value req 'tags)])
			(list
				(cons
					'set_id (add-tag-preset preset-name tags))))))
(http-json "/droppreset"
	(admin-gate "reports" (req)
		(let ([preset-id (query-value-number req 'preset)])
			(list (cons 'next_id (drop-tag-preset preset-id))))))
(http-json "/loadtags"
	(admin-gate "reports" (req)
		(let ([preset (query-value-number req 'preset)])
			(list
				(cons 'tags (get-tag-set preset))))))
(add-javascript-logger "reports")
