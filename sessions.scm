
(include "database.scm")

(define (session-key http-request)
	(assq-ref http-request 'session))
(define session-read
	(let ([query
				"select profile
					from sessions
					where (key=~a)"])
		(lambda (http-request)
			(let ([row (pg-one-row (sdb) query
							(session-key http-request))])
				(if row (json-decode (pg-cell row 'profile)) '())))))
(define (session-get http-request key)
	(assq-ref (session-read http-request) key))
(define session-set
	(let ([update
				"update sessions set
					profile=~a,
					touched=~a
					where (key=~a)"]
			[insert
				"insert into sessions (key, profile, touched)
					values (~a, ~a, ~a)"])
		(define (update-session key profile)
			(> (pg-cmd-tuples
					(pg-exec (sdb) update	
						(json-encode profile) (time-now) key)) 0))
		(lambda (http-request key value)
			(let ([profile (session-read http-request)]
					[s-key (session-key http-request)])
				(set! profile (assq-set! profile key value))
				(set! profile
					(assq-set! profile 'stamp
						(to-i (time-epoch (time-now)))))
				(or (update-session s-key profile)
					(pg-exec (sdb) insert
						s-key (json-encode profile) (time-now)))))))
(define session-unset
	(let ([query
				"update sessions set
					profile=~a,
					touched=~a
					where (key=~a)"])
		(lambda (http-request key)
			(let ([profile (session-read http-request)])
				(set! profile (assq-remove! profile key))
				(set! profile
					(assq-set! profile 'stamp
						(to-i (time-epoch (time-now)))))
				(pg-exec (sdb) query
					(json-encode profile)
					(time-now)
					(session-key http-request))))))
