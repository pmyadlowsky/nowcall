#! /usr/local/bin/gusher -p 3003
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))
(use-modules (ice-9 popen))
(use-modules (ice-9 ftw))
(use-modules (ice-9 rdelim))

(include "lib.scm")
(include "settings_lib.scm")

(define general-menu
	(list
		(list "/schedule" "Staff Schedule")
		(list "/manifest" "Manifest")
		(list "/mobman" "Manifest/Mobile")
		(list "/me" "My Stuff")
		(list "/wiki" "Wiki")))
(define authd-menu
	(list
		(cons 'aircraft (list "/aircraft" "Aircraft"))
		(cons 'people (list "/people" "People"))
		(cons 'accounting (list "/accounting" "Accounting"))
		(cons 'products (list "/products" "Products/Services"))
		(cons 'reports (list "/reports" "Reports"))
		(cons 'backups (list "/backups" "Backups"))
		(cons 'reservations (list "/reservations" "Reservations"))
		(cons 'squawk (list "/squawk" "Squawk"))
		(cons 'whiteboard (list "/whiteboard" "Whiteboard"))
		(cons 'staff_sched (list "/schedule" "Staff Schedule"))
		(cons 'settings (list "/settings" "Settings"))))
(define build-menu
	; build NowCall app menu according to logged-in user's
	; authorization set
	(let ([all-apps
				(map
					(lambda (tuple)
						(symbol->string (car tuple)))
					authd-menu)])
		(define (menu-url-present? menu url)
			(cond 
				[(null? menu) #f]
				[(string=? url (caar menu)) #t]
				[else (menu-url-present? (cdr menu) url)]))
		(define (authd-app? app auths on-dz-only)
			(cond
				[(null? auths) #f]
				[(and
					(string=? (car auths) (symbol->string (car app)))
					(or (not on-dz-only) (env-on-dz-lan))) (cdr app)]
				[else (authd-app? app (cdr auths) on-dz-only)]))
		(define (add-option menu authd-app auths on-dz-only)
			(let ([pair (authd-app? authd-app auths on-dz-only)])
				(if (and pair (not (menu-url-present? menu (car pair))))
					(cons (cdr authd-app) menu) menu)))
		(define (menu-entries authorizations on-dz-only)
			(let scan ([authd authd-menu]
					[menu general-menu])
				(if (null? authd)
					(reverse menu)
					(scan
						(cdr authd)
						(add-option menu (car authd)
							authorizations on-dz-only)))))
		(define (sort-by-label a b) (string<? (cadr a) (cadr b)))
		(lambda (req)
			(let ([auths (user-authorizations req)])
				(sort
					(cond
						[(null? auths) general-menu]
						[(admin-user req)
							(menu-entries all-apps #f)]
						[(can-work-remotely? req)
							(menu-entries auths #f)]
						[else (menu-entries auths #t)])
					sort-by-label)))))
(define frame (make-doc 'file "frame.html"))
(define page-body (make-doc 'file "index.html"))
(define composite
	(make-doc (list frame page-body)
		(lambda ()
			(fill-template (fetch-doc frame) #f
				(cons 'title "Home")
				(cons 'app "index")
				(cons 'script "")
				(cons 'version nowcall-version)
				(cons 'content
					(fill-template (fetch-doc page-body) #t))))))

(log-to "/var/log/nowcall/index.log")

(http-html "/" (lambda (req) (redirect-html "/manifest")))
(http-json "/menu" (lambda (req) (list (cons 'menu (build-menu req)))))
(http-json "/cip"
	(lambda (req)
		(let ([client-ip (assq-ref req 'x-forwarded-for)]
				[client-secret (query-value req 's)])
			(let ([client-auth
						(and
							client-ip
							client-secret
							(string=? client-secret
								(get-setting 'shared-secret (sdb))))])
				(when client-auth
					(set-setting 'dz-ip-addr client-ip (sdb)))
				(list
					(cons 'ip_addr client-ip)
					(cons 'status client-auth))))))
