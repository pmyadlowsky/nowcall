#! /usr/bin/ruby

# Initialize whiteboard sequence numbers; for transition
# to resequence-able whiteboard.

Query = <<END
select id, created::date as date
	from whiteboard
	where (seq is null) order by id;
END

Form = <<END
update whiteboard set
	seq=(select (coalesce(max(seq),0) + 1)
                  from whiteboard
                   where created::date='DATE')
	where (id=ID);
END

pipe = IO::popen("echo '#{Query}' | docker/sql", "r")

header = true
pipe.readlines.each do |line|
	if line =~ /^---/
		# wait for postgres output "----" crossbar 
		header = false
		next
		end
	next if header 
	# data lines have " | " column delimiter
	next unless line =~ / \| /
	parts = line.split("|").map{|s| s.strip}
	query = Form.sub("ID", parts[0].to_i.to_s)
	query = query.sub("DATE", parts[1])
	puts query
	end

pipe.close
