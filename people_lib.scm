(define username-available
	(let ([query
				"select login_name, id
					from people
					where (login_name=~a)"])
		(lambda (name owner dbh)
			(let ([row (pg-one-row dbh query name)])
				(cond
					[(not row) #t]
					[(= (pg-cell row 'id) owner) 'mine]
					[else #f])))))
