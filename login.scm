#! /usr/local/bin/gusher -p 3007
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))
(use-modules (ice-9 regex))

(include "lib.scm")
(include "settings_lib.scm")
(include "email.scm")
(include "sessions.scm")

(define env-dz-email-addr (setting-cache 'dz-email-addr))
(define env-website (setting-cache 'website))
(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-dz-short-name (setting-cache 'dz-short-name))

(define page-body (make-doc 'file "login.html"))
(define page-body-mobile (make-doc 'file "loginmob.html"))
(define set-password-body (make-doc 'file "setpw.html"))
(define failed-login "\
<div style=\"color: red; font-weight: bold;
	font-size: 115%; padding: 0.5em\">
Sorry, authentication failed.
</div>")
(define non-admin "\
<div style=\"color: red; font-weight: bold;
	font-size: 115%; padding: 0.5em\">
Sorry, you're not authorized for administrative privileges.
</div>")
(define redirect
	(let ([script
				(deflate "<script type=\"text/javascript\">
				window.location = \"/~a\";
				</script>")])
		(lambda (req destination)
			(format #f script destination))))
(define not-authd "\
<div style=\"color: red; font-weight: bold;
	font-size: 115%; padding: 0.5em\">
Sorry, you're not authorized for ~a.
</div>")
(define email-template "\
From: [[SENDER]]
To: [[RECIPIENT]]
Bcc: pm.yadlowsky@startmail.com
Subject: NowCall Password Reset
Content-type: text/html; charset=\"utf-8\"
Content-disposition: inline

<html>
<head>
</head>
<body>
<p>Hi[[GREET_NAME]],</p>
<p>Please follow
<a href=\"https://[[HOST_DOMAIN]]/login/setpw?k=[[KEY]]\">this link</a>
to set your NowCall password. Your password reset request expires
[[EXPIRE]] minutes after you receive this email. Thanks!</p>
</body>
</html>")
(define default-destination "manifest")
(define (clean-param str) (string-trim-both (or str "")))
(define ticket-lifespan 30) ;min
(define addressee
	(let ([query
				"select coalesce(goes_by, first_name) as name
					from people
					where (id=~a)"])
		(lambda (person-id)
			(let* ([row (pg-one-row (sdb) query person-id)]
					[name (and row (pg-cell row 'name))])
				(if name
					(if (null? name) "" (format #f " ~a" name)) "")))))
(define send-reset-email
	(lambda (person-id recipient site ticket-id)
		(let ([sender (env-dz-email-addr)])
			(send-email
				sender
				(list recipient)
				(fill-template email-template #f
					(cons 'greet_name (addressee person-id))
					(cons 'sender sender)
					(cons 'recipient recipient)
					(cons 'key ticket-id)
					(cons 'host_domain site)
					(cons 'expire ticket-lifespan))))))
(define verify-email
	(let ([query
				"select id
					from people
					where (lower(email)=lower(~a))
					limit 1"])
		(lambda (email-addr)
			(let ([row (pg-one-row (sdb) query email-addr)])
				(and row (pg-cell row 'id))))))
(define make-registration-ticket
	(let ([query
				"insert into reg_tickets (key, person_id, email)
					values (~a, ~a, ~a)"])
		(lambda (person-id email-addr)
			(let ([key (uuid-generate)])
				(pg-exec (sdb) query key person-id email-addr) key))))
(define contact-link
	(let ()
		(define (email-addr dz-name)
			(let ([email (env-dz-email-addr)])
				(if (string=? email "") #f
					(format #f "<a href=\"mailto:~a\">~a</a>"
						email dz-name))))
		(define (web-link dz-name)
			(let ([url (env-website)])
				(if (string=? url "") #f
					(format #f "<a target=\"_blank\" href=\"~a\">~a</a>"
						url dz-name))))
		(lambda (email-first)
			(let ([dz-name (env-dz-full-name)])
				(or
					(if email-first
						(email-addr dz-name)
						(web-link dz-name))
					(if email-first
						(web-link dz-name)
						(email-addr dz-name))
					dz-name)))))
(define verify-fail-msg
	(let ([template
				(deflate "<div><b>'~a'</b> is not
					known to ~a NowCall. Please contact
					~a to register with us or to update your
					registration.</div>")])
		(define (email-addr dz-name)
			(let ([email (env-dz-email-addr)])
				(if (string=? email "") #f
					(format #f "<a href=\"mailto:~a\">~a</a>"
						email dz-name))))
		(define (web-link dz-name)
			(let ([url (env-website)])
				(if (string=? url "") #f
					(format #f "<a target=\"_blank\" href=\"~a\">~a</a>"
						url dz-name))))
		(define (dz-link)
			(let ([dz-name (env-dz-full-name)])
				(or
					(web-link dz-name)
					(email-addr dz-name)
					dz-name)))
		(lambda (email-addr)
			(format #f template
				email-addr
				(env-dz-short-name)
				(contact-link #t)))))
(define verify-success-msg
	(deflate "Thanks! Email has been sent to the address above
		with instructions on setting your NowCall password."))
(define email-failed-msg
	(deflate "We're sorry, but your reset email could not be sent
		for some reason. Please contact the dropzone (link above)."))
(define get-ticket
	(let ([query
				"delete from reg_tickets
					where (stamp <
						(current_timestamp - interval '~a minutes'));
				select person_id, email
					from reg_tickets
					where (key=~a)"]
			[rip-ticket "delete from reg_tickets where (key=~a)"])
		(lambda (ticket-id)
			(pg-one-row (sdb) query ticket-lifespan ticket-id))))
(define set-credentials
	(let ([query
				"update people set
					login_password=~a
					where (id=~a)"]
			[set-user
				"update people set
					login_name=~a
					where (id=~a)"]
			[set-email-user
				"update people set
					login_name=~a
					where (id=~a)
					and (login_name is null)"]
			[rip-ticket "delete from reg_tickets where (person_id=~a)"])
		(lambda (username person-id email password)
			(pg-exec (sdb) query (sha-256-sum password) person-id)
			(if (string=? username "")
				(pg-exec (sdb) set-email-user email person-id)
				(pg-exec (sdb) set-user username person-id))
			(pg-exec (sdb) rip-ticket person-id))))
(define data-errors
	(let ([avail-query
				"select id
					from people
					where (login_name=~a)
					and (id != ~a)
					limit 1"])
		(define (username-avail username person-id)
			(if (string=? username "") #t
				(let ([row
						(pg-one-row (sdb) avail-query username person-id)])
					(not row))))
		(lambda (person-id password confirm username)
			(let ([errs
						(filter (lambda (item) item)
							(list
								(cond
									[(string=? password "")
										"please provide password"]
									[(not (string=? password confirm))
										"passwords don't match"]
									[else #f])
								(if (username-avail username person-id)
									#f
									"username is not available")))])
				(if (null? errs) #f errs)))))
(define (render-errors errs)
	(string-cat "\n"
		"Please correct:<ul>"
		(map
			(lambda (err) (format "<li>~a</li>" err)) errs) "</ul>"))
(define post-success "\
<b>Success!</b> Login credentials set. You may now use them to
<a href=\"/login/manifest\">sign in</a> to NowCall.")
(define invalid-ticket
	(deflate "Sorry, your password reset request is invalid or has
		expired. <a href=\"/login\">Try again?</a>"))
(define (present-page msg destination mobile)
	(fill-template
		(fetch-doc (if mobile page-body-mobile page-body)) #f
		(cons 'dz_full_name (env-dz-full-name))
		(cons 'dz_short_name (env-dz-short-name))
		(cons 'version nowcall-version)
		(cons 'msg msg)
		(cons 'to_manifest
			(if (env-on-dz-lan)
				(deflate "<p>Never mind, just take me to
					<a href=\"/manifest\">Manifest</a>.</p>") ""))
		(cons 'contact (contact-link #t))
		(cons 'target destination)))
(define (unlock-session req)
	(when (admin-user req)
		(session-set req 'admin-session #f))
	(list (cons 'icon (security-tag req))))
(define lock-session
	(let ([not-auth
				"You're not authorized for administrative privileges."]
			[wrong-pass "Password incorrect."]
			[password-query
				"select id
					from people
					where (id=~a)
					and (login_password=~a)"])
		(define (password-check req password)
			(pg-one-row (sdb) password-query
				(or (session-get req 'person-id) 0)
				(sha-256-sum (to-s password))))
		(lambda (password req)
			(list
				(cons 'error
					(cond
						[(not (admin-user req)) not-auth]
						[(not (password-check req password))
							wrong-pass]
						[else (session-set req 'admin-session #t) ""]))
				(cons 'icon (security-tag req))))))

(log-to "/var/log/nowcall/login.log")

(http-html "/"
	; basic login page
	(lambda (req)
		(let ([destination (assq-ref req 'path-info)])
			(present-page
				(if (and (session-get req 'person-id)
						(not (string=? destination ""))
						(not (authorized-for req destination)))
					(let ([intent (string-upcase (to-s destination))])
						(set! destination default-destination)
						(format #f not-authd intent)) "")
				destination (mobile-user? req)))))
(http-html "/auth"
	; authenticate/authorize user
	(lambda (req)
		(let* ([user (clean-param (query-value req 'user))]
				[password (clean-param (query-value req 'password))]
				[destination (or (query-value req 'target) "")]
				[submit (string-downcase (or (query-value req 'submit) ""))]
				[admin (not (not (query-value req 'admin)))]
				[mobile (mobile-user? req)]
				[authd (user-authenticate user password req (sdb))])
			(if authd
				(cond
					[(admin-user req)
						(session-set req 'admin-session admin)
						(redirect req destination)]
					[admin
						(present-page non-admin destination mobile)]
					[else
						(redirect req destination)])
				(present-page failed-login destination mobile)))))
(http-html "/setpw"
	; compose password form
	(lambda (req)
		(let ([key (clean-param (query-value req 'k))])
			(fill-template (fetch-doc set-password-body) #f
				(cons 'key key)))))
(http-json "/vem"
	; send password reset email containing verification link
	(lambda (req)
		(let* ([email (clean-param (query-value req 'email))]
				[person-id (verify-email email)])
			(if person-id
				(if (send-reset-email person-id email
						(assq-ref req 'x-forwarded-host)
						(make-registration-ticket person-id email))
					(list
						(cons 'status #t)
						(cons 'msg verify-success-msg))
					(begin
						(log-msg "email to ~s failed" email)
						(list
							(cons 'status #f)
							(cons 'msg email-failed-msg))))
				(list
					(cons 'status #f)
					(cons 'msg (verify-fail-msg email)))))))
(http-json "/logout"
	; session logout
	(lambda (req)
		(session-unset req 'person-id)
		(list (cons 'status #t))))
(http-json "/valkey"
	; check validation key, used for password reset
	(lambda (req)
		(let* ([key (clean-param (query-value req 'key))]
				[ticket (get-ticket key)])
			(if ticket
				(list (cons 'status #t))
				(list
					(cons 'status #f)
					(cons 'msg invalid-ticket))))))
(http-json "/postpw"
	; set login credentials
	(lambda (req)
		(let* ([key (clean-param (query-value req 'key))]
				[user (clean-param (query-value req 'username))]
				[password (clean-param (query-value req 'password))]
				[confirm (clean-param (query-value req 'confirm))]
				[ticket (get-ticket key)])
			(if ticket
				(let ([errs
							(data-errors
								(pg-cell ticket 'person_id)
								password confirm user)])
					(list
						(cons 'msg
							(if errs
								(render-errors errs)
								(begin
									(set-credentials user
										(pg-cell ticket 'person_id)
										(pg-cell ticket 'email)
										password)
									post-success)))))
				(list (cons 'msg invalid-ticket))))))
(http-json "/lock"
	; enable/disable session admin privileges
	(lambda (req)
		(let ([lock (query-value-boolean req 'lock)]
				[password (query-value req 'password)])
			(if lock
				(lock-session password req)
				(unlock-session req)))))
(add-javascript-logger #f)
