var admin = false;
function light_buttons(enable) {
	var color;
	if (enable) color = "green";
	else color = "#bbbbbb";
	var btn = document.getElementById("save-button");
	btn.disabled = !enable;
	btn.style.backgroundColor = color;
	btn = document.getElementById("reset-button");
	btn.disabled = !enable;
	btn.style.backgroundColor = color;
	}
function touch_chart() {
	light_buttons(admin);
	}
function show_chart() {
	$.get("/settings/chart", {},
			function(resp) {
				var div = document.getElementById("settings-table");
				div.innerHTML = resp.html;
				light_buttons(false);
				admin = resp.admin;
				},
			"json");
	}
function get_checkbox(name) {
	return document.getElementById(name).checked;
	}
function get_number(name) {
	return parseInt(document.getElementById(name).value, 10);
	}
function get_float(name) {
	return parseFloat(document.getElementById(name).value);
	}
function save_settings() {
	if (!admin) return;
	var db_master = document.getElementById("db-master");
	data = {
		facebook_id: document.getElementById("facebook-id").value,
		ground_rod: get_checkbox("ground-reserve-out-of-date"),
		pub_sheets: get_checkbox("pubsheets"),
		min_sheets: get_number("min-load-sheets"),
		search_chars: get_number("min-search-chars"),
		sheet_cols: get_number("load-sheet-columns"),
		manifast_retain: get_number("manifast-retention"),
		date_format: document.getElementById("date-format").value,
		time_format: document.getElementById("time-format").value,
		clock_interval: document.getElementById("clock-interval").value,
		name_order: document.getElementById("name-order").value,
		waiver_valid: document.getElementById("waiver-valid").value,
		full_name: document.getElementById("dz-full-name").value,
		short_name: document.getElementById("dz-short-name").value,
		stripe_pubkey: document.getElementById("stripe-public-key").value,
		stripe_privkey: document.getElementById("stripe-private-key").value,
		email: document.getElementById("dz-email-addr").value,
		bcc: document.getElementById("dz-copy-to-addrs").value,
		sales_tax: get_float("sales-tax"),
		credit_rate: get_float("credit-card-rate"),
		credit_flat: get_float("credit-card-flat"),
		repack_cycle: get_number("repack-cycle"),
		patron_pay: get_checkbox("patron-self-pay"),
		db_master: (db_master == null ? "localhost" : db_master.value)
		};
	$.post("/settings/update", data,
			function(resp) { window.location = "/settings"; },
			"json");
	}
function init() {
	mark_page("Settings");
	show_chart();
	}
function logout() {
	$.get("/login/logout", {},
			function(resp) { window.location = "/manifest"; },
			"json");
	}
function reset_ip_addr() {
	$.get("/manifest/setip", {},
			function(resp) {
				if (resp.status) {
					document.getElementById("ip-addr").innerHTML = resp.ip;
					}
				}, "json");
	}
