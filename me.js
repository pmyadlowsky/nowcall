/*
** Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
**
** This program is free software ; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation ; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY ; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program ; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var date_format = '[[DATE_FORMAT]]';
var need_scroll = [];
var pay_authorized = [[PAY_AUTH]];
var refresh_interval = 3000;
need_scroll['tabs-jump-log'] = "jump-log";
need_scroll['tabs-acct'] = "ledger-log";
function set_menu(id, value) {
	var menu = document.getElementById(id);
	var opts = menu.options;
	for (i = 0; i < opts.length; i++) {
		if (opts[i].value == value) {
			menu.selectedIndex = i;
			break;
			}
		}
	}
function get_value(id) {
	return document.getElementById(id).value;
	}
function set_value(id, value) {
	document.getElementById(id).value = value;
	}
function aircraft_selections() {
	var inputs = document.getElementsByTagName("input");
	var ids = [];
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].type != "checkbox") continue;
		if (!inputs[i].checked) continue;
		res = inputs[i].id.match(/^ac(\d+)/);
		if (res == null) continue;
		ids.push(res[1]);
		}
	return ids;
	}
function skydive_selections() {
	var inputs = document.getElementsByTagName("input");
	var ids = [];
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].type != "checkbox") continue;
		if (!inputs[i].checked) continue;
		res = inputs[i].id.match(/^role_(.+)/);
		if (res == null) continue;
		ids.push(res[1]);
		}
	return ids;
	}
function arm_search(arm) {
	var btn = document.getElementById("search");
	if (arm) {
		btn.style.color = "green"
		btn.style.fontWeight = "bold";
		}
	else {
		btn.style.color = ""
		btn.style.fontWeight = "normal";
		}
	}
function date_change(obj) {
	set_menu("date-presets", "manual");
	update_aircraft_skydives();
	arm_search(true);
	}
function get_date_value(id) {
	var date = get_value(id);
	if (date != "") date = get_value(id + "-alt");
	return date;
	}
function set_aircraft_jumps(aircraft_html, jumps_html) {
	var div = document.getElementById("jump-aircraft");
	div.innerHTML = aircraft_html;
	div = document.getElementById("jump-types");
	div.innerHTML = jumps_html;
	}
function scroll_down(tab_id) {
	if (need_scroll[tab_id] == null) return;
	var div = document.getElementById(need_scroll[tab_id]);
	if (div.scrollHeight > 0) {
		need_scroll[tab_id] = null;
		div.scrollTop = div.scrollHeight;
		}
	}
function get_log() {
	$.post("/me/log", {
			from_date: get_date_value("log-from-date"),
			to_date: get_date_value("log-to-date"),
			aircraft: JSON.stringify(aircraft_selections()),
			jumps: JSON.stringify(skydive_selections())
			},
			function(resp) {
				if (resp.status) {
					var div = document.getElementById("jump-log");
					div.innerHTML = resp.log;
					scroll_down("tabs-jump-log");
					set_aircraft_jumps(resp.aircraft, resp.jumps);
					div = document.getElementById("jump-total");
					div.innerHTML = resp.total + "";
					arm_search(false);
					}
				},
			"json");
	}
function hide_obj(name) {
	var obj = document.getElementById(name);
	if (obj != null) obj.style.display = "none";
	}
function show_obj(name) {
	var obj = document.getElementById(name);
	if (obj != null) obj.style.display = "block";
	}
function disable_obj(name) {
	var obj = document.getElementById(name);
	if (obj != null) {
		obj.disabled = true;
		return;
		}
	obj = document.getElementsByName(name);
	for (var i = 0; i < obj.length; i++) obj[i].disabled = true;
	}
function get_profile() {
	$.get("/me/profile", {},
			function(resp) {
				var obj = document.getElementById("tabs-profile");
				obj.innerHTML = resp.html;
				hide_obj("people-notes-section");
				hide_obj("people-cancel-button" + resp.pid);
				hide_obj("people-del-button" + resp.pid);
				show_obj("people-authblock" + resp.pid);
				show_obj("people-confirm");
				hide_obj("people-authorizations");
				disable_obj("status" + resp.pid);
				disable_obj("people-last-name" + resp.pid);
				disable_obj("people-first-name" + resp.pid);
				disable_obj("people-goes-by" + resp.pid);
				disable_obj("people-nickname" + resp.pid);
				disable_obj("people-disambiguate" + resp.pid);
				disable_obj("people-pilot" + resp.pid);
				disable_obj("people-maint" + resp.pid);
				disable_obj("people-manifest" + resp.pid);
				disable_obj("people-packer" + resp.pid);
				disable_obj("people-active" + resp.pid);
				disable_obj("people-uspa-id" + resp.pid);
				disable_obj("people-license" + resp.pid);
				disable_obj("people-uspa-expires" + resp.pid);
				disable_obj("people-last-repack" + resp.pid);
				disable_obj("people-last-repack-2" + resp.pid);
				disable_obj("people-waiver-date" + resp.pid);
				disable_obj("people-gearrent" + resp.pid);
				obj = document.getElementById("people-password" + resp.pid);
				if (obj != null) {
					obj.type = "password";
					obj.value = "**********";
					}
				$("#people-birth" + resp.pid).datepicker({
					dateFormat: resp.date_format,
					firstDay: 1,
					yearRange: "-90:-10",
					changeYear: true
					});
				People.panel_assign_button("save", resp.pid,
							function(){ update_detail(resp.pid) });
				},
			"json");
	}
function show_open_manifest(ignore) {
	$.get("/me/open_loads", {},
			function(resp) {
				if (resp.status) {
					var tab = document.getElementById("manifest-clicks");
					tab.innerHTML = resp.html
					}
				}, "json");
	}
function get_ledger() {
	$.get("/me/ledger", {},
			function(resp) {
				if (!resp.status) return;
				var div = document.getElementById("ledger-log");
				div.innerHTML = resp.ledger;
				scroll_down("tabs-acct");
				div = document.getElementById("ledger-total");
				div.innerHTML = resp.total + "";
				if (pay_authorized) Payment.set_email(resp.email);
				my_cards();
				},
			"json");
	}
function update_aircraft_skydives() {
	$.post("/me/acjumps", {
			from_date: get_date_value("log-from-date"),
			to_date: get_date_value("log-to-date")
			},
			function(resp) {
				if (resp.status)
					set_aircraft_jumps(resp.aircraft, resp.jumps);
				},
			"json");
	}
function set_log_dates(menu) {
	if (menu.value == "all") {
		set_value("log-from-date", "");
		set_value("log-from-date-alt", "");
		set_value("log-to-date", "");
		set_value("log-to-date-alt", "");
		}
	if (menu.value == "manual") return;
	$.get("/me/logdates", { key: menu.value },
			function(resp) {
				$("#log-from-date").datepicker("setDate", resp.from_date);
				$("#log-to-date").datepicker("setDate", resp.to_date);
				update_aircraft_skydives();
				arm_search(true);
				},
			"json");
	}
function panel_id(tag, panel) {
	//var m = panel.attr('id').match(/([0-9]+)/);
	console.log(tag + " " + panel.attr("id"));
	}
function drop_card(card_token, label) {
	local_confirm("Payment Cards", "Yes", "No",
		function() {
			$.get("/payment/drop", {},
					function(resp) { my_cards(); }, "json");
			}, null, "Forget card '" + label + "'?");
	}
function render_cardlist(resp) {
	var html;
	if (resp.opts.length == 0) {
		html = "You have no credit/debit cards on file.";
		}
	else {
		html = "<table>\n";
		for (var i = 0; i < resp.opts.length; i++) {
			html += "<tr>";
			html += "<td><img src=\"/img/delete-8.png\" title=\"remove this card\" alt=\"remove card\" onclick=\"drop_card('" + resp.opts[i].id + "','" + resp.opts[i].label + "')\"/></td>";
			html += "<td>" + resp.opts[i].label + "</td>";
			//						resp.opts[i].id);
			html += "</tr>\n";
			}
		html += "</table>\n";
		}
	document.getElementById("card-list").innerHTML = html;
	Payment.load_card_menu("", false);
	}
function my_cards() {
	$.post("/payment/cards", { }, render_cardlist, "json");
	}
function init() {
	mark_page("My Stuff");
	$("#tabs").tabs({
			active: 0,
			activate: function(ev, ui) {
						//panel_id("activate", ui.newPanel);
						scroll_down(ui.newPanel.attr('id'));
						}
			});
	$("#log-from-date").datepicker({
		dateFormat: date_format,
		altFormat: "yy-mm-dd",
		altField: "#log-from-date-alt",
		firstDay: 1,
		yearRange: "-90:-10",
		changeYear: true
		});
	$("#log-to-date").datepicker({
		dateFormat: date_format,
		altFormat: "yy-mm-dd",
		altField: "#log-to-date-alt",
		firstDay: 1,
		yearRange: "-90:-10",
		changeYear: true
		});
	$.get("/payment/check", {},
			function(resp) {
				if (!pay_authorized || !resp.status) return;
				document.getElementById("payment").style.display = "block";
				Payment.config_stripe(
					function(resp) {
						if (resp.status) {
							need_scroll['tabs-acct'] = "ledger-log";
							get_ledger();
							Payment.set_amount(0);
							Payment.load_card_menu("", false);
							}
						else alert(resp.msg);
						});
				Payment.show_fee();
				Payment.load_card_menu("", false);
				}, "json");
	LongPoll.set_poll_url("/manifest/track");
	LongPoll.set_payload_handler(show_open_manifest);
	LongPoll.set_fallback_timeout(30);
	get_log();
	get_ledger();
	get_profile();
	LongPoll.poll();
	show_open_manifest(null);
	}
function card_response(resp) {
	if (resp.status) {
		need_scroll['tabs-acct'] = "ledger-log";
		get_ledger();
		Payment.set_amount(0);
		}
	else alert(resp.msg);
	}
function cell_value(id, column) {
	return document.getElementById(column + id).value.trim();
	}
function checkbox_value(id, column) {
	if (document.getElementById(column + id).checked) return 1;
	return 0;
	}
function update_detail(pid) {
	var data;
	var passtouch = cell_value(pid, "people-passtouch");
	var password = cell_value(pid, "people-password");
	var passconf = cell_value(pid, "people-confirm");
	var errs = Array();
	if ((passtouch == "1") && (password != passconf)) {
		errs.push("passwords don't match");
		}
	if (errs.length > 0) {
		var msg = "Please correct:<ul><li>" +
			errs.join("</li><li>") + "</li></ul>";
		local_alert("Data Entry Errors", msg);
		return;
		}
	data = {
		pid: pid,
		exit_weight: cell_value(pid, "people-exit-weight"),
		street1: cell_value(pid, "people-street1"),
		street2: cell_value(pid, "people-street2"),
		city: cell_value(pid, "people-city"),
		state: cell_value(pid, "people-state"),
		country: cell_value(pid, "people-country"),
		zipcode: cell_value(pid, "people-zipcode"),
		phone_home: cell_value(pid, "people-phone-home"),
		phone_mobile: cell_value(pid, "people-phone-mobile"),
		email: cell_value(pid, "people-email"),
		remind: checkbox_value(pid, "people-remind"),
		emerg_contact: cell_value(pid, "people-emerg-contact"),
		birth: cell_value(pid, "people-birth"),
		emerg_phone: cell_value(pid, "people-emerg-phone"),
		username: cell_value(pid, "people-username"),
		password: password,
		passtouch: passtouch,
		passconf: passconf
		};
	$.post("/me/update_pro", data,
				function(resp) {
					document.getElementById("people-namecheck"
							+ pid).innerHTML = "&nbsp;";
					local_alert("Profile Update", resp.msg);
					if (resp.status) {
						if (resp.username != "") {
							var key = "people-username" + resp.pid;
							document.getElementById(key).value =
								resp.username;
							}
						}
					},
				"json");
	}
function logout() {
	$.get("/login/logout", {},
			function(resp) { window.location = "/manifest"; },
			"json");
	}
function check_available(box, pid) {
	var name = box.value.trim();
	if (name == "") {
		document.getElementById("people-namecheck"
						+ pid).innerHTML = "&nbsp;";
		return;
		}
	$.get("/people/checkname",
			{
				pid: pid,
				name: name
				}, 
			function(resp) {
				var text, color;
				if (resp.avail == "mine") {
					text = "&nbsp;";
					color = "black";
					}
				else if (resp.avail == "avail") {
					text = "AVAIL";
					color = "green";
					}
				else {
					text = "TAKEN";
					color = "red"
					}
				var lite = document.getElementById("people-namecheck"
								+ resp.pid);
				lite.style.color = color;
				lite.innerHTML = text;
				},
			"json");
	}
function show_repack_due(box, cycle, date_format) {
	if (box.value == "") return;
	var date = $.datepicker.parseDate(date_format, box.value);
	var due_date = new Date(date.getTime() + cycle * 24 * 3600 * 1000);
	box.title = "next due " + $.datepicker.formatDate("m/d/yy", due_date);
	}
function manifest(person_id, aircraft_id, load_num, load_date, slot_num) {
	$.post("/manifest/board",
			{
				ids: person_id,
				roles: "",
				extras: JSON.stringify([false]),
				dst_aircraft: aircraft_id,
				src_aircraft: aircraft_id,
				load: load_num,
				slot: 0,
				date: load_date
				}, function(resp) {
						if (!resp.status)
							local_alert("Self-Manifest", resp.msg);
						}, "json");
	}
function scratch(person_id, aircraft_id, load_num, load_date, slot_num) {
	$.post("/manifest/scratch",
			{
				date: load_date,
				load: load_num,
				aircraft: aircraft_id,
				slots: slot_num
				}, function(resp) {}, "json");
	}
