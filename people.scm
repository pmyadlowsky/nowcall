#! /usr/local/bin/gusher -p 3002
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))
(use-modules (ice-9 regex))
(use-modules (gusher cron))

(include "lib.scm")
(include "settings_lib.scm")
(include "people_lib.scm")
(include "acct.scm")
(include "email.scm")
(include "sessions.scm")

(define env-date-format (setting-cache 'date-format))
(define env-time-format (setting-cache 'time-format))
(define env-repack-cycle (setting-cache 'repack-cycle))
(define env-dz-email-addr (setting-cache 'dz-email-addr))
(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-name-order (setting-cache 'name-order))
(define env-waiver-valid (setting-cache 'waiver-valid))

(define frame (make-doc 'file "frame.html"))
(define script (make-doc 'file "people.js"))
(define detail-form (make-doc 'file "people_detail.html"))
(define page-body (make-doc 'file "people.html"))
(define busy-color "yellow")
(define non-busy-color "#dddddd")
(define group-size 100)
(define reminder-template
	(make-doc 'file "/vol/profile/reminder.txt"))

(define chart-tpt "\
<div id=\"chart_hdr\">
<table style=\"border-collapse: collapse\">
<tbody><tr>
<td class=\"hcell detail_col\">&nbsp;</td>
<td class=\"hcell lname_col\">Last<br/>Name</td>
<td class=\"hcell fname_col\">First<br/>Name</td>
<td class=\"hcell uspa_col\">USPA<br/>ID</td>
<td class=\"hcell license_col\">License</td>
<td class=\"hcell date_col\">Member<br/>Thru</td>
<td class=\"hcell date_col\">Repack</td>
<td class=\"hcell date_col\">Waiver</td>
</tr></tbody>
</table></div>
<div id=\"chart_body\">
<table id=\"chart\" style=\"width: 100%\">
<tbody>
</tbody>
</table></div>")

(define (member-out-of-date date)
	(if (null? date) #f
		(> (time-diff (time-now) (time-add date (* 24 3600))) 0)))
(define (latest-repack row)
	(let ([repack (pg-cell row 'last_repack)]
			[repack-2 (pg-cell row 'last_repack_2)])
		(cond
			[(null? repack-2) repack]
			[(null? repack) repack-2]
			[(> (time-diff repack repack-2) 0) repack]
			[else repack-2])))
(define (repack-out-of-date date)
	(if (null? date) #t
		(< (time-diff
				(time-add date (* (+ (env-repack-cycle) 1) 24 3600))
				(time-now)) 0)))
(define (repacks-out-of-date row)
	(let ([repack (pg-cell row 'last_repack)]
			[repack-2 (pg-cell row 'last_repack_2)])
		(if (and (null? repack) (null? repack-2)) #f
			(and
				(repack-out-of-date repack)
				(repack-out-of-date repack-2)))))
(define (waiver-out-of-date date)
	(if (null? date) #f
		(if (string=? (env-waiver-valid) "this-year")
			(not (= (time-year date) (time-year (time-now))))
			(< (time-diff
				(time-add date (* 365 24 3600))
				(time-now)) 0))))
(define last-name-cell
	(let ([hilite-query
				"select id
					from people_log
					where (person=~a)
					and highlight
					limit 1"]
			[last-name-template
				(deflate "<div style=\"float: left\">~a</div>
					<div style=\"float: right\">
					<img src=\"/img/notepad-16.png\"
						title=\"see notes\" alt=\"note\"/></div>")])
		(define (note-hilite pid) (pg-one-row (sdb) hilite-query pid))
		(lambda (pid last-name)
			(if (note-hilite pid)
				(format #f last-name-template last-name) last-name))))
(define chart-row
	(let ([template
				(deflate "<td class=\"rcell detail_col\"
					id=\"cbcell[[PID]]\"
					style=\"background-color: [[BUSY_COLOR]]\">
				<input type=\"checkbox\" id=\"detail[[PID]]\"
					onclick=\"toggle_detail(this, [[PID]])\"/></td>
				<td class=\"rcell lname_col [[TINT]]\"><div
					id=\"Clast_name[[PID]]\"
					class=\"cliner\">[[LAST_NAME]]</div></td>
				<td class=\"rcell fname_col [[TINT]]\"><div
					id=\"Cfirst_name[[PID]]\"
					class=\"cliner\">[[FIRST_NAME]][[GOES_BY]]</div></td>
				<td class=\"rcell uspa_col [[TINT]]\"><div
					id=\"Cuspa_id[[PID]]\"
					class=\"cliner\">[[USPA_ID]]</div></td>
				<td class=\"rcell license_col [[TINT]]\"><div
					id=\"Clicense[[PID]]\"
					class=\"cliner\">[[LICENSE]]</div></td>
				<td class=\"rcell date_col [[TINT]]\"><div
					id=\"Cuspa_expires[[PID]]\"
					class=\"cliner\"[[MEMBERSHIP_BORDER]]>[[MEMBERSHIP]]</div></td>
				<td class=\"rcell date_col [[TINT]]\"><div
					id=\"Clast_repack[[PID]]\"
					class=\"cliner\"[[REPACK_BORDER]]>[[REPACK]]</div></td>
				<td class=\"rcell date_col [[TINT]]\"><div
					id=\"Cwaiver_date[[PID]]\"
					class=\"cliner\"[[WAIVER_BORDER]]>[[WAIVER]]</div></td>")]
			)
		(lambda (row tint)
			(let ([id (pg-cell row 'id)]
					[expires (pg-cell row 'uspa_expires)]
					[repack (pg-cell row 'last_repack)]
					[waiver (pg-cell row 'waiver_date)]
					[goes-by (pg-cell row 'goes_by)]
					[tint-class (if tint "tint_row" "plain_row")])
				(fill-template template #f
					(cons 'pid id)
					(cons 'busy_color
						(if (is-busy id '())
							busy-color non-busy-color))
					(cons 'tint (if tint "tint_row" "plain_row"))
					(cons 'last_name
						 (last-name-cell id
							(to-s (pg-cell row 'last_name))))
					(cons 'first_name (to-s (pg-cell row 'first_name)))
					(cons 'goes_by
						(if (null? goes-by) ""
							(format #f " \"~a\"" goes-by)))
					(cons 'uspa_id (to-s (pg-cell row 'uspa_id)))
					(cons 'license (to-s (pg-cell row 'license)))
					(cons 'membership_border
						(if (member-out-of-date expires)
							" style=\"border: 3px solid red\"" ""))
					(cons 'membership
						(show-date expires (env-date-format)))
					(cons 'repack_border
						(if (repacks-out-of-date row)
							" style=\"border: 3px solid red\"" ""))
					(cons 'repack
						(show-date (latest-repack row) (env-date-format)))
					(cons 'waiver_border
						(if (waiver-out-of-date waiver)
							" style=\"border: 3px solid red\"" ""))
					(cons 'waiver
						(show-date waiver (env-date-format))))))))
(define empty-record
	(let ([query
				"select last_name
					from people
					where (new or
						((nickname is null) and
							(last_name is null) and
							(first_name is null)))
					and (id=~a)"])
		(lambda (id) (pg-one-row (sdb) query id))))
(define people-name
	(let ([query
				"select last_name, first_name, nickname,
						disambiguate, goes_by
					from people
					where (id=~a)"])
		(lambda (id)
			(let* ([row (pg-one-row (sdb) query id)])
				(format #f "~a, ~a~a"
					(pg-cell row 'last_name)
					(pg-cell row 'first_name)
					(let ([disamb (pg-cell row 'disambiguate)])
						(if (null? disamb) ""
							(format #f " (~a)" disamb))))))))
(define add-role
	(let ([query "insert into roles (who, role) values (~a, ~a)"])
		(lambda (pid role)
			(pg-exec (sdb) query pid role))))
(define add-roles
	(let ()
		(define (wipe-roles pid)
			(pg-exec (sdb) "delete from roles where (who=~a)" pid))
		(lambda (pid req)
			(let ([role1 (string->symbol (query-value req 'role1))]
					[affi (query-value-boolean req 'affi)]
					[ti (query-value-boolean req 'ti)]
					[vid (query-value-boolean req 'vid)]
					[coach (query-value-boolean req 'coach)]
					[pilot (query-value-boolean req 'pilot)]
					[manifest (query-value-boolean req 'manifest)]
					[packer (query-value-boolean req 'packer)]
					[maint (query-value-boolean req 'maint)])
				(wipe-roles pid)
				(add-role pid (to-s role1))
				(when (eq? role1 'LIC)
					(when affi (add-role pid "AFFI"))
					(when ti (add-role pid "TI"))
					(when vid (add-role pid "VID"))
					(when coach (add-role pid "C")))
				(when pilot (add-role pid "PILOT"))
				(when manifest (add-role pid "MANFST"))
				(when packer (add-role pid "PACKER"))
				(when maint (add-role pid "MAINT"))))))
(define same-name?
	(let ([query
				"select count(*) as n
					from people
					where (id != ~d)
					and (lower(coalesce(last_name, ''))=~a)
					and (lower(coalesce(first_name, ''))=~a)
					and (lower(coalesce(disambiguate, ''))=~a)"])
		(define (string-normalize str)
			(let ([dstr (string-downcase (to-s str))])
				(if (= (string-length dstr) 0) "''"
					(pg-format (sdb) dstr))))
		(lambda (pid last-name first-name disambig)
			(let* ([query
						(format #f query
							pid
							(string-normalize last-name)
							(string-normalize first-name)
							(string-normalize disambig))]
					[row (pg-one-row (sdb) query)])
				(> (to-i (pg-cell row 'n)) 0)))))
(define update-person
	(let ([query
				"update people set
					last_name=~a,
					first_name=~a,
					uspa_id=~a,
					license=~a,
					nickname=~a,
					active=~a,
					uspa_expires=~a,
					last_repack=~a,
					last_repack_2=~a,
					street1=~a,
					street2=~a,
					city=~a,
					state=~a,
					country=~a,
					zipcode=~a,
					phone_home=~a,
					phone_mobile=~a,
					email=~a,
					emerg_contact=~a,
					birth=~a,
					emerg_phone=~a,
					waiver_date=~a,
					gear_rental=~a,
					new='f',
					disambiguate=~a,
					goes_by=~a,
					page_key=(upper(~a) || '-' || upper(~a) ||
								'-' || lpad(id::varchar, 12, '0')),
					exit_weight=~a,
					notify=~a,
					updated=current_timestamp
					where (id=~a)"]
			[update-reserv
				"update reservations set
					name=~a,
					email=~a,
					phone=~a
					where (person_id=~a)"])
		(define (resolve-date req key)
			(or (normalize-date (to-s (query-value req key))) '()))
		(lambda (pid req)
			(let ([last-name (query-value-cap req 'last_name)]
					[first-name (query-value-cap req 'first_name)]
					[exit-weight
						(to-i (query-value-number req 'exit_weight))]
					[email (to-s (query-value req 'email))]
					[phone-home (to-s (query-value req 'phone_home))]
					[phone-cell (to-s (query-value req 'phone_mobile))])
				(pg-exec (sdb) query
					last-name
					first-name
					(to-s (query-value req 'uspa_id))
					(to-s (query-value req 'license))
					(to-s (query-value req 'nickname))
					(to-s (query-value-boolean req 'active))
					(resolve-date req 'uspa_expires)
					(resolve-date req 'last_repack)
					(resolve-date req 'last_repack_2)
					(to-s (query-value req 'street1))
					(to-s (query-value req 'street2))
					(to-s (query-value req 'city))
					(to-s (query-value req 'state))
					(to-s (query-value req 'country))
					(to-s (query-value req 'zipcode))
					phone-home
					phone-cell
					email
					(to-s (query-value req 'emerg_contact))
					(resolve-date req 'birth)
					(to-s (query-value req 'emerg_phone))
					(resolve-date req 'waiver_date)
					(resolve-date req 'gear_rental)
					(to-s (query-value req 'disambiguate))
					(to-s (query-value req 'goes_by))
					last-name first-name
					(if (= exit-weight 0) '() exit-weight)
					(to-s (query-value-boolean req 'remind))
					pid)
				(pg-exec (sdb) update-reserv
					(format #f "~a ~a" first-name last-name)
					email
					(if (= (string-length phone-cell) 0)
						phone-home phone-cell)
					pid)
				(add-roles pid req)))))
(define update-auth
	(let ([update-username
				"update people set
					login_name=~a
					where (id=~a)"]
			[update-password
				"update people set
					login_password=~a
					where (id=~a)"]
			[set-app-auth-query
				"insert into app_auth (person_id, app) values (~a, ~a);"])
		(define (set-auth pid app-name req key)
			(if (query-value-boolean req key)
				(list
					(pg-query (sdb) set-app-auth-query
						(list pid app-name))) '()))
		(lambda (pid req)
			(let ([username
						(string-trim-both
							(or (query-value req 'username) ""))]
					[passtouch (query-value-boolean req 'passtouch)]
					[password
						(string-trim-both
							(or (query-value req 'password) ""))])
				(when (username-available username pid (sdb))
					(pg-exec (sdb) update-username username pid))
				(when (and passtouch (not (string=? password "")))
					(pg-exec (sdb) update-password
						(sha-256-sum password) pid))
				(pg-exec (sdb)
					(string-cat "\n"
						(pg-query (sdb)
							"delete from app_auth where (person_id=~a);"
							(list pid))
						(append
							(set-auth pid "admin" req 'auth_admin)
							(set-auth pid "manifest" req 'auth_manifest)
							(set-auth pid "accounting" req
								'auth_accounting)
							(set-auth pid "people" req 'auth_people)
							(set-auth pid "products" req 'auth_products)
							(set-auth pid "aircraft" req 'auth_aircraft)
							(set-auth pid "backups" req 'auth_backups)
							(set-auth pid "settings" req 'auth_settings)
							(set-auth pid "reservations" req
								'auth_reservations)
							(set-auth pid "whiteboard" req
								'auth_whiteboard)
							(set-auth pid "schedule" req
								'auth_schedule)
							(set-auth pid "work_remote" req 'auth_remote)
							(set-auth pid "reports" req
								'auth_reports))))))))
(define composite
	(make-doc (list frame page-body script)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "People")
				(cons 'app "people")
				(cons 'script (fetch-doc script))
				(cons 'version nowcall-version)
				(cons 'content (fetch-doc page-body))))))
(define latest-event
	; latest person update time
	(let ([query
				"select (date_trunc('sec', max(updated)) +
							interval '1 sec') as stamp
					from people"])
		(lambda ()
			(let ([row (pg-one-row (sdb) query)])
				(time-epoch (pg-cell row 'stamp))))))
(define (clear-busy)
	(pg-exec (sdb) "update people set busy=NULL"))
(define new-person-id
	; create new person record and return people.id
	(let ([query
				"insert into people (active, new, last_name,
										first_name, page_key, waiver_date)
					values ('t', 't', ~a, ~a,
						(upper(~a) || '-' || upper(~a)), ~a);
				select max(id) as pid from people where new"])
		(lambda (last-name first-name)
			(let ([row
					(pg-one-row (sdb) query
						last-name first-name
						last-name first-name
						(time-format (time-now) "%Y-%m-%d"))])
				(pg-cell row 'pid)))))
(define delete-person
	(let ([query
				"delete from roles where who=~a;
				delete from app_auth where person_id=~a;
				delete from people_log where (person=~a);
				update reservations set
					person_id=null
					where (person_id=~a);
				delete from whiteboard where (student=~a);
				delete from acct_track
					using acct
					where (acct.owner=~a)
					and (acct_track.acct_id=acct.id);
				delete from acct where owner=~a;
				delete from people where id=~a"])
		(lambda (pid)
			(pg-exec (sdb) query pid pid pid pid pid pid pid pid))))
(define (box-checked state) (if state " checked=\"checked\"" ""))
(define (box-enabled state) (if state "" " disabled=\"disabled\""))
(define (any? symbol symbols)
	(cond
		[(null? symbols) #f]
		[(eq? (car symbols) symbol) #t]
		[else (any? symbol (cdr symbols))]))
(define (roles pid)
	(pg-map-rows
		(pg-exec (sdb)
			"select role from roles where (who=~a)"
			pid)
		(lambda (row) (string->symbol (pg-cell row 'role)))))
(define (authorizations pid)
	(pg-map-rows
		(pg-exec (sdb)
			"select app from app_auth where (person_id=~a)"
			pid)
		(lambda (row) (pg-cell row 'app))))
(define person-references
	(let ([has-xacts
				"select acct_xact.id
					from acct, acct_xact
					where (acct.owner=~a)
					and ((acct_xact.from_acct=acct.id) or
						(acct_xact.to_acct=acct.id))
					limit 1"]
			[has-log-notes
				"select id
					from people_log
					where (author=~a)
					limit 1"]
			[on-loadsheets
				"select passenger
					from load_slots
					where (passenger=~a)
					limit 1"])
		(define (ref-member result marker)
			(if result (list marker) '()))
		(lambda (pid)
			(append
				(ref-member (pg-one-row (sdb) has-xacts pid) "accounting")
				(ref-member (pg-one-row (sdb) has-log-notes pid) "remarks")
				(ref-member
					(pg-one-row (sdb) on-loadsheets pid) "manifest")))))
(define (has-auth auth auths)
	(cond
		[(null? auths) ""]
		[(string=? auth (car auths)) " checked=\"checked\""]
		[else (has-auth auth (cdr auths))]))
(define notes-log
	(let ([notes-query
				"select id, stamp, note, author, highlight
					from people_log
					where (person=~a)
					order by stamp desc"]
			[note-box
				(deflate "<input style=\"width: 95%\"
					type=\"text\"
					onchange=\"people_note_edit(this,'~d')\"
					value=\"~a\"/>")]
			[highlight-check
				(deflate "<input title=\"indicate in people directory\"
					onclick=\"people_note_hilite(this,'~d','~d')\"
					type=\"checkbox\"~a/>")]
			[entry-template
				(deflate "<tr>
					<td style=\"width: 12em\"
						class=\"people-note-entry-cell\">[[WHEN]]</td>
					<td style=\"width: 10em\"
						class=\"people-note-entry-cell\">[[WHO]]</td>
					<td style=\"width: 40em\"
						class=\"people-note-entry-cell\">[[WHAT]]</td>
					<td class=\"people-note-entry-cell\">[[STATE]]</td>
					<td class=\"people-note-entry-cell\">[[DEL]]</td>
					</tr>")]
			[author-query
				"select first_name, last_name
					from people
					where (id=~a)"]
			[delete-template
				(deflate "<img title=\"delete this note\"
					onclick=\"people_note_del('~d','~d')\"
					src=\"/img/delete-16.png\" alt=\"delete\"/>")])
		(define (author-name pid)
			(let ([row (pg-one-row (sdb) author-query pid)])
				(if row
					(format #f "~a ~a"
						(pg-cell row 'first_name)
						(pg-cell row 'last_name))
					"unknown")))
		(define (log-entry pid row)
			(let ([stamp (pg-cell row 'stamp)]
					[note-id (pg-cell row 'id)])
				(fill-template entry-template #f
					(cons 'when
						(format #f "~a ~a"
							(show-date stamp (env-date-format))
							(wall-clock-time stamp (env-time-format))))
					(cons 'who (author-name (pg-cell row 'author)))
					(cons 'what
						(format #f note-box
							note-id (to-s (pg-cell row 'note))))
					(cons 'state
						(format #f highlight-check
							note-id
							pid
							(if (pg-cell row 'highlight)
								" checked=\"checked\"" "")))
					(cons 'del
						(format #f delete-template note-id pid)))))
		(define (add-note pid)
			(format #f
				(deflate "<img src=\"/img/notepad-16.png\"
					onclick=\"people_note_add('~d')\"
					alt=\"add note\" title=\"add new note\"/>") pid))
		(define (table-header pid)
			(fill-template entry-template #f
				(cons 'when "<b>WHEN</b>")
				(cons 'who "<b>AUTHOR</b>")
				(cons 'what "<b>NOTE</b>")
				(cons 'state "")
				(cons 'del (add-note pid))))
		(lambda (pid)
			(let ([res (pg-exec (sdb) notes-query pid)])
				(string-cat "\n"
					"<table>"
					(table-header pid)
					(pg-map-rows res
						(lambda (row) (log-entry pid row)))
					"</table>")))))
(define build-detail
	(let ([query
				"select street1, street2, city, state, country, zipcode,
						phone_home, phone_mobile, email, emerg_contact,
						emerg_phone, birth, active, waiver_date,
						last_name, first_name, nickname, last_repack,
						uspa_id, uspa_expires, license, disambiguate,
						goes_by, login_name, login_password, gear_rental,
						last_repack_2, exit_weight, notify
					from people
					where (id=~a)"])
		(lambda (pid first-last)
			(let* ([row (pg-one-row (sdb) query pid)]
					[roles (roles pid)]
					[auths (authorizations pid)]
					[licensed (any? 'LIC roles)]
					[friend (any? 'FRIEND roles)]
					[refs (person-references pid)])
				(fill-template (fetch-doc detail-form) #t
					(cons 'pid pid)
					(cons 'repack_cycle (env-repack-cycle))
					(cons 'date_format (env-date-format))
					(cons 'name_1_label (if first-last "First" "Last"))
					(cons 'name_2_label (if first-last "Last" "First"))
					(cons 'name_1_pre (if first-last "first" "last"))
					(cons 'name_2_pre (if first-last "last" "first"))
					(cons 'name_1_value
						(if first-last
							(to-s (pg-cell row 'first_name))
							(to-s (pg-cell row 'last_name))))
					(cons 'name_2_value
						(if first-last
							(to-s (pg-cell row 'last_name))
							(to-s (pg-cell row 'first_name))))
					(cons 'nickname (to-s (pg-cell row 'nickname)))
					(cons 'goes_by (to-s (pg-cell row 'goes_by)))
					(cons 'exit_weight (to-s (pg-cell row 'exit_weight)))
					(cons 'username (to-s (pg-cell row 'login_name)))
					(cons 'password
						(if (null? (pg-cell row 'login_password))
							"" "**********"))
					(cons 'checked_auth_admin (has-auth "admin" auths))
					(cons 'checked_auth_manifest
						(has-auth "manifest" auths))
					(cons 'checked_auth_accounting
						(has-auth "accounting" auths))
					(cons 'checked_auth_people (has-auth "people" auths))
					(cons 'checked_auth_products
						(has-auth "products" auths))
					(cons 'checked_auth_aircraft
						(has-auth "aircraft" auths))
					(cons 'checked_auth_backups (has-auth "backups" auths))
					(cons 'checked_auth_settings
						(has-auth "settings" auths))
					(cons 'checked_auth_reports (has-auth "reports" auths))
					(cons 'checked_auth_reservations
						(has-auth "reservations" auths))
					(cons 'checked_auth_whiteboard
						(has-auth "whiteboard" auths))
					(cons 'checked_auth_schedule
						(has-auth "schedule" auths))
					(cons 'checked_auth_remote
						(has-auth "work_remote" auths))
					(cons 'disambiguate (to-s (pg-cell row 'disambiguate)))
					(cons 'street1 (to-s (pg-cell row 'street1)))
					(cons 'street2 (to-s (pg-cell row 'street2)))
					(cons 'city (to-s (pg-cell row 'city)))
					(cons 'state (to-s (pg-cell row 'state)))
					(cons 'zip (to-s (pg-cell row 'zipcode)))
					(cons 'country (to-s (pg-cell row 'country)))
					(cons 'home_phone (to-s (pg-cell row 'phone_home)))
					(cons 'mobile_phone (to-s (pg-cell row 'phone_mobile)))
					(cons 'email (to-s (pg-cell row 'email)))
					(cons 'checked_remind
						(if (pg-cell row 'notify)
							" checked=\"checked\"" ""))
					(cons 'emerg_con (to-s (pg-cell row 'emerg_contact)))
					(cons 'emerg_phone (to-s (pg-cell row 'emerg_phone)))
					(cons 'dob
						(show-date (pg-cell row 'birth) (env-date-format)))
					(cons 'gear_rental
						(show-date (pg-cell row 'gear_rental)
							(env-date-format)))
					(cons 'notes (notes-log pid))
					(cons 'waiver
						(show-date (pg-cell row 'waiver_date)
							(env-date-format)))
					(cons 'repack
						(show-date (pg-cell row 'last_repack)
							(env-date-format)))
					(cons 'repack2
						(show-date (pg-cell row 'last_repack_2)
							(env-date-format)))
					(cons 'uspa_id (to-s (pg-cell row 'uspa_id)))
					(cons 'expires
						(show-date (pg-cell row 'uspa_expires)
							(env-date-format)))
					(cons 'license (to-s (pg-cell row 'license)))
					(cons 'checked_active
						(box-checked (pg-cell row 'active)))
					(cons 'checked_tan (box-checked (any? 'TAN roles)))
					(cons 'checked_stu (box-checked (any? 'STU roles)))
					(cons 'checked_unl (box-checked (any? 'UNL roles)))
					(cons 'checked_lic (box-checked licensed))
					(cons 'checked_friend (box-checked friend))
					(cons 'checked_pilot (box-checked (any? 'PILOT roles)))
					(cons 'checked_maint (box-checked (any? 'MAINT roles)))
					(cons 'checked_manifest
						(box-checked (any? 'MANFST roles)))
					(cons 'enable_ins (box-enabled licensed))
					(cons 'enable_friend
						(box-enabled (or licensed friend)))
					(cons 'delete_disable (box-enabled (null? refs)))
					(cons 'delete_title
						(if (null? refs) ""
							(format #f " title=\"records in ~a\""
								(string-cat ", " refs))))
					(cons 'checked_affi (box-checked (any? 'AFFI roles)))
					(cons 'checked_ti (box-checked (any? 'TI roles)))
					(cons 'checked_vid (box-checked (any? 'VID roles)))
					(cons 'checked_packer
						(box-checked (any? 'PACKER roles)))
					(cons 'checked_coach
						(box-checked (any? 'C roles))))))))
(define mark-busy
	(let ([query
				"update people set
					busy=~a,
					updated=current_timestamp
					where (id=~a)
					and ((busy is null) or (busy=~a))"])
		(lambda (pid worker status)
			(pg-exec (sdb) query (if status worker '()) pid worker))))
(define get-busy
	(let ([query
				"select busy
					from people
					where (id=~a)"])
		(lambda (pid) (pg-one-row (sdb) query pid))))
(define (is-busy pid worker)
	(let* ([row (get-busy pid)]
			[busy (and row (pg-cell row 'busy))])
		(cond
			[(not busy) #f]
			[(null? busy) #f]
			[(null? worker) #t]
			[else (not (= busy worker))])))
(define (held-by pid)
	(let ([row (get-busy pid)])
		(if row
			(person-name (pg-cell row 'busy) "first-last" (sdb))
			"somebody")))
(define (assemble-patch row)
	(let ([expires (pg-cell row 'uspa_expires)]
			[repack (pg-cell row 'last_repack)]
			[goes-by (pg-cell row 'goes_by)]
			[person-id (pg-cell row 'id)]
			[waiver (pg-cell row 'waiver_date)])
		(list
			(cons 'pid person-id)
			(cons 'last_name
				;(to-s (pg-cell row 'last_name))
				(last-name-cell person-id
					(to-s (pg-cell row 'last_name))))
			(cons 'first_name
				(format #f "~a~a"
					(to-s (pg-cell row 'first_name))
					(if (null? goes-by) "" (format #f " (~a)" goes-by))))
			(cons 'goes_by (to-s (pg-cell row 'goes_by)))
			(cons 'uspa_id (to-s (pg-cell row 'uspa_id)))
			(cons 'license (to-s (pg-cell row 'license)))
			(cons 'uspa_expires (show-date expires (env-date-format)))
			(cons 'expires_border
				(if (member-out-of-date expires) "3px solid red" "0px"))
			(cons 'last_repack
				(show-date (latest-repack row) (env-date-format)))
			(cons 'repack_border
				(if (repacks-out-of-date row) "3px solid red" "0px"))
			(cons 'waiver_date (show-date waiver (env-date-format)))
			(cons 'waiver_border
				(if (waiver-out-of-date waiver)
					"3px solid red" "0px")))))
(define patch-chart
	(let ([query
				"select last_name, first_name, nickname, uspa_id,
						license, id, uspa_expires, last_repack,
						waiver_date, goes_by, last_repack_2
					from people
					where (updated >= ~a)"])
		(lambda (last-refresh)
			(let ([res (pg-exec (sdb) query
						(time-at (1- last-refresh)))])
				(pg-map-rows res
					(lambda (row)
						(assemble-patch row)))))))
(define detail-slot
	(let ([html "<td id=\"dcell[[PID]]\" colspan=\"9\">&nbsp;</td>"])
		(lambda (pid) (fill-template html #f (cons 'pid pid)))))
(define new-record
	(let ([query
				"select last_name, first_name, uspa_id,
						license, id, uspa_expires, last_repack,
						waiver_date, page_key, last_repack_2,
						goes_by
					from people
					where new
					and (id=~a)"])
		(lambda (person-id)
			(let ([res (pg-exec (sdb) query person-id)])
				(pg-map-rows res
					(lambda (row)
						(list
							(chart-row row #t)
							(pg-cell row 'id)
							"key-stub"
							(detail-slot (pg-cell row 'id)))))))))
(define group-catalog
	(let ([query
				"select last_name, first_name, uspa_id,
						license, id, uspa_expires, last_repack,
						waiver_date, page_key, goes_by, last_repack_2
					from people
					where (id in (~a)) order by page_key asc"]
			[all-query "select id from people"]
			[sub-query
				"select people.id as id
					from acct_xact, acct, people
					where ((from_acct=acct.id) or
						(to_acct=acct.id) or (acct.created >= ~a))
					and acct.personal
					and (acct.owner=people.id)
					and ((date_posted >= ~a) or
							(acct.created >= ~a))
					group by (people.id)"])
		(define (date-limit filter-key)
			(cond
				[(= filter-key 12)
					(time-format
						(time-add (time-now) (- (* 366 24 3600)))
						"%Y-%m-%d")]
				[(= filter-key 60)
					(time-format
						(time-add (time-now) (- (* 60 24 3600)))
						"%Y-%m-%d")]
				[else "1970-01-01"]))
		(define (sub-select page-key filter-key group-size)
			(if (= filter-key 0)
				all-query
				(let ([date (date-limit filter-key)]
						[one-day
							(time-format
								(time-add (time-now) (- (* 24 3600)))
								"%Y-%m-%d")])
					(pg-query (sdb) sub-query
						(list one-day date one-day)))))
		(lambda (active-filter page-key)
			(let* ([res
						(pg-exec (sdb)
							(format #f query
								(sub-select page-key active-filter
									group-size)))]
					[tint #f])
				(pg-map-rows res
					(lambda (row)
						(set! tint (not tint))
						(list
							(chart-row row tint)
							(pg-cell row 'id)
							(pg-cell row 'page_key)
							(detail-slot (pg-cell row 'id)))))))))
(define disambiguation-msg "\
There's a similar name in the people database.\r
Please add a disambiguation hint that will\r
distinguish this person. Consider also that you may have
duplicate records that need to be resolved.")
(define new-person?
	(let ([query "select new from people where (id=~a)"])
		(lambda (pid)
			(let ([row (pg-one-row (sdb) query pid)])
				(and row (pg-cell row 'new))))))
(define query-value-cap
	(let ([pat (make-regexp "^([a-z])(.*)$")])
		(lambda (req sym)
			(let* ([src (or (query-value req sym) "")]
					[match (regexp-exec pat src)])
				(if match
					(string-cat ""
						(string-upcase (match:substring match 1))
						(match:substring match 2)) src)))))
(define delete-abandoned-new
	(let ([drops
				"delete from roles
					where (roles.who=[[PID]]);
				delete from acct
					where (acct.owner=[[PID]]);
				delete from people where (id=[[PID]]);"]
			[query
				"select id
					from people
					where new
					and (current_timestamp >
						(created + interval '12 hours'))"])
		(lambda ()
			(pg-each-row
				(pg-exec (sdb) query)
				(lambda (row)
					(pg-exec (sdb)
						(fill-template drops #f
							(cons 'pid (pg-cell row 'id)))))))))
(define new-note
	(let ([query
				"insert into people_log (person, stamp, author)
					values (~a, ~a, ~a)"])
		(lambda (person author)
			(pg-exec (sdb) query person (time-now) author))))
(define edit-note
	(let ([query
				"update people_log set
					note=~a
					where (id=~a)"])
		(lambda (note-id note)
			(pg-exec (sdb) query note note-id))))
(define delete-note
	(let ([query "delete from people_log where (id=~a)"])
		(lambda (note-id) (pg-exec (sdb) query note-id))))
(define hilite-note
	(let ([query
				"update people_log set
					highlight=~a
					where (id=~a);
				update people set
					updated=current_timestamp
					where (id=~a)"])
		(lambda (note-id state person-id)
			(pg-exec (sdb) query state note-id person-id))))
(define expirations
	(let ([query
				"select id, uspa_expires, last_repack, last_repack_2,
						waiver_date, email,
						coalesce(goes_by,first_name,'Jumper') as name
					from people
					where notify and active
					and (email is not null)"]
			[day-seconds (* 24 3600)]
			[ladder '(1 7 14 30)])
		(define (deadline-uspa date) date)
		(define (deadline-waiver date)
			(time-local (1+ (time-year date)) 1 1 0 0 0))
		(define (deadline-repack date cycle)
			(time-add date (* cycle day-seconds)))
		(define (deadline date column cycle)
			(cond
				[(eq? column 'uspa_expires) date]
				[(eq? column 'waiver_date)
					(deadline-waiver date)]
				[(eq? column 'last_repack)
					(deadline-repack date cycle)]
				[(eq? column 'last_repack_2)
					(deadline-repack date cycle)]))
		(define (quantize days)
			(let scan ([rungs ladder])
				(cond
					[(null? rungs) #f]
					[(= days (car rungs)) days]
					[else (scan (cdr rungs))])))
		(define (noon timestamp)
			(time-local
				(time-year timestamp)
				(time-month timestamp)
				(time-mday timestamp) 12 0 0))
		(define (days-till expiration)
			(quantize
				(to-i (/ (time-diff (noon expiration) (noon (time-now)))
					day-seconds))))
		(define (days-out row column cycle)
			(let ([date (pg-cell row column)])
				(if (null? date)
					(cons #f #f)
					(let ([dl (deadline date column cycle)])
						(cons dl (days-till dl))))))
		(define (non-empty entry)
			(or (cdr (assq-ref entry 'uspa))
				(cdr (assq-ref entry 'waiver))
				(cdr (assq-ref entry 'repack_1))
				(cdr (assq-ref entry 'repack_2))))
		(define (prep row)
			(list
				(cons 'id (pg-cell row 'id))
				(cons 'name (pg-cell row 'name))
				(cons 'email (pg-cell row 'email))
				(cons 'uspa
					(days-out row 'uspa_expires
						(env-repack-cycle)))
				(cons 'waiver
					(days-out row 'waiver_date
						(env-repack-cycle)))
				(cons 'repack_1
					(days-out row 'last_repack
						(env-repack-cycle)))
				(cons 'repack_2
					(days-out row
						'last_repack_2 (env-repack-cycle)))))
		(lambda ()
			(filter non-empty
				(pg-map-rows (pg-exec (sdb) query)
					(lambda (row) (prep row)))))))
(define send-reminders
	(let ([descriptions
				(list
					'(uspa . "USPA membership")
					'(waiver . "dropzone waiver")
					'(repack_1 . "Primary rig inspection and repack")
					'(repack_2 . "Secondary rig inspection and repack"))]
			[bcc "pmy@linux.com"]
			[log-insert
				"insert into reminder_log (person,
							email_addr, detail, stamp)
					values (~a, ~a, ~a, ~a)"])
		(define (log-reminder person-id addr msg)
			(pg-exec (sdb) log-insert person-id addr msg (time-now)))
		(define (days-out expiration key)
			(let ([item (assq-ref expiration key)])
				(and item (cdr item))))
		(define (description expiration key days)
			(format #f "~d day~a: ~a (~a)"
				days (if (= days 1) "" "s")
				(assq-ref descriptions key)
				(time-format
					(car (assq-ref expiration key)) "%Y-%m-%d")))
		(define (listing expiration key)
			(let ([days (days-out expiration key)])
				(and days (description expiration key days))))
		(define (event-list expiration)
			(format #f "- ~a"
				(string-cat "\n\n- "
					(filter identity
						(list
							(listing expiration 'uspa)
							(listing expiration 'waiver)
							(listing expiration 'repack_1)
							(listing expiration 'repack_2))))))
		(define (send-msg expiration)
			(let* ([recip (assq-ref expiration 'email)]
					[msg
						(fill-template (fetch-doc reminder-template) #f
							(cons 'sender (env-dz-email-addr))
							(cons 'recip recip)
							(cons 'bcc
								(if bcc (format #f "\nBcc: ~a" bcc) ""))
							(cons 'dz_name (env-dz-full-name))
							(cons 'jumper_name
								(assq-ref expiration 'name))
							(cons 'list (event-list expiration)))])
				(send-email (env-dz-email-addr)
					(append (list recip) (if bcc (list bcc) '())) msg)
				(log-reminder
					(assq-ref expiration 'id)
					(assq-ref expiration 'email) msg)))
		(lambda ()
			(for-each
				(lambda (expiration) (send-msg expiration))
				(expirations)))))
(define report-duplicates
	(let ([query
				"with dups as (select count(*) as n,
						upper(last_name) as last_name,
						upper(first_name) as first_name,
						upper(disambiguate) as disambiguate
					from people
					where active
					group by upper(last_name),
						upper(first_name), upper(disambiguate)
					order by upper(last_name), upper(first_name))
				select last_name, first_name
					from dups
					where (n > 1)"]
			[html-top
				(deflate "<p>Two or more personnel records with
					identical names may indicate data entry duplication.
					It's important to resolve such duplicates sooner
					rather than later.</p>
					<p>This search runs a simple
					verbatim comparison of first and last names
					and any disambiguation tags. It
					won't recognize \"Thomas Smith\" and \"Tom Smith\"
					(for example) as potential duplicates.</p>~a")]
			[no-dups "<p>No duplicate names found.</p>"]
			[some-dups
				(deflate "<p>Possible duplicate records found: ~a</p>
					<p>If you don't see them, try selecting
					'All Records' in 'Find' menu.</p>")])
		(define (dup-list res)
			(string-cat "\n"
				"<ul>"
				(pg-map-rows res
					(lambda (row)
						(format #f "<li>~a, ~a</li>"
							(pg-cell row 'last_name)
							(pg-cell row 'first_name)))) "</ul>"))
		(lambda ()
			(let ([res (pg-exec (sdb) query)])
				(if (< (pg-tuples res) 1)
					(format #f html-top no-dups)
					(format #f html-top
						(format #f some-dups (dup-list res))))))))

(log-to "/var/log/nowcall/people.log")

(clear-busy)
(cron-add 4 0 (lambda (now) (send-reminders)))
(http-html "/"
	(lambda (req)
		(if (authorized-for req "people")
			(begin
				(session-set req 'people_refreshed 0)
				(fill-template (fetch-doc composite) #f
					(cons 'dz_full_name (env-dz-full-name))
					(cons 'admin_check (admin-light req (sdb)))
					(cons 'chart chart-tpt)))
			(redirect-html "/login/people"))))
(http-json "/rowgroup"
	; display a group of records, as part of a series of
	; fetch-and-display cycles
	(admin-gate "people" (req)
		(let ([active-filter (query-value-number req 'active_filter)]
				[page-key (query-value req 'page_key)])
			(list
				(cons 'rows
					(group-catalog active-filter page-key))))))
(http-json "/chart"
	; refresh people chart
	(admin-gate "people" (req)
		(let ([latest (latest-event)]
				[last-refresh
					(to-i (session-get req 'people_refreshed))])
			(delete-abandoned-new)
			(if (> latest last-refresh)
				(begin
					(session-set req 'people_refreshed latest)
					(list
						(cons 'refresh #t)
						(cons 'patches
							(patch-chart last-refresh))))
				(list (cons 'refresh #f))))))
(http-json "/new"
	; Create new empty people record.
	(admin-gate "people" (req)
		(let* ([last-name (query-value-cap req 'last_name)]
				[first-name (query-value-cap req 'first_name)]
				[role (query-value req 'role)]
				[pid (new-person-id last-name first-name)])
			(add-role pid role)
			(acct-assure-cash-acct pid (sdb))
			(list
				(cons 'pid pid)
				(cons 'rows (new-record pid))))))
(http-json "/del"
	; Delete people entry, after confirmation if non-empty.
	(admin-gate "people" (req)
		(let ([pid (query-value-number req 'pid)])
			(delete-person pid)
			(list (cons 'pid pid)))))
(http-json "/predel"
	; If people entry is empty (no first, last, nickname)
	; delete it straight away. Otherwise, request confirmation.
	(admin-gate "people" (req)
		(let ([pid (query-value-number req 'pid)])
			(list
				(cons 'blank (empty-record pid))
				(cons 'pid pid)
				(cons 'name (people-name pid))))))
(http-json "/detail"
	(admin-gate "people" (req)
		(let* ([pid (query-value-number req 'pid)]
				[first-last (string=? (env-name-order) "first-last")]
				[worker (to-i (session-get req 'person-id))]
				[busy (is-busy pid worker)])
			(mark-busy pid worker #t)
			(list
				(cons 'status #t)
				(cons 'busy busy)
				(cons 'holder (held-by pid))
				(cons 'pid pid)
				(cons 'first_last first-last)
				(cons 'cb_color busy-color)
				(cons 'admin (admin-user req))
				(cons 'date_format (env-date-format))
				(cons 'html (build-detail pid first-last))))))
(http-json "/close_det"
	(admin-gate "people" (req)
		(let ([pid (query-value-number req 'pid)]
				[worker (to-i (session-get req 'person-id))]
				)
			(mark-busy pid worker #f)
			(list
				(cons 'pid pid)
				(cons 'delete (new-person? pid))
				(cons 'cb_color non-busy-color)))))
(http-json "/update_det"
	(admin-gate "people" (req)
		(let ([pid (query-value-number req 'pid)]
				[last-name (query-value-cap req 'last_name)]
				[first-name (query-value-cap req 'first_name)]
				[disambig (query-value req 'disambiguate)])
			(if (same-name? pid last-name first-name disambig)
				(list
					(cons 'status #f)
					(cons 'msg disambiguation-msg))
				(begin
					(update-person pid req)
					(when (admin-user req) (update-auth pid req))
					(list
						(cons 'status #t)
						(cons 'pid pid)
						(cons 'name
							(person-name pid
								(env-name-order) (sdb)))))))))
(http-json "/checkname"
	(lambda (req)
		(let ([pid (query-value-number req 'pid)]
				[name (query-value req 'name)])
			(list
				(cons 'avail
					(let ([avail (username-available name pid (sdb))])
						(cond
							[(eq? avail 'mine) "mine"]
							[avail "avail"]
							[else "taken"])))
				(cons 'pid pid)))))
(http-json "/addnote"
	(admin-gate "people" (req)
		(let ([pid (query-value-number req 'pid)])
			(new-note pid (session-get req 'person-id))
			(list (cons 'html (notes-log pid))))))
(http-json "/ednote"
	(admin-gate "people" (req)
		(let ([note-id (query-value-number req 'nid)]
				[note (query-value req 'note)])
			(edit-note note-id note)
			(list (cons 'status #t)))))
(http-json "/delnote"
	(admin-gate "people" (req)
		(let ([note-id (query-value-number req 'nid)]
				[person-id (query-value-number req 'pid)])
			(delete-note note-id)
			(list (cons 'html (notes-log person-id))))))
(http-json "/hlnote"
	(admin-gate "people" (req)
		(let ([note-id (query-value-number req 'nid)]
				[state (query-value-boolean req 'state)]
				[person-id (query-value-number req 'pid)])
			(hilite-note note-id state person-id)
			(list (cons 'status #t)))))
(http-json "/dups"
	(admin-gate "people" (req)
		(list (cons 'html (report-duplicates)))))
(add-javascript-logger "people")
(cron-start)
