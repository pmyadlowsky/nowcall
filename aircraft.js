/*
** Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
**
** This program is free software ; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation ; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY ; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program ; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var aircraft_list = Array();
var save_count = 0;
var error_messages = Array();
var touched = Array();
function init() {
	mark_page("Aircraft");
	freshen_chart();
	}
function chart_refresh(resp) {
	var div = document.getElementById("aircraft_chart");
	div.innerHTML = resp.chart;
	aircraft_list = resp.list;
	var btn = document.getElementById("new_btn");
	btn.disabled = false;
	btn.title = "add new aircraft";
	}
function freshen_chart() {
	$.get("/aircraft/chart",
		{ active: active_only() }, chart_refresh, "json");
	}
function toggle_inactive() {
	freshen_chart();
	}
function active_only() {
	if (document.getElementById("inactive").checked) return 0;
	return 1;
	}
function aircraft_new() {
	$.get("/aircraft/new", {},
		function(resp) {
			chart_refresh(resp);
			toggle_detail(resp.id);
			}, "json");
	}
function aircraft_delete(id) {
	$.get("/aircraft/del",
		{ id: id, active: active_only() },
		function(resp) {
			if (resp.msg != "") local_alert("Delete Failed", resp.msg);
			chart_refresh(resp);
			},
		"json");
	}
function delete_check(resp) {
	if (resp.blank) aircraft_delete(resp.id);
	else {
		local_confirm("Aircraft", "Yes", "No",
			function() { aircraft_delete(resp.id); }, null,
			"Really? Delete '" + resp.name + "'?");
		}
	}
function toggle_active(id, cbox) {
	var state = (cbox.checked ? 1 : 0);
	$.get("/aircraft/tact",
		{ active: active_only(), id: id, state: state },
		chart_refresh, "json");
	}
function aircraft_predelete(id) {
	$.get("/aircraft/predel", { id: id }, delete_check, "json");
	}
function cell_value(id, column) {
	return document.getElementById(column + id).value;
	}
function enable_save(aircraft_id) {
	document.getElementById("save" + aircraft_id).disabled = false;
	}
function disable_save(aircraft_id) {
	document.getElementById("save" + aircraft_id).disabled = true;
	}
function echo_chart(id, field, value) {
	document.getElementById("ch-" + id + "-" + field).innerHTML = value;
	}
function echo_form(id, field, value) {
	document.getElementById(field + id).value = value;
	}
function save_handler(resp) {
	if (resp.status) {
		var id = resp.aircraft_id;
		disable_save(resp.aircraft_id);
		echo_chart(id, "nickname", resp.nickname);
		echo_chart(id, "make", resp.make);
		echo_chart(id, "model", resp.model);
		echo_chart(id, "tail_num", resp.tail_num);
		echo_chart(id, "capacity", resp.capacity);
		echo_chart(id, "rank", resp.rank);
		echo_form(id, "nickname", resp.nickname);
		echo_form(id, "make", resp.make);
		echo_form(id, "model", resp.model);
		echo_form(id, "tail_num", resp.tail_num);
		echo_form(id, "capacity", resp.capacity);
		echo_form(id, "rank", resp.rank);
		echo_form(id, "fuel_capacity", resp.fuel_capacity);
		echo_form(id, "fuel_burn", resp.fuel_burn);
		echo_form(id, "empty_weight", resp.empty_weight);
		echo_form(id, "basic_arm", resp.basic_arm);
		echo_form(id, "fuel_arm", resp.fuel_arm);
		echo_form(id, "pilot_arm", resp.pilot_arm);
		echo_form(id, "avg_payload_arm", resp.avg_payload_arm);
		echo_form(id, "notes", resp.notes);
		}
	else {
		local_alert("Data Errors", resp.msg);
		}
	}
function aircraft_save(aircraft_id) {
	var data;
	var observer = document.getElementById("observer" + aircraft_id);
	data = {
		id: aircraft_id,
		make: cell_value(aircraft_id, "make"),
		model: cell_value(aircraft_id, "model"),
		tail_num: cell_value(aircraft_id, "tail_num"),
		nickname: cell_value(aircraft_id, "nickname"),
		observer: (observer.checked ? "1" : "0"),
		capacity: cell_value(aircraft_id, "capacity"),
		rank: cell_value(aircraft_id, "rank"),
		empty_weight: cell_value(aircraft_id, "empty_weight"),
		basic_arm: cell_value(aircraft_id, "basic_arm"),
		fuel_arm: cell_value(aircraft_id, "fuel_arm"),
		fuel_capacity: cell_value(aircraft_id, "fuel_capacity"),
		fuel_burn: cell_value(aircraft_id, "fuel_burn"),
		pilot_arm: cell_value(aircraft_id, "pilot_arm"),
		avg_payload_arm: cell_value(aircraft_id, "avg_payload_arm"),
		fuel_type: cell_value(aircraft_id, "fuel_type"),
		notes: cell_value(aircraft_id, "notes")
		};
	$.post("/aircraft/upd", data, save_handler, "json");
	}
function mark_touched(id) {
	touched[id] = "1";
	enable_save();
	}
function logout() {
	$.get("/login/logout", {},
		function(resp) { window.location = "/manifest"; },
		"json");
	}
function toggle_detail(aircraft_id) {
	var row = document.getElementById("drow" + aircraft_id);
	if (row.style.display == "none") {
		$.get("/aircraft/acdet", { id: aircraft_id },
			function(resp) {
				var obj;
				obj = document.getElementById("detail" + resp.id);
				obj.innerHTML = resp.html;
				obj = document.getElementById("drow" + resp.id);
				obj.style.display = "table-row";
				document.getElementById("save" + resp.id).disabled = true;
				},
			"json");
		}
	else row.style.display = "none";
	}
