drop table settings;
create table settings (
	sym_name varchar primary key,
	long_name varchar,
	value varchar,
	seq integer,
	data_type char(1),
	stamp timestamp default current_timestamp,
	description text
	);

insert into settings (sym_name, long_name, value, seq, data_type, description)
	values (
		'ground-reserve-out-of-date',
		'ground out-of-date reserves',
		'f',
		1,
		'b',
		'Disallow manifesting anyone whose reserve repack is out of date.'
		);
insert into settings (sym_name, long_name, value, seq, data_type, description)
	values (
		'min-load-sheets',
		'minimum load sheets per page',
		'20',
		2,
		'i',
		'minimum load sheets per manifest page'
		);
insert into settings (sym_name, long_name, value, seq, data_type, description)
	values (
		'load-sheet-incr',
		'load sheet increment',
		'4',
		3,
		'i',
		'load sheet increment, when adding more'
		);
insert into settings (sym_name, long_name, value, seq, data_type, description)
	values (
		'min-search-chars',
		'minimum search characters',
		'3',
		4,
		'i',
		'minimum typed characters to trigger people search for manifest'
		);
