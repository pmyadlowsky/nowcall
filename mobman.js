var polling = false;
var scroll_cursor = Array();
var showing_calls = Array();
var selected_aircraft_id = "";
var user_id = -1;
function selected_aircraft() {
	var menu = document.getElementById("aircraft");
	return menu.options[menu.selectedIndex].value;
	}
function fetch_sheets() {
	var aircraft = selected_aircraft(); 
	selected_aircraft_id = aircraft;
	$.get("/mobman/sheets", { aircraft: aircraft },
			function(resp) {
				if (!resp.status) {
					alert("how'd you get in here?");
					return;
					}
				var div = document.getElementById("loadsheets");
				div.innerHTML = resp.sheets;
				user_id = resp.user;
				scroll_to_recent_load(true);
				if (!polling) {
					polling = true;
					LongPoll.poll();
					}
				}, "json");
	}
function load_sheets() {
	var aircraft = selected_aircraft();
	if (aircraft == "logout") {
		$.get("/login/logout", {},
			function(resp) { window.location = "/mobman"; },
			"json");
		return;
		}
	if (aircraft == "login") {
		window.location = "/login/mobman";
		return;
		}
	if (aircraft == "desktop") {
		window.location = "/manifest";
		return;
		}
	$.get("/manifest/msheets",
			{
				aircraft: aircraft,
				date: $.datepicker.formatDate("yy-mm-dd", new Date())
				},
			function(resp) {
				if (resp.status) fetch_sheets();
				}, "json");
	}
function populate_aircraft() {
	$.get("/mobman/aircraft", {},
			function(resp) {
				var menu = document.getElementById("aircraft");
				var option;
				menu.options.length = 0;
				for (var i = 0; i < resp.aircraft.length; i++) {
					option = new Option(resp.aircraft[i][1],
									resp.aircraft[i][0]);
					menu.add(option);
					}
				menu.add(new Option("Manifest/Desktop", "desktop"));
				if (resp.logged_in)
					menu.add(new Option("Sign Out", "logout"));
				else 
					menu.add(new Option("Sign In", "login"));
				load_sheets();
				}, "json");
	}
function sheet_date(tab_id, day_offset) {
	return $.datepicker.formatDate("yy-mm-dd", new Date());
	}
function color_sheet(aircraft_id, load, color, sheet_status) {
	var key = aircraft_id + "_" + load;
	var sheet = document.getElementById("load" + key);
	if (sheet == null) return;
	sheet.style.border = "6px solid " + color;
	var toprow = document.getElementById("toprow" + key);
	toprow.style.backgroundColor = color;
	var btn = document.getElementById("access" + key);
	if (btn != null) btn.value = sheet_status;
	}
function role_menu(key, roles, finish) {
	var menu = document.getElementById("role" + key);
	if (menu == null) return;
	var option;
	menu.options.length = 0;
	if (roles == null) {
		menu.add(new Option("-", "-"));
		}
	else {
		var preset = roles[0];
		var selected = 0;
		for (var i = 1; i < roles.length; i++) {
			option = new Option(roles[i], roles[i]);
			menu.add(option);
			if (roles[i] == preset) selected = i - 1;
			}
		menu.selectedIndex = selected;
		}
	if (finish != null) finish(menu);
	}
function touch_object(id, action) {
	var obj = document.getElementById(id);
	if (obj != null) action(obj);
	}
function mmss(sec) {
	if (sec < 1) return "NOW!";
	var mins = Math.floor(sec / 60);
	var secs = sec % 60;
	var out = "";
	if (mins < 10) out += "0";
	out += mins;
	out += ":";
	if (secs < 10) out += "0";
	out += secs;
	return out
	}
function show_countdown(aircraft, load_num, remain) {
	var option, label;
	var menu = document.getElementById("aircraft");
	for (var i = 0; i < menu.options.length; i++) {
		option = menu.options[i];
		if (option.value != aircraft) continue;
		label = option.text.replace(/ \[LOAD .+$/, "");
		if (load_num > 0)
			label += " [LOAD " + load_num + " - " + mmss(remain) + "]";
		option.text = label;
		break;
		}
	}
function hide_countdown(aircraft) {
	show_countdown(aircraft, -1, 0);
	showing_calls[aircraft] = {
		active: false,
		load: 0,
		remain: 0
		};
	}
function paint_load_calls(calls) {
	// update load call countdowns
	var n = (calls == null ? 0 : calls.length);
	for (var i = 0; i < n; i++) {
		tuple = calls[i];
		if (tuple.drop) hide_countdown(tuple.aircraft);
		else {
			showing_calls[tuple.aircraft] = {
				active: true,
				load: tuple.load,
				remain: tuple.remain
				};
			}
		}
	}
function update_manifest(resp, retrack) {
	var n;
	var div;
	var tuple;
	if (resp.load_calls != null) paint_load_calls(resp.load_calls);
	else {
		var aircraft;
		for (aircraft in showing_calls) {
			if (showing_calls[aircraft].active) hide_countdown(aircraft);
			}
		}
	if (!resp.refresh || (resp.patches == null)) {
		//if (retrack) LongPoll.poll();
		return;
		}
	n = resp.patches.length;
	var date;
	var patch;
	var key;
	var closed;
	var loads_touched = {};
	for (var i = 0; i < n; i++) {
		patch = resp.patches[i];
		key = patch.aircraft_id + "_" + patch.load;
		date = sheet_date(patch.aircraft_id, 0);
		if (date != patch.date) continue;
		if (!loads_touched.hasOwnProperty(key)) {
			loads_touched[key] = true;
			color_sheet(patch.aircraft_id, patch.load,
				patch.color, patch.status);
			}
		closed = (patch.closed && !resp.admin);
		key = key + "_" + patch.slot;
		touch_object("name" + key,
			function(obj) { obj.innerHTML = patch.name_form });
		var slot_owner = (patch.passenger == user_id);
		touch_object("scratch" + key,
			function(obj) {
				obj.disabled = closed || !slot_owner;
				});
		role_menu(key, patch.roles,
			function(menu) {
				menu.disabled = closed
						|| (patch.roles == null)
						|| !slot_owner;
				});
		}
	}
function scroll_to_load(load_id, align_top) {
	var sheet = document.getElementById(load_id);
	if (sheet != null) {
		sheet.scrollIntoView(align_top);
		scroll_cursor[selected_aircraft_id] = load_id;
		}
	}
function scroll_to_recent_load(force) {
	$.post("/manifest/autoscroll", {
				force: (force ? 1 : 0),
				aircraft: selected_aircraft_id,
				date: sheet_date(selected_aircraft_id, 0)
				},
			function(resp) {
				scroll_to_load(resp.recent_load, false);
				},
			"json");
	}
function process_payload(payload) {
	if (payload.cmd == "patch") {
		update_manifest(payload, true);
		scroll_to_recent_load(false);
		var patch, slot;
		for (var i = 0; i < payload.patches.length; i++) {
			patch = payload.patches[i];
			slot = document.getElementById("tabload" + patch.aircraft_id);
			if (slot == null) continue;
			if (patch.recent_load > 0)
				slot.innerHTML = "[" + patch.recent_load + "]";
			else slot.innerHTML = "";
			}
		}
	else if (payload.cmd == "calls-only") {
		if (payload.load_calls != null)
			paint_load_calls(payload.load_calls);
		}
	else if (payload.cmd == "update-qualifiers") {
		var n = payload.qualifiers.length;
		var qualifier, div;
		for (var i = 0; i < n; i++) {
			qualifier = payload.qualifiers[i];
			div = document.getElementById("qualifier" + qualifier.id);
			div.innerHTML = qualifier.button;
			}
		}
	else if (payload.cmd == "repaint") {
		window.location.reload(true);
		}
	}
function refresh_countdowns() {
	var aircraft;
	for (aircraft in showing_calls) {
		if (!showing_calls[aircraft].active) continue;
		show_countdown(aircraft,
						showing_calls[aircraft].load,
						showing_calls[aircraft].remain);
		showing_calls[aircraft].remain -= 1;
		}
	}
function time_ref() {
	var msec = (new Date().getTime()) % 1000;
	setTimeout(time_ref, 1000 - msec);
	refresh_countdowns();
	}
function roll_role(menu) {
	var m = menu.id.match(/^role[^0-9]*([0-9]+)_([0-9]+)_([0-9]+)/);
	var aircraft_id = m[1];
	var load_num = parseInt(m[2]);
	var slot_num = parseInt(m[3]);
	var role = menu.value;
	$.post("/manifest/role",
			{
				aircraft: aircraft_id,
				load: load_num,
				slot: slot_num,
				date: sheet_date(aircraft_id, 0),
				role: role
				},
			function(resp){}, "json")
	}
function update_load_weight(aircraft_id, load_num) {
	$.post("/manifest/updwt",
			{
				aircraft: aircraft_id,
				load: load_num,
				date: sheet_date(aircraft_id, 0)
				},
			function(resp){}, "json");
	}
function scratch(aircraft_id, load, slot) {
	$.post("/manifest/scratch",
		{
			date: sheet_date(aircraft_id, 0),
			load: load,
			aircraft: aircraft_id,
			slots: slot
			},
		function(resp) {
			if (!resp.status) return;
			var key;
			for (var i = 0; i < resp.slots.length; i++) {
				key = resp.aircraft_id + "_" + resp.load +
					"_" + resp.slots[i];
				touch_object("name" + key,
					function(slot){
						slot.innerHTML = resp.forms[i];
						slot.style.backgroundColor = "white";
						slot.focus();
						});
				role_menu(key, null,
					function(menu){ menu.disabled = true; });
				}
			update_load_weight(resp.aircraft_id, resp.load);
			},
		"json");
	}
function fill_slots(resp) {
	var name;
	if (!resp.status) {
		name = document.getElementById("name" + resp.slot_key);
		if (name != null) {
			name.innerHTML = resp.form;
			name.childNodes[0].focus();
			}
		local_alert("Boarding Errors", resp.msg);
		return;
		}
	var key, menu, alt_key;
	for (var i = 0; i < resp.slot_keys.length; i++) {
		key = resp.slot_keys[i];
		touch_object("name" + key,
			function(obj){ obj.innerHTML = resp.forms[i] }
			);
		role_menu(key, resp.roles[i],
			function(menu){ menu.disabled = false });
		}
	update_load_weight(resp.dst_aircraft, resp.load_num);
	}
function board(obj, pid, aircraft, load, state) {
	obj.blur();
	if (state != "ok") {
		if (state == "auth")
			local_alert("Manifest", "Must be on DZ premises to self-manifest.")
		else
			local_alert("Manifest", "Must be logged in and on DZ premises to self-manifest.")
		return false;
		}
	$.post("/manifest/board",
		{
			ids: pid,
			roles: "",
			extras: "[false]",
			dst_aircraft: aircraft,
			src_aircraft: aircraft,
			load: load,
			slot: 0,
			date: sheet_date(aircraft, 0)
			}, fill_slots, "json");
	return false;
	}
function live_search(box, aircraft, load, slot, evnt) {
	// stub for board()
	if (user_id > 0)
		return board(box, user_id, aircraft, load);
	else return false;
	}
function slot_unfocused() {
	// stub
	}
function init() {
	populate_aircraft();
	LongPoll.set_poll_url("/manifest/track");
	LongPoll.set_payload_handler(process_payload);
	LongPoll.set_fallback_timeout(30);
	time_ref();
	}
