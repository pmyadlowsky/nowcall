#! /usr/local/bin/gusher -p 3003
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))
(use-modules (ice-9 regex))

(include "lib.scm")
(include "settings_lib.scm")
(include "database.scm")

(define env-date-format (setting-cache 'date-format))
(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-name-order (setting-cache 'name-order))
(define env-time-format (setting-cache 'time-format))
(define env-manifest-worker
	; user has manifest role
	(request-cache
		(let ([query
					"select who
						from roles
						where (who=~a)
						and (role='MANFST')
						limit 1"])
			(lambda (http-req)
				(and
					(not (admin-user http-req))
					(let ([row
								(pg-one-row (sdb) query
									(or (env-person-id) -1))])
						(and row (pg-cell row 'who))))))))

(define frame (make-doc 'file "frame.html"))
(define script (make-doc 'file "schedule.js"))
(define page-body (make-doc 'file "schedule.html"))
(define cancel-color "ea7a7d")
(define admin-cancel-color "ffd700")
(define touched)
(define (touch)
	; timestamp schedule change
	(set! touched (* (time-epoch (time-now)) 1000)) touched)
(define composite
	(make-doc (list frame script page-body)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "Staff Schedule")
				(cons 'app "schedule")
				(cons 'script (fetch-doc script))
				(cons 'version nowcall-version)
				(cons 'ext_scripts
					(load-javascript
						(list
							"/js/moment.min.js"
							"/js/fullcalendar.min.js")))
				(cons 'content
					(fill-template (fetch-doc page-body) #t))))))
(define day-title
	(let ([query
				"select day_title
					from staff_schedule
					where (sched_date=~a)
					and (person_id=0)
					and (day_title is not null)"])
		(lambda (date)
			(let ([row (pg-one-row (sdb) query date)])
				(and row (pg-cell row 'day_title))))))
(define staff-selector
	(let ([role-query
				"select key, description
					from role_key
					where schedulable
					and (key in (select distinct role from roles))
					order by description asc"]
			[table-tpt
				(deflate "<table>
				<tr><td>Title:</td>
					<td><input type=\"text\" id=\"day-title-~a\"
					placeholder=\"optional day title\"
					value=\"~a\"/></td></tr>
				<tr><td>Role:</td><td>~a</td></tr>
				<tr><td valign=\"top\">Staff:</td>
				<td id=\"staff-list\"></td></tr>
				</table>")])
		(define (pair row)
			(cons (pg-cell row 'key) (pg-cell row 'description)))
		(lambda (date)
			(format #f table-tpt
				date (or (day-title date) "")
				(html-select
					"staff-role"
					(format #f "onchange=\"select_role('~a')\"" date)
					""
					(pg-map-rows (pg-exec (sdb) role-query) pair))))))
(define staff-menu
	(let ([query
				"select distinct people.id as id,
						people.last_name, people.first_name
					from people, roles
					where (roles.who=people.id)
					and (roles.role=~a)
					and people.active
					and (people.id not in
						(select person_id from staff_schedule
							where (sched_date=~a) and (role=~a)))
					order by people.last_name, people.first_name"]
			[passive-tpt "<div>~a</div>"]
			[active-tpt
				(deflate "<div><a href=\"#\"
					title=\"schedule this person for this date and role\"
					onclick=\"staff('~a','~d');return false\">~a</a>
					</div>")])
		(define (li person-id login-id self-staff date)
				(let ([name (person-name person-id (env-name-order) (sdb))])
				(if (and self-staff (not (= person-id login-id)))
					(format #f passive-tpt name)
					(format #f active-tpt date person-id name))))
		(lambda (login-id self-staff date role)
			(let ([res (pg-exec (sdb) query role date role)])
				(string-cat "\n"
					(pg-map-rows res
						(lambda (row)
							(li (pg-cell row 'id)
								login-id self-staff date))))))))
(define schedule-staff
	(let ([insert
				"insert into staff_schedule (person_id, sched_date, role)
					values (~a, ~a, ~a)"]
			[check
				"select id
					from staff_schedule
					where (person_id=~a)
					and (role=~a)
					and (sched_date=~a)"])
		(define (already-on? person-id sched-date role)
			(pg-one-row (sdb) check person-id role sched-date))
		(lambda (person-id login-id self-staff sched-date role)
			(and
				(not (already-on? person-id sched-date role))
				(if self-staff (= person-id login-id) #t)
				(pg-exec (sdb) insert person-id sched-date role)
				(touch) #t))))
(define schedule-slots
	; People scheduled for each date across a date range,
	; and the staff roles they serve.
	(let ([query
				"select id, person_id, sched_date,
						role_key.color_code as color,
						role_key.description as role,
						role_key.sched_seq as role_seq,
						staff_schedule.canceled as canceled,
						staff_schedule.admin_canceled as admin_canceled,
						staff_schedule.tentative as tentative
					from staff_schedule, role_key
					where (day_title is null)~a
					and (sched_date >= ~~a)
					and (sched_date <= ~~a)
					and (role_key.key=staff_schedule.role)"])
		(define (event-color row)
			(string-cat "" "#"
				(cond
					[(pg-cell row 'canceled) cancel-color]
					[(pg-cell row 'admin_canceled) admin-cancel-color]
					[else (pg-cell row 'color)])))
		(define (event-title row)
			(format #f "~a~a"
				(person-name
					(pg-cell row 'person_id)
					(env-name-order) (sdb))
				(if (pg-cell row 'tentative) " ?" "")))
		(define (sequence-key row)
			(format #f "B~2,'0d:~15,'0d"
				(pg-cell row 'role_seq)
				(pg-cell row 'id)))
		(define (mod-query self-staff login-id)
			(format #f query
				(if self-staff
					(format #f " and ((person_id=~d)~a)" login-id
						(if (env-manifest-worker)
							" or (sched_date=current_date)" "")) "")))
		(define (person-item row self-staff)
			(list
				(cons 'id (pg-cell row 'id))
				(cons 'title (event-title row))
				(cons 'type "person")
				(cons 'role (pg-cell row 'role))
				(cons 'seq (sequence-key row))
				(cons 'can_delete (not self-staff))
				(cons 'backgroundColor (event-color row))
				(cons 'allDay #t)
				(cons 'start (pg-cell row 'sched_date))
				(cons 'editable #f)
				(cons 'textColor "black")
				(cons 'startEditable #f)
				(cons 'durationEditable #f)))
		(lambda (start-date end-date self-staff login-id)
			(pg-map-rows
				(pg-exec (sdb) (mod-query self-staff login-id)
					start-date end-date)
				(lambda (row) (person-item row self-staff))))))
(define remove-sched-entry
	(let ([query "delete from staff_schedule where id=~a"])
		(lambda (entry-id)
			(pg-exec (sdb) query entry-id) (touch) #t)))
(define cancel-entry
	(let ([query
				"update staff_schedule set
					canceled=~a
					where (id=~a)"])
		(lambda (entry-id state)
			(pg-exec (sdb) query state entry-id) (touch) #t)))
(define admin-cancel-entry
	(let ([query
				"update staff_schedule set
					admin_canceled=~a
					where (id=~a)"])
		(lambda (entry-id state)
			(pg-exec (sdb) query state entry-id) (touch) #t)))
(define entry-edit-form
	(let ([query
				"select person_id, sched_date, canceled, tentative,
						admin_canceled,
						role_key.description as role
					from staff_schedule, role_key
					where (staff_schedule.id=~a)
					and (role_key.key=staff_schedule.role)"]
			[html
				(deflate "<div class=\"entry-head-block\">
					<div>~a</div>
					<div>~a</div>
					<div>~a</div></div>
					<div><input type=\"checkbox\"~a
						onclick=\"cancel_entry('~d',this)\"
						/>worker canceled</div>
					<div><input type=\"checkbox\"~a
						onclick=\"admin_cancel_entry('~d',this)\"
						/>admin canceled</div>
					<div><input type=\"checkbox\"~a
						onclick=\"set_maybe('~d',this)\"
						/>tentative</div>")])
		(define (checkbox row column)
			(if (pg-cell row column) " checked=\"checked\"" ""))
		(lambda (entry-id)
			(let ([row (pg-one-row (sdb) query entry-id)])
				(and row
					(format #f html
						(person-name (pg-cell row 'person_id)
							(env-name-order) (sdb))
						(show-date (pg-cell row 'sched_date)
							(env-date-format))
						(pg-cell row 'role)
						(checkbox row 'canceled) entry-id
						(checkbox row 'admin_canceled) entry-id
						(checkbox row 'tentative) entry-id))))))
(define set-tentative
	(let ([query
				"update staff_schedule set
					tentative=~a
					where (id=~a)"])
		(lambda (entry-id state)
			(pg-exec (sdb) query state entry-id) (touch) #t)))
(define (staff-authorized? req)
	(or
		(authorized-for req "schedule")
		(and
			(env-person-id)
			(schedulable-person? (env-person-id) (sdb)))))
(define (self-staffing? req)
	(and
		(not (authorized-for req "schedule"))
		(schedulable-person? (env-person-id) (sdb))))
(define day-titles
	; fetch day titles, for display at top of each date cell
	(let ([query
				"select sched_date, day_title
					from staff_schedule
					where (day_title is not null)
					and (sched_date >= ~a)
					and (sched_date <= ~a)
					and (person_id=0)"])
		(lambda (start-date end-date)
			(pg-map-rows
				(pg-exec (sdb) query start-date end-date)
				(lambda (row)
					(list
						(time-format (pg-cell row 'sched_date) "%Y-%m-%d")
						(pg-cell row 'day_title)))))))
(define work-slot-counts
	; compute work slot counts (instructor slots plus video slots)
	; for each day in date range, return date/count duples
	(let ([query 
				"select jump_date,
						sum(product_item.instr_num_req) as instructors,
						sum(case when media is null then 0 else 1 end)
							as video
					from reservations, reservation_groups, product_item
					where (reservations.group_id=reservation_groups.id)
					and (not reservation_groups.hold)
					and (skydive=product_item.id)
					and (jump_date >= ~a)
					and (jump_date <= ~a)
					group by jump_date"])
		(define (day-item row)
			(list
				(cons 'id 0)
				(cons 'type "meta")
				(cons 'title
					(format #f "WORK SLOTS: ~d"
						(+ (pg-cell row 'instructors)
							(pg-cell row 'video))))
				(cons 'role "instructional, video slot count")
				(cons 'seq "A")
				(cons 'can_delete #f)
				(cons 'backgroundColor "grey")
				(cons 'allDay #t)
				(cons 'start
					(time-format (pg-cell row 'jump_date) "%Y-%m-%d"))
				(cons 'editable #f)
				(cons 'textColor "white")
				(cons 'startEditable #f)
				(cons 'durationEditable #f)))
		(lambda (start-date end-date)
			(pg-map-rows
				(pg-exec (sdb) query start-date end-date) day-item))))
(define set-day-title
	; set date's title, for top of cell in calendar
	(let ([delete
				"delete from staff_schedule
					where (sched_date=~a)
					and (person_id=0)
					and (day_title is not null)"]
			[insert
				"insert into staff_schedule (person_id, sched_date,
						day_title) values (0, ~a, ~a)"])
		(lambda (date title)
			(pg-exec (sdb) delete date)
			(unless (string=? title "")
				(pg-exec (sdb) insert date title)) (touch))))
(define work-slots
	(let ([query "select class_slot, product_item.name as jump,
				product_item.instr_num_req as instrs, media
				from reservations, reservation_groups, product_item
				where (reservations.group_id=reservation_groups.id)
				and (not reservation_groups.hold)
				and (reservations.skydive=product_item.id)
				and (jump_date=~a)
				order by class_slot, jump"]
			[media-query
				"select name from product_item where (id=~a)"])
		(define (media product-id)
			(or
				(and (null? product-id) "")
				(let ([row (pg-one-row (sdb) media-query product-id)])
					(or (and row (pg-cell row 'name)) ""))))
		(define (slot-row row)
			(list
				(cons 'slot
					(wall-clock-time (pg-cell row 'class_slot)
						(env-time-format)))
				(cons 'skydive (pg-cell row 'jump))
				(cons 'instructors (pg-cell row 'instrs))
				(cons 'media (media (pg-cell row 'media)))))
		(lambda (cal-date)
			(pg-map-rows (pg-exec (sdb) query cal-date) slot-row))))

(log-to "/var/log/nowcall/schedule.log")

(touch)
(http-html "/"
	(lambda (req)
		(if (staff-authorized? req)
			(fill-template (fetch-doc composite) #f
				(cons 'dz_full_name (env-dz-full-name))
				(cons 'admin_check (admin-light req (sdb))))
			(redirect-html "/login/schedule"))))
(http-json "/imenu"
	; staff role menu
	(lambda (req)
		(if (staff-authorized? req)
			(let ([date (query-value req 'date)]
					[tdate (query-value req 'tdate)])
				(list
					(cons 'html (staff-selector date))
					(cons 'tdate tdate)))
			(list (cons 'status #f)))))
(http-json "/slots"
	; occupied schedule slots
	(lambda (req)
		(if (staff-authorized? req)
			(let ([start-date (query-value req 'start)]
					[end-date (query-value req 'end)])
				(list
					(cons 'events
						(append
							(schedule-slots start-date end-date
								(self-staffing? req)
								(env-person-id))
							(work-slot-counts start-date end-date)))
					(cons 'titles (day-titles start-date end-date))))
			(list (cons 'status #f)))))
(http-json "/staff"
	; create schedule slot
	(lambda (req)
		(if (staff-authorized? req)
			(let ([sched-date (query-value req 'date)]
					[person-id (query-value-number req 'pid)]
					[role (query-value req 'role)])
				(list
					(cons 'status
						(schedule-staff
							person-id
							(env-person-id)
							(self-staffing? req)
							sched-date role))))
			(list (cons 'status #f)))))
(http-json "/rem"
	; delete schedule entry
	(admin-gate "schedule" (req)
		(let ([entry-id (query-value req 'id)])
			(list
				(cons 'id entry-id)
				(cons 'status
					(remove-sched-entry (to-i entry-id)))))))
(http-json "/smenu"
	; return list of staff who have given role
	(lambda (req)
		(if (staff-authorized? req)
			(let ([role (query-value req 'role)]
					[date (query-value req 'date)])
				(list
					(cons 'status #t)
					(cons 'html
						(staff-menu
							(env-person-id)
							(self-staffing? req)
							date role))))
			(list (cons 'status #f)))))
(http-json "/edit"
	; edit schedule entry
	(admin-gate "schedule" (req)
		(let ([entry-id (query-value-number req 'id)])
			(list (cons 'html (entry-edit-form entry-id))))))
(http-json "/cancel"
	; mark schedule entry canceled (worker initiated)
	(admin-gate "schedule" (req)
		(let ([entry-id (query-value-number req 'id)]
				[state (query-value-boolean req 'state)])
			(list
				(cons 'status (cancel-entry entry-id state))))))
(http-json "/admcancel"
	; mark schedule entry canceled (admin initiated)
	(admin-gate "schedule" (req)
		(let ([entry-id (query-value-number req 'id)]
				[state (query-value-boolean req 'state)])
			(list
				(cons 'status (admin-cancel-entry entry-id state))))))
(http-json "/maybe"
	; mark schedule entry tentative
	(admin-gate "schedule" (req)
		(let ([entry-id (query-value-number req 'id)]
				[state (query-value-boolean req 'state)])
			(list
				(cons 'status (set-tentative entry-id state))))))
(http-json "/stitle"
	; mark schedule entry tentative
	(admin-gate "schedule" (req)
		(let ([title (query-value req 'title)]
				[date (query-value req 'date)])
			(set-day-title date title)
			(list (cons 'status #t)))))
(http-json "/check"
	(admin-gate "schedule" (req) (list (cons 'stamp touched))))
(http-json "/workslots"
	(admin-gate "schedule" (req)
		(let ([cal-date (query-value req 'start)])
			(list
				(cons 'date cal-date)
				(cons 'slots (work-slots cal-date))))))
(add-javascript-logger "schedule")
