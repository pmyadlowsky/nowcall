#! /usr/local/bin/gusher -p 3008
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))
(use-modules (ice-9 regex))

(include "lib.scm")
(include "settings_lib.scm")

(define env-dz-full-name (setting-cache 'dz-full-name))
(define frame (make-doc 'file "frame.html"))
(define script (make-doc 'file "products.js"))
(define page-body (make-doc 'file "products.html"))
(define detail-form (make-doc 'file "product_detail.html"))
(define show-item-ids #f)
(define composite
	(make-doc (list frame page-body script)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "Products &amp; Services")
				(cons 'app "products")
				(cons 'script (fetch-doc script))
				(cons 'version nowcall-version)
				(cons 'content
					(fill-template (fetch-doc page-body) #t))))))
(define role-present?
	(let ([query
				"select role
					from product_roles
					where (product=~a)
					and (role=~a)"])
		(lambda (item-id role)
			(pg-one-row (sdb) query item-id role))))
(define apropos-roles
	(let ([query
				"select role_key.apropos_name as role,
						role_key.key as key
				from role_key, product_item
				where product_item.manifest_apropos
				and (product_item.payout=role_key.payout)
				and (product_item.id=~a)
				order by role_key.apropos_name"]
			[role-list
				"<input type=\"hidden\" id=\"itemroles~d\" value=\"~a\"/>"]
			[role-checkbox
				"<div style=\"float: left\">
				<input type=\"checkbox\" id=\"role~d_~a\"~a/>
				<span class=\"apropos-label\">~a</span></div>"])
		(lambda (item-id)
			(let ([roles
						(pg-map-rows
							(pg-exec (sdb) query item-id)
							(lambda (row)
								(cons (pg-cell row 'role)
									(pg-cell row 'key))))]
					[keys '()])
				(if (null? roles) #f
					(string-cat "\n"
						(map
							(lambda (pair)
								(set! keys (cons (cdr pair) keys))
								(format #f role-checkbox
									item-id
									(cdr pair)
									(if (role-present? item-id (cdr pair))
										" checked=\"checked\"" "")
									(car pair)))
							roles)
						(format #f role-list
							item-id (string-cat ":" keys))))))))
(define apropos-section
	(let ([html
				(deflate "<tr>
				<td class=\"edit-label\">
				<abbr title=\"as appears in load sheet 'plan' menu\">Manifest Menu:</abbr></td>
				<td><input id=\"mansym[[ITEM_ID]]\" type=\"text\"
					title=\"manifest role selector\"
					size=\"6\" value=\"[[SYMBOL]]\"/></td>
				</tr>
				<tr>
				<td class=\"edit-label\">
				<abbr title=\"manifest 'plan' menu sequence\">Menu Order:</abbr></td>
				<td><input id=\"manseq[[ITEM_ID]]\" type=\"text\"
					style=\"text-align: right\"
					title=\"manifest menu order\"
					size=\"6\" value=\"[[MANSEQ]]\"/></td>
				</tr>
				<tr>
				<td class=\"edit-label\">
				<abbr title=\"number of instructors needed\">Instructors:</abbr></td>
				<td><input type=\"text\" size=\"6\"
					style=\"text-align: right\"
					id=\"instrs[[ITEM_ID]]\"
					value=\"[[INSTRS]]\"/></td>
				</tr>
				<tr>
				<td class=\"edit-label\">
				<abbr title=\"media included in pricing\">Includes Media:</abbr></td>
				<td><input id=\"inclmedia[[ITEM_ID]]\" type=\"checkbox\"
						[[INCLMEDIA_CHECKED]]/></td>
				</tr>
				<tr>
				<td class=\"edit-label\">
				<abbr title=\"offer as an extra item for purchase in the slot's context menu (right-click)\">Offer Extras:</abbr></td>
				<td><input id=\"offerext[[ITEM_ID]]\" type=\"checkbox\"
						[[OFFEREXT_CHECKED]]/></td>
				</tr>
				<tr>
				<td class=\"edit-label\">
				<abbr title=\"this activity occupies right-seat\">Right Seat:</abbr></td>
				<td><input id=\"right-seat[[ITEM_ID]]\" type=\"checkbox\"
						[[RIGHT_SEAT_CHECKED]]/></td>
				</tr>
				<tr>
				<td class=\"edit-label\">Appropriate For:</td>
				<td>[[ROLES]]</td>
				</tr>")]
			[instr-query
				"select instr_num_req as num
					from product_item
					where (id=~a)"]
			[instr-menu-query
				"select key, apropos_name as name
					from role_key
					where instructional
					order by apropos_name asc"])
		(define (menu-opt key num name rating req)
			(format #f "<option value=\"~a_~d\"~a>~a</option>"
				key num
				(if (and (= num req) (string=? rating key))
					" selected=\"selected\"" "")
				(if (string=? key "AFFI")
					(format #f "~a x ~d" name num) name)))
		(define (menu-prep-opt row rating nreq)
			(let ([key (pg-cell row 'key)]
					[name (pg-cell row 'name)])
				(string-cat "\n"
					(menu-opt key 1 name rating nreq)
					(if (string=? key "AFFI")
						(menu-opt key 2 name rating nreq) ""))))
		(define (instructors-menu item-id)
			(let* ([profile (pg-one-row (sdb) instr-query item-id)])
				(to-s (pg-cell profile 'num))
;				(string-cat "\n"
;					"<option value=\"none\">none</option>"
;					(pg-map-rows
;						(pg-exec (sdb) instr-menu-query)
;						(lambda (row)
;							(menu-prep-opt row rating numreq)))
;					)
				)
			)
		(lambda (item-id manifest-symbol manifest-sequence
					offer-extras media-included right-seat)
			(let ([roles (apropos-roles item-id)])
				(fill-template html #f
					(cons 'symbol (to-s manifest-symbol))
					(cons 'manseq (to-s manifest-sequence))
					(cons 'item_id item-id)
					(cons 'roles (or roles ""))
					(cons 'instrs (instructors-menu item-id))
					(cons 'inclmedia_checked
						(if media-included " checked=\"checked\"" ""))
					(cons 'right_seat_checked
						(if right-seat " checked=\"checked\"" ""))
					(cons 'offerext_checked
						(if offer-extras " checked=\"checked\"" "")))))))
(define get-product-item
	(let ([query
				"select cash_price, payout, manifest_symbol,
					manifest_apropos, notes, compute_balance, sales_tax,
					acct_transfer, manifest_extra, offer_extras,
					reservable, media_included, manifest_seq,
					served_by, wage, right_seat
				from product_item
				where (id=~a)"])
		(lambda (item-id)
			(pg-one-row (sdb) query item-id))))
(define (money amount)
	(let* ([pennies (to-i amount)]
			[dollars (quotient pennies 100)]
			[cents (remainder pennies 100)])
		(if (= cents 0)
			(format #f "$~d" dollars)
			(format #f "$~d.~2,'0d" dollars cents))))
(define served-by-menu
	(let ([query
				"select product_item.id as id,
						product_item.name as pname,
						product_category.name as cname
					from product_item, product_category
					where (product_item.category=product_category.id)
					and (product_item.payout)
					order by product_category.name asc,
						product_item.name asc"])
		(define (tuple row)
			(cons
				(pg-cell row 'id)
				(format #f "~a::~a"
					(pg-cell row 'cname)
					(pg-cell row 'pname))))
		(lambda (item-id product-served-by)
			(html-select
				(format #f "served-by~d" item-id)
				'()
				product-served-by
				(cons
					(cons 0 "N/A")
					(pg-map-rows (pg-exec (sdb) query) tuple))))))
(define (gsub src pat rep)
	(regexp-substitute/global #f pat src 'pre rep 'post))
(define product-tags
	(let ([query
				"select tag
					from product_tags
					where (product=~a)
					order by tag asc"])
		(lambda (product-id)
			(pg-map-rows
				(pg-exec (sdb) query product-id)
				(lambda (row) (pg-cell row 'tag))))))
(define tag-menu
	(let ([add-link-tpt
				(deflate "<a href=\"#\"\
					title=\"add this tag to item\"
					onclick=\"add_tag('~a','~d');return false\"\
					>~a</a><img title=\"remove tag from all items\"
					onclick=\"drop_tag_all('~d','~a')\"
					src=\"/img/delete-8.png\"/>&nbsp;")]
			[new-tags-tpt
				(deflate "<div><abbr title=\"enter new tags \
					separated by spaces, tab out to add\">new</abbr>:
					<input type=\"text\"
					onchange=\"new_tags(this, '~a')\"
					id=\"new-tags~a\"/>
					</div>")]
			[query
				"select distinct tag
					from product_tags
					where (tag not in
						(select tag from product_tags where product=~a))
					order by tag asc"])
		(define (product-tags-avail product-id)
			(pg-map-rows
				(pg-exec (sdb) query product-id)
				(lambda (row) (pg-cell row 'tag))))
		(define (add-link product-id tag)
			(format #f add-link-tpt tag product-id tag product-id tag))
		(lambda (product-id)
			(string-cat " "
				"<hr/>"
				"<div>"
				(format #f new-tags-tpt product-id product-id)
				(map
					(lambda (tag) (add-link product-id tag))
					(product-tags-avail product-id)) "</div>"))))
(define set-product-tag
	(let ([check-query
				"select tag
					from product_tags
					where (product=~a)
					and (tag=~a)
					limit 1"]
			[set-query
				"insert into product_tags (product, tag, derived)
					values (~a, ~a, ~a)"]
			[tag-filter (make-regexp "[^0-9a-z-]")]
			[min-length 3])
		(define (tag-exists? product-id tag)
			(pg-one-row (sdb) check-query product-id tag))
		(lambda (product-id tag derived)
			(let ([lc-tag
					(gsub (string-downcase tag) tag-filter "")])
				(unless
					(or
						(< (string-length lc-tag) min-length)
						(tag-exists? product-id lc-tag))
					(pg-exec (sdb) set-query
						product-id lc-tag derived))))))
(define drop-product-tag
	(let ([query
				"delete from product_tags
					where (product=~a)
					and (tag=~a)"])
		(lambda (product-id tag)
			(pg-exec (sdb) query product-id tag))))
(define drop-tag-all
	; drop all instances of tag
	(let ([query "delete from product_tags where (tag=~a)"])
		(lambda (tag) (pg-exec (sdb) query tag))))
(define pretag-product
	(let ([query
				"select product_item.name as pname,
						product_category.name as cname
					from product_item, product_category
					where (product_item.id=~a)
					and (not product_item.divider)
					and (product_item.category=product_category.id)"])
		(lambda (product-id)
			(let ([row (pg-one-row (sdb) query product-id)])
				(when row
					(for-each
						(lambda (token)
							(set-product-tag product-id token #t))
						(string-tokenize
							(format #f "~a ~a"
								(pg-cell row 'cname)
								(pg-cell row 'pname)))))))))
(define pretag-products
	(let ([query
			"delete from product_tags;
			select id from product_item"]
			[empty-query
				"select count(*) as n from product_tags"])
		(define (tags-present)
			(> (pg-cell (pg-one-row (sdb) empty-query) 'n) 0))
		(lambda (force)
			(when (or (not (tags-present)) force)
				(pg-each-row
					(pg-exec (sdb) query)
					(lambda (row)
						(pretag-product (pg-cell row 'id))))))))
(define tag-set
	(let ([tag-link-tpt
				(deflate "~a<img title=\"remove tag from this item\"
					onclick=\"drop_tag(~d,'~a')\"
					src=\"/img/delete-8.png\"/>&nbsp;")]
			[add-tag-tpt
				(deflate "<img title=\"add tags to this item\"
							src=\"/img/addtag.png\" alt=\"add tag\"
							onclick=\"toggle_tag_menu('~d')\"
							/>")])
		(define (linked-tag product-id tag)
			(format #f tag-link-tpt tag product-id tag))
		(define (add-tag-link product-id)
			(format #f add-tag-tpt product-id))
		(lambda (item-id)
			(string-cat " "
				(map
					(lambda (tag) (linked-tag item-id tag))
					(product-tags item-id))
				(add-tag-link item-id)))))
(define item-edit-panel
	(let ()
		(lambda (item-id)
			(let* ([row (get-product-item item-id)]
					[manifestable (pg-cell row 'manifest_apropos)]
					[notes (pg-cell row 'notes)])
				(fill-template (fetch-doc detail-form) #f
					(cons 'item_id item-id)
					(cons 'cash_price (money (pg-cell row 'cash_price)))
					(cons 'notes (if (null? notes) "" notes))
					(cons 'payout_checked
						(if (pg-cell row 'payout)
							" checked=\"checked\"" ""))
					(cons 'wage_checked
						(if (pg-cell row 'wage)
							" checked=\"checked\"" ""))
					(cons 'acct_bal_checked
						(if (pg-cell row 'compute_balance)
							" checked=\"checked\"" ""))
					(cons 'acct_xfer_checked
						(if (pg-cell row 'acct_transfer)
							" checked=\"checked\"" ""))
					(cons 'sales_tax_checked
						(if (pg-cell row 'sales_tax)
							" checked=\"checked\"" ""))
					(cons 'served_by_menu
						(served-by-menu item-id (pg-cell row 'served_by)))
					(cons 'tagset (tag-set item-id))
					(cons 'manextra_checked
						(if (pg-cell row 'manifest_extra)
							" checked=\"checked\"" ""))
					(cons 'reservations_checked
						(if (pg-cell row 'reservable)
							" checked=\"checked\"" ""))
					(cons 'manapp_checked
						(if manifestable " checked=\"checked\"" ""))
					(cons 'apropos
						(if manifestable
							(apropos-section item-id
								(pg-cell row 'manifest_symbol)
								(pg-cell row 'manifest_seq)
								(pg-cell row 'offer_extras)
								(pg-cell row 'media_included)
								(pg-cell row 'right_seat)) "")))))))
(define (show-prices cash expense)
	(let ([icash (to-i cash)])
		(if expense
			(format #f "[~a]" (money icash))
			(money icash))))
(define item-menu
	(let ([query
				"select id, name, cash_price, payout, divider
				from product_item
				where (category=~a)
				order by seq asc"]
			[item-html
				(deflate "<li id=\"item[[CATEGORY]]_[[ITEM]]\"
					class=\"item itx\">
				<div onclick=\"open_item([[ITEM]])\"
					title=\"click to open\">
				<div id=\"itemlabel[[CATEGORY]]_[[ITEM]]\"
					class=\"item-label\">[[NAME]][[ITEM_ID]]</div>
				<div id=\"itemamt[[ITEM]]\"
					class=\"item-amt\">[[AMOUNT]]</div>
				<div style=\"clear: both\"></div>
				</div>
				<div id=\"itemedit[[ITEM]]\" style=\"display: none\">
				<div id=\"editpanel[[ITEM]]\" class=\"item-edit\"></div>
				</div>
				</li>")]
			[divider-html
				(deflate "<li id=\"item[[CATEGORY]]_[[ITEM]]\"
					class=\"divider itd\"></li>")]
			[new-item-box
				(deflate "<li class=\"item\">\
				<input type=\"text\" id=\"newitem[[CATEGORY]]\"
					style=\"margin-right: 0.5em\" size=\"40\"
					onfocus=\"mark_category([[CATEGORY]])\"
					onkeypress=\"finish_itemadd(event)\"/>
				<input type=\"button\" class=\"add-button\"
					value=\"Add Item\" onclick=\"additem([[CATEGORY]])\"/>
				 <input type=\"button\" class=\"add-button\"
					value=\"Add Divider\"
					onclick=\"adddiv([[CATEGORY]])\"/>
				<input type=\"button\" class=\"add-button\"
					value=\"Sort Items\"
					title=\"alphabetical order, within dividers\"
					onclick=\"sort_items([[CATEGORY]])\"/></li>")])
		(define (id-display item-id)
			(if show-item-ids (format #f " (#~d)" item-id) ""))
		(lambda (category)
			(string-cat "\n"
				(format #f "<ul id=\"catitems~d\" class=\"item-list\">"
					category)
				(pg-map-rows
					(pg-exec (sdb) query category)
					(lambda (row)
						(let ([item-id (pg-cell row 'id)])
							(if (pg-cell row 'divider)
								(fill-template divider-html #f
									(cons 'category category)
									(cons 'item item-id))
								(fill-template item-html #f
									(cons 'category category)
									(cons 'item item-id)
									(cons 'item_id (id-display item-id))
									(cons 'name (pg-cell row 'name))
									(cons 'amount 
										(show-prices
											(pg-cell row 'cash_price)
											(pg-cell row 'payout))))))))
				(fill-template new-item-box #f
					(cons 'category category)) "</ul>"))))
(define category-menu
	(let ([query
				"select id, name
					from product_category
					order by seq asc"]
			[new-box-html
				(deflate "<li class=\"category\">\
				<input type=\"text\" id=\"newcat\"
					style=\"margin-right: 0.5em\"\
				 size=\"40\"
				 onkeypress=\"finish_catadd(event)\"/>\
				<input type=\"button\" class=\"add-button\"\
					value=\"Add Category\" onclick=\"addcat()\"/></li>")]
			[cat-box
				(deflate "<li id=\"cat[[CATEGORY]]\" class=\"category ctx\">
				<div id=\"catlabel[[CATEGORY]]\"
					onclick=\"cat_clicked([[CATEGORY]])\">[[NAME]]</div>
				<div id=\"items[[CATEGORY]]\"
					style=\"display: none\"></div>
				</li>")])
		(lambda ()
			(string-cat "\n"
				"<ul id=\"cat_sortable\">"
				(pg-map-rows
					(pg-exec (sdb) query)
					(lambda (row)
						(fill-template cat-box #f
							(cons 'category (pg-cell row 'id))
							(cons 'name (pg-cell row 'name)))))
				new-box-html
				"</ul>"))))
(define next-object
	(let ([query
				"select coalesce(max(id),0) as mid,
						coalesce(max(seq),0) as mseq
					from ~a"])
		(lambda (table)
			(let ([row (pg-one-row (sdb) (format #f query table))])
				(cons (1+ (pg-cell row 'mid)) (1+ (pg-cell row 'mseq)))))))
(define add-category
	(let ([query
				"insert into product_category (id, name, seq)
					values (~a, ~a, ~a)"])
		(lambda (name)
			(let ([nexts (next-object "product_category")])
				(pg-exec (sdb) query (car nexts) name (cdr nexts))))))
(define add-item
	(let ([query
				"insert into product_item (id, category, name, seq, divider)
					values (~a, ~a, ~a, ~a, ~a)"])
		(lambda (name category divider)
			(let ([nexts (next-object "product_item")])
				(pg-exec (sdb) query
					(car nexts) category name (cdr nexts) divider)
				(pretag-product (car nexts))))))
(define set-cat-seq
	(let ([query "update product_category set seq=~a where (id=~a)"])
		(lambda (cat-id seq)
			(pg-exec (sdb) query seq (to-i cat-id)))))
(define set-item-seq
	(let ([query
				"update product_item set
					seq=~a
					where (id=~a)
					and (category=~a)"])
		(lambda (item-id cat-id seq)
			(pg-exec (sdb) query seq (to-i item-id) (to-i cat-id)))))
(define (strip obj) (string-trim-both (or obj "")))
(define delete-product-item
	(let ([query
				"delete from product_roles where (product=~a);
				delete from product_tags where (product=~a);
				delete from product_item where (id=~a)"])
		(lambda (item-id)
			(pg-exec (sdb) query item-id item-id item-id))))
(define rename-category
	(let ([query
				"update product_category set
					name=~a
					where (name=~a)"])
		(lambda (new-name old-name)
			(pg-exec (sdb) query new-name old-name))))
(define rename-product
	(let ([query
				"update product_item set
					name=~a
					where (id=~a)"]
			[drop-tags
				"delete from product_tags
					where (product=~a)
					and derived"])
		(lambda (new-name item-id)
			(pg-exec (sdb) drop-tags item-id)
			(pg-exec (sdb) query new-name item-id)
			(pretag-product item-id))))
(define unique-cat-name
	(let ([query
				"select name
					from product_category
					where (lower(name)=lower(~a))"])
		(lambda (name)
			(let ([row (pg-one-row (sdb) query name)]) (not row)))))
(define unique-item-name
	(let ([query
				"select name
					from product_item
					where (category=~a)
					and (lower(name)=lower(~a))"])
		(lambda (name category)
			(let ([row (pg-one-row (sdb) query category name)])
				(not row)))))
(define (cat-already-exists name)
	(list
		(cons 'status #f)
		(cons 'msg
			(format #f "Category '~a' already exists." name))))
(define (item-already-exists name)
	(list
		(cons 'status #f)
		(cons 'msg
			(format #f "Item '~a' already exists in this category." name))))
(define manifest-symbol-already-exists
	(let ([query
				"select count(*) as n
					from product_item
					where (manifest_symbol=~a)
					and (id != ~a)"])
		(lambda (symbol item-id)
			(let ([row (pg-one-row (sdb) query symbol item-id)])
				(> (pg-cell row 'n) 0)))))
(define set-apropos
	(let ([query
				"update product_item set
					manifest_apropos=~a,
					payout=~a,
					cash_price=~a
					where (id=~a)"])
		(lambda (apropos payout cash-price item-id)
			(pg-exec (sdb) query
				apropos payout cash-price
				item-id))))
(define clear-product-roles
	(let ([query
				"delete from product_roles where product=~a;
				update product_item set
					manifest_symbol=null where id=~a"])
		(lambda (item-id) (pg-exec (sdb) query item-id item-id))))
(define get-manifest-symbol
	(let ([query
				"select manifest_symbol
					from product_item
					where (id=~a)"])
		(lambda (item-id)
			(let ( [row (pg-one-row (sdb) query item-id)])
				(and row (pg-cell row 'manifest_symbol))))))
(define update-manifest-roles
	(let ([query "update load_slots set role=~a where (role=~a)"])
		(lambda (item-id new-symbol)
			(let ([old-symbol (get-manifest-symbol item-id)])
				(when old-symbol
					(pg-exec (sdb) query new-symbol old-symbol))))))
(define (zero-is-null value) (if (= value 0) '() value))
(define save-edit
	(let ([update-item
				"update product_item set
					cash_price=~a,
					payout=~a,
					notes=~a,
					manifest_symbol=~a,
					compute_balance=~a,
					sales_tax=~a,
					acct_transfer=~a,
					manifest_extra=~a,
					offer_extras=~a,
					reservable=~a,
					media_included=~a,
					manifest_seq=~a,
					served_by=~a,
					instr_num_req=~a,
					wage=~a,
					right_seat=~a
					where (id=~a)"]
			[add-role
				"insert into product_roles (product, role)
					values (~a, ~a)"])
		(lambda (item-id cash-price
					payout manifest-symbol roles notes acct-bal sales-tax
					acct-xfer manifest-extra offer-extras reservations
					media-included manifest-seq
					served-by wage instr-num-req right-seat)
			(unless (string=? manifest-symbol "")
				(update-manifest-roles item-id manifest-symbol))
			(pg-exec (sdb) update-item
				cash-price
				payout notes manifest-symbol acct-bal sales-tax
				acct-xfer manifest-extra offer-extras reservations
				media-included manifest-seq
				(zero-is-null served-by) instr-num-req
				wage right-seat item-id)
			(pg-exec (sdb)
				"delete from product_roles where product=~a" item-id)
			(for-each
				(lambda (role)
					(pg-exec (sdb) add-role item-id role)) roles))))
(define all-tags
	(let ([query
				"select distinct tag
					from product_tags
					order by tag asc"]
			[link-tpt
				"<a href=\"#\"\
					onclick=\"add_tag('~a');return false\">~a</a>"])
		(define (link tag) (format #f link-tpt tag tag))
		(lambda ()
			(string-cat " "
				"<hr/>"
				(pg-map-rows
					(pg-exec (sdb) query)
					(lambda (row) (link (pg-cell row 'tag))))))))
(define delete-category
	; delete product category if it contains no product items
	(let ([count-query
				"select count(*) as n
					from product_item
					where (category=~a)"]
			[delete "delete from product_category where (id=~a)"])
		(define (no-items? cat-id)
			(let ([row (pg-one-row (sdb) count-query cat-id)])
				(< (pg-cell row 'n) 1)))
		(lambda (cat-id)
			(and
				(no-items? cat-id)
				(pg-exec (sdb) delete cat-id) #t))))
(define sort-items
	(let ([query-items
				"select id, name
					from product_item
					where (category=~a)
					and (not divider)
					and (seq > ~a)
					and (seq < ~a)
					order by name asc"]
			[set-seq-query
				"update product_item set
					seq=~a
					where (id=~a)"]
			[query-seq
				"select seq
					from product_item
					where (category=~a)
					and divider
					order by seq asc"])
		(define (set-seq item-id seq)
			(pg-exec (sdb) set-seq-query seq item-id))
		(define (sections category)
			; make section duples from category divider positions
			(let collect ([prev 0]
					[divs
						(pg-map-rows
							(pg-exec (sdb) query-seq category)
							(lambda (row) (pg-cell row 'seq)))]
					[bag '()])
				(if (null? divs)
					(reverse (cons (cons prev 1000000) bag))
					(collect (car divs) (cdr divs) 
						(cons (cons prev (car divs)) bag)))))
		(define (sort-group category from-div to-div)
			; sort a divider-bracketed group of items
			(let ([seq (1+ from-div)])
				(pg-each-row
					(pg-exec (sdb) query-items category from-div to-div)
					(lambda (row)
						(set-seq (pg-cell row 'id) seq)
						(set! seq (1+ seq))))))
		(lambda (category)
			(for-each
				(lambda (bracket)
					(sort-group category (car bracket) (cdr bracket)))
				(sections category)))))

(log-to "/var/log/nowcall/products.log")

(pretag-products #f)

(http-html "/"
	(lambda (req)
		(if (authorized-for req "products")
			(fill-template (fetch-doc composite) #f
				(cons 'dz_full_name (env-dz-full-name))
				(cons 'admin_check (admin-light req (sdb)))
				(cons 'category_menu (category-menu)))
			(redirect-html "/login/products"))))
(http-json "/addcat"
	(admin-gate "products" (req)
		(let ([name (strip (query-value req 'newcat))])
			(if (unique-cat-name name)
				(begin
					(add-category name)
					(list
						(cons 'status #t)
						(cons 'html (category-menu))))
				(cat-already-exists name)))))
(http-json "/delcat"
	(admin-gate "products" (req)
		(let ([cat (query-value-number req 'cat)])
			(if (delete-category cat)
				(list
					(cons 'status #t)
					(cons 'html (category-menu)))
				(list (cons 'status #f))))))
(http-json "/renamecat"
	(admin-gate "products" (req)
		(let ([orig (strip (query-value req 'orig))]
				[new-name (strip (query-value req 'repl))])
			(if (unique-cat-name new-name)
				(begin
					(rename-category new-name orig)
					(list
						(cons 'status #t)
						(cons 'repl new-name)))
				(cat-already-exists new-name)))))
(http-json "/ordercats"
	(admin-gate "products" (req)
		(let ([order (or (json-decode (query-value req 'order)) '())])
			(let loop ([cats order]
					[seq 1])
				(unless (null? cats)
					(set-cat-seq (car cats) seq)
					(loop (cdr cats) (1+ seq))))
			(list (cons 'status #t)))))
(http-json "/additem"
	(admin-gate "products" (req)
		(let ([cat (query-value-number req 'category)]
				[name (strip (query-value req 'newitem))]
				[divider (query-value-boolean req 'div)])
			(if (or divider (unique-item-name name cat))
				(begin
					(add-item name cat divider)
					(list
						(cons 'status #t)
						(cons 'category cat)
						(cons 'html (item-menu cat))))
				(item-already-exists name)))))
(http-json "/ocat"
	(admin-gate "products" (req)
		(let ([cat (query-value-number req 'category)])
			(list (cons 'html (item-menu cat))))))
(http-json "/oitem"
	(admin-gate "products" (req)
		(let ([item-id (query-value-number req 'item)])
			(list
				(cons 'html (item-edit-panel item-id))))))
(http-json "/orderitems"
	(admin-gate "products" (req)
		(let ([order (or (json-decode (query-value req 'order)) '())]
				[category (query-value-number req 'category)])
			(let loop ([items order]
					[seq 1])
				(unless (null? items)
					(set-item-seq (car items) category seq)
					(loop (cdr items) (1+ seq))))
			(list (cons 'status #t)))))
(http-json "/delitem"
	(admin-gate "products" (req)
		(let ([item-id (query-value-number req 'item)]
				[cat (query-value-number req 'category)])
			(delete-product-item item-id)
			(list
				(cons 'status #t)
				(cons 'category cat)
				(cons 'html (item-menu cat))))))
(http-json "/renameitem"
	(admin-gate "products" (req)
		(let ([item-id (strip (query-value req 'item))]
				[new-name (strip (query-value req 'repl))]
				[category (query-value-number req 'category)]
				)
			(if (unique-item-name new-name category)
				(begin
					(rename-product new-name item-id)
					(list
						(cons 'status #t)
						(cons 'repl new-name)))
				(item-already-exists new-name)))))
(http-json "/edititem"
	(admin-gate "products" (req)
		(let ([item-id (query-value-number req 'item)]
				[cash (query-value-number req 'cash)]
				[payout (query-value-boolean req 'payout)]
				[wage (query-value-boolean req 'wage)]
				[acct-bal (query-value-boolean req 'acct_bal)]
				[acct-xfer (query-value-boolean req 'acct_xfer)]
				[sales-tax (query-value-boolean req 'sales_tax)]
				[manifest-extra (query-value-boolean req 'manifest_extra)]
				[manifest-seq (query-value-number req 'manifest_seq)]
				[reservations (query-value-boolean req 'reservations)]
				[offer-extras (query-value-boolean req 'offer_extras)]
				[right-seat (query-value-boolean req 'right_seat)]
				[media-included (query-value-boolean req 'media_included)]
				[manifest-sym
					(string-upcase (to-s (query-value req 'manifest)))]
				[notes (query-value req 'notes)]
				[roles (or (json-decode (query-value req 'roles)) '())]
				[served-by (query-value-number req 'served_by)]
				[instr-num-req (query-value-number req 'instr_num_req)]
				[row #f])
			(unless payout (set! wage #f))
			(if wage (set! payout #t))
			(if (manifest-symbol-already-exists manifest-sym item-id)
				(list
					(cons 'status #f)
					(cons 'mansym (get-manifest-symbol item-id))
					(cons 'item item-id)
					(cons 'msg
						(format #f "manifest symbol '~a' is already in use"
							manifest-sym)))
				(begin
					(save-edit item-id cash payout
						manifest-sym roles notes acct-bal sales-tax
						acct-xfer manifest-extra offer-extras
						reservations media-included
						manifest-seq
						served-by wage instr-num-req right-seat)
					(set! row (get-product-item item-id))
					(list
						(cons 'status #t)
						(cons 'amounts
							(show-prices
								(pg-cell row 'cash_price)
								payout))
						(cons 'item item-id)))))))
(http-json "/flipman"
	(admin-gate "products" (req)
		(let ([apropos (query-value-boolean req 'apropos)]
				[payout (query-value-boolean req 'payout)]
				[item-id (query-value-number req 'item)]
				[cash (query-value-number req 'cash)]
				)
			(set-apropos apropos payout cash item-id)
			(unless apropos (clear-product-roles item-id))
			(list
				(cons 'item_id item-id)
				(cons 'html (item-edit-panel item-id))))))
(http-json "/avtags"
	(admin-gate "products" (req)
		(let ([item-id (query-value-number req 'item)])
			(list (cons 'html (tag-menu item-id))))))
(http-json "/alltags"
	(admin-gate "products" (req)
		(list (cons 'html (all-tags)))))
(http-json "/addtags"
	(admin-gate "products" (req)
		(let ([item-id (query-value-number req 'item)]
				[tags (json-decode (query-value req 'tag))])
			(for-each
				(lambda (tag) (set-product-tag item-id tag #f)) tags)
			(list
				(cons 'menu (tag-menu item-id))
				(cons 'tags (tag-set item-id))))))
(http-json "/deltag"
	(admin-gate "products" (req)
		(let ([item-id (query-value-number req 'item)]
				[tag (query-value req 'tag)])
			(drop-product-tag item-id tag)
			(list
				(cons 'menu (tag-menu item-id))
				(cons 'tags (tag-set item-id))))))
(http-json "/dropall"
	(admin-gate "products" (req)
		(let ([item-id (query-value-number req 'item)]
				[tag (query-value req 'tag)])
			(drop-tag-all tag)
			(list
				(cons 'menu (tag-menu item-id))
				(cons 'tags (tag-set item-id))))))
(http-json "/sortitems"
	(admin-gate "products" (req)
		(let ([cat (query-value-number req 'category)])
			(sort-items cat)
			(list
				(cons 'category cat)
				(cons 'html (item-menu cat))))))
(add-javascript-logger "products")
