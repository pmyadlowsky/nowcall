#! /usr/local/bin/gusher -p 3006
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))

(include "lib.scm")
(include "settings_lib.scm")

(define frame (make-doc 'file "frame.html"))
(define script (make-doc 'file "settings.js"))
(define page-body (make-doc 'file "settings.html"))
(define (wrap-abbr visible-text title)
	(if (or (null? title) (string=? title "")) visible-text
		(format #f "<abbr title=\"~a\">~a</abbr>"
			title visible-text)))
(define make-checkbox
	(let ([html
				(deflate "<tr>
				<td align=\"right\"><input type=\"checkbox\"
					id=\"~a\"~a onclick=\"touch_chart()\"/></td>
				<td>~a</td>
				</tr>")])
		(lambda (row)
			(format #f html
				(pg-cell row 'sym_name)
				(if (string=? (to-s (pg-cell row 'value)) "t")
					" checked=\"checked\"" "")
				(wrap-abbr
					(pg-cell row 'long_name)
					(pg-cell row 'description))))))
(define make-intbox
	(let ([html
				(deflate "<tr>
				<td align=\"right\"><input type=\"text\"
					style=\"text-align: right\" size=\"3\" id=\"~a\"
					value=\"~d\"
					onkeypress=\"touch_chart()\"/></td>
				<td>~a</td>
				</tr>")])
		(lambda (row)
			(format #f html
				(pg-cell row 'sym_name)
				(to-i (pg-cell row 'value))
				(wrap-abbr
					(pg-cell row 'long_name)
					(pg-cell row 'description))))))
(define make-fltbox
	(let ([html
				(deflate "<tr>
				<td align=\"right\"><input type=\"text\"
					style=\"text-align: right\" size=\"3\" id=\"~a\"
					value=\"~d.~2,'0d\"
					onkeypress=\"touch_chart()\"/></td>
				<td>~a</td>
				</tr>")])
		(lambda (row)
			(let ([cents (to-i (+ 0.5 (* 100
					(to-f (pg-cell row 'value)))))])
				(format #f html
					(pg-cell row 'sym_name)
					(quotient cents 100)
					(remainder cents 100)
					(wrap-abbr
						(pg-cell row 'long_name)
						(pg-cell row 'description)))))))
(define make-txtbox
	(let ([html
				(deflate "<tr>
				<td align=\"right\"><input type=\"text\" id=\"~a\"
					value=\"~a\" onkeypress=\"touch_chart()\"></td>
				<td>~a</td>
				</tr>")])
		(lambda (row)
			(format #f html
				(pg-cell row 'sym_name)
				(to-s (pg-cell row 'value))
				(wrap-abbr
					(pg-cell row 'long_name)
					(pg-cell row 'description))))))
(define (settings-menu id default items)
	(html-select
		id
		(list
			"class=\"settigns-menu\""
			"style=\"text-align: center\""
			"onchange=\"touch_chart()\"") default items))
(define ip-reset-button "\
<tr><td align=\"right\">
<input type=\"button\" value=\"Reset\"[[DISABLED]]
	title=\"mark this workstation's address as DZ's\"
	onclick=\"reset_ip_addr()\"/>
</td><td><abbr title=\"as it appears to NowCall server (click 'Reset' from a DZ computer)\">DZ Network Address</abbr>: <b><span id=\"ip-addr\">[[IP_ADDR]]</span></b></td></tr>")
(define (get-dz-ip-addr)
	(let* ([addr (get-setting 'dz-ip-addr (sdb))])
		(if (= (to-i addr) 0) "0.0.0.0" addr)))
(define settings-chart
	(let ([query
				"select sym_name, long_name, value, seq,
						data_type, description
					from settings
					where (sym_name != 'date-format')
					and (not internal)~a
					and (sym_name != 'time-format')
					and (sym_name != 'name-order')
					and (sym_name != 'waiver-valid')
					and (sym_name != 'default-card-payment') -- deprecate
					and (sym_name != 'online-payment') -- deprecate
					order by seq asc"]
			[read-only
				(deflate "(<abbr
					title=\"lock this session for
					read-write access\">read-only</abbr>)")])
		(define (edited-query query req)
			(format #f query
				(if (admin-session? req) "" " and (not secret)")))
		(lambda (req)
			(let ([res (pg-exec (sdb) (edited-query query req))])
				(string-cat "\n"
					(if (admin-session? req) "" read-only)
					"<table>"
					(pg-map-rows res
						(lambda (row)
							(let ([dtype (pg-cell row 'data_type)])
								(cond
									[(string=? dtype "b")
										(make-checkbox row)]
									[(string=? dtype "i")
										(make-intbox row)]
									[(string=? dtype "f")
										(make-fltbox row)]
									[else (make-txtbox row)]))))
					"<tr><td align=\"right\">"
					(settings-menu "waiver-valid"
						(get-setting 'waiver-valid (sdb))
						(list
							(cons "this-year" "this year only")
							(cons "one-year" "one year from signing")))
					"</td><td>Waiver Validity</td></tr>"
					"<tr><td align=\"right\">"
					(settings-menu "date-format"
						(get-setting 'date-format (sdb))
						(list
							(cons "m/d/yy" "m/d/yyyy")
							(cons "yy-mm-dd" "yyyy-mm-dd")))
					"</td><td>Date Format</td></tr>"
					"<tr><td align=\"right\">"
					(settings-menu "time-format"
						(get-setting 'time-format (sdb))
						(list
							(cons "12" "12-hour")
							(cons "24" "24-hour")))
					"</td><td>Time Format</td></tr>"
					"<tr><td align=\"right\">"
					(settings-menu "name-order"
						(get-setting 'name-order (sdb))
						(list
							(cons "first-last" "first last")
							(cons "last-first" "last, first")))
					"</td><td>Person Name Order</td></tr>"
					(fill-template ip-reset-button #t
						(cons 'ip_addr (get-dz-ip-addr))
						(cons 'disabled
							;(if (admin-user req)
								""
							;	" disabled=\"disabled\"")
							)
						)
					"</table>")))))
(define composite
	(make-doc (list frame script page-body)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "Settings")
				(cons 'app "settings")
				(cons 'script (fetch-doc script))
				(cons 'version nowcall-version)
				(cons 'content
					(fill-template (fetch-doc page-body) #t))))))
(define (validate-min-sheets n)
	(let ([sheets (max n 2)])
		(if (even? sheets) sheets (1+ sheets))))

(log-to "/var/log/nowcall/settings.log")

(http-html "/"
	(lambda (req)
		(if (authorized-for req "settings")
			(fill-template (fetch-doc composite) #f
				(cons 'dz_full_name (get-setting 'dz-full-name (sdb)))
				(cons 'admin_check (admin-light req (sdb))))
			(redirect-html "/login/settings"))))
(http-json "/chart"
	; display settings chart
	(admin-gate "settings" (req)
		(list
			(cons 'html (settings-chart req))
			(cons 'admin (admin-session? req)))))
(http-json "/update"
	; update settings
	(admin-gate "settings" (req)
		(let ([ground-rod (query-value-boolean req 'ground_rod)]
				[facebook-id (query-value req 'facebook_id)]
				[pub-sheets (query-value-boolean req 'pub_sheets)]
				[pub-manifest (query-value-boolean req 'pub_manifest)]
				[full-name (query-value req 'full_name)]
				[short-name (query-value req 'short_name)]
				[email-addr (query-value req 'email)]
				[bcc-addrs (query-value req 'bcc)]
				[min-sheets (query-value-number req 'min_sheets)]
				[search-chars (query-value-number req 'search_chars)]
				[load-sheet-cols (query-value-number req 'sheet_cols)]
				[manifast-retain (query-value-number req 'manifast_retain)]
				[date-format (query-value req 'date_format)]
				[time-format (query-value req 'time_format)]
				[clock-interval (query-value req 'clock_interval)]
				[name-order (query-value req 'name_order)]
				[waiver-valid (query-value req 'waiver_valid)]
				[repack-cycle (query-value-number req 'repack_cycle)]
				[stripe-public-key (query-value req 'stripe_pubkey)]
				[stripe-private-key (query-value req 'stripe_privkey)]
				[db-master (query-value req 'db_master)]
				[sales-tax (query-value req 'sales_tax)]
				[patron-self-pay (query-value-boolean req 'patron_pay)]
				[credit-card-rate (query-value req 'credit_rate)]
				[credit-card-flat (query-value req 'credit_flat)])
			(when (admin-session? req)
				(set-setting 'db-master db-master (sdb))
				(set-setting 'ground-reserve-out-of-date ground-rod (sdb))
				(set-setting 'facebook-id facebook-id (sdb))
				(set-setting 'pubsheets pub-sheets (sdb))
				(set-setting 'min-load-sheets
					(validate-min-sheets min-sheets) (sdb))
				(set-setting 'min-search-chars (max search-chars 2) (sdb))
				(set-setting 'load-sheet-columns
					(max load-sheet-cols 1) (sdb))
				(set-setting 'manifast-retention
					(max manifast-retain 1) (sdb))
				(set-setting 'sales-tax sales-tax (sdb))
				(set-setting 'credit-card-rate credit-card-rate (sdb))
				(set-setting 'credit-card-flat credit-card-flat (sdb))
				(set-setting 'date-format date-format (sdb))
				(set-setting 'time-format time-format (sdb))
				(set-setting 'clock-interval clock-interval (sdb))
				(set-setting 'name-order name-order (sdb))
				(set-setting 'waiver-valid waiver-valid (sdb))
				(set-setting 'patron-self-pay patron-self-pay (sdb))
				(set-setting 'repack-cycle repack-cycle (sdb))
				(set-setting 'dz-full-name full-name (sdb))
				(set-setting 'dz-short-name short-name (sdb))
				(set-setting 'dz-email-addr email-addr (sdb))
				(set-setting 'dz-copy-to-addrs bcc-addrs (sdb))
				(set-setting 'stripe-public-key stripe-public-key (sdb))
				(set-setting 'stripe-private-key stripe-private-key (sdb)))
			(list (cons 'status (admin-session? req))))))
(add-javascript-logger "settings")
