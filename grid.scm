(define (grid-tag-pairs query dbh)
	; generate a list of symbol-label-index tuples
	; maps SQL columns to grid indices and to grid labels
	(let* ([res (pg-exec dbh query)]
			[pairs
				(pg-map-rows res
					(lambda (row)
						(cons
							(string->symbol (to-s (pg-cell row 'symbol)))
							(pg-cell row 'label))))])
		(let loop ([i 0]
				[queue pairs]
				[bag '()])
			(if (null? queue) (reverse bag)
				(loop (1+ i)
					(cdr queue)
					(cons
						(cons (caar queue) (cons i (cdar queue))) bag))))))
(define  (grid-sum-row grid row cols)
	; sum one row of a grid, put result in last column
	(let loop ([sum 0]
			[col 0])
		(if (= col cols) sum
			(loop (+ sum (assq-ref (array-ref grid row col) 'count))
				(1+ col)))))
(define (grid-sum-rows grid)
	(let* ([dims (array-dimensions grid)]
			[rows (car dims)]
			[cols (1- (cadr dims))])
		(let loop ([i 0])
			(when (< i rows)
				(array-set! grid
					(list (cons 'count (grid-sum-row grid i cols)))
					i cols)
				(loop (1+ i))))))
(define  (grid-sum-col grid col rows)
	; sum one column of a grid, put result in last row
	(let loop ([sum 0]
			[row 0])
		(if (= row rows) sum
			(loop (+ sum (assq-ref (array-ref grid row col) 'count))
				(1+ row)))))
(define (grid-sum-cols grid)
	(let* ([dims (array-dimensions grid)]
			[rows (1- (car dims))]
			[cols (cadr dims)])
		(let loop ([i 0])
			(when (< i cols)
				(array-set! grid
					(list (cons 'count (grid-sum-col grid i rows)))
					rows i)
				(loop (1+ i))))))
(define (add-sum-slot? n) (if (= n 1) 1 (1+ n)))
(define (grid-make row-set col-set cell-query dbh)
	; scan grid of integers, computing and storing row/col sums
	(let* ([res (pg-exec dbh cell-query)]
			[grid
				(make-array (list (cons 'count 0))
					(add-sum-slot? (length row-set))
					(add-sum-slot? (length col-set)))])
		(pg-each-row res
			(lambda (row)
				(array-set! grid
					(list
						(cons 'count (pg-cell row 'n))
						(cons 'extra (pg-cell row 'extra)))
					(car (assq-ref row-set
						(string->symbol (to-s (pg-cell row 'row)))))
					(car (assq-ref col-set
						(string->symbol (to-s (pg-cell row 'col))))))))
		(when (> (length col-set) 1) (grid-sum-rows grid))
		(when (> (length row-set) 1) (grid-sum-cols grid))
		grid))
(define (grid-table-header col-set)
	; column headers, with first cell empty to accommodate row labels
	(string-cat ""
		"<tr><th class=\"grid_header_label\">&nbsp;</th>"
		(map
			(lambda (tag-pair)
				(format #f "<th class=\"grid_header_label\">~a</th>"
					(cddr tag-pair))) col-set)
		"<th class=\"grid_header_label\">+</th></tr>"))
(define (grid-row grid row)
	; extract one row of a grid
	(let ([cols (cadr (array-dimensions grid))])
		(let loop ([i 0]
				[bag '()])
			(if (= i cols) (reverse bag)
				(loop (1+ i)
					(cons (array-ref grid row i) bag))))))
(define (grid-table-row grid row tag-pair compose)
	; label in first cell, followed by numbers
	(string-cat ""
		"<tr>"
		(format #f "<td class=\"grid_row_label\">~a</td>" (cddr tag-pair))
		(map
			(lambda (tuple)
				(format #f "<td class=\"grid_data_cell\">~a</td>"
					(if compose
						(compose
							(assq-ref tuple 'count)
							(assq-ref tuple 'extra))
						(to-s (assq-ref tuple 'count)))))
			(grid-row grid row))
		"</tr>"))
(define (grid-table grid row-set col-set compose)
	(string-cat "\n"
		"<table class=\"grid_table\">"
		(grid-table-header col-set)
		(let loop ([row 0]
				[queue row-set]
				[bag '()])
			(if (null? queue) (reverse bag)
				(loop
					(1+ row)
					(cdr queue)
					(cons (grid-table-row grid row (car queue) compose)
						bag))))
		"</table>"))
(define (grid-html row-query col-query cell-query compose dbh)
	(let* ([col-labels (grid-tag-pairs col-query dbh)]
				[row-labels (grid-tag-pairs row-query dbh)]
				[grid (grid-make row-labels col-labels cell-query dbh)]
				)
		(grid-table grid
			(if (= (length row-labels) 1) row-labels
				(append row-labels
					(list (cons 'totals (cons 0 "+")))))
			col-labels compose)))
