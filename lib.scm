; common, shared tools

(use-modules (ice-9 format))
(use-modules (ice-9 regex))

(include "sessions.scm")
(include "settings_lib.scm")
(include "database.scm")

(define-syntax admin-gate
	; require user authorization to
	; access application in question
	(syntax-rules ()
		((new-admin-gate app args exp ...)
			(lambda (req)
				(if (authorized-for req app)
					((lambda args exp ...) req)
					(list (cons 'status #f)))))))
(define deflate
	; wring out spans of whitespace
	(let ([pat (make-regexp "[ \t][ \t]+")])
		(lambda (src)
			(regexp-substitute/global #f pat
				(string-trim-both src) 'pre " " 'post))))
(define person-name
	; display person name according to specified ordering
	(let ([query
				"select last_name, coalesce(goes_by, first_name) as fname
					from people
					where (id=~a)"])
		(lambda (id order dbh)
			(let* ([row (pg-one-row dbh query id)]
					[last-name (pg-cell row 'last_name)]
					[first-name (pg-cell row 'fname)])
				(cond
					[(eq? order 'default)
						(format #f "~a, ~a" last-name first-name)]
					[(string=? order "first-last")
						(format #f "~a ~a" first-name last-name)]
					[else (format #f "~a, ~a" last-name first-name)])))))
(define schedulable-person?
	; schedulable staff
	(let ([query
				"select people.id as id
					from people, roles
					where (people.id=~a)
					and (roles.who=people.id)
					and (roles.role in
						(select key from role_key where schedulable))"])
		(lambda (person-id dbh) (pg-one-row dbh query person-id))))
(define mobile-user?
	(let ([mob-pat (make-regexp "Mobile")])
		(lambda (req)
			(regexp-exec mob-pat (to-s (assq-ref req 'user-agent))))))
(define unique-strings
	; remove duplicates from list of strings
	(let ()
		(define (string-member? str strs)
			(cond
				[(null? strs) #f]
				[(string=? str (car strs)) #t]
				[else (string-member? str (cdr strs))]))
		(lambda (strings)
			(let loop ([urn strings]
					[bag '()])
				(cond
					[(null? urn) (reverse bag)]
					[(string-member? (car urn) bag) (loop (cdr urn) bag)]
					[else (loop (cdr urn) (cons (car urn) bag))])))))
(define user-authenticate
	; authenticate user
	; if successful, add authorization set to user session
	(let ([authenticate-query
				"select id
					from people
					where (login_name=~a)
					and (login_password=~a)"])
		(lambda (username password req dbh)
			(let ([row
						(pg-one-row dbh authenticate-query
							(to-s username)
							(sha-256-sum (to-s password)))])
				(and row
					(session-set req 'person-id (pg-cell row 'id))
					(session-set req 'proxy-person-id 0)
					(session-set req 'admin-session #f) #t)))))
(define user-authorizations
	(let ([authorization-query
				"select app
					from app_auth
					where person_id=~a"]
			[man-query
				"select who
					from roles
					where (who=~a)
					and (role='MANFST')
					limit 1"])
		(define (manifest? person-id dbh)
			(let ([row (pg-one-row dbh man-query person-id)])
				(and row (pg-cell row 'who))))
		(define (auth-set person-id dbh)
			(unique-strings
				(append
					(pg-map-rows
						(pg-exec dbh authorization-query person-id)
						(lambda (row) (pg-cell row 'app)))
					(if (schedulable-person? person-id dbh)
						(list "staff_sched") '())
					(if (manifest? person-id dbh)
						(list "reservations") '()))))
		(lambda (req)
			(let ([person-id (session-get req 'person-id)])
				(if person-id (auth-set person-id (sdb)) '())))))
(define (admin-session? req) (session-get req 'admin-session))
(define (logged-in req) (session-get req 'person-id))
(define env-dz-ip-addr (setting-cache 'dz-ip-addr))
(define env-on-dz-lan
	(request-cache
		(lambda (http-req)
			(string=?
				(env-dz-ip-addr)
				(or (assq-ref http-req 'x-forwarded-for) "none")))))
(define (can-work-remotely? req)
	(let scan ([auth-set (user-authorizations req)])
		(cond
			[(null? auth-set) #f]
			[(string=? (car auth-set) "work_remote") #t]
			[else (scan (cdr auth-set))])))
(define (authorized-for req app)
	; check user authorization for app of interest
	(let scan ([auth-set (user-authorizations req)])
		(cond
			[(null? auth-set) #f]
			[(string=? (car auth-set) "admin") #t]
			[(and (string=? (car auth-set) app)
				(or (env-on-dz-lan) (can-work-remotely? req))) #t]
			[else (scan (cdr auth-set))])))
(define (admin-user req) (authorized-for req "admin"))
(define redirect-html
	; answer request with redirect, e.g. to login page
	(let ([html
				(deflate
				"<!DOCTYPE html>
				<html lang=\"en\">
				<head>
				<title>Redirecting...</title>
				<meta http-equiv=\"refresh\" content=\"0; url=~a\"/>
				</head>
				<body>
				</body>
				</html>")])
		(lambda (url) (format #f html url))))
(define security-tag
	(let ([secured-img
				(deflate "<img alt=\"secured\" src=\"/img/lock-16.png\"
					onclick=\"Security.secure_session(false)\"
					title=\"secured session, full admin privileges
						click to downgrade\"/>")]
			[unsecured-img
				(deflate "<img alt=\"unsecured\" src=\"/img/unlock-16.png\"
					onclick=\"Security.secure_session(true)\"
					title=\"unsecured session, limited privileges
						click to upgrade\"/>")])
		(lambda (req)
			(if (admin-session? req)
				secured-img unsecured-img))))
(define env-person-id
	(request-cache
		(lambda (http-req) (session-get http-req 'person-id))))
(define admin-light
	; display logout button and user greeting
	(let ([html
				(deflate
				"<div class=\"admin-light\"
					style=\"float: left; width: 6em; margin-left: 0.3em\"
					onclick=\"logout()\">SIGN OUT</div>
				<div style=\"float: left; font-size: 110%;
					margin-left: 0.3em; padding-top: 3px\">~a</div>")]
			[span-wrapper "<span id=\"security-tag\">~a</span>"]
			[on-dz
				" <span style=\"font-weight: bold; color: green\"><abbr
				title=\"this workstation seems to be on DZ premises\">@DZ</abbr></span>"]
			[name-query
				"select coalesce(goes_by, first_name) as name
					from people
					where (id=~a)"])
		(define (first-name id dbh)
			(let ([row (pg-one-row dbh name-query id)])
				(if row (pg-cell row 'name) "...wait, who are you?")))
		(lambda (req dbh)
			(let ([person (session-get req 'person-id)])
				(format #f html
					(if person
						(format #f "Hello, ~a~a ~a"
							(first-name (to-i person) dbh)
							(if (env-on-dz-lan) on-dz "")
							(if (admin-user req)
								(format #f span-wrapper (security-tag req))
								"")) ""))))))
(define parse-date
	(let ([date-pat (make-regexp "^([0-9]+)-([0-9]+)-([0-9]+)$")])
		(lambda (date)
			(let ([match (regexp-exec date-pat date)])
				(time-local
					(to-i (match:substring match 1))
					(to-i (match:substring match 2))
					(to-i (match:substring match 3)) 12 0 0)))))
(define (show-date date format)
	; display date with specified format
	(cond
		[(or (not date) (null? date)) ""]
		[(string=? format "m/d/yy") (time-format date "%m/%d/%Y")]
		[else (time-format date "%Y-%m-%d")]))
(define wall-clock-time
	; format wall-clock time according to DZ's time format pref
	(let ([pat (make-regexp "^([0-9]+):([0-9]+)")])
		(lambda (time-slot time-notation)
			(if (string? time-slot)
				(let ([match (regexp-exec pat time-slot)])
					(if (string=? time-notation "12")
						(let ([hour (to-i (match:substring match 1))])
							(format #f "~d:~a~a"
								(if (> hour 12) (- hour 12) hour)
								(match:substring match 2)
								(if (>= hour 12) "pm" "am")))
						(format #f "~a:~a"
							(match:substring match 1)
							(match:substring match 2))))
				(time-format time-slot ; assume timestamp object
					(if (string=? time-notation "12")
						"%I:%M%P" "%H:%M"))))))
(define normalize-date
	(let ([mdy-pat (make-regexp "([0-9]+)/([0-9]+)/([0-9]+)")]
			[ymd-pat (make-regexp "([0-9]+)\\-([0-9]+)\\-([0-9]+)")])
		(define (check-mdy date)
			(let ([match (regexp-exec mdy-pat date)])
				(and
					match
					(list
						(to-i (match:substring match 3))
						(to-i (match:substring match 1))
						(to-i (match:substring match 2))))))
		(define (check-ymd date)
			(let ([match (regexp-exec ymd-pat date)])
				(and
					match
					(list
						(to-i (match:substring match 1))
						(to-i (match:substring match 2))
						(to-i (match:substring match 3))))))
		(define (resolve-year year)
			(let* ([now (time-now)]
					[century (* (quotient (time-year now) 100) 100)]
					[this-year (remainder (time-year now) 100)])
				(cond
					[(> year 100) year]
					[(<= year (+ this-year 10)) (+ year century)]
					[else (+ year (- century 100))])))
		(lambda (date)
			(let ([y-m-d
						(or
							(check-mdy date)
							(check-ymd date))])
				(and y-m-d
					(format #f "~d-~2,'0d-~2,'0d"
						(resolve-year (car y-m-d))
						(cadr y-m-d) (caddr y-m-d)))))))
(define html-select
	(let ()
		(define (join-handlers handlers)
			(if (null? handlers) ""
				(format #f " ~a" (string-cat " " handlers))))
		(define (make-option option pre-select)
			(format #f "<option value=\"~a\"~a>~a</option>"
				(car option)
				(cond
					[(null? pre-select) ""]
					[(string=? (to-s (car option)) (to-s pre-select))
						" selected=\"selected\""]
					[else ""])
				(cdr option)))
		(lambda (id handlers pre-select options)
			(string-cat "\n"
				(format #f "<select id=\"~a\"~a>"
					id
					(join-handlers
						(if (string? handlers) (list handlers) handlers)))
				(map (lambda (option) (make-option option pre-select))
					options)
				"</select>"))))
(define (html-ul items)
	(if (null? items) ""
		(format #f "<ul><li>~a</li></ul>"
			(string-cat "</li><li>" items))))
(define load-javascript
	(let ()
		(define (src path)
			(format #f "<script src=\"~a\"></script>" path))
		(lambda (script-paths)
			(string-cat "\n"
				(map src
					(if (string? script-paths)
						(list script-paths) script-paths))))))
(define add-javascript-logger
	; add JSON handler for logging client-side javascript errors
	(let ()
		(define (logger req)
			(let ([msg (query-value req 'msg)]
					[url (query-value req 'url)]
					[line (query-value-number req 'line)]
					[stack (or (query-value req 'stack) "none")])
				(log-msg "JSERROR: ~a @ ~a:~d stack: ~a" msg url line stack)
				(list (cons 'status #t))))
		(lambda (app)
			(http-json "/jserr"
				(if app
					(admin-gate app (req) (logger req)) logger)))))
(define nowcall-version "v1.19.2")
(define observer-slot -1)
