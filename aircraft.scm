#! /usr/local/bin/gusher -p 3001
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 regex))
(use-modules (ice-9 format))

(include "lib.scm")
(include "settings_lib.scm")

(define env-dz-full-name (setting-cache 'dz-full-name))

(define frame (make-doc 'file "frame.html"))
(define script (make-doc 'file "aircraft.js"))
(define page-body (make-doc 'file "aircraft.html"))
(define default-tail-num "TAILNUM")

(define aircraft-detail-tpt
"<div class=\"detcell\">
<table><tr>
<td valign=\"top\">
<table>
<tr><td class=\"label\">Make:</td>
<td><input type=\"text\" class=\"detinp\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"make[[PID]]\" value=\"[[MAKE]]\"/></td></tr>
<tr><td class=\"label\">Model:</td>
<td><input type=\"text\" class=\"detinp\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"model[[PID]]\" value=\"[[MODEL]]\"/></td></tr>
<tr><td class=\"label\">Tail Num:</td>
<td>
<div style=\"float: left\">
<input type=\"text\" class=\"detinp\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"tail_num[[PID]]\" value=\"[[TAIL_NUM]]\"/>
</div>
<div style=\"float:left; margin-top: 4px\">
<a href=\"http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=[[TAIL_NUM]]\" style=\"outline: none\" title=\"check FAA registry\" target=\"_blank\"><img src=\"/img/faa-24.png\" alt=\"FAA registry\"/></a>
</div>
</td></tr>
<tr><td class=\"label\">Name:</td>
<td><input type=\"text\" class=\"detinp\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"nickname[[PID]]\" value=\"[[NICKNAME]]\"/></td></tr>
<tr><td class=\"label\"><abbr title=\"does not occupy jump slot\">Observer<br/>Slot</abbr>:</td>
<td><input type=\"checkbox\"
	onclick=\"enable_save('[[PID]]')\"
	id=\"observer[[PID]]\"[[OBSERVER_CHECKED]]\"/></td></tr>
</table></td>
<td valign=\"top\">
<table>
<tr><td class=\"label\"><abbr
	title=\"max capacity, not including pilot\">Passengers</abbr>:</td>
<td><input type=\"text\" class=\"detinpn\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"capacity[[PID]]\" value=\"[[CAPACITY]]\"/></td></tr>
<tr><td class=\"label\"><abbr title=\"manifest tab order\">Tab Order</abbr>:</td>
<td><input type=\"text\" class=\"detinpn\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"rank[[PID]]\" value=\"[[RANK]]\"/></td></tr>
<tr><td class=\"label\"><abbr title=\"after refueling\">Max Fuel</abbr>:</td>
<td><input type=\"text\" class=\"detinpn\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"fuel_capacity[[PID]]\" value=\"[[FUEL_CAPACITY]]\"/>gal</td></tr>
<tr><td class=\"label\">Fuel Burn:</td>
<td><input type=\"text\" class=\"detinpn\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"fuel_burn[[PID]]\" value=\"[[FUEL_BURN]]\"/>gal/load</td></tr>
<tr><td class=\"label\">Fuel Type:</td>
<td>
[[FUEL_TYPE_MENU]]
</td></tr>
</table></td>
<td valign=\"top\">
<table>
<tr><td class=\"label\">Empty Weight:</td>
<td><input type=\"text\" class=\"detinpn\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"empty_weight[[PID]]\" value=\"[[EMPTY_WEIGHT]]\"/>lb</td></tr>
<tr><td class=\"label\">Basic Arm:</td>
<td><input type=\"text\" class=\"detinpn\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"basic_arm[[PID]]\" value=\"[[BASIC_ARM]]\"/>in</td></tr>
<tr><td class=\"label\">Fuel Arm:</td>
<td><input type=\"text\" class=\"detinpn\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"fuel_arm[[PID]]\" value=\"[[FUEL_ARM]]\"/>in</td></tr>
<tr><td class=\"label\">Pilot Arm:</td>
<td><input type=\"text\" class=\"detinpn\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"pilot_arm[[PID]]\" value=\"[[PILOT_ARM]]\"/>in</td></tr>
<tr><td class=\"label\"><abbr title=\"weighted for typical load distribution\">Avg Payload Arm</abbr>:</td>
<td><input type=\"text\" class=\"detinpn\"
	onkeypress=\"enable_save('[[PID]]')\"
	id=\"avg_payload_arm[[PID]]\"
	value=\"[[AVG_PAYLOAD_ARM]]\"/>in</td></tr>
</table></td>
</tr>
<tr><td colspan=\"3\">
<table><tr>
<td class=\"label\" valign=\"middle\">Notes:</td>
<td><textarea rows=\"2\" cols=\"50\"
	onchange=\"enable_save('[[PID]]')\"
	id=\"notes[[PID]]\">[[NOTES]]</textarea></td>
</tr></table>
</td></tr>
</table>
<input type=\"button\" value=\"Delete\"
	title=\"delete this aircraft\"
	class=\"btn rounded-corners-5\"[[DELETE_DISABLE]]
	onclick=\"aircraft_predelete('[[PID]]')\"/>
<input type=\"button\" value=\"Save\"
	id=\"save[[PID]]\" class=\"btn rounded-corners-5\"
	onclick=\"aircraft_save('[[PID]]')\"/>
<input type=\"button\" value=\"Cancel\"
	class=\"btn rounded-corners-5\"
	onclick=\"toggle_detail('[[PID]]')\"/>
</div>")

(define fuel-type-menu
	(let ([query
				"select symbol, name
					from fuel_type
					order by seq asc"])
		(lambda (aircraft-id preset)
			(html-select
				(format #f "fuel_type~d" aircraft-id)
				(format #f "onchange=\"enable_save('~d')\"" aircraft-id)
				preset
				(cons
					(cons "" "Select")
					(pg-map-rows (pg-exec (sdb) query)
						(lambda (row)
							(cons
								(pg-cell row 'symbol)
								(pg-cell row 'name)))))))))
(define aircraft-detail-panel
	(let ([query
				"select make, model, tail_num, nickname, capacity,
						fuel_cycle, active, id, rank, notes,
						fuel_capacity, fuel_burn, fuel_type,
						empty_weight, basic_arm, fuel_arm, pilot_arm,
						avg_payload_arm, observer_slot
					from aircraft
					where (id=~a)"])
		(define (t-set row column) (cons column (pg-cell row column)))
		(lambda (aircraft-id)
			(let ([row (pg-one-row (sdb) query aircraft-id)])
				(fill-template aircraft-detail-tpt #f
					(cons 'pid aircraft-id)
					(t-set row 'make)
					(t-set row 'model)
					(t-set row 'tail_num)
					(t-set row 'nickname)
					(cons 'observer_checked
						(if (pg-cell row 'observer_slot)
							" checked=\"checked\"" ""))
					(t-set row 'capacity)
					(t-set row 'rank)
					(t-set row 'fuel_capacity)
					(cons 'fuel_type_menu
						(fuel-type-menu aircraft-id
							(to-s (pg-cell row 'fuel_type))))
					(t-set row 'fuel_burn)
					(t-set row 'empty_weight)
					(t-set row 'basic_arm)
					(t-set row 'fuel_arm)
					(t-set row 'pilot_arm)
					(t-set row 'avg_payload_arm)
					(t-set row 'notes)
					(cons 'delete_disable
						(if (is-referenced aircraft-id)
							" disabled=\"disabled\"" "")))))))
(define chart-row-tpt "\
<tr>
<td title=\"aircraft detail\" onclick=\"toggle_detail('[[PID]]')\"><img src=\"/img/folder-24.png\" alt=\"detail\"/></td>
<td class=\"bcell\" id=\"ch-[[PID]]-nickname\">[[NICKNAME]]</td>
<td class=\"bcell\" id=\"ch-[[PID]]-make\">[[MAKE]]</td>
<td class=\"bcell\" id=\"ch-[[PID]]-model\">[[MODEL]]</td>
<td class=\"bcell\" id=\"ch-[[PID]]-tail_num\">[[TAIL_NUM]]</td>
<td class=\"bncell\" id=\"ch-[[PID]]-capacity\">[[CAPACITY]]</td>
<td class=\"bncell\" id=\"ch-[[PID]]-fuel_cycle\">[[FUEL_CYCLE]]</td>
<td class=\"bncell\" id=\"ch-[[PID]]-rank\">[[RANK]]</td>
<td class=\"bcell\" style=\"text-align: center\">
<input type=\"checkbox\" id=\"active[[{PID]]\"[[ACTIVE_CHECKED]]
	onclick=\"toggle_active('[[PID]]',this)\"/>
</td>
</tr>
<tr id=\"drow[[PID]]\" style=\"display: none\"><td></td>
<td id=\"detail[[PID]]\" colspan=\"8\"></td></tr>")

(define chart-row
	(let ()
		(define (t-set row column) (cons column (pg-cell row column)))
		(lambda (row)
			(let ([id (pg-cell row 'id)])
				(fill-template chart-row-tpt #f
					(cons 'pid id)
					(t-set row 'nickname)
					(t-set row 'make)
					(t-set row 'model)
					(t-set row 'tail_num)
					(t-set row 'capacity)
					(t-set row 'fuel_cycle)
					(t-set row 'rank)
					(cons 'active_checked
						(if (pg-cell row 'active)
							" checked=\"checked\"" "")))))))
(define (active-only req) (query-value-boolean req 'active))
(define aircraft-list
	(let ([query "select id from aircraft~a"])
		(lambda (active-only)
			(pg-map-rows
				(pg-exec (sdb)
					(format #f query
						(if active-only " where active" "")))
				(lambda (row) (pg-cell row 'id))))))
(define aircraft-load-ids
	(let ([query
				"select id
					from load_sheets
					where (aircraft_id=~a)"])
		(lambda (aircraft-id)
			(pg-map-rows (pg-exec (sdb) query aircraft-id)
				(lambda (row) (pg-cell row 'id))))))
(define is-referenced
	; Does this aircraft have non-empty load sheets?
	(let ([occ-query
				"select count(*) as n
					from load_slots
					where (load_id=~a)
					and (passenger is not null)"])
		(define (occupied-sheet load-id)
			(let ([row (pg-one-row (sdb) occ-query load-id)])
				(> (pg-cell row 'n) 0)))
		(lambda (aircraft-id)
			(let ([load-ids (aircraft-load-ids aircraft-id)])
				(and
					(not (null? load-ids))
					(let scan ([ids load-ids])
						(cond
							[(null? ids) #f]
							[(occupied-sheet (car ids)) #t]
							[else (scan (cdr ids))])))))))
(define delete-aircraft
	(let ([del-sheet
				"delete from load_slots where (load_id=~a);
					delete from load_sheets where (id=~a);"]
			[del-aircraft "delete from aircraft where id=~a"])
		(lambda (aircraft-id)
			(if (is-referenced aircraft-id) #f
				(begin
					(let delete ([load-ids
							(aircraft-load-ids aircraft-id)])
						(unless (null? load-ids)
							(pg-exec (sdb) del-sheet
								(car load-ids) (car load-ids))
							(delete (cdr load-ids))))
					(pg-exec (sdb) del-aircraft aircraft-id) #t)))))
(define aircraft-catalog
	(let ([query
				"select make, model, tail_num, nickname, capacity,
						fuel_cycle, active, id, rank
					from aircraft~a
					order by nickname asc"]
			[no-aircraft
				(deflate "<tr>
				<td class=\"bcell\"
					style=\"text-align: center\" colspan=\"9\">You don't have any aircraft defined, or all are inactive.</td>
				</tr>")])
		(lambda (active-only)
			(let* ([res
						(pg-exec (sdb)
							(format #f query
								(if active-only " where active" "")))])
				(if (< (pg-tuples res) 1)
					no-aircraft
					(string-cat "\n"
						(pg-map-rows res
							(lambda (row)
								(chart-row row)))))))))
(define blank-record
	; incomplete aircraft record
	(let ([query
				"select nickname
					from aircraft
					where (nickname is null)
					and (make is null)
					and (model is null)
					and (tail_num=~a)
					and (id=~a)"])
		(lambda (id) (pg-one-row (sdb) query default-tail-num id))))
(define aircraft-name
	(let ([query
				"select nickname, make, model, tail_num
					from aircraft
					where (id=~a)"])
		(lambda (id)
			(let* ([row (pg-one-row (sdb) query id)]
					[nickname (pg-cell row 'nickname)]
					[tail-num (pg-cell row 'tail_num)])
				(cond
					[(not (null? nickname)) nickname]
					[(not (null? tail-num)) tail-num]
					[else
						(string-cat " "
							(to-s (pg-cell row 'make))
							(to-s (pg-cell row 'model)))])))))
(define new-aircraft?
	(let ([query
				"select count(*) as n
					from aircraft
					where tail_num=~a"])
		(lambda ()
			(let ([row (pg-one-row (sdb) query default-tail-num)])
				(> (pg-cell row 'n) 0)))))
(define aircraft-chart
	(let ([html
				(deflate "<table>
				<thead><tr>
				<th></th>
				<th class=\"hcell\">Nickname</th>
				<th class=\"hcell\">Make</th>
				<th class=\"hcell\">Model</th>
				<th class=\"hcell\">Tail<br/>Number</th>
				<th class=\"hcell\">
					<span>Jumper<br/>Capacity</span></th>
				<th class=\"hcell\"><span
					title=\"loads between fuelings\">
					Fueling<br/>Cycle</span></th>
				<th class=\"hcell\"><span title=\"manifest tab order\">Tab<br/>Order</span></th>
				<th class=\"hcell\">Active</th>
				</tr></thead>
				<tbody>
				~a
				</tbody>
				</table>")])
		(lambda (req)
			(format #f html (aircraft-catalog (active-only req))))))
(define (aircraft-data req)
	(list
		(cons 'chart (aircraft-chart req))
		(cons 'list (aircraft-list (active-only req)))))
(define assure-unique
	; assure no duplicate aircraft tail numbers or nicknames
	(let ([query
				"select count(*) as n
					from aircraft
					where ((nickname ilike ~a) or (tail_num ilike ~a))
					and (id != ~a)"])
		(lambda (req)
	(let* ([row
				(pg-one-row (sdb) query
					(query-value req 'nickname)
					(query-value req 'tail_num)
					(query-value-number req 'id))])
		(< (pg-cell row 'n) 1)))))
(define tail-num-scrub (make-regexp "[^A-Z0-9]"))
(define update-aircraft
	(let ([update-query
				"update aircraft set
					nickname=~a,
					make=~a,
					model=~a,
					tail_num=~a,
					capacity=~a,
					rank=~a,
					empty_weight=~a,
					basic_arm=~a,
					fuel_arm=~a,
					pilot_arm=~a,
					avg_payload_arm=~a,
					fuel_capacity=~a,
					fuel_burn=~a,
					fuel_type=~a,
					notes=~a,
					observer_slot=~a,
					updated=current_timestamp
					where (id=~a)"]
			[capacity-query
				"select coalesce(capacity,0) as n
					from aircraft
					where (id=~a)"]
			[incr-delta
				"update load_sheets set
					slot_delta=(slot_delta + ~a)
					where (aircraft_id=~a)"]
			[readback-query
				"select *
					from aircraft
					where (id=~a)"])
		(define (sanitize-tail-num tail-num)
			(regexp-substitute/global #f tail-num-scrub
				(if tail-num (string-upcase (to-s tail-num)) "")
				'pre "" 'post))
		(define (null-zero value) (if (= value 0) '() value))
		(define (incr-load-sheet-deltas aircraft-id new-capacity)
			; update load sheet slot deltas to track changes
			; in aircraft capacity
			(let ([row (pg-one-row (sdb) capacity-query aircraft-id)])
				(pg-exec (sdb) incr-delta
					(if row (- new-capacity (pg-cell row 'n)) 0)
					aircraft-id)))
		(lambda (req)
			(let ([aircraft-id (query-value-number req 'id)]
					[capacity (query-value-number req 'capacity)])
				(incr-load-sheet-deltas aircraft-id capacity)
				(pg-exec (sdb) update-query
					(query-value req 'nickname)
					(query-value req 'make)
					(query-value req 'model)
					(sanitize-tail-num (query-value req 'tail_num))
					(null-zero capacity)
					(max (query-value-number req 'rank) 1)
					(null-zero (query-value-number req 'empty_weight))
					(null-zero (query-value-number req 'basic_arm))
					(null-zero (query-value-number req 'fuel_arm))
					(null-zero (query-value-number req 'pilot_arm))
					(null-zero (query-value-number req 'avg_payload_arm))
					(null-zero (query-value-number req 'fuel_capacity))
					(null-zero (query-value-number req 'fuel_burn))
					(query-value req 'fuel_type)
					(query-value req 'notes)
					(query-value-boolean req 'observer)
					aircraft-id)
				(cons (cons 'aircraft_id aircraft-id)
					(pg-one-row (sdb) readback-query aircraft-id))))))
(define composite
	(make-doc (list frame script page-body)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "Aircraft")
				(cons 'app "aircraft")
				(cons 'version nowcall-version)
				(cons 'script (fetch-doc script))
				(cons 'content (fetch-doc page-body))))))
(define set-active
	(let ([query "update aircraft set active=~a where (id=~a)"])
		(lambda (active id) (pg-exec (sdb) query active id))))
(define data-errors
	; validate aircraft data entry pre-update
	(let ([tail-num-query
				"select count(*) as n
					from aircraft
					where (lower(tail_num)=lower(~a))
					and (id != ~a)"])
		(define (check-tail-num tail-num aircraft-id)
			(< (pg-cell
				(pg-one-row (sdb) tail-num-query
					tail-num aircraft-id) 'n) 1))
		(lambda (req)
			(let ([errs '()]
					[passengers (query-value-number req 'capacity)]
					[tail-num (query-value req 'tail_num)]
					[aircraft-id (query-value-number req 'id)])
				(when (string=? (or tail-num "") "")
					(set! errs (cons "tail number required" errs)))
				(when (< passengers 1)
					(set! errs (cons "passenger capacity required" errs)))
				(unless (check-tail-num tail-num aircraft-id)
					(set! errs (cons "duplicate tail number" errs)))
				errs))))
(define create-aircraft
	; create minimal aircraft record to be fleshed out with
	; data update
	(let ([id-query "select nextval('aircraft_seq') as id"]
			[insert
				"insert into aircraft (id, tail_num, active)
					values (~a, ~a, 't')"])
		(lambda (req)
			(let ([id (pg-cell (pg-one-row (sdb) id-query) 'id)])
				(pg-exec (sdb) insert id default-tail-num)
				(cons
					(cons 'id id)
					(aircraft-data req))))))

(log-to "/var/log/nowcall/aircraft.log")

(http-html "/"
	(lambda (req)
		(if (authorized-for req "aircraft")
			(fill-template (fetch-doc composite) #f
				(cons 'dz_full_name (env-dz-full-name))
				(cons 'admin_check (admin-light req (sdb))))
			(redirect-html "/login/aircraft"))))
(http-json "/chart"
	; Display aircraft chart.
	(admin-gate "aircraft" (req) (aircraft-data req)))
(http-json "/new"
	; Create new empty aircraft record.
	(admin-gate "aircraft" (req) (create-aircraft req)))
(http-json "/del"
	; Delete aircraft entry, after confirmation if non-empty.
	(admin-gate "aircraft" (req)
		(let ([msg
					(if (delete-aircraft (query-value-number req 'id))
						""
						"Can't delete; this aircraft has occupied loads")])
			(cons (cons 'msg msg) (aircraft-data req)))))
(http-json "/predel"
	; If aircraft entry is empty (no name, make, model or tail#)
	; delete it straight away. Otherwise, request confirmation.
	(admin-gate "aircraft" (req)
		(let ([id (query-value-number req 'id)])
			(list
				(cons 'blank (blank-record id))
				(cons 'id id)
				(cons 'name (aircraft-name id))))))
(http-json "/tact"
	; Set aircraft "active" status and repaint chart.
	(admin-gate "aircraft" (req)
		(let ([id (query-value-number req 'id)]
				[active (query-value-boolean req 'state)])
			(set-active active id)
			(aircraft-data req))))
(http-json "/upd"
	; update aircraft data
	(admin-gate "aircraft" (req)
		(let ([errs (data-errors req)])
			(if (null? errs)
				(let ([data (update-aircraft req)])
					(cons (cons 'status #t) data))
				(list
					(cons 'status #f)
					(cons 'msg
						(format #f "Please correct and resubmit: ~a"
							(html-ul errs))))))))
(http-json "/acdet"
	; render aircraft detail panel
	(admin-gate "aircraft" (req)
		(let ([aircraft-id (query-value-number req 'id)])
			(list
				(cons 'id aircraft-id)
				(cons 'html (aircraft-detail-panel aircraft-id))))))
(add-javascript-logger "aircraft")
