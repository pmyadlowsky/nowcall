#! /usr/local/bin/gusher
!#

(use-modules (ice-9 format))

(include "lib.scm")

(define (table-row row cell-class)
	(string-cat ""
		"<tr>"
		(map
			(lambda (cell)
				(format #f "<td class=\"~a~a\">~a</td>"
					cell-class
					(if (string? cell) ""
						(if (assq-ref cell 'class)
							(format #f " ~a" (assq-ref cell 'class)) ""))
					(if (string? cell) cell
						(or (assq-ref cell 'data) ""))))
			row)
		"</tr>"))
(define spreadsheet-link
	(let ([html
				(deflate "<a href=\"#\"
					onclick=\"run_report('~a', true); return false\">\
				<img title=\"spreadsheet\" src=\"/img/ssheet.png\"/></a>")])
		(lambda (report-key) (format #f html report-key))))
(define (build-table title headers report-key rows)
	(string-cat "\n"
		(format #f "<div class=\"report-title\">~a</div>" title)
		(format #f "<div>~a</div>"
			(if (= (length rows) 0) "nothing found"
				(format #f "~d record~a ~a"
					(length rows)
					(if (= (length rows) 1) "" "s")
					(spreadsheet-link report-key))))
		"<table>"
		(if (or (= (length rows) 0) (< (length headers) 1)) ""
			(table-row headers "report-hdr-cell"))
		(map (lambda (row) (table-row row "report-data-cell")) rows)
		"</table>"))
(define (spreadsheet-row row)
	(string-cat ","
		(map
			(lambda (cell)
				(format #f "\"~a\""
					(if (string? cell) cell
						(assq-ref cell 'data)))) row)))
(define (build-spreadsheet title headers rows)
	(format #f "~a\r\n"
		(string-cat "\r\n"
			(format #f "\"~a\"~a" title
				(if (< (length headers) 1) ""
					(format #f "\r\n~a" (spreadsheet-row headers))))
			(map (lambda (row) (spreadsheet-row row)) rows))))
(define (dollars pennies)
	(format #f "$~d.~2,'0d"
		(quotient pennies 100)
		(remainder pennies 100)))
(define (fiscal-quarter month year)
	(format #f "~d-Q~d" year
		(cond
			[(< month 4) 1]
			[(< month 7) 2]
			[(< month 10) 3]
			[else 4])))
(define (sql-date year month)
	(format #f "~d-~2,'0d-~2,'0d" year month 1))
(define (quarter year month)
	(cond
		[(< month 4)
			(cons (sql-date year 1) (sql-date year 4))]
		[(< month 7)
			(cons (sql-date year 4) (sql-date year 7))]
		[(< month 10)
			(cons (sql-date year 7) (sql-date year 10))]
		[else
			(cons (sql-date year 10) (sql-date (1+ year) 1))]))
(define (quarter-tax-query template year month)
	(let ([dates (quarter year month)])
		(fill-template template #f
			(cons 'qtr_start (car dates))
			(cons 'qtr_end (cdr dates)))))
(define quarter-sales-tax-by-date
	(let ([header
				(list
					"date"
					"description"
					"unit price"
					"quantity"
					"subtotal"
					"sales tax"
					"total")]
			[query-tmp "\
				select date_posted, amount, sales_tax,
						acct_xact.description as description,
						quantity
					from acct_xact, acct
					where (to_acct=acct.id)
					and (acct.name='general')
					and (sales_tax > 0)
					and (date_posted >= '[[QTR_START]]')
					and (date_posted < '[[QTR_END]]')
					order by date_posted asc, entered asc"])
		(define (spreadsheet-row row)
			(let* ([quantity (pg-cell row 'quantity)]
					[subtotal (* quantity (pg-cell row 'amount))]
					[tax (* quantity (pg-cell row 'sales_tax))]
					[total (+ subtotal tax)])
				(list
					(list
						(time-format
							(pg-cell row 'date_posted) "%m/%d/%Y")
						(pg-cell row 'description)
						(dollars (pg-cell row 'amount))
						(to-s quantity)
						(dollars subtotal)
						(dollars tax)
						(dollars total)) tax total)))
		(define (sums rows)
			(let loop ([data rows]
					[grand-total 0]
					[tax-total 0])
				(if (null? data)
					(list "" "" "" "" ""
						(dollars tax-total) (dollars grand-total))
					(loop
						(cdr data)
						(+ grand-total (caddr (car data)))
						(+ tax-total (cadr (car data)))))))
		(lambda (year month spreadsheet dbh)
			(let* ([chart
						(pg-map-rows
							(pg-exec dbh
								(quarter-tax-query query-tmp year month))
							spreadsheet-row)]
					[data (append (map car chart) (list (sums chart)))]
					[title
						(format #f "Sales Tax by Date, ~a"
							(fiscal-quarter month year))])
				(if spreadsheet
					(build-spreadsheet title header data)
					(build-table title header "taxsales" data))))))
(define quarter-sales-tax-by-item
	(let ([header
				(list
					"description"
					"quantity"
					"subtotal"
					"sales tax"
					"total")]
			[query-tmp "\
				select acct_xact.description as description,
							sum(quantity) as quantity,
							sum(quantity * amount) as subtotal,
							sum(quantity * sales_tax) as tax,
							(sum(quantity * amount) +
								sum(quantity * sales_tax)) as total
					from acct_xact, acct
					where (to_acct=acct.id)
					and (acct.name='general')
					and (sales_tax > 0)
					and (date_posted >= '2016-01-01')
					and (date_posted < '2016-04-01')
					group by acct_xact.description
					order by acct_xact.description asc"])
		(define (spreadsheet-row row)
			(list
				(list
					(pg-cell row 'description)
					(to-s (pg-cell row 'quantity))
					(dollars (pg-cell row 'subtotal))
					(dollars (pg-cell row 'tax))
					(dollars (pg-cell row 'total)))
				(pg-cell row 'tax) (pg-cell row 'total)))
		(define (sums rows)
			(let loop ([data rows]
					[grand-total 0]
					[tax-total 0])
				(if (null? data)
					(list "" "" ""
						(dollars tax-total) (dollars grand-total))
					(loop
						(cdr data)
						(+ grand-total (caddr (car data)))
						(+ tax-total (cadr (car data)))))))
		(lambda (year month spreadsheet dbh)
			(let* ([chart
						(pg-map-rows
							(pg-exec dbh
								(quarter-tax-query query-tmp year month))
							spreadsheet-row)]
					[data (append (map car chart) (list (sums chart)))]
					[title
						(format #f "Sales Tax by Item, ~a"
							(fiscal-quarter month year))])
				(if spreadsheet
					(build-spreadsheet title header data)
					(build-table title header "taxsales" data))))))

(configure-database "dbname=nowcall user=nowcall port=5432")
(define dbh (pg-open))
(define spreadsheet #f)
(format #t (quarter-sales-tax-by-date 2016 3 spreadsheet dbh))
(format #t "\r\n")
(format #t (quarter-sales-tax-by-item 2016 3 spreadsheet dbh))
(pg-close dbh)
(exit)
