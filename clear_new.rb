#! /usr/bin/ruby

# pipe output to psql

SQLCmd = "psql nowcall nowcall"
NewQuery = "select id from people where new"

SQL = <<END
delete from roles where who=PID;
delete from acct_track
	using acct
	where (acct.owner=PID)
	and (acct_track.acct_id=acct.id);
delete from acct where owner=PID;
delete from people where id=PID;
END

`echo "#{NewQuery}" | #{SQLCmd}`.split(/\n/).each do |line|
	next unless line.strip =~ /^\d+$/
	puts SQL.gsub("PID", line.strip)
	end
