-- Migrate load_sheets to load_sheets2, load_slots and slot_extras2.
delete from slot_extras2;
delete from load_slots;
delete from load_sheets2;
insert into load_sheets2 (aircraft_id, load_date, load_num, status)
	select distinct aircraft_id, load_date, load_num, status
		from load_sheets;
insert into load_slots (load_id, slot_num, stamp, passenger, role,
							reserved, locked, acct_xact_id)
	select load_sheets2.id, slot_num, load_sheets.stamp, passenger, role,
				reserved, locked, transaction
		from load_sheets, load_sheets2
		where (load_sheets2.aircraft_id=load_sheets.aircraft_id)
		and (load_sheets2.load_date=load_sheets.load_date)
		and (load_sheets2.load_num=load_sheets.load_num);
insert into slot_extras2 (acct_xact_id, item_id, slot_id)
	select slot_extras.acct_xact, slot_extras.item_id,
				load_slots.id
		from load_sheets2, load_slots, slot_extras
		where (load_sheets2.aircraft_id=slot_extras.aircraft_id)
		and (load_sheets2.load_date=slot_extras.load_date)
		and (load_sheets2.load_num=slot_extras.load_num)
		and (load_slots.load_id=load_sheets2.id)
		and (load_slots.slot_num=slot_extras.slot_num);
