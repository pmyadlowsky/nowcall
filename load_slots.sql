-- Split load_sheets out into load_sheets2 and load_slots
-- to partition load-sheet and load-slot data spaces.
-- Eventually, drop obsolete load_sheets and rename load_sheets2.
drop sequence load_sheets_seq cascade;
create sequence load_sheets_seq;
drop sequence load_slots_seq cascade;
create sequence load_slots_seq;

drop table load_sheets2 cascade;
create table load_sheets2 (
	id integer primary key default nextval('load_sheets_seq'),
	aircraft_id integer not null,
	load_date date not null,
	load_num integer not null,
	stamp timestamp,
	pilot integer,
	copilot integer,
	fuel_at_takeoff integer,
	status varchar default 'closed',
	qualifier varchar default 'shut',
	qualifier_change timestamp,
	call_time timestamp,
	call_tic timestamp,
	call_abort boolean default 'f',
	foreign key (aircraft_id) references aircraft(id),
	foreign key (pilot) references people(id),
	foreign key (copilot) references people(id)
	);
-- drop table load_calls;
drop table load_slots cascade;
create table load_slots (
	id integer primary key default nextval('load_slots_seq'),
	load_id integer not null,
	slot_num integer not null,
	stamp timestamp,
	passenger integer,
	role varchar,
	reserved boolean default false,
	locked boolean default false,
	acct_xact_id integer,
	foreign key (load_id) references load_sheets2(id),
	foreign key (passenger) references people(id),
	foreign key (acct_xact_id) references acct_xact(id)
	);
drop table slot_extras2;
create table slot_extras2 (
	acct_xact_id integer,
	item_id integer not null,
	slot_id integer not null,
	foreign key (slot_id) references load_slots(id),
	foreign key (item_id) references product_item(id)
	);
