/*
** Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
**
** This program is free software ; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation ; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY ; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program ; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var selection = Array();
var tracking = true;
var min_search_length = 3;
var ctl_mode = false;
var loaded_acct = null;
var charge_authorized = false;
var card_service_available = false;
var card_service_detail = "";
var payload_cache = null;

function card_response(resp) {
	if (resp.status) Payment.set_amount(0);
	else alert(resp.msg);
	}
function show_account(resp) {
	min_search_length = resp.search_length;
	set_menu("acct", resp.acct)
	select_acct();
	}
function slot_selected(event, ui) {
	var m = ui.selected.id.match(/^feesel([0-9]+)/)
	if (m == null) return;
	var id = "fee" + m[1];
	if (document.getElementById(id) != null) selection.push(id);
	}
function slot_unselected(event, ui) {
	}
function select_start(event, ui) {
	selection = Array();
	}
function select_stop(event, ui) {
	if (selection.length < 1) return;
	for (var i = 0; i < selection.length; i++) {
		document.getElementById(selection[i]).click();
		}
	}
function set_drag_select() {
	$(".selectable").selectable({
		tolerance: "touch",
		selected: slot_selected,
		unselected: slot_unselected,
		stop: select_stop,
		start: select_start,
		filter: ".row_select"
		});
	}
function focus_menu(id) {
	if (document.getElementById(id) == null) return;
	$("#" + id).focus();
	$("#" + id).menu("expand");
	}
function control_key(key) {
	return ((key == "Meta") || (key == "OS"));
	}
function console_debug(msg) {
	//console.log(msg);
	}
function process_payload(payload) {
	if (tracking) {
		render_accounts(payload);
		}
	else {
		payload_cache = payload;
		console.log("payload cached");
		}
	}
function init() {
	load_acct_menu("acct", true, null,
		function() {
			$.get("/accounting/dacct", {}, show_account, "json")
			});
	load_acct_menu("xfer-acct", false, null, null);
	$("#special-acct-dialog").dialog({
		autoOpen: false,
		draggable: true,
		show: true,
		hide: true,
		closeOnEscape: false,
		height: "auto",
		width: "auto",
		autoResize: true,
		dialogClass: "my-alert",
		modal: true
		});
	$(document).bind("keydown",
		function(event) {
			if (control_key(event.key)) {
				ctl_mode = true;
				console_debug("ctl on");
				return false;
				}
			console_debug("keydown |" + event.key + "|");
			return !ctl_mode;
			}
		);
	$(document).bind("keyup",
		function(event) {
			if (control_key(event.key) && ctl_mode) {
				ctl_mode = false;
				console_debug("ctl off");
				return true;
				}
			console_debug("keyup |" + event.key + "|");
			if (ctl_mode) {
				var key = event.key.toLowerCase();
				if (key == "d") {
					focus_menu("charge_acct");
					return false;
					}
				else if (key == "c") {
					focus_menu("payment_acct");
					return false;
					}
				else if (key == "a") {
					Acct.pay();
					return false;
					}
				else if (key == "x") {
					Acct.clear_controls();
					return false;
					}
				return true;
				}
			return true;
			}
		);
	Payment.config_stripe(
		function(resp) {
			if (resp.status) Payment.set_amount(0);
			else local_alert("Payment Error", resp.msg);
			});
	Payment.require_checkout(false);
	$.get("/payment/check", {},
			function(resp) {
				var btn = document.getElementById("pay-button");
				card_service_available = resp.status;
				btn.disabled = !resp.status;
				if (resp.status) {
					card_service_detail = btn.title = "";
					return;
					}
				if (resp.err == "keys")
					card_service_detail = "payment service not configured";
				else card_service_detail = "payment service not available";
				btn.title = card_service_detail;
				}, "json");
	mark_page("Accounting");
	Payment.load_card_menu("", true);
	LongPoll.set_poll_url("/accounting/poll");
	LongPoll.set_payload_handler(process_payload);
	LongPoll.set_fallback_timeout(30);
	set_drag_select();
	document.getElementById("refresh-btn").disabled = false;
	LongPoll.poll(
		function() {
			return {
				acct: Acct.get_current_account(),
				limit: get_ledger_limit()
				};
			});
	}
function set_menu(mname, value) {
	var menu = document.getElementById(mname);
	var option;
	for (var i = 0; i < menu.length; i++) {
		option = menu.options[i];
		if (option.value == value) {
			menu.selectedIndex = i;
			break;
			}
		}
	}
function enable_pay_button() {
	var btn = document.getElementById("pay-button");
	var charge_to = document.getElementById("charge-to").value;
	if (charge_to == "cash") {
		btn.disabled = false;
		btn.title = "";
		}
	else if (charge_to == "offline") {
		btn.disabled = false;
		btn.title = "";
		}
	else if (!charge_authorized) {
		btn.disabled = true;
		btn.title = "not authorized to use charge service";
		}
	else {
		btn.disabled = !card_service_available;
		btn.title = card_service_detail;
		}
	if (btn.disabled) {
		btn.style.color = "";
		btn.style.fontWeight = "normal";
		}
	else {
		btn.style.color = "green";
		btn.style.fontWeight = "bold";
		}
	}
function ready_transfer_menu() {
	load_acct_menu("xfer-acct", false, null, null);
	var xfer_acct = document.getElementById("xfer-acct");
	if (xfer_acct != null) xfer_acct.disabled = true;
	}
function render_accounts(resp) {
	if (!resp.status) return;
	if ((Acct.get_current_account() != "") &&
			(Acct.get_current_account() != resp.acct_id))
		return;
	document.getElementById("chart").innerHTML = resp.chart;
	document.getElementById("controls").innerHTML = resp.controls;
	ready_transfer_menu();
	var recv = document.getElementById("receivable");
	var recv_top, pay_top;
	if (recv != null) {
		recv_top = recv.scrollTop;
		pay_top = document.getElementById("payable").scrollTop;
		}
	document.getElementById("summary").innerHTML = resp.balances;
	document.getElementById("payment").style.display =
		(resp.showpay ? "block" : "none");
	/* For some reason, inline "onchange" assignment is being
	   ignored or clobbered, so add it here */
	document.getElementById("charge-to").addEventListener("change",
		enable_pay_button);
	if (resp.showpay) {
		if (resp.balance > 0) Payment.set_amount(resp.balance);
		else Payment.set_amount(0);
		document.getElementById("cust-name").innerHTML = resp.cust_name;
		Payment.set_email(resp.email);
		Payment.load_card_menu(resp.customer, true);
		}
	charge_authorized = resp.authorized;
	enable_pay_button();
	if (recv != null) {
		document.getElementById("receivable").scrollTop = recv_top;
		document.getElementById("payable").scrollTop = pay_top;
		}
	Acct.configure_product_item_menus();
	var search = document.getElementById("acct_srch");
	if (resp.owner) {
		set_menu("acct", "pacct");
		search.disabled = false;
		search.value = resp.owner;
		}
	else {
		set_menu("acct", resp.acct_id.toString());
		search.disabled = true;
		search.value = "";
		}
	$("input.dp").datepicker({
		dateFormat: resp.date_format,
		altFormat: "yy-mm-dd",
		setDate: new Date(Date.now),
		firstDay: 1,
		changeYear: true
		});
	set_drag_select();
	var chart_wrap = document.getElementById("chart-wrap");
	if (chart_wrap != null)
		// scroll to bottom
		chart_wrap.scrollTop = chart_wrap.scrollHeight;
	$(".add-trig").keypress(
		function(evnt) {
			if (evnt.keyCode == 13) {
				Acct.pay();
				evnt.preventDefault();
				}
			});
	}
function new_record_alert() {
	local_alert("Accounting", "Please visit People to create new personnel record.");
	}
function select_person(event, ui) {
	if (ui.item.match) display_acct(ui.item.acct_id);
	else new_record_alert();
	return true;
	}
function live_search(box) {
	box.value = "";
	$("#" + box.id).autocomplete({
		source: "/accounting/search",
		minLength: min_search_length,
		select: select_person
		});
	}
function live_xfer_search(box) {
	box.value = "";
	$("#" + box.id).autocomplete({
		source: "/accounting/search",
		minLength: min_search_length,
		select:
			function(event, ui) {
				if (ui.item.match)
					document.getElementById("acct-xfer-id").value =
						ui.item.acct_id;
				else new_record_alert();
				}
		});
	}
function get_ledger_limit() {
	return get_menu("view-limit")
	}
function display_acct(acct_id) {
	loaded_acct = acct_id;
	$("#spinner").show();
	Acct.set_current_account(acct_id);
	$.get("/accounting/chart", {
			acct: acct_id,
			limit: get_ledger_limit()
			},
			function(resp) {
				render_accounts(resp);
				$("#spinner").hide();
				}, "json");
	}
function get_menu(mname) {
	var menu = document.getElementById(mname);
	var opt = menu.options[menu.selectedIndex];
	return opt.value;
	}
function select_acct() {
	var selected_acct = get_menu("acct");
	var search = document.getElementById("acct_srch");
	search.value = "";
	if (selected_acct == "pacct") {
		search.disabled = false;
		search.focus();
		}
	else if (selected_acct == "special") {
		document.getElementById("special-name").value = "";
		document.getElementById("special-desc").value = "";
		$("#special-acct-dialog").dialog("open");
		}
	else {
		search.disabled = true;
		display_acct(selected_acct);
		}
	}
function select_xfer_acct() {
	var selected_acct = get_menu("xfer-acct");
	var search = document.getElementById("person-acct-srch");
	search.value = "";
	if (selected_acct == "pacct") {
		search.disabled = false;
		search.focus();
		}
	else {
		search.disabled = true;
		document.getElementById("acct-xfer-id").value = selected_acct;
		}
	}
function load_acct_menu(menu_name, with_special, preset, post_load) {
	if (document.getElementById(menu_name) == null) return;
	$.get("/accounting/menu", { ws: (with_special ? 1 : 0) },
			function(resp) {
				var menu = document.getElementById(menu_name);
				var option;
				menu.options.length = 0;
				for (var i = 0; i < resp.options.length; i++) {
					option = new Option(resp.options[i][1],
									resp.options[i][0]);
					menu.add(option);
					}
				if (preset != null) set_menu(menu_name, preset)
				if (post_load != null) post_load();
				}, "json");
	}
function load_acct_menus(preset) {
	load_acct_menu("acct", true, preset, null);
	load_acct_menu("xfer-acct", false, preset, null);
	}
function delete_account(acct_id) {
	$.get("/accounting/delacct", { id: acct_id },
			function(resp) {
				load_acct_menus(null);
				$.get("/accounting/dacct", {}, show_account, "json")
				}, "json");
	}
function create_account() {
	var name = document.getElementById("special-name").value.trim();
	if (name == "") {
		local_alert("Special Account", "Please provide an account name.");
		return;
		}
	$.post("/accounting/cracct",
		{
			name: name,
			desc: document.getElementById("special-desc").value
			},
		function(resp) {
			if (resp.status) {
				$("#special-acct-dialog").dialog("close");
				load_acct_menus(resp.acct);
				display_acct(resp.acct);
				}
			else local_alert("Special Account", resp.msg);
			}, "json");
	}
function cancel_create() {
	$("#special-acct-dialog").dialog("close");
	$.get("/accounting/dacct", {}, show_account, "json")
	}
function enable_acct_tracking(state) {
	if (state) {
		tracking = true;
		if (payload_cache != null) {
			console.log("flush payload cache");
			render_accounts(payload_cache);
			payload_cache = null;
			}
		console.log("resume tracking");
		}
	else {
		tracking = false;
		console.log("suspend tracking");
		}
	}
function refresh_current_account() {
	if (loaded_acct != null) display_acct(loaded_acct);
	}
function logout_handler(resp) {
	window.location = "/manifest";
	}
function logout() {
	$.get("/login/logout", {}, logout_handler, "json");
	}
