#! /usr/local/bin/gusher -p 3004
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 regex))
(use-modules (ice-9 format))
(use-modules (gusher cron))
(use-modules (srfi srfi-1))

(include "lib.scm")
(include "grid.scm")
(include "acct.scm")
(include "settings_lib.scm")
(include "weight_balance.scm")
(include "longpoll.scm")
(include "sessions.scm")

(define env-date-format (setting-cache 'date-format))
(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-ground-reserve-out-of-date
	(setting-cache 'ground-reserve-out-of-date))
(define env-load-sheet-columns (setting-cache 'load-sheet-columns))
(define env-manifast-retention (setting-cache 'manifast-retention))
(define env-min-load-sheets (setting-cache 'min-load-sheets))
(define env-min-search-chars (setting-cache 'min-search-chars))
(define env-name-order (setting-cache 'name-order))
(define env-pubsheets (setting-cache 'pubsheets))
(define env-repack-cycle (setting-cache 'repack-cycle))
(define env-waiver-valid (setting-cache 'waiver-valid))
(define null-ip-address "0.0.0.0")

(define req-cache '())
(define admin-page (make-doc 'file "manifest.html"))
(define kiosk-page (make-doc 'file "kiosk.html"))
(define main-script (make-doc 'file "manifest.js"))
(define acct-lib-script (make-doc 'file "docroot/js/acct_lib.js"))
(define scripts
	(make-doc (list main-script acct-lib-script)
		(lambda ()
			(string-cat "\n"
				(fetch-doc acct-lib-script)
				(fetch-doc main-script)))))
(define manifast-max-rows 20)
(define load-call-update-interval 5) ; sec
(define nowcall-sustain 30) ; how long (sec) to show "NOW" call
(define role-trend-window 8)
(define long-poll-interval 10)
(define load-states
	(list
		(cons 'closed
			(list
				(cons 'title "VIEW")
				(cons 'color "#946d53")))
		(cons 'open
			(list
				(cons 'title "OPEN")
				(cons 'color "#7a9453")))
		(cons 'gone
			(list
				(cons 'title "GONE")
				(cons 'color "#6d5394")))))
(define load-state-default 'closed)

(define (find-load-status button-label)
	(caar
		(filter
			(lambda (state)
				(string=? (assq-ref (cdr state) 'title) button-label))
			load-states)))
(define (next-load-status present-status)
	(let scan ([states (map (lambda (state) (car state)) load-states)])
		(cond
			[(null? (cdr states)) (caar load-states)]
			[(eq? present-status (car states))
				(cadr states)]
			[else (scan (cdr states))])))
(define (load-status-button status)
	(assq-ref (assq-ref load-states status) 'title))
(define (load-status-color status)
	(assq-ref (assq-ref load-states status) 'color))
(define aircraft-id
	(let ([query "select id from aircraft where tail_num=~a"])
		(lambda (tail-num)
			(let ([row (pg-one-row (sdb) query tail-num)])
				(and row (pg-cell row 'id))))))
(define aircraft-tail-num
	(let ([query "select tail_num from aircraft where (id=~a)"])
		(lambda (aircraft-id)
			(pg-cell (pg-one-row (sdb) query aircraft-id)
				'tail_num))))
(define add-load-slots
	(let ([insert-slot
				"insert into load_slots (load_id, slot_num, stamp)
					values (~a, ~a, current_timestamp)"]
			[last-slot-query
				"select max(slot_num) as last
					from load_slots
					where (load_id=~a)"])
		(define (last-slot load-id)
			(let* ([row (pg-one-row (sdb) last-slot-query load-id)]
					[slot-num (pg-cell row 'last)])
				(if (null? slot-num) 0 slot-num)))
		(lambda (load-id slots)
			(let ([next-slot-num (1+ (last-slot load-id))])
				(let loop ([count 0])
					(when (< count slots)
						(pg-exec (sdb) insert-slot load-id
							(+ count next-slot-num))
						(loop (1+ count))))))))
(define make-sheet
	; generate one load sheet in DB
	(let ([next-id "select nextval('load_sheets_seq') as id"]
			[insert-sheet
				"insert into load_sheets (id, aircraft_id,
							load_date, load_num, stamp, status)
					values (~a, ~a, ~a, ~a, current_timestamp, ~a)"])
		(define (next-sheet-id)
			(pg-cell (pg-one-row (sdb) next-id) 'id))
		(lambda (aircraft-id date load-num capacity)
			(let ([load-id (next-sheet-id)])
				(pg-exec (sdb) insert-sheet
					load-id aircraft-id date load-num
					(load-status-button load-state-default))
				(add-load-slots load-id capacity)))))
(define get-capacity
	; get aircraft slot capacity
	(let ([query
				"select capacity
					from aircraft
					where (id=~a)"])
		(lambda (aircraft-id)
			(let* ([row (pg-one-row (sdb) query aircraft-id)])
				(pg-cell row 'capacity)))))
(define upsert-page-touch
	; update/insert page touch event
	(let ([update-query
				"update page_touch set
					stamp=current_timestamp
					where (load_date=~a) and (aircraft_id=~a)"]
			[insert-query
				"insert into page_touch (load_date,
						aircraft_id, stamp)
					values (~a, ~a, current_timestamp)"]
			[mutex (make-mutex)])
		(lambda (date aircraft-id)
			(lock-mutex mutex)
			(let ([res (pg-exec (sdb) update-query date aircraft-id)])
				(when (< (pg-cmd-tuples res) 1)
					(pg-clear res)
					(pg-exec (sdb) insert-query date aircraft-id)))
			(unlock-mutex mutex))))
(define (make-sheets aircraft-id date how-many)
	; generate N additional load sheets for aircraft and date
	; return index of first sheet added
	(let ([capacity (get-capacity aircraft-id)]
			[start
				(+ (first-sheet-today date aircraft-id)
					(count-sheets aircraft-id date))])
		(let loop ([count 0]
					[load-num start])
			(when (< count how-many)
				(make-sheet aircraft-id date load-num capacity)
				(loop (1+ count) (1+ load-num))))
		(upsert-page-touch date aircraft-id) start))
(define passenger-products
	(let ([query
				"select distinct product_item.name,
						product_item.manifest_symbol as symbol,
						product_category.seq, product_item.seq,
						product_item.manifest_seq
					from roles, product_roles, product_item,
						product_category
					where (roles.who=~a)
					and (product_item.manifest_symbol is not null)
					and (roles.role=product_roles.role)
					and (product_roles.product=product_item.id)
					and (product_item.category=product_category.id)
					and (not product_item.divider)
					order by
						product_item.manifest_seq asc,
						product_category.seq asc,
						product_item.seq asc"])
		(lambda (pid)
			(pg-map-rows
				(pg-exec (sdb) query pid)
				(lambda (row) (pg-cell row 'symbol))))))
(define disable-form " disabled=\"disabled\"")
(define (mani-admin req) (authorized-for req "manifest"))
(define env-manifest-admin
	(request-cache (lambda (http-req) (mani-admin http-req))))
(define env-login-id
	(request-cache
		(lambda (http-req)
			(if (logged-in http-req)
				(session-get http-req 'person-id) 0))))
(define (observer-slot? slot-num) (= slot-num observer-slot))
(define (jump-slot slot-num) (> slot-num 0))
(define (slot-num-tag slot-num)
	(if (observer-slot? slot-num) "rs" (to-s slot-num)))
(define blank-slot-form
	; make input text box for empty passenger name slot
	(let ([html
				(deflate "<input type=\"text\" id=\"bname~d_~d_~a\"
					value=\"~a\" class=\"blank-form~a\"
					onkeypress=\"search_key(event);return true\"
					onblur=\"slot_unfocused()\"
					onfocus=\"return live_search(this,'~d',~d,~d,event)\"~a/>")])
		(lambda (aircraft-id load-num slot-num closed pre-fill)
			(format #f html
				aircraft-id load-num (slot-num-tag slot-num)
				pre-fill
				(if (env-manifest-admin) " blank-slot-ctx" "")
				aircraft-id load-num slot-num
				(if closed disable-form "")))))
(define one-year-waiver "\
waiver_date < (current_date - interval '1 year')")
(define current-year-waiver "\
date_part('year',waiver_date) != date_part('year',current_date)")
(define out-of-date-note
	; produce warning code and float-over title to mark
	; people whose reserve, waiver or membership dates need updating
	(let ([query
				"select
					((license is not null) and
						((last_repack is null) or
							(last_repack <
								(current_date - interval '~d days'))) and
						((last_repack_2 is null) or
							(last_repack_2 <
								(current_date - interval '~d days'))))
							as rod,
					((license is not null) and
						((uspa_expires is null) or
							(uspa_expires < current_date))) as mod,
					((waiver_date is null) or (~a)) as wod,
					(exit_weight is null) as nox
					from people
					where (id=~d)"])
		(define (od-tag od-set)
			; out-of-date tag for load slot
			(if (null? od-set) ""
				(format #f "&nbsp;OD-~a"
					(string-cat ""
						(map
							(lambda (pair)
								(car pair)) od-set)))))
		(define (od-title od-set)
			; out-of-date tooltip for load slot
			(if (null? od-set) ""
				(format #f " title=\"expired/missing: ~a\""
					(string-cat ","
						(map
							(lambda (pair)
								(cdr pair)) od-set)))))
		(define (tag-title od-set)
			(cons (od-tag od-set) (od-title od-set)))
		(lambda (passenger)
			(let* ([waiver-valid (env-waiver-valid)]
					[row
						(pg-one-row-primitive (sdb)
							(format #f query
								(env-repack-cycle)
								(env-repack-cycle)
								(if (string=? waiver-valid "this-year")
									current-year-waiver
									one-year-waiver)
								passenger))]
					[rig-ood (and row (pg-cell row 'rod))]
					[membership-ood (and row (pg-cell row 'mod))]
					[waiver-ood (and row (pg-cell row 'wod))]
					[nox (and row (pg-cell row 'nox))])
				(tag-title
					(filter (lambda (ood) ood)
						(list
							(and rig-ood (cons "R" "reserve"))
							(and membership-ood (cons "M" "membership"))
							(and nox (cons "X" "exit weight"))
							(and waiver-ood (cons "W" "waiver")))))))))
(define filled-slot-form
	; make populated <div> for occupied passenger name slot
	; inter-load draggable unless reserved
	(let ([html
				(deflate "<div class=\"rounded-corners-5 filled-form\">
				<div id=\"fname~d_~d_~d_~a\"~a class=\"dragger~a\">
				<div style=\"float: left\">~a</div>
				<div style=\"float: left; font-weight: bold;
					font-size: 50%; color: red\">~a</div>
				<div style=\"float: right\">~a</div>
				<div style=\"clear: both\"></div>
				</div>
				</div>")]
			[video-icon
				"<img src=\"/img/vidicon.png\" alt=\"extras\"/>"])
		(lambda (aircraft-id load-num slot-num passenger has-extras)
			(let ([notes
						(if (env-manifest-admin)
							(out-of-date-note passenger)
							(cons "" ""))])
				(format #f html
					passenger
					aircraft-id
					load-num
					(slot-num-tag slot-num)
					(cdr notes)
					(if (env-manifest-admin) " filled-slot-ctx" "")
					(person-name passenger (env-name-order) (sdb))
					(car notes)
					(if has-extras video-icon ""))))))
(define reserved-slot-form
	(let ([html
				(deflate "<div class=\"rounded-corners-5 filled-form\">
				<div id=\"fname0_~d_~d_~d\" class=\"dragger\">
				<div style=\"float: left\">~a</div>
				<div style=\"clear: both\"></div>
				</div>
				</div>")])
		(lambda (aircraft-id load-num slot-num)
			(if (env-manifest-admin)
				(blank-slot-form aircraft-id load-num
					slot-num #f "reserved")
				(format #f html
					aircraft-id
					load-num slot-num "<i>reserved</i>")))))
(define (load-slot-name-content aircraft-id load-num slot-num passenger
					reserved has-extras self-manifest)
	(cond
		[reserved
			(reserved-slot-form aircraft-id load-num slot-num)]
		[(not (null? passenger))
			(filled-slot-form
				aircraft-id load-num slot-num
				passenger has-extras)]
		[else
			(blank-slot-form
				aircraft-id
				load-num slot-num
				(not (or (env-manifest-admin) self-manifest)) "")]))
(define load-slot-name
	; add name slot to load sheet, occupied or empty
	(let ([html
				(deflate "<td style=\"padding: 3px\"
					id=\"name~a~d_~d_~a\">~a</td>")])
		(lambda (passenger aircraft-id load-num slot-num
					reserved self-manifest has-extras multi)
			(format #f html
				(if multi "-all-" "")
				aircraft-id load-num
				(slot-num-tag slot-num)
				(load-slot-name-content aircraft-id load-num
					slot-num passenger reserved has-extras
					self-manifest)))))
(define load-slot-scratch
	; scratch button
	(let ([html
				(deflate "<td style=\"width: 2em\"><input type=\"button\"
					title=\"scratch\" class=\"scratch rounded-corners-5\"
					onclick=\"scratch('~d',~d,[~d])\" value=\"x\"
					id=\"scratch~a~d_~d_~a\"~a/></td>")])
		(lambda (aircraft-id load-num slot-num locked self-manifest multi)
			(format #f html
				aircraft-id load-num slot-num
				(if multi "-all-" "")
				aircraft-id load-num
				(slot-num-tag slot-num)
				(if (or locked (not self-manifest)) disable-form "")))))
(define (role-menu preset roles)
	(if (null? roles)
		"<option value=\"\">-</option>"
		(string-cat "\n"
			(map
				(lambda (role)
					(format #f "<option value=\"~a\"~a>~a</option>"
						role
						(if (string=? role preset)
							" selected=\"selected\"" "")
						role))
				roles))))
(define load-slot-role
	; slot role menu
	(let ([html
				(deflate "<td style=\"width: 3em\">
				<select class=\"role-option\"
					title=\"select role\"
					onchange=\"roll_role(this)\"
					id=\"role~a~d_~d_~d\"~a>~a</select></td>")])
		(lambda (role passenger aircraft-id load-num
					slot-num self-manifest multi)
			(let ([products
						(if (null? passenger) '()
							(passenger-products passenger))])
				(format #f html
					(if multi "-all-" "")
					aircraft-id load-num slot-num
					(if (or (not self-manifest) (< (length products) 1))
						disable-form "")
					(role-menu (if (null? role) "" role) products))))))
(define (open-for-manifest? load-status)
	(or (env-manifest-admin)
		(and (eq? load-status 'open) (env-on-dz-lan))))
(define load-sheet-line
	; build one load sheet entry
	(let ([html
				(deflate "<tr id=\"slot[[AIRCRAFT_ID]]_[[LOAD]]_[[SLOT]]\"
				class=\"catcher\">
				<td id=\"row[[AIRCRAFT_ID]]_[[LOAD]]_[[SLOT]]\"
					class=\"ui-widget-content row_select\">[[SLOT]]</td>
				[[NAME]]
				[[SCRATCH]]
				[[ROLE]]
				</tr>")])
		(lambda (aircraft-id load-num slot passenger role reserved locked
					status has-extras multi)
			(let ([open-slots (open-for-manifest? status)])
				(fill-template html #f
					(cons 'aircraft_id aircraft-id)
					(cons 'load load-num)
					(cons 'slot slot)
					(cons 'name
						(load-slot-name passenger
							aircraft-id load-num slot reserved
							open-slots has-extras multi))
					(cons 'scratch
						(load-slot-scratch aircraft-id load-num
							slot locked open-slots multi))
					(cons 'role
						(load-slot-role role passenger aircraft-id
							load-num slot open-slots multi)))))))
(define copilot-on-load
	(let ([query
				"select copilot
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"])
		(lambda (aircraft-id load-date load-num)
			(let ([row
						(pg-one-row (sdb) query
							aircraft-id load-date load-num)])
				(or (and row (pg-cell row 'copilot)) '())))))
(define load-sheet-observer
	; special observer entry
	(let ([html
				(deflate "<tr id=\"slot[[AIRCRAFT_ID]]_[[LOAD]]_rs\"
				class=\"catcher\">
				<td id=\"row[[AIRCRAFT_ID]]_[[LOAD]]_rs\"
					class=\"ui-widget-content row_select\"><abbr title=\"right seat\">RS</abbr></td>
				[[NAME]]
				[[SCRATCH]]
				<td><b>Observer</b></td>
				</tr>")])
		(lambda (aircraft-id load-date load-num locked multi)
			(let ([open-slots (env-manifest-admin)]
					[slot-num observer-slot]
					[reserved #f]
					[has-extras #f])
				(fill-template html #f
					(cons 'aircraft_id aircraft-id)
					(cons 'load load-num)
					(cons 'name
						(load-slot-name
							(copilot-on-load aircraft-id
								load-date load-num)
							aircraft-id load-num slot-num reserved
							open-slots has-extras multi))
					(cons 'scratch
						(load-slot-scratch aircraft-id load-num
							slot-num locked open-slots multi)))))))
(define call-button
	; put load on call (set timer)
	(let ([html
				(deflate "<input type=\"button\" value=\"Detail\"
					id=\"call-btn~a~d_~d\"
					class=\"rounded-corners-5\"
					style=\"width: 5em\"
					onclick=\"toggle_call('~d',~d,'~a')\"/>
				<div id=\"call~a~d_~d\" style=\"display: none\"></div>")])
		(lambda (aircraft-id load-num suffix)
			(format #f html
				suffix aircraft-id load-num
				aircraft-id load-num suffix
				suffix aircraft-id load-num))))
(define manifast-button
	; manifast pop-up
	(let ([html
				(deflate "<input type=\"button\"
					id=\"manifast_~d_~d\"
					title=\"ManiFast\"
					class=\"manifast-btn rounded-corners-5\"
					style=\"width: 2em; display: ~a\"
					value=\"&nbsp;\"
					onclick=\"open_manifast('~d',~d)\"/>")])
		(lambda (aircraft-id load-num reveal)
			(format #f html
				aircraft-id load-num
				(if reveal "block" "none")
				aircraft-id load-num))))
(define access-button
	; load sheet access control: admin-only, self-manifest, gone
	(let ([html
				(deflate "<input type=\"button\" value=\"~a\"
					title=\"self-manifest access\"
					class=\"rounded-corners-5\"
					style=\"width: 4em\"
					id=\"access~d_~d\"~a
					onclick=\"toggle_access('~d',~d)\"/>")])
		(lambda (aircraft-id load-num status)
			(format #f html
				(load-status-button status)
				aircraft-id load-num
				(if (env-manifest-admin) "" " disabled=\"disabled\"")
				aircraft-id load-num))))
(define load-num-selector
	(let ([html
				(deflate "<input type=\"text\"
					class=\"rounded-corners-5 load-select\"
					value=\"~d\"
					onchange=\"set_first_load('~d',this.value)\"
					title=\"set first load of the day\"/>")])
		(lambda (load-num aircraft-id)
			(format #f html load-num aircraft-id))))
(define qualifier-button
	(let ([icon "<img title=\"~a\" src=\"/img/~a\" alt=\"~a\"/>"]
			[fuel-load-color "#daa520"] ;for reference
			[hot-load-color "red"])
		(lambda (qualifier)
			(cond
				[(string=? qualifier "fuel")
					(format #f icon "fuel load" "fuel.png" "fuel load")]
				[(string=? qualifier "hot")
					(format #f icon "hot load" "hotload.png" "hot load")]
				[else ""]))))
(define qualifier-lite
	(let ([query
				"select qualifier
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"])
		(lambda (aircraft-id date load-num)
			(let* ([row
					(pg-one-row (sdb) query aircraft-id date load-num)])
				(qualifier-button
					(or (and row (pg-cell row 'qualifier)) "none"))))))
(define (load-id-key aircraft-id load-num)
	(format #f "~d_~d" aircraft-id load-num))
(define sheet-top
	; top of each load sheet: headings and controls
	(let ([html
				(deflate "<div id=\"load[[LOAD_ID]]\"
					class=\"load-sheet rounded-corners-10\"
					style=\"border: 6px solid [[BGCOLOR]]\">\
				<table class=\"selectable\" style=\"border-spacing: 0px;
					background-color: white; width: 100%\">
				<tr id=\"toprow[[TOPROW_ID]]\"
					style=\"background-color: [[BGCOLOR]]\">
				<td colspan=\"3\">
				<div class=\"load-number\">[[LOAD_NUM]]</div>
				<div style=\"float: left;
					margin-right: 4px\">[[CALL_BUTTON]]</div>
				<div style=\"float: left;
					margin-right: 4px\">[[ACCESS_BUTTON]]</div>
				<div style=\"float: left;
					margin-right: 4px\">[[MANIFAST_BUTTON]]</div>
				<div style=\"float: left; margin-right: 4px\"
					id=\"qualifier[[LOAD_ID]]\">[[QUALIFIER_BUTTON]]</div>
				<div style=\"clear: left\"></div>
				</td>
				<td class=\"role-header\">plan</td>
				</tr>")])
		(lambda (aircraft-id load-date load-num status
					first-sheet suffix)
			(let ([key (load-id-key aircraft-id load-num)])
				(fill-template html #f
					(cons 'load_id key)
					(cons 'toprow_id key)
					(cons 'load_num
						(if (and first-sheet (env-manifest-admin))
							(load-num-selector load-num aircraft-id)
							load-num))
					(cons 'bgcolor (load-status-color status))
					(cons 'call_button
						(if (env-manifest-admin)
							(call-button aircraft-id load-num suffix) ""))
					(cons 'access_button
						(access-button aircraft-id load-num status))
					(cons 'manifast_button
						(manifast-button aircraft-id load-num
							(open-for-manifest? status)))
					(cons 'qualifier_button
						(qualifier-lite aircraft-id load-date
							load-num)))))))

(define call-mins-remaining
	(let ([query
				"select call_time as time
					from load_sheets
					where (aircraft_id=~a)
					and (load_num=~a)
					and (call_time is not null)
					and (call_time > current_timestamp)"])
		(lambda (aircraft-id load-num)
			(let* ([row (pg-one-row (sdb) query aircraft-id load-num)])
				(if row
					(quotient
						(to-i (time-diff (pg-cell row 'time)
								(db-time-now))) 60) 20)))))
(define load-qualifier
	(let ([load-qual
				"select qualifier
					from load_sheets
					where (id=~a)"]
			[set-qual
				"update load_sheets set
					qualifier=~a
					where (id=~a)"]
			[default-qualifier "shut"])
		(lambda (load-id)
			(let ([row (pg-one-row (sdb) load-qual load-id)])
				(string->symbol
					(if row
						(pg-cell row 'qualifier)
						(begin
							(pg-exec (sdb) set-qual
								default-qualifier load-id)
							default-qualifier)))))))
(define display-load-weight
	(let ([weight-full
				"<div style=\"color: black; font-weight: bold\">~a</div>"]
			[weight-partial
				(deflate "<abbr title=\"some weights/arms not known, presumed zero\"><span style=\"color: #aaaaaa; font-style: italic; font-weight: bold\">~a</span></abbr>")])
		(lambda (weight)
			(format #f (if (car weight) weight-full weight-partial)
				(to-s (cdr weight))))))
(define pilots-menu
	(let ([query
				"select people.id as id, last_name,
						coalesce(goes_by, first_name) as first_name
					from people, roles
					where (roles.role='PILOT')
					and (people.id=roles.who)
					and people.active
					order by last_name, first_name"]
			[pilot-query "select pilot from load_sheets where (id=~a)"]
			[header
				"<select class=\"pilot-menu\"
					onchange=\"log_pilot(this,~d,~d,'pilot')\"
					id=\"pilot~d_~d\">"]
			[option "<option value=\"~d\"~a>~a, ~a</option>"])
		(define (menu-option preset row)
			(let ([id (pg-cell row 'id)])
				(format #f option
					id
					(if (= id preset) " selected=\"selected\"" "")
					(pg-cell row 'last_name)
					(pg-cell row 'first_name))))
		(define (pilot-on-load load-id)
			(let ([pilot
						(pg-cell
							(pg-one-row (sdb) pilot-query
								load-id) 'pilot)])
				(if (null? pilot) 0 pilot)))
		(lambda (aircraft-id load-num load-id)
			(let ([preset (pilot-on-load load-id)])
				(string-cat "\n"
					(format #f header
						aircraft-id load-num aircraft-id load-num)
					"<option value=\"0\">select...</option>"
					(pg-map-rows
						(pg-exec (sdb) query)
						(lambda (row) (menu-option preset row)))
					"</select>")))))
(define copilots-menu
	(let ([query
				"select people.id as id, last_name,
						coalesce(goes_by, first_name) as first_name
					from people, load_sheets, load_slots
					where (people.id=passenger)
					and (passenger is not null)
					and (aircraft_id=~a)
					and (load_num=~a)
					and (load_date=~a)
					and (load_slots.load_id=load_sheets.id)
					order by last_name, first_name"]
			[header "<select class=\"pilot-menu\"
						onchange=\"log_pilot(this,~d,~d,'copilot')\">"]
			[option "<option value=\"~d\"~a>~a, ~a</option>"])
		(define (menu-option preset row)
			(let ([id (pg-cell row 'id)])
				(format #f option
					id
					(if (= id preset) " selected=\"selected\"" "")
					(pg-cell row 'last_name)
					(pg-cell row 'first_name))))
		(lambda (aircraft-id load-num load-date)
			(let ([preset
					(copilot-on-load aircraft-id load-date load-num)])
				(string-cat "\n"
					(format #f header aircraft-id load-num)
					"<option value=\"0\">none</option>"
					(pg-map-rows
						(pg-exec (sdb) query
							aircraft-id load-num load-date)
						(lambda (row)
							(menu-option
								(if (null? preset) 0 preset) row)))
					"</select>")))))
(define (compute-cg weight-bal)
	; total moment / total weight
	(let ([weight (cdar weight-bal)])
		(cons
			(and (caar weight-bal) (cadr weight-bal))
			(if (= weight 0) "N/A "
				(format #f "~1,1f"
					(/ (to-f (cddr weight-bal)) weight))))))
(define detail-dialog
	; pop-down set-load-call dialog
	; opened with "set call" button
	(let ([html
				(deflate "<div class=\"call-dialog\">
				<input type=\"text\"
					style=\"width: 3em; text-align: right\"
					value=\"[[REMAIN]]\"
					id=\"calltime[[SUFFIX]][[AIRCRAFT_ID]]_[[LOAD]]\"/>min
				<input type=\"button\" style=\"width: 5em\"
					value=\"SET\" title=\"set load call\"
					class=\"rounded-corners-5\"
					onclick=\"set_call('[[AIRCRAFT_ID]]',[[LOAD]],'[[SUFFIX]]')\"/>
				<input type=\"button\" style=\"width: 5em\"
					value=\"ABORT\" title=\"abort load call\"
					class=\"rounded-corners-5\"
					onclick=\"abort_call('[[AIRCRAFT_ID]]',[[LOAD]],'[[SUFFIX]]')\"/>
				<div>
				<input type=\"radio\"
					name=\"call-det[[AIRCRAFT_ID]]_[[LOAD]]\"
					onclick=\"qualify('[[AIRCRAFT_ID]]',[[LOAD]],'hot')\"
					value=\"hot\"[[CHECK_HOT]]/>Hot
				<input type=\"radio\"
					name=\"call-det[[AIRCRAFT_ID]]_[[LOAD]]\"
					onclick=\"qualify('[[AIRCRAFT_ID]]',[[LOAD]],'fuel')\"
					value=\"fuel\"[[CHECK_FUEL]]/>Fuel
				<input type=\"radio\"
					name=\"call-det[[AIRCRAFT_ID]]_[[LOAD]]\"
					onclick=\"qualify('[[AIRCRAFT_ID]]',[[LOAD]],'shut')\"
					value=\"shut\"[[CHECK_SHUT]]/>Shutdown
				</div><div>
				<div style=\"margin-top: 2px\">
				<table>
				<tr>
				<td align=\"right\">
				<abbr title=\"fuel aboard at take-off\">Fuel</abbr>:</td>
				<td><input type=\"text\"
					id=\"fuelob[[AIRCRAFT_ID]]_[[LOAD]]\"
					value=\"[[FUEL]]\"
					onchange=\"load_fuel_change(this,[[AIRCRAFT_ID]],[[LOAD]])\"
					style=\"width: 3em; text-align: right\"/>gal
				<input type=\"hidden\"
					id=\"fobret[[AIRCRAFT_ID]]_[[LOAD]]\"
					value=\"[[FUEL]]\"/>
				</td></tr>
				<tr><td align=\"right\">Pilot:</td>
					<td>[[PILOT_MENU]]</td></tr>
				<!-- tr><td align=\"right\">Co-pilot:</td>
					<td id=\"copilot[[AIRCRAFT_ID]]_[[LOAD]]\">[[COPILOT_MENU]]</td></tr -->
				<tr><td align=\"right\">Weight:</td>
					<td>
					<div id=\"loadwt[[AIRCRAFT_ID]]_[[LOAD]]\" style=\"float: left; width: 5em; text-align: right\">[[WEIGHT]]</div>
					<div style=\"float: left\">lbs</div></td></tr>
				<tr><td align=\"right\">CG:</td>
					<td>
					<div id=\"cg[[AIRCRAFT_ID]]_[[LOAD]]\" style=\"float: left; width: 5em; text-align: right\">[[CG]]</div>
					<div style=\"float: left\">in</div></td></tr>
				</table>
				</div>
				</div>
				</div>")]
			[no-cg "<abbr title=\"insufficient data; weights missing\">INS</abbr>"]
			[fuel-query
				"select fuel_at_takeoff as fuel
					from load_sheets
					where (id=~a)"])
		(define (get-fuel load-id)
			(let ([fuel
						(pg-cell
							(pg-one-row (sdb) fuel-query load-id) 'fuel)])
				(if (null? fuel) "" fuel)))
		(lambda (aircraft-id date load-num suffix)
			(let* ([load-id
						(get-load-id aircraft-id date load-num)]
					[qual (load-qualifier load-id)]
					[weight-bal (total-weight-balance load-id (sdb))])
				(fill-template html #f
					(cons 'remain
						(call-mins-remaining aircraft-id load-num))
					(cons 'aircraft_id aircraft-id)
					(cons 'load load-num)
					(cons 'suffix suffix)
					(cons 'cg
						(display-load-weight (compute-cg weight-bal)))
					(cons 'fuel (get-fuel load-id))
					(cons 'pilot_menu
						(pilots-menu aircraft-id load-num load-id))
					;(cons 'copilot_menu
					;	(copilots-menu aircraft-id load-num date))
					(cons 'weight
						(display-load-weight (car weight-bal)))
					(cons 'check_hot
						(if (eq? qual 'hot) " checked=\"checked\"" ""))
					(cons 'check_fuel
						(if (eq? qual 'fuel) " checked=\"checked\"" ""))
					(cons 'check_shut
						(if (eq? qual 'shut)
							" checked=\"checked\"" "")))))))
(define has-extras
	(let ([query
				"select count(*) as n
					from load_sheets, load_slots, slot_extras
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (slot_num=~a)
					and (slot_extras.slot_id=load_slots.id)"])
		(lambda (aircraft-id load-date load-num slot-num)
			(let ([row (pg-one-row (sdb) query
						aircraft-id load-date load-num slot-num)])
				(and row (> (pg-cell row 'n) 0))))))
(define has-observer-slot?
	(let ([query
				"select observer_slot
					from aircraft
					where (id=~a)"])
		(lambda (aircraft-id)
			(let ([row (pg-one-row (sdb) query aircraft-id)])
				(and row (pg-cell row 'observer_slot))))))
(define (render-loadsheet-row row load-date aircraft-id load-num
			need-top first-sheet multi)
	(let* ([status (find-load-status (pg-cell row 'status))]
			[cell
				(load-sheet-line aircraft-id load-num
					(pg-cell row 'slot_num)
					(pg-cell row 'passenger)
					(pg-cell row 'role)
					(pg-cell row 'reserved)
					(and (not (env-manifest-admin)) (pg-cell row 'locked))
					status
					(has-extras aircraft-id load-date load-num
						(pg-cell row 'slot_num)) multi)])
		(if need-top
			(string-cat "\n"
				(sheet-top aircraft-id load-date load-num
					status first-sheet (if multi "-all-" ""))
				(if (has-observer-slot? aircraft-id)
					(load-sheet-observer
						aircraft-id
						load-date
						load-num
						(not (env-manifest-admin))
						multi
						)
					""
					)
				cell) cell)))
(define first-sheet-today
	(let ([query
				"select min(load_num) as first
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=~a)"])
		(lambda (load-date aircraft-id)
			(let* ([row (pg-one-row (sdb) query aircraft-id load-date)]
					[first (pg-cell row 'first)])
				(if (null? first) 1 first)))))
(define get-load-sheet
	(let ([query
				"select slot_num, passenger, role, reserved, locked,
						load_sheets.status as status,
						load_sheets.slot_delta as delta
					from load_sheets, load_slots
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_sheets.id=load_slots.load_id)
					order by slot_num asc"]
			[delta-query
				"select id, slot_delta
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"]
			[decr-delta-query
				"update load_sheets set
					slot_delta=(slot_delta - ~a)
					where (id=~a)"]
			[empties-query
				"select id, (passenger is not null) as occupied
					from load_slots
					where (load_id=~a)
					order by slot_num desc"]
			[delete-empty
				"delete from load_slots where (id=~a);
				update load_sheets set
					slot_delta=(slot_delta + 1)
					where (id=~a)"])
		(define (decr-delta load-id slots)
			(pg-exec (sdb) decr-delta-query slots load-id))
		(define (truncate-empties load-id delta)
			; remove last N unoccupied load slots
			(let loop ([empties
							(pg-map-rows
								(pg-exec (sdb) empties-query
									load-id))]
						[count 0])
				(cond
					[(null? empties) 'done]
					[(pg-cell (car empties) 'occupied) 'done]
					[(>= count delta) 'done]
					[else
						(pg-exec (sdb) delete-empty
							(pg-cell (car empties) 'id)
							load-id)
						(loop (cdr empties) (1+ count))])))
		(define (check-slot-delta aircraft-id load-date load-num)
			; Track changes to aircraft jumper capacity.
			; Add or remove slots as needed to match that capacity.
			; Exception: occupied load slots are not removed.
			(let* ([row
						(pg-one-row (sdb) delta-query
							aircraft-id load-date load-num)]
					[delta (if row (pg-cell row 'slot_delta) 0)]
					[load-id (and row (pg-cell row 'id))])
				(cond
					[(> delta 0)
						(add-load-slots load-id delta)
						(decr-delta load-id delta)]
					[(< delta 0)
						(truncate-empties load-id (- delta))])))
		(lambda (load-date aircraft-id load-num first-sheet multi)
			(check-slot-delta aircraft-id load-date load-num)
			(let ([res (pg-exec (sdb) query aircraft-id load-date load-num)]
					[need-top #t])
				(and (> (pg-tuples res) 0)
					(string-cat "\n"
						(pg-map-rows res
							(lambda (row)
								(let ([cell
											(render-loadsheet-row row
												load-date aircraft-id
												load-num
												need-top first-sheet
												multi)])
									(set! need-top #f)
									cell)))
						"</table></div>"))))))
(define (range start finish)
	(let loop ([n start]
			[bag '()])
		(if (> n finish) (reverse bag)
			(loop (1+ n) (cons n bag)))))
(define empty-sheet
	(let ([query
				"select count(*) as n
					from load_slots
					where (load_id=~a)
					and (passenger is not null)"])
		(lambda (load-id dbh)
			(let ([row (pg-one-row dbh query load-id)])
				(= (pg-cell row 'n) 0)))))
(define get-load-sheet-row
	(let ()
		(define (sheet-html load-date aircraft-id
					load-num first-sheet multi)
			(or
				(get-load-sheet load-date aircraft-id
					load-num (= load-num first-sheet) multi) "&nbsp;"))
		(define (empty? aircraft-id load-date load-num)
			; empty load sheet?
			(empty-sheet
				(or (get-load-id aircraft-id load-date load-num) -1)
				(sdb)))
		(define (cell-data aircraft-id load-date
					load-num first-sheet multi)
			(list
				(cons 'html
					(sheet-html load-date aircraft-id
						load-num first-sheet multi))
				(cons 'id (load-id-key aircraft-id load-num))
				(cons 'empty (empty? aircraft-id load-date load-num))
				(cons 'marker
					(load-marker aircraft-id load-num))))
		(lambda (load-date aircraft-id start-load sheet-count multi)
			(let ([sheets
						(if (= sheet-count 0)
							(count-sheets aircraft-id load-date)
							sheet-count)]
					[end-load
						(+ (env-load-sheet-columns) start-load -1)]
					[first-sheet
						(first-sheet-today load-date aircraft-id)])
				(list
					(cons 'backup_timeout (* long-poll-interval 2))
					(cons 'admin (env-manifest-admin))
					(cons 'aircraft aircraft-id)
					(cons 'sheets sheets)
					(cons 'recent_load
						(load-marker aircraft-id
							(min end-load
								(recent-load aircraft-id load-date))))
					(cons 'cells
						(map
							(lambda (load-num)
								(cell-data aircraft-id load-date
									load-num first-sheet multi))
							(range start-load end-load)))
					(cons 'next
						(if (>= end-load (+ first-sheet sheets -1))
							0 (1+ end-load))))))))
(define assure-load-sheets
	(let ([query
				"select count(*) as n
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=~a)"]
			[mutex (make-mutex)])
		(define (bail key . params)
			(log-msg "ERR ASSURE SHEETS: ~s, ~s" key params))
		(lambda (aircraft-id load-date)
			(lock-mutex mutex)
			(catch #t
				(lambda ()
					(let ([row (pg-one-row (sdb) query
								aircraft-id load-date)])
						(and (< (pg-cell row 'n) 1)
							(make-sheets aircraft-id load-date
								(env-min-load-sheets))))) bail)
			(unlock-mutex mutex))))
(define help-button "\
<input type=\"button\" style=\"font-size: 83%; width: 5em\"
	value=\"Help[F1]\"
	class=\"rounded-corners-5\"
	onclick=\"manifest_help()\"/>")
(define stats-button
	(let ([template
				(deflate "<input type=\"button\"
					style=\"font-size: 83%; width: 5em\"
					value=\"Stats\"
					class=\"rounded-corners-5\"
					title=\"selected date's jump statistics\"
					onclick=\"open_stats('~a')\"/>")])
		(lambda (aircraft-id) (format #f template (to-s aircraft-id)))))
(define sign-in-button "\
<div>
<div class=\"admin-light\"
	style=\"float: right; width: 6em; font-size: 83%\"
	onclick=\"window.location='/login/manifest'\">SIGN IN</div>
<div style=\"float: right; margin-right: 1em; font-size: 75%\"><a href=\"/wiki\">Wiki</a></div>
</div>")
(define count-sheets
	(let ([query
				"select (max(load_num)-min(load_num)+1) as sheets
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=~a)"])
		(lambda (aircraft-id date)
			(let ([row (pg-one-row (sdb) query aircraft-id date)])
				(to-i (pg-cell row 'sheets))))))
(define add-sheets-btn
	(let ([html
				(deflate "<input type=\"button\"
					style=\"font-size: 83%; width: 6em\"
					value=\"+~d Loads\"
					id=\"addrow~d\"
					title=\"+ ~d load sheets = ~d\"
					class=\"rounded-corners-5\"
					onclick=\"add_sheets('~d','~a')\"/>")])
		(lambda (aircraft-id date)
			(let ([incr (env-load-sheet-columns)])
				(format #f html
					incr aircraft-id incr
					(+ (count-sheets aircraft-id date) incr)
					aircraft-id date)))))
(define make-printable-btn
	(let ([html
				(deflate "<input type=\"button\"
					style=\"font-size: 83%; width: 6em\"
					value=\"Print\"
					class=\"rounded-corners-5\"
					title=\"print these load sheets\"
					onclick=\"make_printable('~a')\"/>")])
		(lambda (aircraft) (format #f html (to-s aircraft)))))

(define date-form
	; date form at top of Manifest (read-only for kiosk)
	(let ([arrow-tpt
				(deflate "<a title=\"~a day\"
						style=\"text-decoration: none\"
						href=\"#\"
						onclick=\"date_change('~a',~d);return false\">&~a;</a>")]
			[admin-tpt
				(deflate "Date: [[LEFT_ARROW]] [[RIGHT_ARROW]]
				<input type=\"text\" id=\"date[[DATE_ID]]\" size=\"10\"
					class=\"rounded-corners-5 dp\"
					value=\"[[CURRENT_DATE]]\"
					onchange=\"date_change('[[DATE_CHANGE_ID]]',0)\" />
				<span id=\"day[[DAY_SPAN]]\">[[WEEKDAY]]</span>")]
			[kiosk-tpt
				(deflate "Date: [[CURRENT_DATE]]
				<input type=\"hidden\" id=\"date[[DATE_ID]]\"
					value=\"[[CURRENT_DATE]]\"/>
				<span id=\"day[[DAY_SPAN]]\">[[WEEKDAY]]</span>")])
		(define (arrow direction aircraft-id)
			(format #f arrow-tpt
				(if (eq? direction 'prev) "prev" "next")
				aircraft-id
				(if (eq? direction 'prev) -1 1)
				(if (eq? direction 'prev) "larr" "rarr")))
		(lambda (aircraft-id formatted-date weekday kiosk)
			(if kiosk
				(fill-template kiosk-tpt #f
					(cons 'current_date formatted-date)
					(cons 'day_span aircraft-id)
					(cons 'date_id aircraft-id)
					(cons 'weekday weekday))
				(fill-template admin-tpt #f
					(cons 'left_arrow (arrow 'prev aircraft-id))
					(cons 'right_arrow (arrow 'next aircraft-id))
					(cons 'date_change_id aircraft-id)
					(cons 'date_id aircraft-id)
					(cons 'day_span aircraft-id)
					(cons 'current_date formatted-date)
					(cons 'weekday weekday))))))
(define aircraft-man-tpt "\
<div class=\"aircraft-panel\" id=\"tabs-[[PANEL_ID]]\">
<div style=\"margin-top: 0.2em; font-size: 120%\">
<div style=\"float: left\">
[[DATE_FORM]]
[[HELP_BUTTON]]
[[STATS_BUTTON]]
[[ADD_SHEETS_BUTTON]]
[[MAKE_PRINTABLE_BUTTON]]
<span style=\"display: none\" class=\"search-track\"
	id=\"search-track[[PANEL_ID]]\"><abbr
	title=\"start typing to search names, left/right-arrow to step, esc to clear\">search</abbr>:
<span id=\"search-track-drop[[PANEL_ID]]\"></span></span>
</div>
[[SIGN_IN_BUTTON]]
<div style=\"clear: both\"></div>
</div>
<div style=\"float: left\">
<span class=\"big_nickname\">[[NICKNAME]]</span>
- [[MAKE]] [[MODEL]] <span class=\"tail_num\">[[[TAIL_NUM]]]</span>
</div>
<div id=\"loadcall[[LOADCALL_ID]]\" class=\"load_call\"></div>
<div style=\"clear: both\"></div>
<div id=\"sheets[[SHEET_ID]]\" class=\"load-sheets\">
</div>
</div>")
(define all-man-tpt "\
<div class=\"aircraft-panel\" id=\"tabs-all\">
<div style=\"margin-top: 0.2em; font-size: 120%\">
<div style=\"float: left\">
[[DATE_FORM]]
[[HELP_BUTTON]]
[[STATS_BUTTON]]
[[ADD_SHEETS_BUTTON]]
[[MAKE_PRINTABLE_BUTTON]]
<span style=\"display: none\" class=\"search-track\"
	id=\"search-trackall\"><abbr
	title=\"start typing to search names, left/right-arrow to step, esc to clear\">search</abbr>:
<span id=\"search-track-dropall\"></span></span>
</div>
[[SIGN_IN_BUTTON]]
<div style=\"clear: both\"></div>
</div>
<div class=\"load-sheets-all\" id=\"sheetsall\">
[[COLUMNS]]
</div>
</div>")
(define (aircraft-manifest logged-in date row kiosk)
	; build manifest page for given date and aircraft
	(let ([tail-num (to-s (pg-cell row 'tail_num))]
			[aircraft-id (pg-cell row 'id)]
			[formatted-date
				(show-date date (env-date-format))])
		(fill-template aircraft-man-tpt #f
			(cons 'date_form
				(date-form aircraft-id formatted-date
					(time-format date "%A") kiosk))
			(cons 'panel_id aircraft-id)
			(cons 'nickname (to-s (pg-cell row 'nickname)))
			(cons 'make (to-s (pg-cell row 'make)))
			(cons 'model (to-s (pg-cell row 'model)))
			(cons 'tail_num tail-num)
			(cons 'loadcall_id aircraft-id)
			(cons 'date_id aircraft-id)
			(cons 'aircraft_id aircraft-id)
			(cons 'current_date formatted-date)
			(cons 'date_change_id aircraft-id)
			(cons 'day_span aircraft-id)
			(cons 'help_button help-button)
			(cons 'stats_button (stats-button aircraft-id))
			(cons 'add_sheets_button
				(if (env-manifest-admin)
					(add-sheets-btn aircraft-id formatted-date) ""))
			(cons 'make_printable_button
				(if (env-manifest-admin)
					(make-printable-btn aircraft-id) ""))
			(cons 'sign_in_button (if logged-in "" sign-in-button))
			(cons 'sheet_id aircraft-id))))
(define all-manifest
	(let ([column-tpt
				(deflate "<div style=\"float: left; margin-left: 2px\">
				<div style=\"float: left\">
				<span class=\"big_nickname\">[[NICKNAME]]</span>
				<span class=\"tail_num\">[[[TAIL_NUM]]]</span>
				</div>
				<div id=\"loadcall-1-[[AIRCRAFT_ID]]\"
					class=\"sload_call\"></div>
				<div style=\"clear: both\"></div>
				<div id=\"sheets-all-[[AIRCRAFT_ID]]\"
					class=\"load-sheets-inline\">
				</div>
				</div>")])
		(define (column row)
			(fill-template column-tpt #f
				(cons 'nickname (pg-cell row 'nickname))
				(cons 'tail_num (pg-cell row 'tail_num))
				(cons 'aircraft_id (pg-cell row 'id))))
		(lambda (aircraft date logged-in kiosk)
			(let ([aircraft-id "all"]
					[formatted-date
						(show-date date (env-date-format))])
				(fill-template all-man-tpt #f
					(cons 'date_form
						(date-form aircraft-id formatted-date
							(time-format date "%A") kiosk))
					(cons 'date_id aircraft-id)
					(cons 'current_date formatted-date)
					(cons 'date_change_id aircraft-id)
					(cons 'day_span aircraft-id)
					(cons 'help_button help-button)
					(cons 'stats_button (stats-button aircraft-id))
					(cons 'add_sheets_button "")
					(cons 'make_printable_button
						(if (env-manifest-admin)
							(make-printable-btn aircraft-id) ""))
					(cons 'sign_in_button (if logged-in "" sign-in-button))
					(cons 'columns
						(string-cat "\n"
							(map (lambda (row) (column row))
								aircraft))))))))
(define active-aircraft-past-present
	(let ()
		(define active-aircraft
			(let ([query
						"select id from aircraft
							where active order by rank asc"])
				(lambda ()
					(pg-map-rows
						(pg-exec (sdb) query)
						(lambda (row) (pg-cell row 'id))))))
		(define aircraft-on-date
			(let ([query
						"select distinct aircraft_id as id
							from load_sheets
							where (load_date=~a)"])
				(lambda (load-date)
					(pg-map-rows
						(pg-exec (sdb) query load-date)
						(lambda (row) (pg-cell row 'id))))))
		(define (aircraft-on-board load-date)
			(let ([date-set (aircraft-on-date load-date)])
				(if (null? date-set) (active-aircraft) date-set)))
		(define (normalize-date stamp)
			(time-local
				(time-year stamp)
				(time-month stamp)
				(time-mday stamp) 12 0 0))
		(lambda (load-date)
			(if (> (time-diff (normalize-date (time-now))
						(normalize-date load-date)) 0)
				(aircraft-on-board load-date)
				(active-aircraft)))))
(define aircraft-tabs
	; populate aircraft tabs with manifest pages, one per active
	; aircraft
	(let ([detail-query
				"select nickname, make, model, tail_num, id,
						row_number() over(order by rank asc) as n
					from aircraft
					where id in (~a)
					and (capacity is not null)
					and (capacity > 0)
					and (nickname is not null)
					and (tail_num is not null)
					order by rank asc"]
			[aircraft-count-query
				"select count(*) as n from aircraft where active"]
			[no-aircraft
				"<div style=\"margin-top: 0.5em; margin: 1em\">
				You seem to have no <a href=\"/aircraft\">aircraft</a>
				defined, or they're all marked inactive. You'll need
				at least one.</div>"]
			[aircraft-tab-link
				"<li><a href=\"#tabs-~a\" id=\"atab~a\"
				class=\"tab-catcher\">~a
				<span id=\"tabload~a\">~a</span>
				<span id=\"tabcount~a\"></span></a></li>"])
		(define (count-active)
			(let ([row (pg-one-row (sdb) aircraft-count-query)])
				(pg-cell row 'n)))
		(define (aircraft-detail aircraft-ids)
			(if (null? aircraft-ids) '()
				(pg-map-rows
					(pg-exec (sdb)
						(format #f detail-query
							(string-cat "," aircraft-ids))))))
		(define (collect-aircraft-detail load-date)
			(aircraft-detail
				(active-aircraft-past-present load-date)))
		(define (tab row load-date)
			(let* ([aircraft-id (pg-cell row 'id)]
					[load-num (recent-load aircraft-id load-date)])
				(format #f aircraft-tab-link
					(to-s aircraft-id)
					(to-s (1- (pg-cell row 'n)))
					(to-s (pg-cell row 'nickname))
					(to-s aircraft-id)
					(if (< load-num 1) "" (format #f "[~d]" load-num))
					(to-s aircraft-id))))
		(define (all-tab)
			(format #f aircraft-tab-link
				"all" "all" "ALL" "all" "" "all"))
		(lambda (logged-in date kiosk)
			(let ([aircraft (collect-aircraft-detail date)]
					[include-all (> (count-active) 1)])
				(string-cat "\n"
					"<div class=\"rounded-corners-10\" id=\"tabs\"><ul>"
					(append
						(map (lambda (row) (tab row date)) aircraft)
						(if include-all (list (all-tab)) '()))
					"</ul>"
					(map
						(lambda (row)
							(aircraft-manifest logged-in date row kiosk))
						aircraft)
					(if include-all
						(list
							(all-manifest
								aircraft date logged-in kiosk)) '())
					"</div>"
					(if (> (length aircraft) 0) "" no-aircraft))))))
(define patched-script
	(make-doc (list scripts)
		(lambda ()
			(fill-template (fetch-doc scripts) #t
				(cons 'track_tag (uuid-generate))))))
(define composite
	(make-doc (list admin-page patched-script)
		(lambda ()
			(fill-template (fetch-doc admin-page) #t
				(cons 'version nowcall-version)
				(cons 'script (fetch-doc patched-script))
				(cons 'header "<h3 id=\"title\">Manifest</h3>")))))
(define kiosk-composite
	(make-doc (list kiosk-page patched-script)
		(lambda ()
			(fill-template (fetch-doc kiosk-page) #t
				(cons 'version nowcall-version)
				(cons 'script (fetch-doc patched-script))))))
(define already-on
	; passenger already on load?
	(let ([query
				"select passenger
					from load_sheets, load_slots
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_sheets.id=load_slots.load_id)
					and (passenger=~a)"])
		(lambda (pid aircraft-id date load-num)
			(or (pg-one-row (sdb) query aircraft-id date load-num pid)
				(let ([copilot
							(copilot-on-load aircraft-id date load-num)])
					(and (not (null? copilot)) (= copilot pid)))))))
(define (role-list roles) (format #f "'~a'" (string-cat "','" roles)))
(define most-recent-role
	; passenger's most recent role
	(let ([query
				"select role
					from load_slots
					where (passenger=~d)
					and (role in (~a))
					order by stamp desc
					limit 1"])
		(lambda (passenger eligible-roles)
			(let ([row
					(pg-one-row-primitive (sdb)
						(format #f query
							passenger
							(role-list eligible-roles)))])
				(and row (pg-cell row 'role))))))
(define role-counts
	; most recent role is weighted with an extra count
	(let ([query
				"select count(role) as n, role
					from ( select role
								from load_slots
								where (passenger=~d)
								and (role in (~a))
								order by stamp desc
								limit ~d
							) as roles
					group by role
					order by n desc"])
		(define (count-role row recent-role)
			(let ([role (pg-cell row 'role)])
				(cons role
					(+ (pg-cell row 'n)
						(if (string=? role recent-role) 1 0)))))
		(define (sort-by-freq a b) (< (cdr b) (cdr a)))
		(lambda (passenger recent-role window eligible-roles)
			(let ([res
					(pg-exec-primitive (sdb)
						(format #f query
							passenger
							(role-list eligible-roles)
							window))])
				(sort-list
					(pg-map-rows res
						(lambda (row) (count-role row recent-role)))
					sort-by-freq)))))
(define (trending-role passenger eligible-roles)
	; most-used jumper-eligible role for last N jumps
	(let ([recent-role (most-recent-role passenger eligible-roles)])
		(and recent-role
			(let ([counts
					(role-counts passenger recent-role
						role-trend-window eligible-roles)])
				(caar counts)))))
(define (board-role role pid)
	(if (string=? role "")
		(let ([eligible (passenger-products pid)])
			(or
				(trending-role pid eligible)
				(if (null? eligible) "none" (car eligible)))) role))
(define get-load-id
	(let ([query
				"select id
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"])
		(lambda (aircraft-id load-date load-num)
			(let ([row
						(pg-one-row (sdb) query
							aircraft-id load-date load-num)])
				(and row (pg-cell row 'id))))))
(define get-slot-id
	(let ([query
				"select load_slots.id as id
					from load_sheets, load_slots
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_sheets.id=load_slots.load_id)
					and (slot_num=~a)"])
		(lambda (aircraft-id load-date load-num slot-num)
			(let ([row
					(pg-one-row (sdb) query
						aircraft-id load-date load-num slot-num)])
				(and row (pg-cell row 'id))))))
(define track-condvar (make-condition-variable))
(define longpoller
	(longpoll-responder track-condvar long-poll-interval))
(define (release-track-sessions)
	; fire a long-poll interrupt to broadcast something newsworthy
	(broadcast-condition-variable track-condvar))
(define manifest-update
	(let ([query
				"update load_slots set
						passenger=~a,
						role=~a,
						reserved='f',
						locked=~a,
						stamp=current_timestamp
					where (id=~a)
					and (passenger is null)"])
		(lambda (passenger aircraft-id load-num
					slot-num load-date role extras)
			(pg-exec (sdb) query
				passenger
				(board-role role passenger)
				(env-manifest-admin)
				(get-slot-id aircraft-id load-date load-num slot-num))
			(for-each
				(lambda (extra)
					(add-slot-extra load-date
						(format #f "~d_~d_~d"
							aircraft-id
							load-num slot-num)
						(assq-ref extra 'item)
						(assq-ref extra 'xact)))
				extras)
			(release-track-sessions)
(log-msg "charge-slot: manifest-update")
			(charge-slot passenger load-date aircraft-id
				load-num slot-num) slot-num)))
(define db-sync
	; mutex-sync DB operation
	(let ([mutex (make-mutex)])
		(define (bail key . params)
			(log-msg "ERR DB-SYNC: ~s, ~s; request ~s"
					key params req-cache))
		(lambda (thunk)
			(let ([cache #f])
				(lock-mutex mutex)
				(catch #t (lambda () (set! cache (thunk))) bail)
				(unlock-mutex mutex) cache))))
(define alloc-load-slot
	; reserve next available slot on given load, or indicate
	; none available
	; If slot is spec'd (non-zero) and available, take it.
	; Otherwise, allocate next available.
	(let ([query
				"select min(slot_num) as slot
					from load_sheets, load_slots
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (not reserved)
					and (passenger is null)"]
			[slot-query
				"select slot_num
					from load_sheets, load_slots
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (slot_num=~a)
					and (not reserved)
					and (passenger is null)"])
		(define (slot-available aircraft-id load-date load-num slot-num)
			(let ([row (pg-one-row (sdb) slot-query
						aircraft-id load-date load-num slot-num)])
				(and row (pg-cell row 'slot_num))))
		(lambda (aircraft-id load-date load-num slot-num)
			(or
				(and
					(jump-slot slot-num)
					(slot-available aircraft-id load-date
						load-num slot-num))
				(let ([row (pg-one-row (sdb) query aircraft-id
							load-date load-num)])
					(and row (pg-cell row 'slot)))))))
(define load-status
	; load status: closed, open for self-manifest, gone (wheels-up)
	(let ([query
				"select status
					from load_sheets
					where (load_date=~a)
					and (aircraft_id=~a)
					and (load_num=~a)
					limit 1"])
		(lambda (date aircraft-id load-num)
			(let* ([row
						(pg-one-row (sdb) query
							date aircraft-id load-num)])
				(and row (find-load-status (pg-cell row 'status)))))))
(define (load-open? date aircraft-id load-num)
	; this load open for manifest (self-manifest or admin privileges)?
	(open-for-manifest? (load-status date aircraft-id load-num)))
(define load-full
	(let ([query
				"select count(*) as n
					from load_sheets, load_slots
					where (load_date=~a)
					and (aircraft_id=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and ((passenger is not null) or reserved)"])
		(lambda (load-date aircraft-id load-num additional)
			(let ([row
						(pg-one-row (sdb) query
							load-date aircraft-id load-num)])
				(> (+ (pg-cell row 'n) additional)
					(get-capacity aircraft-id))))))
(define valid-date?
	(let ([pat (make-regexp "^[-0-9/]+$")])
		(lambda (date)
			(and date (regexp-exec pat (to-s date))))))
(define (check-manifest aircraft-id load-num
			slot-num passengers load-date)
	(append
		(if (valid-date? load-date) '()
			(list (format #f "invalid load date: ~s" load-date)))
		(if (load-open? load-date aircraft-id load-num) '()
			(list (format #f "load ~d not open for manifest" load-num)))
		(if (and
				(not (observer-slot? slot-num))
				(load-full load-date aircraft-id
					load-num (length passengers)))
			(list (format #f "insufficient space on load ~d" load-num))
			'())
		(if (and (observer-slot? slot-num)
				(not (null?
						(copilot-on-load aircraft-id
							load-date load-num))))
			(list (format #f "observer slot on load ~d taken" load-num))
			'())
		(delete #f
			(map
				(lambda (passenger)
					(if (already-on passenger aircraft-id
							load-date load-num)
						(format #f "~a already on load ~d"
							(person-name passenger 'default (sdb))
							load-num)
						#f))
				passengers))))
(define set-load-pilot
	; set pilot or copilot for given load
	; If setting pilot, set for this load and all following loads
	; (this aircraft and date) for those loads that don't already
	; have a pilot set.
	(let ([pilot-query
				"update load_sheets set
					pilot=~a,
					stamp=current_timestamp
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num >= ~a)"]
			[copilot-query
				"update load_sheets set
					copilot=~a,
					stamp=current_timestamp
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"])
		(define (null-zero num) (if (= num 0) '() num))
		(lambda (aircraft-id load-date load-num pilot-id selector)
			(if (string=? selector "copilot")
				(pg-exec (sdb) copilot-query
					(null-zero pilot-id)
					aircraft-id load-date load-num)
				(pg-exec (sdb) pilot-query
					(null-zero pilot-id)
					aircraft-id load-date load-num)))))
(define charge-observer
	(let ([sheet-update
				"update load_sheets set
					observer_xact=~a
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"]
			[item-query
				"select cash_price as price,
						id as item_id,
						name as item
					from product_item
					where right_seat"])
		(lambda (observer aircraft-id load-date load-num)
			(let* ([detail (or (pg-one-row (sdb) item-query) '())]
					[xact-id
						(acct-charge-person
							observer
							(account-id "general" (sdb))
							(assq-ref detail 'price)
							(assq-ref detail 'item_id)
							(string-cat "" ; description
								(assq-ref detail 'item)
								", load " load-num ", "
								(aircraft-tail-num aircraft-id))
							load-date
							#t ; manifest
							"manifest" (env-login-id)
							(sdb))])
				(pg-exec (sdb) sheet-update
					xact-id aircraft-id load-date load-num)))))
(define manifest-observer
	(let ([drop-trans
				"update load_sheets set
					observer_xact=NULL
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"])
		(define (drop-transaction aircraft-id load-num load-date)
			(pg-exec (sdb) drop-trans aircraft-id load-date load-num))
		(lambda (passenger aircraft-id load-num load-date)
			(set-load-pilot aircraft-id load-date
				load-num passenger "copilot")
			(if (= passenger 0)
				(drop-transaction aircraft-id load-num load-date)
				(charge-observer passenger
					aircraft-id load-date load-num))
			(release-track-sessions) observer-slot)))
(define manifest-person
	(let ([passenger-query
				"select id
					from people
					where (id=~a)"])
		(define (passenger-valid? passenger)
			(and
				(> passenger 0)
				(pg-one-row (sdb) passenger-query passenger)))
		(lambda (aircraft-id load-num slot-num passenger
					load-date role extras)
			(and (passenger-valid? passenger)
				(if (and
						(or (observer-slot? slot-num)
							(observer-role? role))
						(has-observer-slot? aircraft-id)
						(null? (copilot-on-load aircraft-id
									load-date load-num)))
					(manifest-observer passenger aircraft-id
						load-num load-date)
					(manifest-update passenger aircraft-id
						load-num
						(alloc-load-slot aircraft-id load-date
							load-num slot-num)
						load-date role extras))))))
(define set-search-where
	; set where clause on basic search query
	(let ([base-query
				"select first_name, last_name, id,
						upper(coalesce(goes_by,first_name)) as fname,
						upper(last_name) as lname, disambiguate,
						exit_weight,
					((license is not null) and
						((last_repack is null) or
							(last_repack <
								(current_date - interval '~d days'))) and
						((last_repack_2 is null) or
							(last_repack_2 <
								(current_date - interval '~d days'))))
						as rod
					from people
					where ~a
					and active
					order by lname asc, fname asc"])
		(lambda (where-clause)
			(format #f base-query
				(env-repack-cycle)
				(env-repack-cycle)
				where-clause))))
(define make-search-query
	; construct people search according to clues in query expression
	(let ()
		(define (like-string src)
			(pg-format (sdb) (format #f "~a%" src)))
		(define try-uspa-id/phone
			; numeric ID search
			(let ([pat (make-regexp "([0-9]+)")]
					[clause
						"((right(phone_home, 4) like ~a) or
							(right(phone_mobile, 4) like ~a) or
							(uspa_id like ~a))"])
				(lambda (query)
					(let* ([match (regexp-exec pat query)]
							[phone-pat
								(and match
									(like-string
										(match:substring match 1)))])
						(and match
							(format #f
								(set-search-where clause)
								phone-pat phone-pat
								(like-string
									(match:substring match 1))))))))
		(define (search-query-f)
			; search on first name only
			(set-search-where
				"(upper(coalesce(goes_by,first_name)) like ~a)"))
		(define try-fname
			(let ([pat (make-regexp "([A-Z]+)[ ]+")])
				(lambda (query)
					(let ([match (regexp-exec pat query)])
						(and match
							(format #f (search-query-f)
								 (like-string
									(match:substring match 1))))))))
		(define (search-query-l)
			; search on last name only
			 (set-search-where "(upper(last_name) like ~a)"))
		(define try-lname
			(let ([pat (make-regexp "([A-Z]+)[ ]*,")])
				(lambda (query)
					(let ([match (regexp-exec pat query)])
						(and match
							(format #f (search-query-l)
								(like-string
									(match:substring match 1))))))))
		(define (search-query-fl)
			; search on first-last, or last-first
			(set-search-where "\
				(upper(last_name) like ~a)
				and (upper(coalesce(goes_by,first_name)) like ~a)"))
		(define try-fname-lname
			(let ([pat (make-regexp "([A-Z]+)[ ]+([A-Z]+)")])
				(lambda (query)
					(let ([match (regexp-exec pat query)])
						(and match
							(format #f (search-query-fl)
								(like-string (match:substring match 2))
								(like-string
									(match:substring match 1))))))))
		(define try-lname-fname
			(let ([pat (make-regexp "([A-Z]+)[ ]*,[ ]*([A-Z]+)")])
				(lambda (query)
					(let ([match (regexp-exec pat query)])
						(and match
							(format #f (search-query-fl)
								(like-string (match:substring match 1))
								(like-string
									(match:substring match 2))))))))
		(define (search-query)
			; general last-first-nick search
			(set-search-where "\
				((upper(last_name) like ~a) or
					(upper(first_name) like ~a) or
					(upper(goes_by) like ~a) or
					(upper(nickname) like ~a))"))
		(lambda (query)
			(let ([upcase (string-upcase query)])
				(or
					(try-uspa-id/phone upcase)
					(try-fname-lname upcase)
					(try-lname-fname upcase)
					(try-lname upcase)
					(try-fname upcase)
					(let ([term (like-string upcase)])
						(format #f (search-query)
							term term term term)))))))
(define search-results
	(let ()
		(define (old-name-mark-up name reserve-out exit-weight)
			(format #f "~a~a~a"
				(if reserve-out "!" "")
				(if (null? exit-weight) "#" "") name))
		(define (name-mark-up row)
			(let ([disambig (pg-cell row 'disambiguate)])
				(format #f "~a~a~a~a"
					(if (pg-cell row 'rod) "!" "")
					(if (null? (pg-cell row 'exit_weight)) "#" "")
					(person-name (pg-cell row 'id) (env-name-order) (sdb))
					(if (null? disambig) ""
						(format #f " [~a]" disambig)))))
		(define (person-match row)
			(let ([name (name-mark-up row)])
				(list
					(cons 'pid (pg-cell row 'id))
					(cons 'label name)
					(cons 'grounded
						(and (pg-cell row 'rod)
							(env-ground-reserve-out-of-date)))
					(cons 'value name))))
		(define (make-new query)
			(list
				(cons 'pid 0)
				(cons 'value query)
				(cons 'label "NEW RECORD")))
		(lambda (query include-new)
			(let ([res (pg-exec (sdb) (make-search-query query))])
				(if (< (pg-tuples res) 1)
					(list (make-new query))
					(append
						(pg-map-rows res person-match)
						(if include-new (list (make-new query)) '())))))))
(define search-panel-tpt "\
<div style=\"background-color: #945279; color: white;
	font-weight: bold; border: 1px solid #1c2931; padding: 2px\">
~a
</div>")
(define xact-id
	(let ([query
				"select acct_xact_id as transaction
					from load_sheets, load_slots
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (slot_num=~a)"]
			[obs-query
				"select observer_xact as transaction
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"])
		(define (slot-transaction aircraft-id load-date load-num slot-num)
			(pg-one-row (sdb) query
				aircraft-id load-date load-num slot-num))
		(define (observer-transaction aircraft-id load-date load-num)
			(pg-one-row (sdb) obs-query
				aircraft-id load-date load-num))
		(define (get-transaction aircraft-id load-date load-num slot-num)
			(let ([row
						(if (observer-slot? slot-num)
							(observer-transaction aircraft-id
								load-date load-num)
							(slot-transaction aircraft-id
								load-date load-num slot-num))])
				(and row (pg-cell row 'transaction))))
		(lambda (aircraft-id date load-num slot-num)
			(let ([transaction
						(get-transaction aircraft-id date
							load-num slot-num)])
				(and 
					transaction
					(not (null? transaction)) transaction)))))
(define last-occupied
	; highest slot number currently occupied on load
	(let ([query
				"select max(slot_num) as slot
					from load_sheets, load_slots
					where (load_date=~a)
					and (aircraft_id=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (passenger is not null)"])
		(lambda (load-date aircraft-id load-num)
			(let* ([row
						(pg-one-row (sdb) query
							load-date aircraft-id load-num)]
					[slot (pg-cell row 'slot)])
				(and (not (null? slot)) slot)))))
(define scratch-extras
	(let ([del-extras "delete from slot_extras where (slot_id=~a)"])
		(lambda (aircraft-id load-date load-num slot-num)
			(let ([extras
						(get-extras aircraft-id load-date
							load-num slot-num)])
				(pg-exec (sdb) del-extras
					(get-slot-id aircraft-id load-date
						load-num slot-num))
				(for-each
					(lambda (extra)
						(delete-transaction (assq-ref extra 'xact) (sdb)))
					extras)))))
(define scratch-slot
	(let ([query
				"update load_slots set
						passenger=NULL,
						role=NULL,
						reserved='f',
						locked='f',
						acct_xact_id=NULL,
						stamp=current_timestamp
					where (id=~a)"])
		(define (scratch-jump-slot aircraft-id load-date load-num slot-num)
			(let ([slot-id
						(get-slot-id aircraft-id load-date
							load-num slot-num)])
				(if slot-id
					(pg-exec (sdb) query slot-id)
					(log-msg "SCRATCH ERR: ~s"
						(list aircraft-id
							(if (string? load-date) load-date
								(time-format load-date "%Y-%m-%d"))
							load-num slot-num)))
				(scratch-extras aircraft-id load-date
					load-num slot-num)))
		(lambda (load-date load-num aircraft-id slot-num)
			(let ([transaction
						(xact-id aircraft-id load-date
							load-num slot-num)])
				(if (observer-slot? slot-num)
					(manifest-observer 0 aircraft-id load-num load-date)
					(scratch-jump-slot aircraft-id load-date
						load-num slot-num))
				(when transaction
					(delete-transaction transaction (sdb)))))))
(define slot-passenger
	(let ([query
				"select passenger
					from load_sheets, load_slots
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (slot_num=~a)"])
		(lambda (aircraft-id date load-num slot-num)
			(let* ([row (pg-one-row (sdb) query
							aircraft-id date load-num slot-num)]
					[passenger (and row (pg-cell row 'passenger))])
				(and row
					(not (null? passenger)) passenger)))))
(define set-role
	(let ([query
				"update load_slots set
						role=~a,
						stamp=current_timestamp
					where (id=~a)"])
		(define (re-charge-passenger aircraft-id load-date
					load-num slot-num new-role)
			(let ([passenger 
						(slot-passenger aircraft-id
							load-date load-num slot-num)])
				(if passenger
					(begin
						(pg-exec (sdb) query new-role
							(get-slot-id aircraft-id load-date
								load-num slot-num))
						(charge-slot passenger load-date aircraft-id
							load-num slot-num))
					(log-msg "ERROR no passenger ~s"
							(list aircraft-id load-date
								load-num slot-num new-role)))))
		(lambda (role aircraft-id load-date load-num slot-num)
			(log-msg "charge-slot: set-role")
			(db-sync
				(lambda ()
					(let ([old-transaction
								(xact-id aircraft-id
									load-date load-num slot-num)])
						(re-charge-passenger aircraft-id load-date
							load-num slot-num role)
						(if old-transaction
							(delete-transaction old-transaction (sdb))
							(log-msg "ERROR missing xact ~s"
									(list aircraft-id load-date
										load-num slot-num role))))))
			(release-track-sessions) role)))
(define reserve-slots
	(let ([query
				"update load_slots set
					reserved='t',
					locked='t',
					role=NULL
					where (load_id=~a)
					and (passenger is null)
					and (slot_num=~a);
				update load_slots set
					stamp=current_timestamp
					where (load_id=~a)
					and (slot_num=~a)"])
		(lambda (aircraft-id date load-num slots)
			(let ([load-id (get-load-id aircraft-id date load-num)])
				(for-each
					(lambda (slot)
						(pg-exec (sdb) query
							load-id slot
							load-id slot))
					slots)
				(release-track-sessions)))))
(define recent-load
	; compute ID of most recent load, to serve manifest
	; sheet auto-scroll
	(let ([query
				"select coalesce(max(load_num), 0) as load_num
					from load_sheets, load_slots
					where (load_date=~a)
					and (aircraft_id=~a)
					and (load_slots.load_id=load_sheets.id)
					and (passenger is not null)"])
		(lambda (aircraft-id load-date)
			(let ([row (pg-one-row (sdb) query load-date aircraft-id)])
				(or (and row (pg-cell row 'load_num)) 0)))))
(define (assemble-patch row)
	(let ([pid (pg-cell row 'passenger)]
			[role (pg-cell row 'role)]
			[status (find-load-status (pg-cell row 'status))]
			[load-num (pg-cell row 'load_num)]
			[slot-num (pg-cell row 'slot_num)]
			[aircraft-id (pg-cell row 'aircraft_id)]
			[load-date (time-format (pg-cell row 'load_date) "%Y-%m-%d")]
			[self-manifest #f])
		(set! self-manifest
			(and (eq? status 'open) (env-on-dz-lan)
				(not (observer-slot? slot-num))))
		(list
			(cons 'aircraft_id aircraft-id)
			(cons 'date load-date)
			(cons 'recent_load
				(recent-load aircraft-id load-date))
			(cons 'load load-num)
			(cons 'passenger pid)
			(cons 'slot (slot-num-tag slot-num))
			(cons 'status (load-status-button status))
			(cons 'closed (not self-manifest))
			(cons 'color (load-status-color status))
			(cons 'name_form
				(load-slot-name-content
					aircraft-id load-num slot-num
					pid (pg-cell row 'reserved)
					(has-extras
						aircraft-id
						load-date load-num slot-num) self-manifest))
			(cons 'locked (pg-cell row 'locked))
			(cons 'roles
				(if (null? role) '()
					(cons role (passenger-products pid)))))))
(define (load-marker aircraft-id load-num)
	; tag applied in load sheet array for auto-scrolling
	(if (= load-num 0) "none"
		(format #f "load~d_~d" aircraft-id load-num)))
(define patch-manifest
	(let ([query
				"select aircraft_id, load_date, load_num, slot_num,
						passenger, role, reserved, locked, status
					from load_sheets, load_slots
					where ((load_slots.stamp >= ~a) or
							(load_sheets.stamp >= ~a))
					and (load_slots.load_id=load_sheets.id)"]
			[obs-query
				"select aircraft_id, load_date, load_num,
						copilot as passenger, status,
						null as role,
						-1 as slot_num,
						'f'::boolean as reserved,
						'f'::boolean as locked
					from load_sheets
					where (load_sheets.stamp >= ~a)"])
		(define (observer-patches stamp)
			(let ([res (pg-exec (sdb) obs-query stamp)])
				(pg-map-rows res (lambda (row) (assemble-patch row)))))
		(lambda (last-refresh)
			(let* ([stamp (time-at last-refresh)]
					[res (pg-exec (sdb) query stamp stamp)])
				(list
					(cons 'refresh #t)
					(cons 'patches
						(append
							(observer-patches stamp)
							(pg-map-rows res
								(lambda (row)
									(assemble-patch row))))))))))
(define set-call
	; set load call
	(let ([query
				"update load_sheets set
					call_time=(current_timestamp + interval '~a min'),
					call_abort='f',
					call_tic=current_timestamp
					where (load_date=~a)
					and (aircraft_id=~a)
					and (load_num=~a)"])
		(lambda (date aircraft-id load-num mins)
			(pg-exec (sdb) query mins date aircraft-id load-num)
			(release-track-sessions))))
(define abort-call
	(let ([query
				"update load_sheets set
					call_abort='t',
					call_time=current_timestamp
					where (load_date=~a)
					and (aircraft_id=~a)
					and (load_num=~a)"])
		(lambda (date aircraft-id load-num)
			(pg-exec (sdb) query date aircraft-id load-num)
			(release-track-sessions))))
(define update-load-calls
	(let ([query
				"select call_time,
						aircraft.id as aircraft,
						load_num, call_abort
					from load_sheets, aircraft
					where (call_time is not null)
					and (aircraft.id=load_sheets.aircraft_id)
					and (call_time is not null)
					and (call_time >=
						(current_timestamp - interval '120 sec'))"])
		(define (call-time timestamp)
			(let ([diff
					(to-i
						(round (time-diff timestamp (db-time-now))))])
				(if (<= diff 0)
					0 ; "NOW"
					diff)))
		(define (process-call row)
			(let* ([stamp (pg-cell row 'call_time)]
					[diff (time-diff (db-time-now) stamp)])
				(list
					(cons 'drop
						(or (pg-cell row 'call_abort)
							(>= diff nowcall-sustain)))
					(cons 'remain (call-time stamp))
					(cons 'aircraft (pg-cell row 'aircraft))
					(cons 'load (pg-cell row 'load_num)))))
		(lambda ()
			(let ([res (pg-exec (sdb) query)])
				(pg-map-rows res
					(lambda (row) (process-call row)))))))
(define table-fold-up
	(let ([cell "<td style=\"white-space: nowrap\">~a</td>"])
		(define (top-row lists)
			(string-cat ""
				"<tr>"
				(map
				    (lambda (lst)
				        (if (null? lst) "<td></td>"
				            (format #f cell (car lst)))) lists)
				"</tr>"))
		(define (other-rows lists)
			(map
			    (lambda (lst) (if (null? lst) '() (cdr lst))) lists))
		(define (split lst rows)
			(let scan ([src lst]
			        [col '()]
			        [bag '()])
			    (cond
			        [(null? src) (reverse (cons (reverse col) bag))]
			        [(< (length col) rows)
			            (scan (cdr src) (cons (car src) col) bag)]
			        [else
			            (scan src '() (cons (reverse col) bag))])))
		(lambda (source-list max-rows)
			(let loop ([stack '()]
			        [cols (split source-list max-rows)])
			    (if (null? (car cols))
					(string-cat "\n"
						"<table>"
						(reverse stack)
						"</table>")
			        (loop
			            (cons (top-row cols) stack)
			                (other-rows cols)))))))
(define manifast
	; HTML: speed dial entries
	(let ([query
				"select distinct people.last_name as lname,
						coalesce(people.goes_by, people.first_name)
							as fname,
						load_slots.passenger as pid,
						count(*) as loads
					from load_sheets, load_slots, people, roles
					where (load_sheets.load_date <= ~~a)
					and (load_sheets.load_date >
							(date ~~a - integer '~~a'))
					and (load_slots.passenger is not null)
					and (load_slots.passenger=people.id)
					and (load_slots.load_id=load_sheets.id)
					and (people.id=roles.who)
					and (roles.role='LIC')
					group by last_name, first_name, passenger, goes_by
					order by ~a"]
			[html
				(deflate "<a href=\"#\"
					style=\"color: white\"
					onclick=\"speedman(~d,~d,'~d');return false\"
						>~a [~d]</a>")])
		(define (person-name last-name first-name order)
			(if (string=? order "first-last")
				(format #f "~a ~a" first-name last-name)
				(format #f "~a, ~a" last-name first-name)))
		(lambda (aircraft-id load-date load-num)
			(let ([res
						(pg-exec (sdb)
							(format #f query
								(if (string=?
										(env-name-order) "first-last")
									"fname asc, last_name asc"
									"last_name asc, fname asc"))
							load-date load-date
							(env-manifast-retention))])
				(table-fold-up
					(pg-map-rows res
						(lambda (row)
							(format #f html
								(pg-cell row 'pid)
								load-num
								aircraft-id
								(person-name
									(pg-cell row 'lname)
									(pg-cell row 'fname)
									(env-name-order))
								(pg-cell row 'loads))))
					manifast-max-rows)))))
(define charge-detail
	(let ([charge-det
				"select cash_price as revenue,
						id as item_id,
						name as item,
						payout
					from product_item
					where (manifest_symbol=~a)"]
			[role-alt
				"select role
					from load_sheets, load_slots
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (slot_num=~a)
					and (passenger is not null)"])
		(lambda (aircraft-id date load-num slot-num)
			(let* ([slot
						(pg-one-row (sdb) role-alt
							aircraft-id date load-num slot-num)]
					[key (and slot (pg-cell slot 'role))]
					[row
						(and key
							(pg-one-row (sdb) charge-det key))])
				(if row
					(list
						(cons 'price
							(* (pg-cell row 'revenue)
								(if (pg-cell row 'payout) -1 1)))
						(cons 'item_id (pg-cell row 'item_id))
						(cons 'item (pg-cell row 'item)))
					(list
						(cons 'price 0)
						(cons 'item_id 0)
						(cons 'item
							(if slot
								(pg-cell slot 'role) "unknown"))))))))
(define charge-slot
	(let ([query "update load_slots set acct_xact_id=~a where (id=~a)"])
		(lambda (pid date aircraft-id load-num slot-num)
			(let* ([detail
						(charge-detail aircraft-id date
							load-num slot-num)]
					[xact-id
						(acct-charge-person
							pid
							(account-id "general" (sdb))
							(if detail (assq-ref detail 'price) 0) ; amount
							(if detail (assq-ref detail 'item_id) 0)
							(if detail ; description
								(string-cat ""
									(assq-ref detail 'item)
									", load " load-num ", "
									(aircraft-tail-num aircraft-id))
									"")
							date
							#t ; manifest
							"manifest" (env-login-id)
							(sdb))])
				(log-msg "charge-slot ~s"
						(list pid xact-id date
							 aircraft-id load-num slot-num))
				(pg-exec (sdb) query
					xact-id
					(get-slot-id
						aircraft-id date load-num slot-num))))))
(define change-status
	(let ([query
				"update load_sheets set
						status=~a,
						departure=~a,
						stamp=current_timestamp
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"])
		(lambda (aircraft-id load-num date status)
			(pg-exec (sdb) query
				(load-status-button status)
				(if (eq? status 'gone) (time-now) '())
				aircraft-id date load-num)
			(release-track-sessions))))
; stats stuff
(define aircraft-stats-query "\
select tail_num as symbol,
		nickname as label
	from aircraft
	where active
	order by rank asc")
(define role-stats-query "\
select distinct role as symbol,
		role as label
	from load_slots, load_sheets
	where (passenger is not null)
	and (load_date=~a)
	and (load_sheets.id=load_slots.load_id)
	order by role asc")
(define cell-stats-query "\
select aircraft.tail_num as col,
		role as row,
		count(*) as n
	from load_sheets, load_slots, aircraft
	where (passenger is not null)
	and (load_sheets.id=load_slots.load_id)
	and (aircraft.id=load_sheets.aircraft_id)
	and (load_sheets.load_date=~a)
	group by aircraft.tail_num, role")
(define load-stats-query "\
select 'loads' as symbol,
		'Loads' as label")
(define load-count-stats-query "\
select aircraft.tail_num as col,
		'loads' as row,
		count(distinct cast(load_date as varchar) ||
			cast(load_num as varchar)) as n
	from load_sheets, load_slots, aircraft
        where (passenger is not null)
		and (load_sheets.id=load_slots.load_id)
		and (aircraft.id=load_sheets.aircraft_id)
		and (load_date=~a)
        group by aircraft.tail_num")
(define folk-names-query "\
select distinct passenger as symbol,
		(people.last_name || ', ' || people.first_name) as label
	from load_sheets, load_slots, people
	where (passenger is not null)
	and (load_sheets.id=load_slots.load_id)
	and (passenger=people.id)
	and (load_date=~a)
	order by label asc")
(define folk-stats-query "\
select aircraft.tail_num as col,
		passenger as row,
		count(*) as n
	from load_sheets, load_slots, aircraft
	where (passenger is not null)
	and (load_sheets.id=load_slots.load_id)
	and (aircraft.id=load_sheets.aircraft_id)
	and (load_date=~a)
	group by aircraft.tail_num, passenger")
(define stats-tpt "\
<div class=\"stats-box rounded-corners-10\">
<div class=\"stats-title\">Loads</div>
<div>~a</div>
<div class=\"stats-title\">Jump Types</div>
<div>~a</div>
<div class=\"stats-title\">People</div>
<div>~a</div>
</div>")
(define (people-cell count extra)
	(if extra 
		(format #f "[~a]: ~d"
			(string-cat ","
				(sort (json-decode extra) (lambda (a b) (< a b))))
			count)
		(to-s count)))
(define (manifest-stats date)
	; produce statistics panel
	(let ([db-date (pg-format (sdb) date)])
		(format #f stats-tpt
			(grid-html
				load-stats-query
				aircraft-stats-query
				(format #f load-count-stats-query db-date) #f (sdb))
			(grid-html
				(format #f role-stats-query db-date)
				aircraft-stats-query
				(format #f cell-stats-query db-date) #f (sdb))
			(grid-html
				(format #f folk-names-query db-date)
				aircraft-stats-query
				(format #f folk-stats-query db-date) people-cell (sdb)))))
(define clean-sheets
	(let ([del-sheet
				"delete from load_slots where (load_id=~a);
					delete from load_sheets where (id=~a);"]
			[scrub-whiteboard
				"update whiteboard set
					load_id=NULL
					where load_id=~a"]
			[old-sheets
				"select id, load_date, load_num,
						aircraft_id as aircraft
					from load_sheets
					where (stamp <=
						(current_timestamp - interval '2 months'))
					order by load_date asc,
						aircraft_id asc, load_num desc"]
			[set-gone
				"update load_sheets set
					status='GONE'
					where (status='OPEN')
					and (load_date < current_date)"]
			[busy (make-mutex)])
		(define (delete-sheet load-id)
			(pg-exec (sdb) scrub-whiteboard load-id)
			(pg-cmd-tuples (pg-exec (sdb) del-sheet load-id load-id)))
		(define (sheet-key row)
			(format #f "~a:~d"
				(time-format (pg-cell row 'load_date) "%Y-%m-%d")
				(pg-cell row 'aircraft)))
		(define (eraser)
			(let ([res (pg-exec (sdb) old-sheets)]
					[count 0]
					[keep-empties #f]
					[key-track ""])
				(pg-each-row res
					(lambda (row)
						(let ([key (sheet-key row)]
								[load-id (pg-cell row 'id)])
							(unless (string=? key-track key)
								(set! keep-empties #f)
								(set! key-track key))
							(if (empty-sheet load-id (sdb))
								(unless keep-empties
									(set! count
										(+ count
											(delete-sheet load-id))))
								(set! keep-empties #t)))))
				(pg-exec (sdb) set-gone)
				(log-msg "discard ~d empty load sheets" count) count))
		(define (sync-eraser)
			(when (try-mutex busy)
				(eraser)
				(unlock-mutex busy)))
		(lambda () (call-with-new-thread sync-eraser))))
(define click-names-note "\
<div style=\"text-align: center; font-size: 75%\">
click names to manifest:
</div>")
(define db-time-now
	(let ([query "select current_timestamp as stamp"])
		(lambda () (pg-cell (pg-one-row (sdb) query) 'stamp))))
(define (db-epoch-now) (time-epoch (db-time-now)))
(define (max-stamp query)
	(let* ([row (pg-one-row (sdb) query)]
			[stamp (pg-cell row 'stamp)])
		(and (not (null? stamp)) (time-epoch stamp))))
(define refresh-page
	(let ()
		(define (latest-page-touch)
			(max-stamp "select max(stamp) as stamp from page_touch"))
		(lambda (req tag client-stamp)
			(let ([page-touch (latest-page-touch)])
				(and
					page-touch
					(> page-touch client-stamp)
					(list
						(cons 'cmd "repaint")
						(cons 'tag tag)))))))
(define patch-page
	(let ([latest-slot "select max(stamp) as stamp from load_slots"]
			[latest-sheet "select max(stamp) as stamp from load_sheets"])
		(define (latest-event)
			(let ([slot (pg-cell (pg-one-row (sdb) latest-slot) 'stamp)]
					[sheet
						(pg-cell (pg-one-row (sdb) latest-sheet) 'stamp)])
				(cond
					[(and (null? slot) (null? sheet)) #f]
					[(null? slot) (time-epoch sheet)]
					[(null? sheet) (time-epoch slot)]
					[(>= (time-diff slot sheet) 0) (time-epoch slot)]
					[else (time-epoch sheet)])))
		(lambda (req tag client-stamp)
			(let ([sheet-touch (latest-event)])
				(and
					sheet-touch
					(> sheet-touch client-stamp)
					(append
						(list
							(cons 'cmd "patch")
							(cons 'tag tag)
							(cons 'admin (env-manifest-admin))
							(cons 'qualifier "")
							(cons 'load_calls (update-load-calls)))
						(patch-manifest client-stamp)))))))
(define refresh-calls
	(let ()
		(define (latest-call-tic)
			(max-stamp
				"select max(call_tic) as stamp from load_sheets"))
		(lambda (req tag client-stamp)
			(let ([latest-call (latest-call-tic)])
				(and
					latest-call
					(> latest-call client-stamp)
					(list
						(cons 'cmd "calls-only")
						(cons 'tag tag)
						(cons 'refresh #f)
						(cons 'load_calls (update-load-calls))))))))
(define load-qualifiers
	(let ([query
				"select qualifier, aircraft.id as aircraft_id, load_num
					from load_sheets, aircraft
					where (qualifier_change >= ~a)
					and (aircraft.id=load_sheets.aircraft_id)"])
		(define (id-button row)
			(list
				(cons 'id
					(format #f "~d_~d"
						(pg-cell row 'aircraft_id)
						(pg-cell row 'load_num)))
				(cons 'button
					(qualifier-button (pg-cell row 'qualifier)))))
		(lambda (last-refresh)
			(let ([res (pg-exec (sdb) query (time-at last-refresh))])
				(pg-map-rows res id-button)))))
(define (latest-qualifier)
	(max-stamp
		"select max(qualifier_change) as stamp from load_sheets"))
(define (refresh-qualifiers req tag client-stamp)
	(let ([latest (latest-qualifier)])
		(and
			latest
			(> latest client-stamp)
			(list
				(cons 'cmd "update-qualifiers")
				(cons 'tag tag)
				(cons 'qualifiers (load-qualifiers client-stamp))))))
(define (no-event tag)
	(list
		(cons 'status #t)
		(cons 'timeout #t)
		(cons 'payload
			(list (cons 'tag tag)))))
(define (pending-update req tag)
	(let ([client-stamp (query-value-number req 'time_ref)])
		(or
			(refresh-page req tag client-stamp)
			(patch-page req tag client-stamp)
			(refresh-qualifiers req tag client-stamp)
			(refresh-calls req tag client-stamp))))
(define request-date
	; loadsheet date requested in URL
	(let ([pat
				(make-regexp
					"([0-9][0-9][0-9][0-9])([0-9][0-9])([0-9][0-9])")])
		(define (publish req)
			(or
				(env-manifest-admin)
				(env-pubsheets)))
		(lambda (req)
			(let ([match (regexp-exec pat (assq-ref req 'path-info))])
				(if (and match (publish req))
					(time-local
						(to-i (match:substring match 1))
						(to-i (match:substring match 2))
						(to-i (match:substring match 3)) 12 0 0)
					(time-now))))))
(define get-extras
	(let ([query
				"select slot_extras.item_id as item_id,
						slot_extras.acct_xact_id as xact_id
					from load_sheets, load_slots, slot_extras
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (load_slots.slot_num=~a)
					and (slot_extras.slot_id=load_slots.id)"])
		(lambda (aircraft-id load-date load-num slot-num)
			(let ([res
					(pg-exec (sdb) query
						aircraft-id load-date load-num slot-num)])
				(pg-map-rows res
					(lambda (row)
						(list
							(cons 'item (pg-cell row 'item_id))
							(cons 'xact (pg-cell row 'xact_id)))))))))
(define get-pid
	(let ([query
				"select passenger, role
					from load_sheets, load_slots
					where (load_date=~a)
					and (aircraft_id=~a)
					and (load_num=~a)
					and (load_slots.load_id=load_sheets.id)
					and (slot_num=~a)"])
		(lambda (date aircraft-id load-num slot-num)
			(let ([row
					(pg-one-row (sdb) query
						date aircraft-id load-num slot-num)])
				(list
					(pg-cell row 'passenger)
					(pg-cell row 'role)
					(get-extras aircraft-id date
						load-num slot-num))))))
(define (int-list csv-string)
	(map (lambda (int) (to-i int)) (string-split (or csv-string "") #\,)))
(define (string-list csv-string) (string-split (or csv-string "") #\,))
(define (board-reject errs load-date
			aircraft-id load-num slot-num)
	(list ; manifest restrictions/errors
		(cons 'status #f)
		(cons 'msg (html-ul errs))
		(cons 'slot_key
			(string-cat "_" aircraft-id (to-s load-num) (to-s slot-num)))
		(cons 'form
			(blank-slot-form aircraft-id load-num slot-num
				(not
					(load-open? load-date aircraft-id load-num)) ""))))
(define (board-accept passengers roles extras
			dst-aircraft src-aircraft
			load-num slot-num load-date)
	(let ([slots
				(map
					(lambda (pid role extra)
						(manifest-person
							dst-aircraft
							load-num slot-num pid load-date
							(board-role role pid) (or extra '())))
					passengers roles extras)])
		(list
			(cons 'status #t)
			(cons 'load_id (load-id-key dst-aircraft load-num))
			(cons 'admin (env-manifest-admin))
			(cons 'dst_aircraft dst-aircraft)
			(cons 'src_aircraft src-aircraft)
			(cons 'load_num load-num)
			(cons 'slot_keys
				(map
					(lambda (slot)
						(string-cat "_"
							dst-aircraft
							(to-s load-num)
							(slot-num-tag slot)))
					(filter identity slots)))
			(cons 'roles
				(map
					(lambda (pid role)
						(cons
							(board-role role pid)
							(passenger-products pid)))
					passengers roles))
			(cons 'forms
				(map
					(lambda (slot pid)
						(filled-slot-form dst-aircraft load-num
							slot pid #f))
					slots passengers)))))
(define slot-shift
	(let ([query
				"update load_slots set
					slot_num=0,
					stamp=current_timestamp
					where (load_id=~a)
					and (slot_num=~a);
				update load_slots set
					slot_num=(slot_num + ~a),
					stamp=current_timestamp
					where (load_id=~a)
					and (slot_num >= ~a)
					and (slot_num < ~a);
				update load_slots set
					slot_num=~a,
					stamp=current_timestamp
					where (load_id=~a)
					and (slot_num=0);"])
		(lambda (load-date aircraft-id load-num src-slot dst-slot)
			(let ([move-back (>= src-slot dst-slot)]
					[load-id
						(get-load-id aircraft-id load-date load-num)])
				(pg-exec (sdb) query
					; update 1
					load-id src-slot ; update 1
					; update 2
					(if move-back 1 -1)
					load-id
					(if move-back dst-slot (1+ src-slot))
					(if move-back src-slot (1+ dst-slot))
					; update 3
					dst-slot load-id
					; update slot extras
					dst-slot load-date aircraft-id load-num src-slot)))))
(define sink-empty-slot
	(let ([query
				"select slot_num
					from load_slots
					where (load_id=~a)
					and (passenger is null)
					and (slot_num < ~a)
					order by slot_num asc
					limit 1"])
		(lambda (load-date aircraft-id load-num)
			(let* ([end-occ
						(last-occupied load-date aircraft-id load-num)]
					[row
						(and end-occ
							(pg-one-row (sdb) query
								(get-load-id aircraft-id
									load-date load-num)
								end-occ))])
				(and row
					(slot-shift load-date aircraft-id load-num
						(pg-cell row 'slot_num) end-occ) #t)))))
(define (scratch-em slots load-date aircraft-id load-num)
	(db-sync
		(lambda ()
			(for-each ; scratch slots
				(lambda (slot)
					(scratch-slot load-date load-num aircraft-id slot))
				slots)
			(let loop ( ; consolidate empties to bottom of load
					[sunk
						(sink-empty-slot load-date aircraft-id load-num)])
				(when sunk
					(loop
						(sink-empty-slot
							load-date aircraft-id load-num))))))
		(release-track-sessions))
(define increment-loads
	(let ([query
				"update load_sheets set
					load_num = (load_num + ~a)
					where (aircraft_id=~a)
					and (load_date=~a)"])
		(lambda (new-first-load old-first-load aircraft-id load-date)
			(pg-exec (sdb) query
				(- new-first-load old-first-load)
				aircraft-id load-date))))
(define (clear-page-touch)
	; wipe page-touch stamps
	; (used to trigger long-poll responses)
	(pg-exec (sdb) "delete from page_touch"))
(define occupied-dates
	(let ([query
				"select distinct load_date as date
					from load_sheets, load_slots
					where (passenger is not null)
					and (load_slots.load_id=load_sheets.id)"])
		(define (date-pair row)
			(cons
				(string->symbol
					(time-format (pg-cell row 'date) "%Y%m%d")) #t))
		(lambda ()
			(let ([res (pg-exec (sdb) query)])
				(pg-map-rows res date-pair)))))
(define item-detail
	(let ([query
				"select cash_price,
						product_category.name as category,
						product_item.name as item_name
					from product_item, product_category
					where (product_item.id=~a)
					and (product_item.category=product_category.id)"])
		(lambda (item-id)
			(pg-one-row (sdb) query item-id))))
(define manifest-extras
	(let ([query
				"select id, name
					from product_item
					where manifest_extra
					order by name asc"])
		(define (name-id row)
			(list
				(cons 'name (pg-cell row 'name))
				(cons 'id (pg-cell row 'id))))
		(lambda ()
			(pg-map-rows (pg-exec (sdb) query) name-id))))
(define role-extras
	(let ([query
				"select offer_extras
					from product_item
					where (manifest_symbol is not null)
					and (manifest_symbol=~a)"])
		(lambda (role)
			(let ([row (pg-one-row (sdb) query role)])
				(and row (pg-cell row 'offer_extras))))))
(define (context-menu-extras slot-ids roles jumpers dates extras)
	(map
		(lambda (slot role jumper date)
			(list
				(cons 'jumper jumper)
				(cons 'slot_id slot)
				(cons 'date date)
				(cons 'extras
					(and
						(not (null? extras))
						(role-extras role)
						extras))))
		slot-ids roles jumpers dates))
(define add-slot-extra
	(let ([query
				"insert into slot_extras (slot_id, acct_xact_id, item_id)
					values (~a, ~a, ~a);
				update load_slots set
						stamp=current_timestamp
					where (id=~a);"]
			[slot-spec-pat (make-regexp "([0-9]+)_([0-9]+)_([0-9]+)$")])
		(lambda (load-date slot-spec item-id acct-xact)
			(let* ([match (regexp-exec slot-spec-pat slot-spec)]
					[aircraft-id
						(and match (to-i (match:substring match 1)))]
					[load-num
						(and match (to-i (match:substring match 2)))]
					[slot-num
						(and match (to-i (match:substring match 3)))]
					[slot-id
						(and match
							(get-slot-id aircraft-id load-date
									load-num slot-num))])
				(when match
					(pg-exec (sdb) query
						slot-id acct-xact item-id slot-id)
					(release-track-sessions))))))
(define slot-with-extras
	(let ([query
				"select aircraft_id, load_date, load_num, slot_num
					from slot_extras, load_slots, load_sheets
					where (slot_extras.acct_xact_id=~a)
					and (load_slots.id=slot_extras.slot_id)
					and (load_sheets.id=load_slots.load_id)"])
		(lambda (xact-id) (pg-one-row (sdb) query xact-id))))
(define drop-extra
	(let ([query
				"delete from slot_extras
					where (acct_xact_id=~a);
				update load_slots set
						stamp=current_timestamp
					where (id=~a)"])
		(lambda (xact-id load-date aircraft-id load-num slot-num)
			(pg-exec (sdb) query
				xact-id
				(get-slot-id aircraft-id load-date load-num slot-num))
			(release-track-sessions)
			(list (cons 'status #t)))))
(define set-qualifier
	(let ([query
				"update load_sheets set
					qualifier=~a,
					qualifier_change=current_timestamp
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"])
		(lambda (qualifier aircraft-id load-date load-num)
			(pg-exec (sdb) query
				qualifier aircraft-id load-date load-num))))
(define strip-exit-weight
	; search query term: strip everything after and including "#"
	(let ([pat (make-regexp "^([^#]*)")])
		(lambda (query)
			(let ([match (regexp-exec pat query)])
				(if match (match:substring match 1) query)))))
(define set-exit-weight
	(let ([query
				"update people set
					exit_weight=~a
					where (id=~a)"])
		(lambda (person-id weight)
			(pg-exec (sdb) query weight person-id))))
(define get-max-fuel
	(let ([query
				"select fuel_capacity
					from aircraft
					where (id=~a)"])
		(lambda (aircraft-id)
			(let ([fuel
						(pg-cell (pg-one-row (sdb) query aircraft-id)
							'fuel_capacity)])
				(and (not (null? fuel)) fuel)))))
(define set-load-fuel
	; set fuel level for load
	; If aircraft burn rate (gal/load) is known, set fuel
	; levels for subsequent loads as well.
	(let ([blank-fuel
				"update load_sheets set
					fuel_at_takeoff=NULL
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num=~a)"]
			[load-ids-query
				"select id
					from load_sheets
					where (aircraft_id=~a)
					and (load_date=~a)
					and (load_num >= ~a)
					order by load_num"]
			[fuel-burn-query
				"select fuel_burn
					from aircraft
					where (id=~a)"]
			[set-fuel-query
				"update load_sheets set
					fuel_at_takeoff=~a
					where (id=~a)"])
		(define (set-fuel load-ids start-fuel burn)
			(let loop ([ids load-ids]
					[fuel start-fuel])
				(unless (or (null? ids) (<= fuel 1))
					(pg-exec (sdb) set-fuel-query fuel (car ids))
					(loop (cdr ids) (- fuel burn)))))
		(define (load-ids aircraft-id load-date load-num)
			(pg-map-rows
				(pg-exec (sdb) load-ids-query
					aircraft-id load-date load-num)
				(lambda (row) (pg-cell row 'id))))
		(lambda (aircraft-id load-date load-num gals)
			(if (<= gals 0)
				(pg-exec (sdb) blank-fuel
					aircraft-id load-date load-num)
				(let ([burn
						(pg-cell
							(pg-one-row (sdb) fuel-burn-query
								aircraft-id) 'fuel_burn)])
					(set-fuel
						(load-ids aircraft-id load-date load-num)
						gals (if (null? burn) gals burn)))))))
(define set-ip-response "\
<p>
Thanks! The DZ's effective network address has been set to
<b>~a</b>.
</p>")
(define (unset-dz-ip-addr)
	; unset DZ's current internet address, forcing re-query
	(set-setting 'dz-ip-addr null-ip-address (sdb)))
(define (publish-manifest) (env-on-dz-lan))
(define (payload req tag)
	(let ([update (pending-update req tag)])
		(and update
			(list
				(cons 'status #t)
				(cons 'timeout #f)
				(cons 'payload update)))))
(define aircraft-row-groups
	(let ([query
				"select distinct aircraft_id as id
					from load_sheets, load_slots, aircraft
					where ((load_date=~a) or aircraft.active)
					and (load_slots.passenger is not null)
					and (load_slots.load_id=load_sheets.id)
					and aircraft_id=aircraft.id"])
		(define (aircraft-group tab-id load-date)
			(if (string=? tab-id "all")
				(active-aircraft-past-present (parse-date load-date))
				(list (to-i tab-id))))
		(lambda (tab-id sheets load-date start-load req)
			(map
				(lambda (aircraft-id)
					(when (= sheets 0)
						(assure-load-sheets aircraft-id load-date))
					(get-load-sheet-row load-date aircraft-id
						(if (= start-load 0)
							(first-sheet-today load-date aircraft-id)
							start-load)
						sheets
						(string=? tab-id "all")))
				(aircraft-group tab-id load-date)))))
(define observer-role?
	(let ([query
				"select id
					from product_item
					where right_seat
					and manifest_apropos
					and (manifest_symbol=~a)"])
		(lambda (role) (pg-one-row (sdb) query role))))
(define (client-ip-addr req)
	(or (assq-ref req 'x-forwarded-for) null-ip-address))

(log-to "/var/log/nowcall/manifest.log")

(cron-add 3 0 (lambda (now) (clean-sheets)))

(clean-sheets)
(clear-page-touch)
(http-html "/"
	; main page
	(lambda (req)
		(cond
			[(logged-in req)
				(fill-template (fetch-doc composite) #f
					(cons 'dz_full_name (env-dz-full-name))
					(cons 'keypad_checked
						(if (session-get req 'keypad)
							" checked=\"checked\"" ""))
					(cons 'tabs
						(aircraft-tabs #t (request-date req) #f))
					(cons 'logout (admin-light req (sdb))))]
			[(publish-manifest) (redirect-html "/manifest/kiosk")]
			[else (redirect-html "/login/manifest")])))
(http-html "/kiosk"
	; manifest-only kiosk
	(lambda (req)
		(cond
			[(publish-manifest)
				(fill-template (fetch-doc kiosk-composite) #f
					(cons 'dz_full_name (env-dz-full-name))
					(cons 'keypad_checked
						(if (session-get req 'keypad)
							" checked=\"checked\"" ""))
					(cons 'tabs
						(aircraft-tabs #f (request-date req) #t)))]
			[else (redirect-html "/login/manifest")])))
(http-json "/search"
	; live search panel
	(lambda (req)
		(let ([query (to-s (query-value req 'term))]
				[allow-new (query-value-boolean req 'allow_new)])
			(search-results (strip-exit-weight query) allow-new))))
(http-json "/board"
	; manifest one or more
	(lambda (req)
		(set! req-cache (cons "board" req))
		(let ([pids (int-list (query-value req 'ids))]
				[roles (string-list (query-value req 'roles))]
				[extras (or (json-decode (query-value req 'extras)) '())]
				[src-aircraft (query-value-number req 'src_aircraft)]
				[dst-aircraft (query-value-number req 'dst_aircraft)]
				[load-num (query-value-number req 'load)]
				[slot-num (query-value-number req 'slot)]
				[load-date (query-value req 'date)])
			(db-sync
				(lambda ()
					(let ([errs
								(check-manifest
									dst-aircraft load-num slot-num pids
									load-date)])
						(if (null? errs)
							(board-accept pids roles extras
								dst-aircraft src-aircraft
								load-num slot-num load-date)
							(board-reject errs load-date
								dst-aircraft load-num slot-num))))))))
(http-json "/scratch"
	; scratch load slots
	(lambda (req)
		(set! req-cache (cons "scratch" req))
		(let ([load-date (query-value req 'date)]
				[load-num (query-value-number req 'load)]
				[aircraft-id (query-value-number req 'aircraft)]
				[slots (int-list (query-value req 'slots))])
			(if (load-open? load-date aircraft-id load-num)
				(begin
					(scratch-em slots load-date aircraft-id load-num)
					(list
						(cons 'status #t)
						(cons 'emptied
							(if
								(empty-sheet
									(get-load-id aircraft-id
										load-date load-num) (sdb))
								(load-id-key aircraft-id load-num) ""))
						(cons 'forms
							(map
								(lambda (slot)
									(blank-slot-form
										aircraft-id
										load-num slot #f ""))
								slots))
						(cons 'aircraft_id aircraft-id)
						(cons 'load load-num)
						(cons 'slots
							(map (lambda (slot)
									(slot-num-tag slot)) slots))))
				(list (cons 'status #f))))))
(http-json "/role"
	; change slot role
	(lambda (req)
		(set! req-cache (cons "role" req))
		(let ([date (query-value req 'date)]
				[aircraft-id (query-value-number req 'aircraft)]
				[role (query-value req 'role)]
				[load-num (query-value-number req 'load)]
				[slot-num (query-value-number req 'slot)])
			(if (and (observer-role? role)
					(has-observer-slot? aircraft-id)
					(null? (copilot-on-load aircraft-id date load-num)))
				(begin
					(manifest-observer
						(slot-passenger aircraft-id date load-num slot-num)
						aircraft-id load-num date)
					(list
						(cons 'scratch #t)
						(cons 'aircraft aircraft-id)
						(cons 'load load-num)
						(cons 'slot slot-num)))
				(begin
					(set-role role aircraft-id
						date load-num slot-num)
					(list (cons 'scratch #f)))))))
(http-json "/reserve"
	; reserve block of load slots
	(admin-gate "manifest" (req)
		(set! req-cache (cons "reserve" req))
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[date (query-value req 'date)]
				[load-num (query-value-number req 'load)]
				[slots (int-list (query-value req 'slots))])
			(db-sync
				(lambda ()
					(reserve-slots aircraft-id date load-num slots)))
			(list
				(cons 'slots
					(map (lambda (slot)
							(list
								(format #f "name~d_~d_~d"
									aircraft-id load-num slot)
								(reserved-slot-form aircraft-id load-num
									slot))) slots))))))
(http-json "/track"
	(lambda (req)
		(set! req-cache (cons "track" req))
		(let ([tag (query-value req 'tag)])
			(longpoller
				(lambda () (pending-update req tag)) tag))))
(http-json "/manifast"
	; manifast list
	(lambda (req)
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[date (query-value req 'date)]
				[load-num (query-value-number req 'load)])
			(list
				(cons 'manifast
					(string-cat "\n"
						click-names-note
						(manifast aircraft-id date load-num)))))))
(http-json "/call"
	; bring up call dialog
	(lambda (req)
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[date (query-value req 'date)]
				[suffix (query-value req 'suffix)]
				[load-num (query-value-number req 'load)])
			(list
				(cons 'html
					(detail-dialog aircraft-id date load-num suffix))
				(cons 'aircraft aircraft-id)
				(cons 'load load-num)))))
(http-json "/setcall"
	; set load call
	(admin-gate "manifest" (req)
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[load-num (query-value-number req 'load)]
				[mins (query-value-number req 'time)]
				[date (query-value req 'date)])
			(set-call date aircraft-id load-num mins)
			(list
				(cons 'refresh #f)
				(cons 'load_calls (update-load-calls))))))
(http-json "/abtcall"
	; abort load call
	(admin-gate "manifest" (req)
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[load-num (query-value-number req 'load)]
				[date (query-value req 'date)])
			(abort-call date aircraft-id load-num)
			(list (cons 'aircraft aircraft-id)))))
(http-json "/access"
	; set load sheet access: open (self-manifest), closed, gone
	(admin-gate "manifest" (req)
		(let* ([aircraft-id (query-value-number req 'aircraft)]
				[load-num (query-value-number req 'load)]
				[date (query-value req 'date)]
				[status (find-load-status (query-value req 'access))]
				[next-status (next-load-status status)])
			(change-status aircraft-id load-num date next-status)
			(list
				(cons 'status #t)
				(cons 'color (load-status-color next-status))
				(cons 'access (load-status-button next-status))
				(cons 'aircraft_id aircraft-id)
				(cons 'load load-num)))))
(http-json "/stats"
	; load statistics
	(lambda (req)
		(let ([date (query-value req 'date)])
			(list
				(cons 'html (manifest-stats date))))))
(http-json "/addsheets"
	; add a row of load sheets to current page
	(admin-gate "manifest" (req)
		(let* ([aircraft-id (query-value-number req 'aircraft)]
				[date (query-value req 'date)]
				[n-sheets (env-load-sheet-columns)]
				[start-load (make-sheets aircraft-id date n-sheets)]
				[end-load (+ start-load n-sheets -1)])
			(release-track-sessions)
			(list
				(cons 'end_load end-load)
				(cons 'cells
					(map
						(lambda (load-num)
							(list
								(cons 'html
									(or (get-load-sheet date aircraft-id
											load-num #f #f)
										"&nbsp;"))
								(cons 'marker
									(load-marker aircraft-id load-num))))
						(range start-load end-load)))
				(cons 'aircraft aircraft-id)))))
(http-json "/checkadmin"
	; does this client have admin privileges?
	(lambda (req)
		(let ([ip-match
					(string=? (env-dz-ip-addr) (client-ip-addr req))])
			(list
				(cons 'mani_admin (mani-admin req))
				(cons 'abs_admin (admin-user req))))))
(http-json "/setip"
	(admin-gate "settings" (req)
		(set-setting 'dz-ip-addr (client-ip-addr req) (sdb))
		(list
			(cons 'status #t)
			(cons 'ip (env-dz-ip-addr))
			(cons 'msg
				(format #f set-ip-response (env-dz-ip-addr))))))
(http-json "/pids"
	(lambda (req)
		(let ([slots (int-list (query-value req 'slots))]
				[src-aircraft (query-value-number req 'src_aircraft)]
				[dst-aircraft (query-value-number req 'dst_aircraft)]
				[src-load (query-value-number req 'src_load)]
				[dst-load (query-value-number req 'dst_load)]
				[date (query-value req 'date)])
			(list
				(cons 'src_aircraft src-aircraft)
				(cons 'dst_load dst-load)
				(cons 'dst_aircraft dst-aircraft)
				(cons 'pids
					(map
						(lambda (slot)
							(get-pid date src-aircraft
								src-load slot)) slots))))))
(http-json "/reseq"
	; resequence slots on load sheet after drag-n-drop
	(admin-gate "manifest" (req)
		(set! req-cache (cons "reseq" req))
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[load-num (query-value-number req 'load)]
				[date (query-value req 'date)]
				[src-slot (query-value-number req 'src_slot)]
				[dst-slot (query-value-number req 'dst_slot)]
				[tag (query-value req 'tag)])
			(db-sync
				(lambda ()
					(slot-shift date aircraft-id load-num
						src-slot dst-slot)))
			(release-track-sessions)
			(or
				(payload req tag)
				(no-event tag)))))
(http-json "/occdates"
	(lambda (req)
		(list
			(cons 'dates (occupied-dates)))))
(http-json "/sheetrow"
	; draw one row of load sheets, called repeatedly to improve
	; visual response (rather than delaying to draw all sheets
	; at once)
	(lambda (req)
		(let ([tab-id (query-value req 'tab)]
				[load-date (query-value req 'date)]
				[active-tab (query-value-number req 'active)]
				[multi-column (query-value-boolean req 'multi)]
				[start-load (query-value-number req 'start_load)]
				[sheets (query-value-number req 'sheets)])
			(if (or (logged-in req) (publish-manifest))
				(begin
					(session-set req 'aircraft_index active-tab)
					(list
						(cons 'tab_id tab-id)
						(cons 'multi multi-column)
						(cons 'rowgroups
							(aircraft-row-groups tab-id sheets
								load-date start-load req))))
				(list (cons 'status #f))))))
(http-json "/setfirst"
	; set first load of the day (not necessarily "1")
	(admin-gate "manifest" (req)
		(let* ([aircraft-id (query-value-number req 'aircraft)]
				[load-date (query-value req 'date)]
				[new-first-load (max (query-value-number req 'load) 1)]
				[old-first-load
					(first-sheet-today load-date aircraft-id)])
			(if (= new-first-load old-first-load)
				(list 'status #f)
				(begin
					(increment-loads new-first-load old-first-load
						aircraft-id load-date)
					(upsert-page-touch load-date aircraft-id)
					(list
						(cons 'status #t)
						(cons 'aircraft aircraft-id)))))))
(http-json "/datefmt"
	(lambda (req)
		(list
			(cons 'format (env-date-format))
			(cons 'aircraft_index
				(or (session-get req 'aircraft_index) 0))
			(cons 'search_length (env-min-search-chars)))))
(http-json "/ctxmenu"
	; extend slot context menu to offer extra items (e.g. video)
	; on designated skydive types
	(admin-gate "manifest" (req)
		(let ([roles (json-decode (query-value req 'roles))]
				[jumpers (json-decode (query-value req 'jumpers))]
				[slot-ids (json-decode (query-value req 'slot_ids))]
				[dates (json-decode (query-value req 'dates))]
				[extras (manifest-extras)])
			(if (null? roles)
				(list (cons 'status #f))
				(list
					(cons 'status #t)
					(cons 'menu
						(context-menu-extras slot-ids
							roles jumpers dates extras)))))))
(http-json "/chgextra"
	; charge extra item to jumper's account (see "ctxmenu")
	(admin-gate "manifest" (req)
		(set! req-cache (cons "chgextra" req))
		(let* ([jumper-id (query-value-number req 'jumper_id)]
				[item-id (query-value-number req 'item_id)]
				[load-date (query-value req 'date)]
				[slot-id (query-value req 'slot)]
				[detail (item-detail item-id)])
			(db-sync
				(lambda ()
					(add-slot-extra load-date slot-id item-id
						(acct-charge-person jumper-id
							(account-id "general" (sdb))
							(pg-cell detail 'cash_price)
							item-id
							(format #f "~a: ~a"
								(pg-cell detail 'category)
								(pg-cell detail 'item_name))
							(time-format (time-now) "%Y-%m-%d")
							#f ; bound to load slot
							"manifest" (env-login-id)
							(sdb)))))
			(list
				(cons 'status #t)
				(cons 'price (pg-cell detail 'cash_price))))))
(http-json "/dropxtra"
	; drop an extra item from slot
	(admin-gate "manifest" (req)
		(set! req-cache (cons "dropxtra" req))
		(let* ([xact-id (query-value-number req 'xact_id)]
				[slot (slot-with-extras xact-id)])
			(if slot
				(db-sync
					(lambda ()
						(drop-extra xact-id
							(pg-cell slot 'load_date)
							(pg-cell slot 'aircraft_id)
							(pg-cell slot 'load_num)
							(pg-cell slot 'slot_num))))
				(list (cons 'status #f))))))
(http-json "/qualload"
	; set load qualifier; e.g. "hot", "fuel"
	(admin-gate "manifest" (req)
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[date (query-value req 'date)]
				[qualifier (query-value req 'qualifier)]
				[load-num (query-value-number req 'load)])
			(set-qualifier qualifier aircraft-id date load-num)
			(release-track-sessions)
			(list
				(cons 'status #t)
				(cons 'aircraft aircraft-id)
				(cons 'load load-num)
				(cons 'fuel
					(if (string=? qualifier "fuel")
						(or (get-max-fuel aircraft-id) "")
						"restore"))))))
(http-json "/keypad"
	(lambda (req)
		(session-set req 'keypad (query-value-boolean req 'state))
		(list (cons 'status #t))))
(http-json "/autoscroll"
	; auto-scroll to most recent load sheet
	(lambda (req)
		(let ([load-date (query-value req 'date)]
				[force-scroll (query-value-boolean req 'force)]
				[aircraft-id (query-value-number req 'aircraft)])
			(list
				(cons 'recent_load
					(if (and (not force-scroll) (logged-in req))
						"no-autoscroll"
						(load-marker aircraft-id
							(recent-load aircraft-id load-date))))))))
(http-json "/setxitwt"
	; set jumper's exit weight
	(admin-gate "manifest" (req)
		(let ([person-id (query-value-number req 'pid)]
				[weight (query-value-number req 'weight)])
			(set-exit-weight person-id weight)
			(list (cons 'status #t)))))
(http-json "/copilots"
	(admin-gate "manifest" (req)
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[load-num (query-value-number req 'load)]
				[load-date (query-value req 'date)])
			(list
				(cons 'aircraft aircraft-id)
				(cons 'load load-num)
				(cons 'html
					(copilots-menu aircraft-id
						load-num load-date))))))
(http-json "/updwt"
	(admin-gate "manifest" (req)
		(let* ([aircraft-id (query-value-number req 'aircraft)]
				[load-num (query-value-number req 'load)]
				[load-date (query-value req 'date)]
				[weight-bal
					(total-weight-balance
						(get-load-id aircraft-id load-date load-num)
						(sdb))])
			(list
				(cons 'aircraft aircraft-id)
				(cons 'load load-num)
				(cons 'wt_html (display-load-weight (car weight-bal)))
				(cons 'cg_html
					(display-load-weight (compute-cg weight-bal)))))))
(http-json "/setfuel"
	(admin-gate "manifest" (req)
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[load-num (query-value-number req 'load)]
				[load-date (query-value req 'date)]
				[gals (query-value-number req 'value)])
			(set-load-fuel aircraft-id load-date load-num gals)
			(list
				(cons 'aircraft aircraft-id)
				(cons 'load load-num)
				(cons 'gals gals)))))
(http-json "/setpilot"
	(admin-gate "manifest" (req)
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[load-num (query-value-number req 'load)]
				[load-date (query-value req 'date)]
				[pilot-id (query-value-number req 'pilot)]
				[selector (query-value req 'selector)])
			(set-load-pilot
				aircraft-id load-date load-num pilot-id selector)
			(list
				(cons 'aircraft aircraft-id)
				(cons 'load load-num)))))
(http-json "/ip"
	; what's my internet IP?
	(lambda (req)
		(list (cons 'ip (client-ip-addr req)))))
(http-json "/msheets"
	; assure existence of loadsheets for date and aircraft
	(lambda (req)
		(let ([aircraft-id (query-value-number req 'aircraft)]
				[load-date (query-value req 'date)])
			(if (or (logged-in req) (publish-manifest))
				(begin
					(assure-load-sheets aircraft-id load-date)
					(list (cons 'status #t)))
				(list (cons 'status #f))))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; load call countdown monitor
(define (any? bools)
	(cond
		[(not (pair? bools)) #f]
		[(car bools) #t]
		[else (any? (cdr bools))]))
(define five-sec-marks
	(let ([query
				"select ((round(date_part('sec', call_time -
							current_timestamp))::integer % ~d) = 0) as mark
					from load_sheets
					where (call_time is not null)
					and (call_time >
						(current_timestamp - interval '60 sec'))"])
		(lambda (dbh)
			(any?
				(pg-map-rows
					(pg-exec dbh 
						(format #f query load-call-update-interval))
					(lambda (row) (pg-cell row 'mark)))))))
(define (to-next-second)
	(let ([sec (time-sec (time-now))])
		(- 1.01 (- sec (to-i sec)))))
(call-with-new-thread
	(let ([query
				"update load_sheets set
					call_time=NULL
					where (call_time is not null)
					and (call_time <
							(current_timestamp - interval '40 sec'));
				update load_sheets set
					call_tic=current_timestamp
					where (call_time is not null)"])
		(lambda ()
			(let loop ()
				(when (five-sec-marks (sdb))
					(pg-exec (sdb) query)
					(release-track-sessions))
				(snooze (to-next-second))
				(loop)))))
(cron-start)
