#! /usr/bin/ruby

require "pp"

LockFile = "/var/lib/gusher/watch.lock"
Home = "#{ENV['HOME']}/nowcall"
Logs = "/var/log/nowcall"

Apps = [
	"index",
	"aircraft",
	"people",
	"manifest",
	"accounting",
	"login",
	"settings",
	"reports",
	"products",
	"backups",
	]

def alert(app)
	subject = "nowcall check: #{app}"
	pipe = File::popen("mail -s '#{subject}' pmy@linux.com", "w")
	pipe.puts "nowcall check #{app}"
	log = "#{Logs}/#{app}.log"
	if File::exists?(log)
		saved = "/tmp/nowcall_fail_#{app}.log"
		system("cp #{log} #{saved}")
		pipe.puts "saved log: #{saved}"
	else
		pipe.puts "no log file"
		end
	pipe.puts "fire up #{Home}/sh_#{app}"
	pipe.close
	sleep(2)
	system("#{Home}/sh_#{app}")
	end

lock = File.new(LockFile, "w")
unless lock.flock(File::LOCK_EX | File::LOCK_NB)
	lock.close
	exit
	end

running = `ps ax|grep gusher`.split("\n").select do |item|
	item !~ /screen/i
	end

Apps.each do |app|
	if running.any? { |inst| inst =~ /(\/#{app}\.scm)/ }
		#alert(app)
		next
	else
		alert(app)
		end
	end

lock.flock(File::LOCK_UN)
lock.close
