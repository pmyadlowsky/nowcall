/*
** Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
**
** This program is free software ; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation ; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY ; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program ; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var ignore_catclick = false;
var ignore_itemclick = false;
var add_to_cat = 0;
var rename_orig;
var rename_orig_obj;
var non_empty_category = "<p>This category contains product items" +
	" and cannot be deleted. Either delete those items or move" +
	" them to other categories.</p>";

function do_nothing(ev, ui) {}
function set_cat_context_menus() {
	$(".ctx").contextmenu({
		"Dismiss": do_nothing,
		"Rename": rename_cat,
		"Delete": delete_cat
		}, 'right', 1000);
	}
function finish_rename_item() {
	var new_name = document.getElementById("item_rename").value;
	if (rename_orig.toLowerCase() == new_name.toLowerCase()) {
		rename_orig_obj.innerHTML = rename_orig;
		ignore_itemclick = false;
		}
	else {
		var m = rename_orig_obj.id.match(/^itemlabel([0-9]+)_([0-9]+)/);
		$.get("/products/renameitem", {
				category: m[1],
				item: m[2],
				repl: new_name
				},
				function(resp) {
					if (resp.status) {
						rename_orig_obj.innerHTML = resp.repl;
						ignore_itemclick = false;
						}
					else local_alert("Name Taken", resp.msg);
					}, "json");
		}
	return false;
	}
function finish_rename_item_cr(ev) {
	if (ev.keyCode == 13) return finish_rename_item();
	else return true;
	}
function rename_item(ev, ui) {
	var obj = document.getElementById(ui.id.replace("item", "itemlabel"));
	rename_orig_obj = obj;
	rename_orig = obj.innerHTML;
	var box = "<input type=\"text\" id=\"item_rename\" onkeypress=\"return finish_rename_item_cr(event)\" title=\"hit Enter or Tab when done\" onchange=\"return finish_rename_item()\" value=\"" + rename_orig + "\"/>";
	obj.innerHTML = box;
	ignore_itemclick = true;
	}
function paint_items(category, html) {
	var div = document.getElementById("items" + category);
	if (div == null) return;
	div.innerHTML = html;
	$(".item-list").sortable({
		stop: reorder_items,
		distance: 10
		});
	set_item_context_menus();
	}
function delitem_handler(resp) {
	if (resp.status) paint_items(resp.category, resp.html);
	}
function delete_item(ev, ui) {
	var m = ui.id.match(/^item([0-9]+)_([0-9]+)/);
	if (m == null) return;
	$.get("/products/delitem", {
			category: m[1],
			item: m[2]
			}, delitem_handler, "json");
	}
function monify(src) {
	var stripped = src.replace(/[^0-9\.]/, '');
	if (stripped == "") return 0;
	return parseInt(parseFloat(stripped) * 100 + 0.5);
	}
function save_handler(resp) {
	resp.save_button.style.fontWeight = "normal";
	if (resp.status) {
		close_item(resp.item);
		var div = document.getElementById("itemamt" + resp.item);
		div.innerHTML = resp.amounts;
		}
	else {
		document.getElementById("mansym" + resp.item).value = resp.mansym;
		local_alert("Symbol Taken", resp.msg);
		}
	}
function is_checked(id, fallback) {
	var cbox = document.getElementById(id);
	if (cbox == null) return fallback;
	return cbox.checked;
	}
function save_item(save_button, item_id) {
	save_button.style.fontWeight = "bold";
	var cash = document.getElementById("cash" + item_id).value;
	var notes = document.getElementById("notes" + item_id).value;
	var payout = document.getElementById("payout" + item_id).checked;
	var wage = document.getElementById("wage" + item_id).checked;
	var mansym = document.getElementById("mansym" + item_id);
	var acct_bal = document.getElementById("acctbal" + item_id).checked;
	var acct_xfer = document.getElementById("acctxfer" + item_id).checked;
	var sales_tax = document.getElementById("salestax" + item_id).checked;
	var offer_extras = is_checked("offerext" + item_id, false);
	var right_seat = is_checked("right-seat" + item_id, false);
	var media_included = is_checked("inclmedia" + item_id, false);
	var manifest_extra = is_checked("manextra" + item_id, false);
	var reservable = is_checked("reservations" + item_id, false);
	var manifest_seq = document.getElementById("manseq" + item_id);
	if (manifest_seq == null) manifest_seq = "0";
	else manifest_seq = manifest_seq.value
	var manifest;
	if (mansym == null) manifest = "";
	else manifest = mansym.value;
	var roleclump = document.getElementById("itemroles" + item_id);
	var roles;
	if (roleclump == null) roles = "";
	else roles = roleclump.value.split(":");
	var apropos = Array();
	var cb;
	for (var i = 0; i < roles.length; i++) {
		cb = document.getElementById("role" + item_id + "_" + roles[i]);
		if (cb == null) continue;
		if (cb.checked) apropos.push(roles[i]);
		}
	var instructors = document.getElementById("instrs" + item_id);
	var nreq = (instructors == null ? 0 : instructors.value);
	$.post("/products/edititem", {
				item: item_id,
				acct_bal: acct_bal,
				acct_xfer: acct_xfer,
				sales_tax: sales_tax,
				manifest_extra: manifest_extra,
				manifest_seq: manifest_seq,
				reservations: reservable,
				offer_extras: offer_extras,
				right_seat: right_seat,
				media_included: media_included,
				payout: (payout ? 'y' : 'n'),
				wage: (wage ? 'y' : 'n'),
				roles: JSON.stringify(apropos),
				manifest: manifest,
				cash: monify(cash),
				notes: notes,
				served_by: document.getElementById("served-by" +
					item_id).value,
				instr_num_req: nreq,
				},
		function(resp) {
			resp.save_button = save_button;
			save_handler(resp);
			},
		"json");
	}
function close_item(item_id) {
	var div = document.getElementById("itemedit" + item_id);
	div.style.display = "none";
	}
function edit_item(item_id) {
	var div = document.getElementById("itemedit" + item_id);
	if (div.style.display == "block") div.style.display = "none";
	else {
		$.get("/products/oitem", { item: item_id },
			function(resp) {
				var panel = document.getElementById("editpanel" + item_id);
				panel.innerHTML = resp.html;
				div.style.display = "block";
				}, "json");
		}
	}
function open_item(item_id) {
	if (ignore_itemclick) return;
	edit_item(item_id);
	return;
	}
function set_item_context_menus() {
	$(".itx").contextmenu({
		"Dismiss": do_nothing,
		"Rename": rename_item,
		"Delete": delete_item,
		"Edit": function(ev, ui) {
					var m = ui.id.match(/^item[0-9]+_([0-9]+)/);
					edit_item(m[1]);
					}
		}, 'right', 1000);
	$(".itd").contextmenu({
		"Dismiss": do_nothing,
		"Delete": delete_item
		}, 'right', 1000);
	}
function paint_categories(html) {
	document.getElementById("categories").innerHTML = html;
	$("#cat_sortable").sortable({
		stop: reorder_cats,
		distance: 10
		});
	set_cat_context_menus();
	}
function addcat() {
	$.get("/products/addcat", {
			newcat: document.getElementById("newcat").value,
			},
			function(resp) {
				if (resp.status) paint_categories(resp.html);
				else local_alert("Name Taken", resp.msg);
				}, "json");
	}
function mark_category(cat_id) {
	add_to_cat = cat_id;
	}
function additem_handler(resp) {
	if (resp.status) paint_items(resp.category, resp.html);
	else local_alert("Name Taken", resp.msg);
	}
function additem(cat_id) {
	if (add_to_cat == 0) return;
	$.get("/products/additem", {
			div: false,
			category: add_to_cat,
			newitem: document.getElementById("newitem" + add_to_cat).value,
			}, additem_handler, "json");
	}
function adddiv(cat_id) {
	$.get("/products/additem", {
			div: true,
			category: cat_id,
			newitem: ""
			}, additem_handler, "json");
	}
function finish_catadd(ev) {
	if (ev.keyCode == 13) addcat();
	}
function finish_itemadd(ev) {
	if (ev.keyCode == 13) additem(add_to_cat);
	}
function orderitem_handler(resp) {
	ignore_itemclick = false;
	}
function reorder_items(ev, ui) {
	var m = ui.item[0].id.match(/^item([0-9]+)_([0-9]+)/);
	if (m == null) return;
	ignore_itemclick = true;
	var category = m[1];
	var item = m[2];
	var items = document.getElementById("catitems" + category);
	items = items.getElementsByTagName("li");
	var ordered = Array();
	for (var i = 0; i < items.length; i++) {
		m = items[i].id.match(/^item[0-9]+_([0-9]+)/);
		if (m == null) continue;
		ordered.push(m[1]);
		}
	$.get("/products/orderitems", {
			category: category,
			order: JSON.stringify(ordered)
			}, orderitem_handler, "json");
	}
function cat_clicked(cat_id) {
	if (ignore_catclick) return;
	var div = document.getElementById("items" + cat_id);
	if (div.style.display == "block") div.style.display = "none";
	else {
		$.get("/products/ocat", { category: cat_id },
			function(resp) {
				paint_items(cat_id, resp.html);
				div.style.display = "block";
				}, "json");
		}
	}
function ordercat_handler(resp) {
	ignore_catclick = false;
	}
function reorder_cats(ev, ui) {
	ignore_catclick = true;
	var cats = document.getElementById("cat_sortable").getElementsByTagName("li");
	var ordered = Array();
	var m;
	for (var i = 0; i < cats.length; i++) {
		m = cats[i].id.match(/^cat([0-9]+)/);
		if (m == null) continue;
		ordered.push(m[1]);
		}
	$.get("/products/ordercats", {
			order: JSON.stringify(ordered)
			}, ordercat_handler, "json");
	}
function rename_cat(ev, ui) {
	var obj = document.getElementById(ui.id.replace("cat", "catlabel"));
	rename_orig_obj = obj;
	rename_orig = obj.innerHTML;
	var box = "<input type=\"text\" id=\"cat_rename\" onkeypress=\"return finish_rename_cat_cr(event)\" title=\"hit Enter or Tab when done\" onchange=\"return finish_rename_cat()\" value=\"" + rename_orig + "\"/>";
	obj.innerHTML = box;
	ignore_catclick = true;
	}
function rename_cat_handler(resp) {
	if (resp.status) {
		rename_orig_obj.innerHTML = resp.repl;
		ignore_catclick = false;
		}
	else local_alert("Name Taken", resp.msg);
	}
function finish_rename_cat() {
	var new_name = document.getElementById("cat_rename").value;
	if (rename_orig.toLowerCase() == new_name.toLowerCase()) {
		rename_orig_obj.innerHTML = rename_orig;
		ignore_catclick = false;
		}
	else {
		$.get("/products/renamecat", {
				orig: rename_orig,
				repl: new_name
				}, rename_cat_handler, "json");
		}
	return false;
	}
function finish_rename_cat_cr(ev) {
	if (ev.keyCode == 13) return finish_rename_cat();
	else return true;
	}
function delete_cat(ev, ui) {
	var m = ui.id.match(/^cat([0-9]+)/);
	if (m == null) return;
	$.get("/products/delcat", { cat: m[1] },
			function(resp) {
				if (resp.status) paint_categories(resp.html);
				else local_alert("Delete Category", non_empty_category);
				}, "json");
	}
function manifest_toggle(item_id) {
	$.get("/products/flipman", {
			item: item_id,
			apropos: document.getElementById("manapp" + item_id).checked,
			payout: document.getElementById("payout" + item_id).checked,
			cash: monify(document.getElementById("cash" + item_id).value)
			}, 
			function(resp) {
				var div = document.getElementById("editpanel"
								+ resp.item_id);
				div.innerHTML = resp.html;
				}, "json");
	}
function set_roles(item_id) {
	var manifest_apropos = document.getElementById("manapp" + item_id);
	if (!manifest_apropos.checked) return;
	manifest_toggle(item_id);
	}
function refresh_tags(item_id, tag_set, menu) {
	var obj;
	document.getElementById("tag-set" + item_id).innerHTML = tag_set;
	obj = document.getElementById("new-tags" + item_id);
	if (obj != null) obj.value = "";
	obj = document.getElementById("tag-menu" + item_id);
	if (obj != null) obj.innerHTML = menu;
	}
function drop_tag(item_id, tag) {
	$.get("/products/deltag",
			{ item: item_id, tag: tag },
			function(resp) {
				refresh_tags(item_id, resp.tags, resp.menu);
				}, "json");
	}
function drop_tag_all(item_id, tag) {
	local_confirm("Products", "Yes", "No",
		function() {
			$.get("/products/dropall", { item: item_id, tag: tag },
					function(resp) {
						refresh_tags(item_id, resp.tags, resp.menu);
						}, "json");
			}, null,
		"Sure you want to drop all instances of '" + tag + "'?");
	}
function toggle_tag_menu(item_id) {
	var div = document.getElementById("tag-menu" + item_id);
	if (div.style.display == "none") {
		$.get("/products/avtags",
				{ item: item_id },
				function(resp) {
					div.style.display = "block";
					div.innerHTML = resp.html;
					}, "json");
		}
	else div.style.display = "none";
	}
function add_tag(tag, item_id) {
	$.post("/products/addtags",
			{ item: item_id, tag: JSON.stringify([tag]) },
			function(resp) {
				refresh_tags(item_id, resp.tags, resp.menu);
				}, "json");
	}
function new_tags(box, item_id) {
	var words = box.value.split(/\s+/);
	$.post("/products/addtags",
			{ item: item_id, tag: JSON.stringify(words) },
			function(resp) {
				refresh_tags(item_id, resp.tags, resp.menu);
				}, "json");
	}
function sort_items(cat_id) {
	$.get("/products/sortitems", { category: cat_id },
			function(resp) {
				paint_items(resp.category, resp.html);
				},
			"json");
	}
function init() {
	mark_page("Products/Services");
	$("#cat_sortable").sortable({
		stop: reorder_cats,
		distance: 10
		});
	set_cat_context_menus();
	}
function logout_handler(resp) {
	window.location = "/manifest";
	}
function logout() {
	$.get("/login/logout", {}, logout_handler, "json");
	}
