(use-modules (ice-9 regex))
(use-modules (ice-9 rw))
(use-modules (ice-9 format))

; generate CSV spreadsheet from a list of data rows
; if available, convert CSV to ODF

(define docproc-hopper "/vol/store/docproc")
(define docproc-inbox (format #f "~a/inbox" docproc-hopper))
(define docproc-outbox (format #f "~a/done" docproc-hopper))

(define render-csv-spreadsheet
	(let ([quote-pat (make-regexp "\"")])
		(define (escape-quote src)
			(regexp-substitute/global #f quote-pat src 'pre "\"\"" 'post))
		(define (spreadsheet-row row)
			(string-cat ","
				(map
					(lambda (cell)
						(if (number? cell)
							(to-s cell)
							(format #f "\"~a\""
								(escape-quote (to-s cell))))) row)))
		(lambda (rows)
			(string-cat "\r\n"
				(map (lambda (row) (spreadsheet-row row)) rows) ""))))

(define render-spreadsheet
	(let ([csv-mime "application/csv"]
			[csv-charset "UTF-8"]
			[quiet #t]
			[ods-mime "application/vnd.oasis.opendocument.spreadsheet"]
			[ods-charset "ISO-8859-1"])
		(define (slurp port)
			; read file content to a single string
			(let ([buf (make-string 2048)])
				(let reader ([nchars (read-string!/partial buf port)]
							[bag ""])
					(if nchars
						(let ([newbag
								(string-cat "" bag
									(string-take buf nchars))])
							(reader
								(read-string!/partial buf port) newbag))
						bag))))
		(define (dump-to-hopper csv filebase)
			(let ([pre-path
					(format #f "~a/~a.csv" docproc-hopper filebase)]
					[inbox-path
						(format #f "~a/~a.csv" docproc-inbox filebase)])
				(call-with-output-file pre-path
					(lambda (port)
						(write-string/partial csv port)))
				(link pre-path inbox-path) ; move to inbox
				(delete-file pre-path)))
		(define (await-conversion filebase)
			(let ([product
						(format #f "~a/~a.ods" docproc-outbox filebase)])
				(let wait ()
					(unless (access? product R_OK)
						(snooze 1.0)
						(wait)))
				product))
		(define (convert-to-ods csv filebase)
			; run conversion, return resulting content
			(dump-to-hopper csv filebase)
			(call-with-input-file
				(await-conversion filebase)
				(lambda (port) (slurp port))))
		(lambda (rows ods-enabled)
			; render rows to spreadsheet
			; return tuple: mimetype, file suffix, spreadsheet text
			(let ([csv (render-csv-spreadsheet rows)]
					[filebase (uuid-generate)])
				(if (and ods-enabled (access? docproc-inbox W_OK))
					(let ([ods (convert-to-ods csv filebase)])
						(list ods-mime ods-charset "ods" ods))
					(list csv-mime csv-charset "csv" csv))))))

