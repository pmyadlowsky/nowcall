/*
** Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
**
** This program is free software ; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation ; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY ; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program ; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var refresh_suspended = false;
var first_refresh = true;
var rec_count = 0;
function post_rec_count() {
	var span = document.getElementById("reccount");
	if (rec_count == 0) {
		span.style.color = "#666666";
		span.innerHTML = "please wait..."
		}
	else {
		span.style.color = "black";
		span.innerHTML = "" + rec_count + " records";
		}
	}
function rowgroup_handler(resp) {
	var table = document.getElementById("chart");
	var key, row, pid;
	rec_count += resp.rows.length;
	for (var i = 0; i < resp.rows.length; i++) {
		pid = resp.rows[i][1];
		row = table.insertRow(-1); // chart row
		row.id = "crow" + pid;
		row.innerHTML = resp.rows[i][0];
		key = resp.rows[i][2];
		row = table.insertRow(-1); // detail row
		row.style.display = "none";
		row.id = "drow" + pid;
		row.innerHTML = resp.rows[i][3];
		}
	post_rec_count();
	if (first_refresh) {
		first_refresh = false;
		refresh_suspended = false;
		freshen_chart();
		window.setInterval("controlled_refresh()", 3000);
		}
	$("#spinner").hide();
	}
function load_records() {
	rec_count = 0;
	post_rec_count();
	var div = document.getElementById("chart_body");
	div.innerHTML = "<table id=\"chart\" style=\"width: 100%\"></table>";
	$("#spinner").show();
	$.post("/people/rowgroup", {
			active_filter: document.getElementById("active-filter").value,
			page_key: "-"
			}, rowgroup_handler, "json");
	}
function init() {
	mark_page("People");
	load_records();
	}
function controlled_refresh() {
	if (!refresh_suspended) freshen_chart();
	}
function patch_cell(name, pid) {
	return document.getElementById("C" + name + pid);
	}
function patch_chart(resp) {
	if (!resp.refresh) return;
	var n = resp.patches.length;
	var patch;
	var cell;
	var pid;
	for (var i = 0; i < n; i++) {
		patch = resp.patches[i];
		pid = patch.pid;
		cell = patch_cell("last_name", pid);
		if (cell == null) continue;
		cell.innerHTML = patch.last_name;
		cell = patch_cell("first_name", pid);
		cell.innerHTML = patch.first_name;
		cell = patch_cell("uspa_id", pid);
		cell.innerHTML = patch.uspa_id;
		cell = patch_cell("license", pid);
		cell.innerHTML = patch.license;
		cell = patch_cell("uspa_expires", pid);
		cell.innerHTML = patch.uspa_expires;
		cell.style.border = patch.expires_border;
		cell = patch_cell("last_repack", pid);
		cell.innerHTML = patch.last_repack;
		cell.style.border = patch.repack_border;
		cell = patch_cell("waiver_date", pid);
		cell.innerHTML = patch.waiver_date;
		cell.style.border = patch.waiver_border;
		}
	}
function freshen_chart() {
	$.get("/people/chart", { }, patch_chart, "json");
	}
function created_new(resp) {
	var table = document.getElementById("chart");
	var row = table.insertRow(0); // chart row
	row.id = "crow" + resp.pid;
	row.innerHTML = resp.rows[0][0];
	row = table.insertRow(1); // detail row
	row.style.display = "none";
	row.id = "drow" + resp.rows[0][1];
	row.innerHTML = resp.rows[0][3];
	var cb = document.getElementById("detail" + resp.pid);
	cb.checked = true;
	toggle_detail(cb, resp.pid);
	rec_count += 1;
	post_rec_count();
	}
function people_new() {
	$.get("/people/new", {
		last_name: "LAST",
		first_name: "FIRST",
		role: "TAN"
		}, created_new, "json");
	}
function delete_handler(resp) {
	var table = document.getElementById("chart");
	var n = -1;
	var id = "crow" + resp.pid;
	for (var i = 0; i < table.rows.length; i++) {
		if (table.rows[i].id == id) {
			n = i;
			break;
			}
		}
	if (n < 0) {
		local_alert("Delete Record", "can't find row " + id);
		return;
		}
	table.deleteRow(n); // chart row
	table.deleteRow(n); // detail row
	rec_count -= 1;
	post_rec_count();
	}
function delete_person(pid) {
	$.get("/people/del", { pid: pid }, delete_handler, "json");
	}
function suspend_refresh(suspend) {
	refresh_suspended = suspend;
	if (!refresh_suspended) freshen_chart();
	}
function detail_closed(resp) {
	var row = document.getElementById("drow" + resp.pid);
	row.style.display = "none";
	var checkbox = document.getElementById("detail" + resp.pid);
	checkbox.checked = false;
	var cell = document.getElementById("cbcell" + resp.pid);
	cell.style.backgroundColor = resp.cb_color;
	suspend_refresh(false);
	if (resp.delete) delete_person(resp.pid);
	}
function close_detail(pid) {
	$.get("/people/close_det", { pid: pid }, detail_closed, "json");
	}
function details(resp) {
	var locked = (resp.busy && !resp.admin);
	if (locked) {
		local_alert("Record Busy", "This record is held by " + resp.holder
				+ ".<br/>Access is read-only till released.");
		}
	var row = document.getElementById("drow" + resp.pid);
	row.style.display = "table-row";
	var cell = document.getElementById("dcell" + resp.pid);
	cell.innerHTML = resp.html;
	var btn = document.getElementById("people-del-button" + resp.pid);
	People.panel_config(resp.pid, resp.date_format,
							resp.first_last, resp.admin,
		!locked && // save
			function() {
				return People.update_detail(resp.pid,
					function(resp) {
						if (!resp.status)
							local_alert("Update Error", resp.msg);
						});
				},
		function() { close_detail(resp.pid); }, // cancel
		!btn.disabled && !locked && // delete
			function() {
				$.get("/people/predel", { pid: resp.pid },
						function(resp) {
							if (resp.blank) delete_person(resp.pid);
							else {
									local_confirm("People", "Yes", "No",
										function() {
											delete_person(resp.pid);
											}, null,
										"Really? Delete '"
											+ resp.name + "'?");
								}
							},
						"json");
				}
		);
	cell = document.getElementById("cbcell" + resp.pid);
	cell.style.backgroundColor = resp.cb_color;
	}
function toggle_detail(checkbox, pid) {
	if (checkbox.checked) {
		suspend_refresh(true);
		$.get("/people/detail", { pid: pid }, details, "json");
		}
	else close_detail(pid);
	}
function logout_handler(resp) {
	window.location = "/manifest";
	}
function logout() {
	$.get("/login/logout", {}, logout_handler, "json");
	}
function check_available(box, pid) {
	var name = box.value.trim();
	if (name == "") {
		document.getElementById("people-namecheck" + pid).innerHTML =
			"&nbsp;";
		return;
		}
	$.get("/people/checkname",
			{
				pid: pid,
				name: name
				}, 
			function(resp) {
				var text, color;
				if (resp.avail == "mine") {
					text = "&nbsp;";
					color = "black";
					}
				else if (resp.avail == "avail") {
					text = "AVAIL";
					color = "green";
					}
				else {
					text = "TAKEN";
					color = "red"
					}
				var lite = document.getElementById("people-namecheck"
									+ resp.pid);
				lite.style.color = color;
				lite.innerHTML = text;
				},
			"json");
	}
function people_note_add(pid) {
	$.get("/people/addnote", { pid: pid },
			function(resp) {
				var div = document.getElementById("people-notes" + pid);
				div.innerHTML = resp.html;
				}, "json"
			);
	}
function people_note_edit(box, note_id) {
	$.post("/people/ednote",
			{ nid: note_id, note: box.value },
			function(resp) {
				}, "json"
			);
	}
function people_note_del(note_id, person_id) {
	$.get("/people/delnote",
			{ nid: note_id, pid: person_id },
			function(resp) {
				var div = document.getElementById("people-notes" + person_id);
				div.innerHTML = resp.html;
				}, "json");
	}
function people_note_hilite(checkbox, note_id, person_id) {
	$.post("/people/hlnote",
			{ nid: note_id,
				pid: person_id,
				state: (checkbox.checked ? 1 : 0) },
			function(resp) { freshen_chart(); }
			);
	}
function find_dups() {
	$.get("/people/dups", {},
			function(resp) {
				local_alert("Duplicate Records?", resp.html, 500);
				}, "json");
	}
