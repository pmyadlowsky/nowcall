#! /usr/local/bin/gusher -p 3012
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))

(include "lib.scm")
(include "settings_lib.scm")
(include "people_lib.scm")

(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-date-format (setting-cache 'date-format))
(define env-repack-cycle (setting-cache 'repack-cycle))
(define env-name-order (setting-cache 'name-order))
(define env-patron-self-pay (setting-cache 'patron-self-pay))

(define frame (make-doc 'file "frame.html"))
(define page-body (make-doc 'file "me.html"))
(define script (make-doc 'file "me.js"))
(define profile-html (make-doc 'file "people_detail.html"))
(define composite
	(make-doc (list frame script page-body)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "My Stuff")
				(cons 'app "me")
				(cons 'script (fetch-doc script))
				(cons 'version nowcall-version)
				(cons 'content
					(fill-template (fetch-doc page-body) #t))))))
(define (dollars cents)
	(let ([abs-cents (abs cents)]
			[sign (if (< cents 0) "-" "")])
		(if (>= abs-cents 100000)
			(format #f "~a~d,~3,'0d.~2,'0d"
				sign
				(quotient abs-cents 100000)
				(quotient (remainder abs-cents 100000) 100)
				(remainder (remainder abs-cents 100000) 100))
			(format #f "~a~d.~2,'0d"
				sign
				(quotient abs-cents 100)
				(remainder abs-cents 100)))))
(define account-name
	(let ([query
				"select display_name, personal,
						people.last_name as lname,
						people.first_name as fname,
						people.id as pid
					from acct, people
					where (acct.id=~a)
					and (acct.owner=people.id)"])
		(lambda (acct-id)
			(let ([row (pg-one-row (sdb) query acct-id)])
				(if (pg-cell row 'personal)
					(format #f "~a"
						(person-name (pg-cell row 'pid) "default" (sdb)))
					(pg-cell row 'display_name))))))
(define jump-log-aircraft
	(let ([query
				"select distinct aircraft.nickname as aircraft,
						aircraft.id as id
					from load_slots, load_sheets, aircraft
					where (passenger=~~a)~a
					and (load_sheets.id=load_slots.load_id)
					and (load_sheets.aircraft_id=aircraft.id)
					order by aircraft.nickname asc"]
			[template
				(deflate
					"<div style=\"float: left\"><input
							type=\"checkbox\" id=\"ac~d\"
						onchange=\"arm_search(true)\"
						checked=\"checked\"/>~a</div>")])
		(define (render row)
			(format #f template
				(pg-cell row 'id) (pg-cell row 'aircraft)))
		(lambda (person-id date-terms)
			(string-cat "\n"
				(pg-map-rows
					(pg-exec (sdb)
						(format #f query date-terms) person-id) render)))))
(define jump-log-skydives
	(let ([query
				"select distinct load_slots.role as role
					from load_slots, load_sheets
					where (passenger=~~a)~a
					and (load_sheets.id=load_slots.load_id)
					order by load_slots.role asc"]
			[template
				(deflate
					"<div style=\"float: left\"><input
							type=\"checkbox\" id=\"role_~a\"
						onchange=\"arm_search(true)\"
						checked=\"checked\"/>~a</div>")])
		(define (render row)
			(let ([role (pg-cell row 'role)])
				(format #f template role role)))
		(lambda (person-id date-terms)
			(string-cat "\n"
				(pg-map-rows
					(pg-exec (sdb)
						(format #f query date-terms) person-id) render)))))
(define jump-log
	(let ([query
				"select load_slots.role as role,
						load_sheets.load_date as load_date,
						aircraft.nickname || ' [' ||
							aircraft.tail_num || ']' as aircraft,
						load_sheets.load_num as load_num
					from load_slots, load_sheets, aircraft
					where (passenger=~~a)~a~a~a
					and (load_sheets.id=load_slots.load_id)
					and (load_sheets.aircraft_id=aircraft.id)
					order by load_sheets.load_date asc,
						aircraft.nickname asc,
						load_sheets.load_num"]
			[template
				(deflate "<tr><td class=\"date-cell\">[[DATE]]</td>
					<td class=\"aircraft-cell\">[[AIRCRAFT]]</td>
					<td class=\"load-cell-r\">[[LOAD]]</td>
					<td class=\"role-cell\">[[ROLE]]</td>
					</tr>")])
		(define (render row date-format)
			(fill-template template #f
				(cons 'date
					(show-date (pg-cell row 'load_date) date-format))
				(cons 'aircraft (pg-cell row 'aircraft))
				(cons 'load (pg-cell row 'load_num))
				(cons 'role (pg-cell row 'role))))
		(define (aircraft-terms aircraft)
			(if (null? aircraft) ""
				(format #f " and (~a)"
					(string-cat " or "
						(map
							(lambda (id) 
								(format #f "(aircraft.id=~d)"
									(to-i id)))
							aircraft)))))
		(define (jump-terms types)
			(if (null? types) ""
				(format #f " and (~a)"
					(string-cat " or "
						(map
							(lambda (id) 
								(format #f "(load_slots.role='~a')" id))
							types)))))
		(lambda (person-id date-terms aircraft jump-types)
			(let ([log
						(pg-map-rows
							(pg-exec (sdb)
								(format #f query
									date-terms
									(aircraft-terms aircraft)
									(jump-terms jump-types))
								person-id))])
				(list
					(cons 'log
						(if (= (length log) 0)
							"<b>none found, try widening your search</b>"
							(string-cat "\n"
								"<table class=\"log-table\">"
								(map
									(lambda (entry)
										(render entry
											(env-date-format))) log)
								"</table>")))
					(cons 'total (length log)))))))
(define (year-range year today)
	(list
		(time-local year 1 1 3 0 0)
		(or today (time-local year 12 31 3 0 0))))
(define (today-range) (list (time-now) (time-now)))
(define month-range
	(let ()
		(define (final-month-date date)
			(let ([year (time-year date)]
					[month (time-month date)])
				(time-add (time-local
					(if (= month 12) (1+ year) year)
					(if (= month 12) 1 (1+ month))
					1 3 0 0) (* -8 3600))))
		(lambda (date today)
			(list
				(time-local (time-year date) (time-month date) 1 3 0 0)
				(or today (final-month-date date))))))
(define (last-month date)
	(let ([month (time-month date)]
			[year (time-year date)])
		(time-local
			(if (= month 1) (1- year) year)
			(if (= month 1) 12 (1- month)) (time-mday date) 3 0 0)))
(define date-filter
	(let ([template " and (load_sheets.load_date ~a ~~a)"])
		(define (term date compare)
			(if (string=? date "") #f
				(pg-query (sdb) (format #f template compare) (list date))))
		(lambda (from-date to-date)
			(string-cat ""
				(filter identity
					(list
						(term to-date "<=")
						(term from-date ">=")))))))
(define weekend
	(let ()
		(define (days-ago days)
			(time-add (time-now) (* (- days) 24 3600)))
		(lambda ()
			(let* ([today (time-now)]
					[wday (time-wday today)])
				(cond
					[(= wday 1) (list (days-ago 3) (days-ago 1))]
					[(= wday 2) (list (days-ago 4) (days-ago 2))]
					[(= wday 3) (list (days-ago 5) (days-ago 3))]
					[(= wday 4) (list (days-ago 6) (days-ago 4))]
					[(= wday 5) (list today today)]
					[(= wday 6) (list (days-ago 1) today)]
					[(= wday 0) (list (days-ago 2) today)])))))
(define (date-range key)
	(if (string=? key "all") (list "" "")
		(map
			(lambda (date) (show-date date (env-date-format)))
			(cond
				[(string=? key "today") (today-range)]
				[(string=? key "tyear")
					(year-range (time-year (time-now)) (time-now))]
				[(string=? key "lyear")
					(year-range (1- (time-year (time-now))) #f)]
				[(string=? key "tmonth")
					(month-range (time-now) (time-now))]
				[(string=? key "lmonth")
					(month-range (last-month (time-now)) #f)]
				[(string=? key "weekend") (weekend)]
				[else (today-range)]))))
(define ledger
	(let ([query
				"select date_posted,
						acct_xact.description as description,
						amount, quantity, sales_tax,
						(case
							when (from_acct=acct.id) then to_acct
							else from_acct
							end) as other_acct,
						(acct.id=acct_xact.from_acct) as debt
					from acct, acct_xact
					where (acct.owner=~a)
					and acct.personal
					and ((acct.id=acct_xact.from_acct)
							or (acct.id=acct_xact.to_acct))
					order by date_posted asc, entered asc"]
			[email-query
				"select coalesce(email,'') as email
					from people
					where (id=~a)"]
			[template
				(deflate "<tr><td class=\"date-cell\">~a</td>
				<td class=\"ledger-detail-cell\">~a</td>
				<td class=\"ledger-acct-cell\">~a</td>
				<td class=\"money-cell\">~a</td>
				<td class=\"money-cell\">~a</td>
				<td class=\"money-cell\">~a</td></tr>")]
			[in-the-red "<span style=\"color: red\">~a</span>"]
			[acct-query
				"select display_name as name
					from acct
					where (id=~a)"])
		(define (xact-amount row)
			(* (if (pg-cell row 'debt) -1 1)
				(*
					(+
						(pg-cell row 'amount)
						(pg-cell row 'sales_tax))
					(pg-cell row 'quantity))))
		(define (detail row)
			(let ([quantity (to-i (pg-cell row 'quantity))])
				(format #f "~a~a"
					(pg-cell row 'description)
					(if (< quantity 2) ""
						(format #f " <b>[~d]</b>" quantity)))))
		(define (tally rows)
			(let ([balance 0])
				(map
					(lambda (row)
						(let ([amount (xact-amount row)])
							(set! balance (+ balance amount))
							(list
								(cons 'date (pg-cell row 'date_posted))
								(cons 'detail (detail row))
								(cons 'acct (pg-cell row 'other_acct))
								(cons 'amount amount)
								(cons 'balance balance)))) rows)))
		(define (render-row row date-format)
			(let ([amount (pg-cell row 'amount)]
					[balance (pg-cell row 'balance)])
				(format #f template
					(show-date (pg-cell row 'date) date-format)
					(pg-cell row 'detail)
					(account-name (pg-cell row 'acct))
					(if (> amount 0) (dollars amount) "")
					(if (> amount 0) "" (dollars (- amount)))
					(format #f
						(if (>= balance 0) "~a" in-the-red)
						(dollars (pg-cell row 'balance))))))
		(define (render rows)
			(if (= (length rows) 0)
				"<b>none found in this date range</b>"
				(string-cat "\n"
					"<table class=\"ledger-table\">"
					(map
						(lambda (row)
							(render-row row (env-date-format)))
						(tally rows)) "</table>")))
		(define (email person-id)
			(let ([row (pg-one-row (sdb) email-query person-id)])
				(if row (pg-cell row 'email) "")))
		(lambda (person-id)
			(let ([rows
						(pg-map-rows
							(pg-exec (sdb) query person-id))])
				(list
					(cons 'status #t)
					(cons 'email (email person-id))
					(cons 'ledger (render rows))
					(cons 'total (length rows)))))))
(define profile-form
	(let ([query
				"select street1, street2, city, state, country, zipcode,
						phone_home, phone_mobile, email, emerg_contact,
						emerg_phone, birth, active, waiver_date,
						last_name, first_name, nickname, last_repack,
						uspa_id, uspa_expires, license, disambiguate,
						goes_by, login_name, login_password, gear_rental,
						last_repack_2, exit_weight, notify
					from people
					where (id=~a)"]
			[role-query "select role from roles where (who=~a)"])
		(define (box-checked state) (if state " checked=\"checked\"" ""))
		(define (box-enabled state) (if state "" " disabled=\"disabled\""))
		(define (any? symbol symbols)
			(cond
				[(null? symbols) #f]
				[(eq? (car symbols) symbol) #t]
				[else (any? symbol (cdr symbols))]))
		(define (roles pid)
			(pg-map-rows
				(pg-exec (sdb) role-query pid)
				(lambda (row) (string->symbol (pg-cell row 'role)))))
		(lambda (person-id)
			(let* ([row (pg-one-row (sdb) query person-id)]
					[roles (roles person-id)]
					[licensed (any? 'LIC roles)]
					[friend (any? 'FRIEND roles)]
					[first-last (string=? (env-name-order) "first-last")])
				(fill-template (fetch-doc profile-html) #t
					(cons 'pid person-id)
					(cons 'repack_cycle (env-repack-cycle))
					(cons 'date_format (env-date-format))
					(cons 'name_1_label (if first-last "First" "Last"))
					(cons 'name_2_label (if first-last "Last" "First"))
					(cons 'name_1_pre (if first-last "first" "last"))
					(cons 'name_2_pre (if first-last "last" "first"))
					(cons 'name_1_value
						(if first-last
							(to-s (pg-cell row 'first_name))
							(to-s (pg-cell row 'last_name))))
					(cons 'name_2_value
						(if first-last
							(to-s (pg-cell row 'last_name))
							(to-s (pg-cell row 'first_name))))
					(cons 'nickname (to-s (pg-cell row 'nickname)))
					(cons 'goes_by (to-s (pg-cell row 'goes_by)))
					(cons 'exit_weight (to-s (pg-cell row 'exit_weight)))
					(cons 'username (to-s (pg-cell row 'login_name)))
					(cons 'password
						(if (null? (pg-cell row 'login_password))
							"" "**********"))
					(cons 'disambiguate (to-s (pg-cell row 'disambiguate)))
					(cons 'street1 (to-s (pg-cell row 'street1)))
					(cons 'street2 (to-s (pg-cell row 'street2)))
					(cons 'city (to-s (pg-cell row 'city)))
					(cons 'state (to-s (pg-cell row 'state)))
					(cons 'zip (to-s (pg-cell row 'zipcode)))
					(cons 'country (to-s (pg-cell row 'country)))
					(cons 'home_phone (to-s (pg-cell row 'phone_home)))
					(cons 'mobile_phone (to-s (pg-cell row 'phone_mobile)))
					(cons 'email (to-s (pg-cell row 'email)))
					(cons 'checked_remind
						(if (pg-cell row 'notify)
							" checked=\"checked\"" ""))
					(cons 'emerg_con (to-s (pg-cell row 'emerg_contact)))
					(cons 'emerg_phone (to-s (pg-cell row 'emerg_phone)))
					(cons 'dob
						(show-date (pg-cell row 'birth) (env-date-format)))
					(cons 'gear_rental
						(show-date (pg-cell row 'gear_rental)
							(env-date-format)))
					(cons 'waiver
						(show-date (pg-cell row 'waiver_date)
							(env-date-format)))
					(cons 'repack
						(show-date (pg-cell row 'last_repack)
							(env-date-format)))
					(cons 'repack2
						(show-date (pg-cell row 'last_repack_2)
							(env-date-format)))
					(cons 'uspa_id (to-s (pg-cell row 'uspa_id)))
					(cons 'expires
						(show-date (pg-cell row 'uspa_expires)
							(env-date-format)))
					(cons 'license (to-s (pg-cell row 'license)))
					(cons 'checked_active
						(box-checked (pg-cell row 'active)))
					(cons 'checked_tan (box-checked (any? 'TAN roles)))
					(cons 'checked_stu (box-checked (any? 'STU roles)))
					(cons 'checked_unl (box-checked (any? 'UNL roles)))
					(cons 'checked_lic (box-checked licensed))
					(cons 'checked_friend (box-checked friend))
					(cons 'checked_pilot (box-checked (any? 'PILOT roles)))
					(cons 'checked_maint (box-checked (any? 'MAINT roles)))
					(cons 'checked_manifest
						(box-checked (any? 'MANFST roles)))
					(cons 'enable_ins (box-enabled #f))
					(cons 'enable_friend (box-enabled (or licensed friend)))
					(cons 'checked_affi (box-checked (any? 'AFFI roles)))
					(cons 'checked_ti (box-checked (any? 'TI roles)))
					(cons 'checked_vid (box-checked (any? 'VID roles)))
					(cons 'checked_packer
						(box-checked (any? 'PACKER roles)))
					(cons 'checked_coach (box-checked (any? 'C roles))))))))
(define update-profile
	(let ([query
				"update people set
					exit_weight=~a,
					street1=~a,
					street2=~a,
					city=~a,
					state=~a,
					country=~a,
					zipcode=~a,
					phone_home=~a,
					phone_mobile=~a,
					email=~a,
					emerg_contact=~a,
					emerg_phone=~a,
					birth=~a,
					notify=~a,
					updated=~a
					where (id=~a)"]
			[password-query
				"update people set
					login_password=~a
					where (id=~a)"]
			[username-update
				"update people set
					login_name=~a
					where (id=~a)"]
			[username-query
				"select login_name
					from people
					where (id=~a)"]
			[mutex (make-mutex)])
		(define (update-username person-id username)
			; only if username is available
			(lock-mutex mutex)
			(when (username-available username person-id (sdb))
				(pg-exec (sdb) username-update username person-id))
			(unlock-mutex mutex)
			(pg-cell
				(pg-one-row (sdb) username-query person-id) 'login_name))
		(define (update-password person-id password)
			(pg-exec (sdb) password-query
				(sha-256-sum password) person-id))
		(define (assure-string req key)
			(string-trim-both (or (query-value req key) "")))
		(lambda (req)
			(let ([person-id (query-value-number req 'pid)]
					[username (or (query-value req 'username) "")]
					[password (or (query-value req 'password) "")]
					[passtouch (query-value-boolean req 'passtouch)]
					[exit-weight (query-value-number req 'exit_weight)]
					[street1 (assure-string req 'street1)]
					[street2 (assure-string req 'street2)]
					[city (assure-string req 'city)]
					[state (assure-string req 'state)]
					[country (assure-string req 'country)]
					[zipcode (assure-string req 'zipcode)]
					[phone-home (assure-string req 'phone_home)]
					[phone-mobile (assure-string req 'phone_mobile)]
					[email (assure-string req 'email)]
					[notify (query-value-boolean req 'remind)]
					[emerge-contact (assure-string req 'emerg_contact)]
					[emerge-phone (assure-string req 'emerg_phone)]
					[birth (normalize-date (assure-string req 'birth))])
				(pg-exec (sdb) query
					(if (= exit-weight 0) '() exit-weight)
					street1 street2 city state country zipcode
					phone-home phone-mobile email
					emerge-contact emerge-phone
					(or birth '())
					notify
					(time-now)
					person-id)
				(when (and passtouch (not (string=? password "")))
					(update-password person-id password))
				(if (string=? username "") ""
					(update-username person-id username))))))
(define balance-due
	(let ([query
				"select balance
					from acct
					where (owner=~a)
					and personal"])
		(define (money balance)
			(let ([pennies (remainder balance 100)]
					[dollars (quotient balance 100)])
				(if (= pennies 0)
					(format #f "~d" dollars)
					(format #f "~d.~2,'0d" dollars pennies))))
		(lambda (person-id)
			(let* ([row (pg-one-row (sdb) query person-id)]
					[balance (if row (pg-cell row 'balance) 0)])
				(if (<= balance 0) "" (money balance))))))
(define (assure-float item) (or item 0.0))

(log-to "/var/log/nowcall/me.log")

(http-html "/"
	(lambda (req)
		(if (logged-in req)
			(fill-template (fetch-doc composite) #f
				(cons 'dz_full_name (env-dz-full-name))
				(cons 'balance
					(begin
						(session-set req 'proxy-person-id 0)
						(balance-due (session-get req 'person-id))))
				(cons 'pay_auth (if (env-patron-self-pay) "true" "false"))
				(cons 'admin_check (admin-light req (sdb)))
				(cons 'date_format (env-date-format)))
			(redirect-html "/login/me"))))
(http-json "/log"
	(lambda (req)
		(if (logged-in req)
			(let* ([person-id (session-get req 'person-id)]
					[from-date (query-value req 'from_date)]
					[to-date (query-value req 'to_date)]
					[aircraft (json-decode (query-value req 'aircraft))]
					[jump-types (json-decode (query-value req 'jumps))]
					[date-terms
						(date-filter from-date to-date)]
					[log
						(jump-log person-id date-terms
							aircraft jump-types)])
				(list
					(cons 'status #t)
					(cons 'log (assq-ref log 'log))
					(cons 'total (assq-ref log 'total))
					(cons 'jumps
						(jump-log-skydives person-id date-terms))
					(cons 'aircraft
						(jump-log-aircraft person-id date-terms))))
			(list (cons 'status #f)))))
(http-json "/ledger"
	(lambda (req)
		(if (logged-in req)
			(ledger (session-get req 'person-id))
			(list (cons 'status #f)))))
(http-json "/acjumps"
	(lambda (req)
		(if (logged-in req)
			(let* ([person-id (session-get req 'person-id)]
					[from-date (query-value req 'from_date)]
					[to-date (query-value req 'to_date)]
					[date-terms
						(date-filter from-date to-date)])
				(list
					(cons 'status #t)
					(cons 'jumps
						(jump-log-skydives person-id date-terms))
					(cons 'aircraft
						(jump-log-aircraft person-id date-terms))))
			(list (cons 'status #f)))))
(http-json "/logdates"
	(lambda (req)
		(let* ([key (query-value req 'key)]
				[range (date-range key)])
			(list
				(cons 'from_date (car range))
				(cons 'to_date (cadr range))))))
(http-json "/profile"
	(lambda (req)
		(if (logged-in req)
			(let ([person-id (session-get req 'person-id)])
				(list
					(cons 'html (profile-form person-id))
					(cons 'pid person-id)))
			(list (cons 'status #f)))))
(http-json "/update_pro"
	(lambda (req)
		(if (logged-in req)
			(let* ([username (query-value req 'username)]
					[person-id (session-get req 'person-id)]
					[avail (username-available username person-id (sdb))])
				(if avail
					(list
						(cons 'status #t)
						(cons 'pid person-id)
						(cons 'username (update-profile req))
						(cons 'msg "Update successful."))
					(list
						(cons 'status #f)
						(cons 'msg
							(format #f "Sorry, '~a' is taken." username)))))
			(list (cons 'status #f)))))
(define open-loads
	(let ([query
				"select load_sheets.id as id,
						aircraft.id as ac_id,
						load_sheets.load_date as date,
						aircraft.nickname as aircraft, load_num
					from load_sheets, aircraft
					where (load_sheets.load_date=current_date)
					and (aircraft.id=load_sheets.aircraft_id)
					and (load_sheets.status='OPEN')
					order by aircraft.nickname asc, load_num asc"]
			[slots-avail-query
				"select count(*) as n
					from load_slots
					where (load_id=~a)
					and (passenger is null)"]
			[aboard-query
				"select slot_num
					from load_slots
					where (load_id=~a)
					and (passenger=~a)
					limit 1"]
			[btn-tpt
				(deflate "<input type=\"button\" style=\"color: ~a\"
					onclick=\"~a(~d,~d,~d,'~a',~d)\"
					class=\"man-btn\" value=\"~a\"~a/>")]
			[some-loads
				(deflate "<div id=\"man-header\">Loads open for
					self-manifest:</div>")]
			[no-loads
				"There are no loads open for self-manifest at this time."]
			[off-premises "Must be on DZ premises for self-manifest."]
			[click-box-tpt
				(deflate "<tr><td><b>~a</b></td><td><b> - LOAD ~2,'0d</b>
					<span title=\"slots available\">[~d]</span></td>
					<td>&nbsp;~a</td></tr>")])
		(define (slot-num load-id person-id)
			(let ([row (pg-one-row (sdb) aboard-query load-id person-id)])
				(and row (pg-cell row 'slot_num))))
		(define (slots-avail load-id)
			(pg-cell (pg-one-row (sdb) slots-avail-query load-id) 'n))
		(define (action-button load-id person-id
					aircraft-id load-num load-date)
			(let ([slot (slot-num load-id person-id)]
					[load-full (< (slots-avail load-id) 1)])
				(format #f btn-tpt
					(if slot "red" (if load-full "black" "green"))
					(if slot "scratch" "manifest")
					person-id aircraft-id load-num
					(time-format load-date "%Y-%m-%d") (or slot 0)
					(if slot "SCRATCH"
						(if load-full "FULL" "MANIFEST"))
					(if (and (not slot) load-full)
						" disabled=\"disabled\"" ""))))
		(define (click-box row person-id)
			(let ([load-id (pg-cell row 'id)]
					[load-num (pg-cell row 'load_num)])
				(format #f click-box-tpt
					(pg-cell row 'aircraft)
					load-num
					(slots-avail load-id) 
					(action-button load-id person-id (pg-cell row 'ac_id)
						load-num (pg-cell row 'date)))))
		(lambda (person-id)
			(if (env-on-dz-lan)
				(let ([loads (pg-map-rows (pg-exec (sdb) query))])
					(if (null? loads) no-loads
						(string-cat "\n"
							some-loads
							"<table>"
							(map (lambda (row) (click-box row person-id))
								loads) "</table>"))) off-premises))))
(http-json "/open_loads"
	(lambda (req)
		(if (logged-in req)
			(let ([person-id (session-get req 'person-id)])
				(list
					(cons 'status #t)
					(cons 'html (open-loads person-id))))
			(list (cons 'status #f)))))
(add-javascript-logger #f)
