#! /usr/local/bin/gusher -p 3003
!#

;
; Copyright (c) 2014 Peter Yadlowsky <pmy@linux.com>
;
; This program is free software ; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation ; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program ; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;

(use-modules (ice-9 format))

(include "lib.scm")
(include "settings_lib.scm")

(define env-dz-full-name (setting-cache 'dz-full-name))
(define env-name-order (setting-cache 'name-order))
(define env-date-format (setting-cache 'date-format))
(define env-min-search-chars (setting-cache 'min-search-chars))

(define frame (make-doc 'file "frame.html"))
(define script (make-doc 'file "whiteboard.js"))
(define page-body (make-doc 'file "whiteboard.html"))
(define composite
	(make-doc (list frame script page-body)
		(lambda ()
			(fill-template (fetch-doc frame) #t
				(cons 'title "Whiteboard")
				(cons 'app "whiteboard")
				(cons 'script (fetch-doc script))
				(cons 'version nowcall-version)
				(cons 'content
					(fill-template (fetch-doc page-body) #t))))))
(define (null-is-zero value) (if (null? value) 0 value))
(define (zero-is-null value) (if (= value 0) '() value))
(define (sql-date stamp) (time-format stamp "%Y-%m-%d"))
(define (center-text text)
	(format #f "<div style=\"text-align: center\">~a</div>" text))
(define n/a (center-text "N/A"))

(define camera-menu
	(let ([menu-query
				"select people.id as id
					from people, roles
					where (roles.who=people.id)
					and (roles.role='VID')
					order by people.last_name asc,
						people.first_name asc"]
			[camera-query
				"select camera
					from whiteboard
					where (id=~a)"])
		(lambda (board-id name-order)
			(let ([row (pg-one-row (sdb) camera-query board-id)]
					[l-name-order
						(or name-order (env-name-order))])
				(html-select
					(format #f "camera-~d" board-id)
					(format #f "onchange=\"camera_change(~d)\""
						board-id)
					(let ([camera (and row (pg-cell row 'camera))])
						(cond
							[(not camera) '()]
							[(null? camera) '()]
							[else camera]))
					(cons
						(cons 0 "none")
						(pg-map-rows
							(pg-exec (sdb) menu-query)
							(lambda (row)
								(let ([person-id (pg-cell row 'id)])
									(cons
										person-id
										(person-name
											person-id
											l-name-order (sdb))))))))))))
(define instructor-rating
	(let ([query
				"select product_roles.role as rating
					from product_roles
					where (product_roles.product=
						(select product_item.served_by
							from product_item, whiteboard
							where (whiteboard.id=~a)
							and (whiteboard.skydive=product_item.id)))"])
		(lambda (board-id)
			(let ([row (pg-one-row (sdb) query board-id)])
				(pg-cell row 'rating)))))
(define instructor-menu
	(let ([with-rating
				"select people.id as id
					from people, roles
					where (roles.role=~a)
					and (roles.who=people.id)
					and people.active
					order by people.last_name asc,
						people.first_name asc"]
			[instructor-query
				"select instructor~a as instr
					from whiteboard
					where (id=~a)"])
		(define (menu-row row name-order)
			(let ([person-id (pg-cell row 'id)])
				(cons
					person-id
					(person-name person-id name-order (sdb)))))
		(lambda (ordinal name-order board-id)
			(let ([rating (instructor-rating board-id)]
					[l-name-order (or name-order (env-name-order))]
					[instr
						(pg-one-row (sdb) instructor-query
							ordinal board-id)])
				(html-select
					(format #f "instructor-~d_~d" ordinal board-id)
					(format #f "onchange=\"instructor_change(~d,~d)\""
						ordinal board-id)
					(let ([instr (and instr (pg-cell instr 'instr))])
						(cond
							[(not instr) '()]
							[(null? instr) '()]
							[else instr]))
					(cons
						(cons 0 "none")
						(pg-map-rows
							(pg-exec (sdb) with-rating rating)
							(lambda (row)
								(menu-row row l-name-order)))))))))
(define media-menu
	(let ([menu-query
				"select product_item.name as name,
						product_item.id as id
					from product_item, product_category
					where (product_category.internal_name='media')
					and (product_item.category=product_category.id)
					and (not product_item.payout)
					and (not product_item.divider)
					order by product_item.name"]
			[query
				"select media
					from whiteboard
					where (id=~a)"])
		(define (entry-media board-id)
			(let ([row (pg-one-row (sdb) query board-id)])
				(if row (pg-cell row 'media) '())))
		(lambda (board-id)
			(html-select
				(format #f "media-~d" board-id)
				(format #f "onchange=\"media_change('~d')\"" board-id)
				(entry-media board-id)
				(cons
					(cons 0 "none")
					(pg-map-rows (pg-exec (sdb) menu-query)
						(lambda (row)
							(cons
								(pg-cell row 'id)
								(pg-cell row 'name)))))))))
(define media-included?
	(let ([query
				"select media_included
					from product_item
					where (id=~a)"])
		(lambda (skydive)
			(let ([row (pg-one-row (sdb) query skydive)])
				(and row (pg-cell row 'media_included))))))
(define need-camera?
	(let ([query
				"select product_item.media_included as med_inc,
						whiteboard.media as media
					from product_item, whiteboard
					where (whiteboard.id=~a)
					and (product_item.id=whiteboard.skydive)"])
		(lambda (board-id)
			(let ([row (pg-one-row (sdb) query board-id)])
				(and row
					(or
						(pg-cell row 'med_inc)
						(not (null? (pg-cell row 'media)))))))))
(define load-cell
	(let ([query
				"select load_id
					from whiteboard
					where (id=~a)"]
			[load-query
				"select aircraft.nickname as aircraft, load_num
					from load_sheets, aircraft
					where (load_sheets.id=~a)
					and (aircraft.id=load_sheets.aircraft_id)"]
			[not-manifested
				(deflate "<div style=\"width: 5em; text-align: center\"~a>
					<img src=\"/img/airplane-16.png\"
							style=\"width: 16px; margin: auto\"
							alt=\"board\"/></div>")]
			[manifested
				"<div~a>~a, L~2,'0d</div>"])
		(define (clicker read-only board-id load-id)
			(if read-only ""
				(format #f " title=\"~a this group\" onclick=\"~a('~d')\""
					(if (null? load-id) "manifest" "unmanifest")
					(if (null? load-id) "board_group" "unboard_group")
					board-id)))
		(lambda (board-id read-only)
			(let* ([row (pg-one-row (sdb) query board-id)]
					[load-id (pg-cell row 'load_id)])
				(if (null? load-id)
					(format #f not-manifested
						(clicker read-only board-id load-id))
					(let ([row (pg-one-row (sdb) load-query load-id)])
						(format #f manifested
							(clicker read-only board-id load-id)
							(pg-cell row 'aircraft)
							(pg-cell row 'load_num))))))))
(define chart-date
	(let ([query
				"select id, student, instructor1, instructor2,
						camera, skydive, media, load_id
					from whiteboard
					where (created::date=~a)
					order by coalesce(seq,id) asc"]
			[skydive-menu-query
				"select product_item.id as id, product_item.name as name
					from product_item, product_category
					where ((product_category.internal_name='skydives')
								or product_item.right_seat)
					and (product_item.category=product_category.id)
					and reservable
					order by product_item.name asc"]
			[product-name-query
				"select name
					from product_item
					where (id=~a)"]
			[instructor-num-query
				"select instr_num_req as num
					from product_item
					where (id=~a)"]
			[action-tpt
					(deflate "<img src=\"/img/delete-16.png\"
						title=\"drop this whiteboard entry\"
						onclick=\"predelete_entry(this,~d)\"
						name=\"~a\"
						alt=\"delete\"/>")]
			[weight-query
				"select exit_weight
					from people
					where (id=~a)"]
			[weight-box
				(deflate "<input type=\"text\"
					style=\"width: 3em; text-align: right\"
					value=\"~a\"
					onchange=\"weight_change(this,'~d')\"/>")]
			[weight-box-read-only
				(deflate "<div style=\"width: 3em;
						text-align: right\">~a</div>")]
			[student-cell-tpt
				"<td title=\"click/drag to reshuffle\">~a</td>"]
			[header
				(deflate "
					<tr>
					<th>Load</th>
					<th>Student</th>
					~a
					<th>Skydive</th>
					<th>Media</th>
					<th>Instructor 1</th>
					<th>Instructor 2</th>
					<th>Camera</th>
					~a
					</tr>")]
			[exit-weight-header
				"<th><abbr title=\"exit weight\">Wt</abbr></th>"])
		(define (two-instructors? skydive-id)
			; jump needs two instructors
			(let ([row (pg-one-row (sdb) instructor-num-query skydive-id)])
				(and row (> (pg-cell row 'num) 1))))
		(define (instructor-less? skydive-id)
			; jump needs no instructors
			(let ([row (pg-one-row (sdb) instructor-num-query skydive-id)])
				(and row (< (pg-cell row 'num) 1))))
		(define (cell id content)
			(format #f "<td~a>~a</td>"
				(if (string=? id "") "" (format #f " id=\"~a\"" id))
				(to-s content)))
		(define (student-cell name) (format #f student-cell-tpt name))
		(define (pname person-id)
			(if (null? person-id) ""
				(person-name person-id (env-name-order) (sdb))))
		(define (product-name skydive-id)
			(let ([row (pg-one-row (sdb) product-name-query skydive-id)])
				(if row (pg-cell row 'name) "")))
		(define (student-weight person-id read-only)
			(let* ([row (pg-one-row (sdb) weight-query person-id)]
					[weight (and row (pg-cell row 'exit_weight))])
				(if read-only
					(format #f weight-box-read-only
						(if weight (to-s weight) ""))
					(format #f weight-box
						(if weight (to-s weight) "") person-id))))
		(define (set-selected ident-check)
			(if ident-check " selected=\"selected\"" ""))
		(define (skydive-menu selected board-id)
			(html-select
				"jump"
				(format #f "onchange=\"jump_change(this,'~d')\""
					board-id)
				selected
				(pg-map-rows
					(pg-exec (sdb) skydive-menu-query)
					(lambda (row)
						(cons (pg-cell row 'id) (pg-cell row 'name))))))
		(define (media-cell skydive selected board-id locked)
			(if locked
				(product-name selected)
				(if (media-included? skydive)
					(center-text "included")
					(media-menu board-id))))
		(define (camera-cell camera-id skydive media locked board-id)
			(if (need-camera? board-id)
				(if locked
					(pname camera-id)
					(camera-menu board-id (env-name-order)))
				n/a))
		(define (instructor-cell ordinal person-id skydive-id
					locked board-id)
			(if locked
				(pname person-id)
				(instructor-menu ordinal (env-name-order) board-id)))
		(define (skydive-cell skydive-id board-id locked)
			(if locked
				(product-name skydive-id)
				(skydive-menu skydive-id board-id)))
		(define (action-buttons whiteboard-id student-name)
			(format #f action-tpt whiteboard-id student-name))
		(define (action-cell board-id student-id read-only)
			(if read-only ""
				(cell ""
					(action-buttons board-id
						(pname student-id)))))
		(define (render-row row read-only)
			(let ([student-id (pg-cell row 'student)]
					[skydive-id (null-is-zero (pg-cell row 'skydive))]
					[camera-id (pg-cell row 'camera)]
					[board-id (pg-cell row 'id)]
					[manifested (not (null? (pg-cell row 'load_id)))]
					[media-id (pg-cell row 'media)]
					[instructor-1 (pg-cell row 'instructor1)]
					[instructor-2 (pg-cell row 'instructor2)])
				(string-cat "\n"
					(format #f
						"<tr id=\"dd~d\" class=\"dragger ui-widget-content\">"
						board-id)
					(cell (format #f "load~d" board-id)
						(load-cell board-id read-only))
					(student-cell (pname student-id))
					(if read-only ""
						(cell ""
							(student-weight student-id read-only)))
					(cell ""
						(skydive-cell skydive-id board-id
							(or read-only manifested)))
					(cell (format #f "media~d" board-id)
						(media-cell skydive-id
							(null-is-zero media-id) board-id
							(or manifested read-only)))
					(cell (format #f "instructor1_~d" board-id)
						(if (instructor-less? skydive-id) n/a
							(instructor-cell 1 instructor-1 skydive-id
								(or manifested read-only) board-id)))
					(cell (format #f "instructor2_~d" board-id)
						(if (two-instructors? skydive-id)
							(instructor-cell 2 instructor-2 skydive-id
								(or manifested read-only)
								board-id) n/a))
					(cell (format #f "camera~d" board-id)
						(camera-cell camera-id skydive-id
							media-id (or manifested read-only) board-id))
					(action-cell board-id student-id read-only)
					"</tr>")))
		(lambda (date read-only)
			(let ([res (pg-exec (sdb) query date)])
				(string-cat "\n"
					"<div class=\"date-hdr\">"
					(time-format date "%A, %B %d, %Y")
					"</div>"
					"<table style=\"width: 95%\">"
					(format #f header
						(if read-only "" exit-weight-header)
						(if read-only "" "<th>&nbsp;</th>"))
					(pg-map-rows res
						(lambda (row) (render-row row read-only)))
					"</table>")))))
(define chart
	(let ([query
				"select distinct created::date as date
					from whiteboard
					where (created::date >= ~a)
					and (created::date <= ~a)
					order by date asc"])
		(lambda (from-date to-date read-only)
			(let ([res (pg-exec (sdb) query from-date to-date)])
				(if (< (pg-tuples res) 1)
					"<h3>No whiteboard entries for this date range.</h3>"
					(string-cat "\n"
						(pg-map-rows res
							(lambda (row)
								(chart-date
									(pg-cell row 'date) read-only)))))))))
(define drop-entry
	(let ([query
				"delete from whiteboard
					where (id=~a)"])
		(lambda (whiteboard-id)
			(pg-exec (sdb) query whiteboard-id))))
(define media-change
	(let ([query
				"update whiteboard set
					media=~a,
					updated=~a
					where (id=~a)"])
		(lambda (board-id media-id)
			(pg-exec (sdb) query
				(zero-is-null media-id) (time-now) board-id))))
(define instructor-change
	(let ([query
				"update whiteboard set
					instructor~a=~a,
					updated=~a
					where (id=~a)"])
		(lambda (board-id ordinal instr-id)
			(pg-exec (sdb) query
				ordinal (zero-is-null instr-id) (time-now) board-id))))
(define camera-change
	(let ([query
				"update whiteboard set
					camera=~a,
					updated=~a
					where (id=~a)"])
		(lambda (board-id camera-id)
			(pg-exec (sdb) query
				(zero-is-null camera-id) (time-now) board-id))))
(define weight-change
	(let ([query
				"update people set
					exit_weight=~a
					where (id=~a)"])
		(lambda (person-id weight)
			(pg-exec (sdb) query (zero-is-null weight) person-id))))
(define skydive-change
	(let ([update
				"update whiteboard set
					skydive=~a,
					updated=~a
					where (id=~a)"]
			[query
				"select product_item.media_included as media_inc,
						product_item.instr_num_req as instrs
					from product_item, whiteboard
					where (product_item.id=whiteboard.skydive)
					and (whiteboard.id=~a)"])
		(lambda (board-id skydive-id)
			(pg-exec (sdb) update
				(zero-is-null skydive-id) (time-now) board-id)
			(pg-one-row (sdb) query board-id))))
(define last-event
	(let ([query
				"select max(updated) as stamp
					from whiteboard"])
		(lambda ()
			(let* ([row (pg-one-row (sdb) query)]
					[stamp (pg-cell row 'stamp)])
				(if (null? stamp) 0
					(* (time-epoch stamp) 1000))))))
(define validate-boarding
	(let ([query
				"select instructor1, instructor2, camera,
						skydive, media,
						product_item.media_included as med_inc,
						product_item.instr_num_req as instrs
					from whiteboard, product_item
					where (whiteboard.id=~a)
					and (whiteboard.skydive=product_item.id)"])
		(define (check-instructor1 instr-id n-instrs)
			(if (and (null? instr-id) (> n-instrs 0))
				(list "instructor not selected") '()))
		(define (check-instructor2 instr-id n-instrs)
			(if (and (null? instr-id) (> n-instrs 1))
				(list "second instructor required") '()))
		(define (check-camera camera-id media-id media-inc)
			(if (and
					(or media-inc (not (null? media-id)))
					(null? camera-id))
				(list "photo/videographer not selected")
				'()))
		(define (check-double instr1 instr2 n-instrs)
			(if (and
					(> n-instrs 1)
					(not (null? instr1))
					(not (null? instr2))
					(= instr1 instr2))
				(list "instructors 1 & 2 can't be same person")
				'()))
		(lambda (board-id)
			(let* ([row (pg-one-row (sdb) query board-id)]
					[instr1 (pg-cell row 'instructor1)]
					[instr2 (pg-cell row 'instructor2)]
					[instrs (pg-cell row 'instrs)])
				(if row
					(append
						(check-instructor1 instr1 instrs)
						(check-instructor2 instr2 instrs)
						(check-double instr1 instr2 instrs)
						(check-camera
							(pg-cell row 'camera)
							(pg-cell row 'media)
							(pg-cell row 'med_inc)))
					(list "no skydive selected?"))))))
(define load-menu
	(let ([query
				"select aircraft.nickname as aircraft,
						load_sheets.load_num as load_num,
						load_sheets.id as id,
						load_sheets.copilot as copilot,
						aircraft.observer_slot as observer_slot,
						count(*) as avail
					from load_slots, load_sheets, aircraft
					where (load_sheets.load_date=~a)
					and (load_slots.load_id=load_sheets.id)
					and (passenger is null)
					and (aircraft.id=load_sheets.aircraft_id)
					group by aircraft.nickname, aircraft.rank,
						load_sheets.load_num, load_sheets.id,
						aircraft.observer_slot
					order by aircraft.rank, load_sheets.load_num"]
			[observer-query
				"select (whiteboard.skydive=product_item.id) as match
					from whiteboard, product_item
					where (whiteboard.id=~a)
					and product_item.right_seat"]
			[load-query
				"select load_id
					from whiteboard
					where (id=~a)"])
		(define group-size
			(let ([query
						"select instructor1, instructor2, camera
							from whiteboard
							where (id=~a)"])
				(lambda (board-id)
					(let ([row (pg-one-row (sdb) query board-id)])
						(+ 1 ; student
							(if (null? (pg-cell row 'instructor1)) 0 1)
							(if (null? (pg-cell row 'instructor2)) 0 1)
							(if (null? (pg-cell row 'camera)) 0 1))))))
		(define (menu-pair tuple)
			(cons
				(pg-cell tuple 'id)
				(format #f "~a L~2,'0d [~d]"
					(pg-cell tuple 'aircraft)
					(pg-cell tuple 'load_num)
					(pg-cell tuple 'avail))))
		(define (observer? board-id)
			(let ([row (pg-one-row (sdb) observer-query board-id)])
				(and row (pg-cell row 'match))))
		(define (observer-space row observer)
			(or (and observer (pg-cell row 'observer_slot)
				(null? (pg-cell row 'copilot)) 1) 0))
		(define (menu-options board-id)
			(let ([slots-needed (group-size board-id)]
					[observer (observer? board-id)])
				(filter
					(lambda (row)
						(>= (pg-cell row 'avail)
							(- slots-needed
								(observer-space row observer))))
					(pg-map-rows
						(pg-exec (sdb) query (sql-date (time-now)))))))
		(define (load-id board-id)
			(let ([row (pg-one-row (sdb) load-query board-id)])
				(if row (pg-cell row 'load_id) '())))
		(lambda (board-id)
			(let ([options (menu-options board-id)]
					[load-id (load-id board-id)])
				(if (null? options) ""
					(html-select
						(format #f "load-~d" board-id)
						(format #f "onchange=\"select_load(this,'~d')\""
							board-id)
						load-id
						(cons
							(cons 0
								(if (null? load-id) "select" "unmanifest"))
							(map menu-pair options))))))))
(define manifest-group
	(let ([set-load-query
				"update whiteboard set
					load_id=~a
					where (id=~a)"]
			[load-query
				"select load_num, aircraft_id, load_date
					from load_sheets
					where (id=~a)"]
			[board-query
				"select student, instructor1, instructor2, camera
					from whiteboard
					where (id=~a)"]
			[student-query
				"select whiteboard.student as student,
						product_item.manifest_symbol as role
					from whiteboard, product_item
					where (whiteboard.id=~a)
					and (whiteboard.skydive=product_item.id)"]
			[instructor-query
				"select whiteboard.instructor~a as instructor,
						product_item.instr_num_req as num
					from whiteboard, product_item
					where (whiteboard.id=~a)
					and (whiteboard.skydive=product_item.id)"]
			[camera-query
				"select whiteboard.camera as camera,
						whiteboard.media as media,
						product_item.media_included as med_inc
					from whiteboard, product_item
					where (whiteboard.id=~a)
					and (whiteboard.skydive=product_item.id)"]
			[service-role-query
				"select product_item.manifest_symbol as role
					from product_item
					where (product_item.id=
						(select product_item.served_by
							from product_item, whiteboard
							where (whiteboard.id=~~a)
							and (whiteboard.~a=product_item.id)))"]
			[extras-query
				"select whiteboard.media as media,
						product_item.media_included as med_inc
					from whiteboard, product_item
					where (whiteboard.id=~a)
					and (whiteboard.skydive=product_item.id)"])
		(define (student-data board-id)
			(let ([row (pg-one-row (sdb) student-query board-id)])
				(list
					(cons 'id (pg-cell row 'student))
					(cons 'role (pg-cell row 'role)))))
		(define (service-role board-id product-class)
			; student jump's corresponding service roles
			; (instructional, media)
			(let* ([row (pg-one-row (sdb)
						(format #f service-role-query product-class)
						board-id)]
					[role (and row (pg-cell row 'role))])
				(if (and role (not (null? role))) role "VID")))
		(define (instructor-data board-id ordinal)
			(let* ([row
						(pg-one-row (sdb) instructor-query
							ordinal board-id)]
					[instructor (pg-cell row 'instructor)])
				(and (not (null? instructor))
					(<= ordinal (pg-cell row 'num))
					(list
						(cons 'id instructor)
						(cons 'role
							(service-role board-id "skydive"))))))
		(define (camera-data board-id)
			(let* ([row (pg-one-row (sdb) camera-query board-id)]
					[camera (pg-cell row 'camera)])
				(and
					camera
					(or (not (null? (pg-cell row 'media)))
						(pg-cell row 'med_inc))
					(list
						(cons 'id camera)
						(cons 'role
							(service-role board-id "media"))))))
		(define (people-data board-id)
			(append
				(list
					(cons 'student (student-data board-id))
					(cons 'instructor1 (instructor-data board-id 1)))
				(let ([instructor2 (instructor-data board-id 2)])
					(if instructor2
						(list (cons 'instructor2 instructor2)) '()))
				(let ([camera (camera-data board-id)])
					(if camera (list (cons 'camera camera)) '()))))
		(define (extras-data board-id)
			(let* ([row (pg-one-row (sdb) extras-query board-id)]
					[media (pg-cell row 'media)])
				(list
					(cons 'extra
						(cond
							[(pg-cell row 'med_inc) #f]
							[(null? media) #f]
							[else media])))))
		(define (load-data load-id)
			(let ([row (pg-one-row (sdb) load-query load-id)])
				(list
					(cons 'aircraft (pg-cell row 'aircraft_id))
					(cons 'load (pg-cell row 'load_num))
					(cons 'date
						(and row
							(sql-date (pg-cell row 'load_date)))))))
		(lambda (board-id load-id)
			(pg-exec (sdb) set-load-query (zero-is-null load-id) board-id)
			(let ([load (pg-one-row (sdb) load-query load-id)])
				(append
					(load-data load-id)
					(people-data board-id)
					(extras-data board-id)
					(list
						(cons 'html (load-cell board-id #f))))))))
(define scratch-group
	(let ([board-update
				"update whiteboard set
					load_id=null
					where (id=~a)"]
			[load-query
				"select load_sheets.load_date as date,
						load_sheets.aircraft_id as aircraft,
						load_sheets.id as id,
						load_sheets.load_num as load_num
					from load_sheets, whiteboard
					where (whiteboard.id=~a)
					and (load_sheets.load_date >= current_date)
					and (load_sheets.status != 'GONE')
					and (whiteboard.load_id=load_sheets.id)"]
			[slot-query
				"select slot_num
					from load_slots, load_sheets
					where (load_slots.passenger=~a)
					and (load_slots.load_id=load_sheets.id)
					and (load_sheets.id=~a)"]
			[observer-query
				"select copilot
					from load_sheets
					where (copilot=~a)
					and (id=~a)"]
			[board-query
				"select student, instructor1, instructor2, camera
					from whiteboard
					where (id=~a)"])
		(define (check-observer load-id passenger)
			(let ([row
						(pg-one-row (sdb) observer-query
							passenger load-id)])
				(if row (list observer-slot) '())))
		(define (slot load-id passenger)
			(if (null? passenger) '()
				(let ([row (pg-one-row (sdb) slot-query passenger load-id)])
					(if row
						(list (pg-cell row 'slot_num))
						(check-observer load-id passenger)))))
		(define (slots load-data board-data)
			(let ([load-id (pg-cell load-data 'id)])
				(append
					(slot load-id (pg-cell board-data 'student))
					(slot load-id (pg-cell board-data 'instructor1))
					(slot load-id (pg-cell board-data 'instructor2))
					(slot load-id (pg-cell board-data 'camera)))))
		(lambda (board-id)
			(let ([load-data (pg-one-row (sdb) load-query board-id)]
					[board-data (pg-one-row (sdb) board-query board-id)])
				(pg-exec (sdb) board-update board-id)
				(list
					(cons 'status #t)
					(cons 'date
						(and load-data
							(sql-date (pg-cell load-data 'date))))
					(cons 'load (pg-cell load-data 'load_num))
					(cons 'aircraft (pg-cell load-data 'aircraft))
					(cons 'slots
						(if load-data
							(slots load-data board-data) '())))))))
(define get-extra
	; pick up extra (media) item from whiteboard entry
	; for adding/charging to student's load slot
	(let ([query
				"select whiteboard.student as student,
						whiteboard.media as media,
						load_sheets.load_date as date,
						load_sheets.aircraft_id as aircraft,
						load_sheets.load_num as load_num,
						load_slots.slot_num as slot_num
				from whiteboard, load_sheets, load_slots
				where (whiteboard.id=~a)
				and (whiteboard.load_id=load_sheets.id)
				and (load_slots.load_id=load_sheets.id)
				and (whiteboard.media is not null)
				and (load_slots.passenger=whiteboard.student)"])
		(lambda (board-id)
			(let ([row (pg-one-row (sdb) query board-id)])
				(if row
					(list
						(cons 'status #t)
						(cons 'pid (pg-cell row 'student))
						(cons 'media (pg-cell row 'media))
						(cons 'date (sql-date (pg-cell row 'date)))
						(cons 'slot_id
							(format #f "~d_~d_~d"
								(pg-cell row 'aircraft)
								(pg-cell row 'load_num)
								(pg-cell row 'slot_num))))
					(list (cons 'status #f)))))))
(define resolve-date
	(let ([query "select ~a(created)::date as date from whiteboard"])
		(lambda (date func)
			(if (string=? date "")
				(let* ([row
							(pg-one-row (sdb)
								(format #f query func))]
						[mdate (pg-cell row 'date)])
					(time-format
						(if (null? mdate)
							(time-now) mdate) "%Y-%m-%d")) date))))
(define keyed-dates
	; generate from/to dates (for search) from magic keywords
	(let ([all-dates-query
				"select min(created::date) as d1,
						max(created::date) as d2
					from whiteboard"]
			[latest-query
				"select coalesce(max(created::date), current_date) as d1
					from whiteboard"])
		(define (format-date date-obj)
			(time-format date-obj
				(if (string=? (env-date-format) "m/d/yy")
					"%m/%d/%Y" "%Y-%m-%d")))
		(define (blank-null val) (if (null? val) "" (format-date val)))
		(define (all-dates)
			(let ([row (pg-one-row (sdb) all-dates-query)])
				(cons (blank-null (pg-cell row 'd1))
					(blank-null (pg-cell row 'd2)))))
		(define (latest-date)
			(let ([row (pg-one-row (sdb) latest-query)])
				(if row (pg-cell row 'd1) (time-now))))
		(lambda (key)
			(cond
				[(string=? key "all") (all-dates)]
				[(string=? key "today")
					(cons (format-date (time-now)) "")]
				[else
					(let ([today (format-date (time-now))])
						(cons today today))]))))
(define can-manifest?
	; restrict operations to whiteboard entries created today
	(let ([created-query
				"select (created::date = current_date) as ok
					from whiteboard
					where (id=~a)"])
		(lambda (board-id)
			(let ([row (pg-one-row (sdb) created-query board-id)])
				(and row (pg-cell row 'ok))))))
(define post-slot
	(let ([query
				"insert into whiteboard (student, skydive,
							created, updated, seq)
					values (~a, ~a, ~a, ~a,
						(select (coalesce(max(seq),0) + 1)
							from whiteboard
							where created::date=~a::date))"]
			[observer-query
				"select id
					from product_item
					where right_seat
					limit 1"]
			[skydive-query
				"select product_item.id as id
					from product_item, roles, product_roles
					where (roles.who=~a)
					and (product_roles.role=roles.role)
					and product_item.reservable
					and (not product_item.right_seat)
					and (product_item.id=product_roles.product)
					order by product_item.seq asc"])
		(define (observer-item)
			(let ([row (pg-one-row (sdb) observer-query)])
				(or (and row (pg-cell row 'id)) 0)))
		(define (guess-skydive person-id)
			(let ([products
						(pg-map-rows
							(pg-exec (sdb) skydive-query person-id)
							(lambda (row) (pg-cell row 'id)))])
				(if (null? products) (observer-item) (car products))))
		(lambda (person-id)
			(let ([now (time-now)])
				(pg-exec (sdb) query
					person-id (guess-skydive person-id) now now now)))))
(define board-entry-date
	(let ([query
				"select created::date as date
					from whiteboard
					where (id=~a)"])
		(lambda (entry-id)
			(let ([row (pg-one-row (sdb) query entry-id)])
				(time-format (pg-cell row 'date) "%Y-%m-%d")))))
(define slot-shift
	(let ([query
				"update whiteboard set
					seq=0
					where (created::date=~a)
					and (seq=~a);
				update whiteboard set
					seq=(seq + ~a)
					where (created::date=~a)
					and (seq >= ~a)
					and (seq < ~a);
				update whiteboard set
					seq=~a
					where (created::date=~a)
					and (seq=0);"]
			[seq-query
				"select seq
					from whiteboard
					where (id=~a)"])
		(define (get-seq entry-id)
			(let ([row (pg-one-row (sdb) seq-query entry-id)])
				(pg-cell row 'seq)))
		(lambda (src-entry dst-entry)
			(let* ([src-slot (get-seq src-entry)]
					[dst-slot (get-seq dst-entry)]
					[date (board-entry-date dst-entry)]
					[move-back (>= src-slot dst-slot)])
				(pg-exec (sdb) query
					; update 1
					date src-slot ; update 1
					; update 2
					(if move-back 1 -1)
					date
					(if move-back dst-slot (1+ src-slot))
					(if move-back src-slot (1+ dst-slot))
					; update 3
					dst-slot date)))))

(log-to "/var/log/nowcall/whiteboard.log")

(http-html "/"
	(lambda (req)
		(fill-template (fetch-doc composite) #f
			(cons 'dz_full_name (env-dz-full-name))
			(cons 'admin_check
				(if (logged-in req) (admin-light req (sdb)) "")))))
(http-json "/chart"
	(lambda (req)
		(let ([from-date (resolve-date (query-value req 'from) "min")]
				[to-date (resolve-date (query-value req 'to) "max")])
			(list
				(cons 'html
					(chart
						from-date to-date
						(not (authorized-for req "whiteboard"))))))))
(http-json "/track"
	(lambda (req) (list (cons 'msec (last-event)))))
(http-json "/drop"
	(admin-gate "manifest" (req)
		(let ([whiteboard-id (query-value-number req 'id)])
			(drop-entry whiteboard-id)
			(list (cons 'status #t)))))
(http-json "/chmedia"
	(admin-gate "manifest" (req)
		(let ([board-id (query-value-number req 'bid)]
				[product-id (query-value-number req 'pid)])
			(media-change board-id product-id)
			(list
				(cons 'bid board-id)
				(cons 'html
					(if (= product-id 0) n/a
						(camera-menu board-id #f)))))))
(http-json "/chinstr"
	(admin-gate "manifest" (req)
		(let ([board-id (query-value-number req 'bid)]
				[ordinal (query-value-number req 'ord)]
				[instr-id (query-value-number req 'pid)])
			(instructor-change board-id ordinal instr-id)
			(list (cons 'bid board-id)))))
(http-json "/chcam"
	(admin-gate "manifest" (req)
		(let ([board-id (query-value-number req 'bid)]
				[camera-id (query-value-number req 'pid)])
			(camera-change board-id camera-id)
			(list (cons 'bid board-id)))))
(http-json "/chweight"
	(admin-gate "manifest" (req)
		(let ([person-id (query-value-number req 'pid)]
				[weight (query-value-number req 'wt)])
			(weight-change person-id weight)
			(list (cons 'pid person-id)))))
(http-json "/chjump"
	(admin-gate "manifest" (req)
		(let* ([skydive-id (query-value-number req 'jid)]
				[board-id (query-value-number req 'bid)]
				[entry (skydive-change board-id skydive-id)])
			(list
				(cons 'bid board-id)
				(cons 'media
					(if (pg-cell entry 'media_inc)
						(center-text "included")
						(media-menu board-id)))
				(cons 'instr1
					(instructor-menu 1 #f board-id))
				(cons 'instr2
					(if (> (pg-cell entry 'instrs) 1)
						(instructor-menu 2 #f board-id) n/a))
				(cons 'camera
					(if (need-camera? board-id)
						(camera-menu board-id #f) n/a))))))
(http-json "/validate"
	(admin-gate "manifest" (req)
		(let* ([board-id (query-value-number req 'bid)]
				[errs (validate-boarding board-id)])
			(list
				(cons 'bid board-id)
				(cons 'status (null? errs))
				(cons 'msg
					(format #f "Please correct: ~a" (html-ul errs)))))))
(http-json "/loads"
	(admin-gate "manifest" (req)
		(let ([board-id (query-value-number req 'bid)])
			(if (can-manifest? board-id)
				(list
					(cons 'status #t)
					(cons 'bid board-id)
					(cons 'menu (load-menu board-id)))
				(list (cons 'status #f))))))
(http-json "/manifest"
	(admin-gate "manifest" (req)
		(let* ([board-id (query-value-number req 'bid)]
				[load-id (query-value-number req 'load)]
				[manifest (manifest-group board-id load-id)]
				)
			(list
				(cons 'bid board-id)
				(cons 'student (assq-ref manifest 'student))
				(cons 'instructor1 (assq-ref manifest 'instructor1))
				(cons 'instructor2 (assq-ref manifest 'instructor2))
				(cons 'camera (assq-ref manifest 'camera))
				(cons 'has_extra (assq-ref manifest 'extra))
				(cons 'html (assq-ref manifest 'html))
				(cons 'aircraft (assq-ref manifest 'aircraft))
				(cons 'load (assq-ref manifest 'load))
				(cons 'date (assq-ref manifest 'date))))))
(http-json "/scratch"
	(admin-gate "manifest" (req)
		(let ([board-id (query-value-number req 'bid)])
			(if (can-manifest? board-id)
				(scratch-group board-id)
				(list (cons 'status #f))))))
(http-json "/extra"
	(admin-gate "manifest" (req)
		(let ([board-id (query-value-number req 'bid)])
			(get-extra board-id))))
(http-json "/dress"
	; dress page according to system settings
	(admin-gate "whiteboard" (req)
		(list
			(cons 'date_format (env-date-format))
			(cons 'min_search (env-min-search-chars)))))
(http-json "/dpreset"
	(admin-gate "whiteboard" (req)
		(let ([dates (keyed-dates (to-s (query-value req 'key)))])
			(list
				(cons 'from (car dates))
				(cons 'to (cdr dates))))))
(http-json "/post"
	(admin-gate "whiteboard" (req)
		(let ([person-id (query-value-number req 'pid)])
			(post-slot person-id)
			(list (cons 'status #t)))))
(http-json "/shuffle"
	(admin-gate "whiteboard" (req)
		(let ([src-entry (query-value-number req 'src)]
				[dst-entry (query-value-number req 'dst)])
			(if (string=? (board-entry-date src-entry)
					(board-entry-date dst-entry))
				(begin
					(slot-shift src-entry dst-entry)
					(list (cons 'status #t)))
				(list
					(cons 'status #f)
					(cons 'msg
					"Can't move whiteboard entries between dates."))))))
(add-javascript-logger "whiteboard")
