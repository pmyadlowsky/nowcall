#! /usr/bin/guile
!#

(use-modules (ice-9 format))
(use-modules (ice-9 regex))
(use-modules (ice-9 rdelim))
(use-modules (ice-9 popen))

(define roster
	(list
		"index"
		"aircraft"
		"login"
		"settings"
		"reports"
		"products"
		"backups"
		"accounting"
		"reservations"
		"manifest"
		"people"
		"me"
		)
	)
(define home ".")
(define monitor-interval 10) ; sec

(define (log msg)
	(let ([now (localtime (current-time))])
		(format #t "~d-~2,'0d-~2,'0d ~2,'0d:~2,'0d:~2,'0d ~a\n"
			(+ (tm:year now) 1900)
			(1+ (tm:mon now))
			(tm:mday now)
			(tm:hour now)
			(tm:min now)
			(tm:sec now) msg)
		(force-output)))
(define running-apps
	; list running gusher procs
	(let ([pid-pat (make-regexp "^[a-z]+[ ]+([0-9]+)")]
			[cmd-pat (make-regexp "\\.?/([a-z]+)\\.scm[ ]+")]
			[ps-grep "ps uax|grep gusher|grep -v SCREEN"])
		(define (pid-cmd line)
			(let ([pid-match (regexp-exec pid-pat line)]
					[cmd-match (regexp-exec cmd-pat line)])
				(and pid-match cmd-match
					(cons
						(match:substring pid-match 1)
						(match:substring cmd-match 1)))))
		(lambda ()
			(let ([pipe (open-input-pipe ps-grep)])
				(let reader ([apps '()]
						[line (read-line pipe)])
					(if (eof-object? line)
						(begin (close-port pipe) apps)
						(reader
							(let ([pair (pid-cmd line)])
								(if pair (cons pair apps) apps))
							(read-line pipe))))))))
(define awol
	; check running apps against roster, return missing apps
	(let ()
		(define (present? app attendees)
			(cond
				[(null? attendees) #f]
				[(string=? app (cdar attendees)) #t]
				[else (present? app (cdr attendees))]))
		(lambda ()
			(let scan ([attendees (running-apps)]
					[roll roster]
					[missing '()])
				(cond
					[(null? roll) missing]
					[(present? (car roll) attendees)
						(scan attendees (cdr roll) missing)]
					[else
						(scan attendees (cdr roll)
							(cons (car roll) missing))])))))
(define (kill-app app-pair)
	(log
		(format #f "kill '~a': ~a"
			(cdr app-pair)
			(if (system (format #f "kill ~a" (car app-pair)))
				"SUCCESS" "FAIL"))))
(define (stop-app app)
	(let scan ([apps (running-apps)])
		(cond
			[(null? apps) #f]
			[(string=? (cdar apps) app) (kill-app (car apps)) #t]
			[else (scan (cdr apps))])))
(define (stop-all)
	(let loop ([apps (running-apps)])
		(unless (null? apps)
			(kill-app (car apps))
			(loop (cdr apps)))))
(define (start-app app quiet)
	(let ([status (= (system (format #f "~a/sh_~a" home app)) 0)])
		(unless quiet
			(log (format #f "start '~a': ~a" app
				(if status "SUCCESS" "FAIL")))) status))
(define (start-all)
	(let loop ([apps roster])
		(unless (null? apps)
			(start-app (car apps) #f)
			(loop (cdr apps)))))
(define (restart-app app)
	(stop-app app)
	(log (format #f "restart '~a': ~a" app
		(if (start-app app #t) "SUCCESS" "FAIL"))))
(define (restart-fallen)
	(let check ([apps (awol)])
		(unless (null? apps)
			(log (format #f "restart fallen '~a': ~a"
				(car apps)
				(if (start-app (car apps) #t) "SUCCESS" "FAIL")))
			(check (cdr apps)))))
(define (restart-all) (stop-all) (start-all))
(define (walk-beat)
	(let walk ([snooze 0]
			[n (quotient 60 monitor-interval)])
		(when (> n 0)
			(sleep snooze)
			(restart-fallen)
			(walk monitor-interval (1- n)))))
(define base-dir
	(let ([pat (make-regexp "(^.+)/")])
		(lambda ()
			(let ([match (regexp-exec pat (car (command-line)))])
				(match:substring match 1)))))

(set! home (base-dir))
(define args (cdr (command-line)))
(define cmd "")
(define arg1 "")
(unless (null? args)
	(set! cmd (car args))
	(set! args (cdr args)))
(unless (null? args)
	(set! arg1 (car args))
	(set! args (cdr args)))
(cond
	[(string=? cmd "start") (start-app arg1 #f)]
	[(string=? cmd "start-all") (start-all)]
	[(string=? cmd "restart") (restart-app arg1)]
	[(string=? cmd "restart-all") (restart-all)]
	[(string=? cmd "stop") (stop-app arg1)]
	[(string=? cmd "stop-all") (stop-all)]
	[(string=? cmd "police") (walk-beat)]
	[(string=? cmd "list") (format #t "~s\n" (running-apps))]
	[else (format #t "no such command: ~s\n" cmd)])
