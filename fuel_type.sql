drop table fuel_type;
create table fuel_type (
	symbol varchar primary key,
	name varchar,
	density float,
	seq integer
	);

insert into fuel_type values ('jet-a', 'Jet-A', 6.6, 1);
insert into fuel_type values ('100ll', '100-LL', 6.0, 2);
